'use strict';

(function () {
  const start = Date.now()
  var sam = /sam=(\w+)/.exec(document.cookie)
  if (sam) {
    sam = sam[1]
  }
  else {
    sam = Date.now().toString(36)
    document.cookie = 'sam=' + sam + '; path=/; expires=Tue, 01 Jan 2030 00:00:00 GMT'
  }

  if (navigator.sendBeacon instanceof Function) {
    addEventListener('beforeunload', function () {
      const spend = Date.now() - start
      const url = btoa(location.href)
      navigator.sendBeacon('https://my.inbisoft.com/serve/visit/external?'
        + 'spend=' + spend
        + '&sam=' + sam
        + '&url=' + url
      )
    })
  }
})()
