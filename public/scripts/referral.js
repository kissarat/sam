document.addEventListener('DOMContentLoaded', function () {
  const VAR_REF = 'ref'
  const ref = /(\?r=|\/ref\/)([\w_]+)$/.exec(location.href)
  const backOfficeOrigin = 'https://my.' + location.hostname

  function changeAnchors(login) {
    [].forEach.call(document.querySelectorAll('a[href$="/signup"]'), function (anchor) {
      anchor.href = backOfficeOrigin + '/ref/' + login + '?utm_source=' + location.hostname
    })
  }

  function addScript(url) {
    const script = document.createElement('script')
    script.src = url
    document.head.appendChild(script)
  }

  const hasStorage = window.localStorage && window.Storage && localStorage instanceof Storage
  const hasHistory = window.history && window.History && history instanceof History

  if (ref) {
    if (hasStorage && localStorage.getItem(VAR_REF)) {
      addScript(backOfficeOrigin + '/scripts/spy.js')
    }
    let login = ref[2]
    if (location.hostname.indexOf('inbisoft') >= 0) {
      login = login.replace(/^inbisoft_/, '')
      if (hasHistory) {
        if (/\?r(ef)?=([\w_]+)$/.test(location.href)) {
          history.pushState(null, login, location.pathname + 'ref/' + login)
        }
        if (/^\/mlbot\/ref\/([\w_]+)$/.test(location.pathname)) {
          history.pushState(null, login, '/mlbot/ref/' + login)
        }
      }
    }
    if (hasStorage) {
      const old = localStorage.getItem(VAR_REF)
      if (old !== login) {
        localStorage.setItem(VAR_REF, login)
      }
      if (!old) {
        setTimeout(function () {
            addScript(backOfficeOrigin + '/serve/visit/ref?nick=' + login + '&unique=1&callback=at' + Date.now())
          },
          Math.round(Math.random() * 3000 + 1000))
      }
    }
    changeAnchors(login)
  }
  else {
    addScript(backOfficeOrigin + '/scripts/spy.js')
  }

  if (hasStorage && localStorage.getItem(VAR_REF)) {
    changeAnchors(localStorage.getItem(VAR_REF))
  }
})
