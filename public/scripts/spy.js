(function () {
  function addScript(url) {
    const script = document.createElement('script')
    script.src = url
    document.head.appendChild(script)
  }
  const start = Date.now()

  addEventListener('beforeunload', function () {
    const hostname = /(https?:..[^\/]+)/.exec(document.querySelector('script[src$="/scripts/spy.js"]').src)[1]
    const now = Date.now()
    const spend = now - start
    const callback = '_' + now.toString(36)
    addScript(hostname + '/serve/visit/external?callback=' + callback + '&spend=' + spend + '&url=' + btoa(location.href))
  })
})()
