const _ = require('lodash')
const Translation = require('../../common/i18n/translation')

if (['en', 'ru', 'it'].indexOf(localStorage.getItem('language')) < 0) {
  localStorage.language = 'ru'
}

const t = Translation.list[localStorage.getItem('language')].toFunction()

module.exports = t
