import AlertList from '../widget/alert.jsx'
import api from '../connect/api.jsx'
import React, {Component} from 'react'
import t from '../../sites/inbisoft/client/t'
import {browserHistory} from 'react-router'
import {Form, Segment, Button} from 'semantic-ui-react'

export default class Password extends Component {
  state = {
    busy: false
  }

  onChange = (e, {name, value}) => {
    this.setState({[name]: value})
  }

  onSubmit = async(e, {formData}) => {
    e.preventDefault()
    AlertList.clear()
    if (formData.new != formData.repeat) {
      return AlertList.show({
        error: true,
        content: t('You have incorrectly repeated the password'),
        onDismiss: true
      })
    }
    this.setState({busy: true})
    const {success, error} = await api.send('user/password', formData)
    this.setState({busy: false})
    if (success) {
      AlertList.show({
        success: true,
        content: t('Password changed'),
        timeout: true
      })
      browserHistory.push('/settings')
    }
    else {
      AlertList.show({
        error: true,
        content: error.message,
        onDismiss: true
      })
    }
  }

  render() {
    return <Segment className="page password">
      <h1>{t('Password')}</h1>
      <Form onSubmit={this.onSubmit} loading={this.state.busy}>
        <Form.Input
          label={t('Current password')}
          name="old"
          type="password"
          value={this.state.old || ''}
          onChange={this.onChange}
        />
        <Form.Group widths='equal'>
          <Form.Input
            label={t('New password')}
            name="new"
            type="password"
            value={this.state.new || ''}
            onChange={this.onChange}
          />
          <Form.Input
            label={t('Repeat password')}
            name="repeat"
            type="password"
            value={this.state.repeat || ''}
            onChange={this.onChange}
          />
        </Form.Group>
        <div className="control">
          <Button positive>{t('Save')}</Button>
        </div>
      </Form>
    </Segment>
  }
}
