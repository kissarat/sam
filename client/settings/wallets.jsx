import React, {Component} from 'react'
import {Form, Segment, Button, Message} from 'semantic-ui-react'
import api from '../connect/api.jsx'
import {browserHistory} from 'react-router'
import AlertList from '../widget/alert.jsx'
import FormComponent from '../base/form-component.jsx'
import {pick} from 'lodash'

export default class WalletSettings extends FormComponent {
  state = {
    busy: false,
    wallets: []
  }

  componentDidMount() {
    void this.load()
  }

  async load() {
    const {user} = await api.get('user/me')
    if (user) {
      user.busy = false
      user.wallets = [
        {
          name: 'perfect',
          regex: /^U\d+$/,
          label: 'Perfect Money',
        }
      ]
      for (const w of user.wallets) {
        w.disabled = !!user[w.name]
      }
      this.setState(user)
    }
  }

  async submit() {
    const {success} = await api.send('user/save', {id: this.state.id}, pick(this.state, 'perfect'))
    if (success) {
      browserHistory.push('/settings')
    }
  }

  walletInputs() {
    return this.state.wallets
      .map(w => <Form.Input
        key={w.name}
        name={w.name}
        label={w.label}
        value={this.state[w.name]}
        disabled={w.disabled}
        onChange={this.onChange}
      />)
  }

  render() {
    return <Segment className="page password">
      <h1>Изменения кошельков</h1>
      <Form onSubmit={this.onSubmit} loading={this.state.busy}>
        <Message
          icon="warning sign"
          content=""/>
        {this.walletInputs()}
        <div className="control">
          <Button positive>Сохранить</Button>
        </div>
      </Form>
    </Segment>
  }
}
