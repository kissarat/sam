import AlertList from '../widget/alert.jsx'
import api from '../connect/api.jsx'
import Avatar from '../widget/avatar.jsx'
import React, {Component} from 'react'
import t from '../../sites/inbisoft/client/t'
import {browserHistory} from 'react-router'
import {Form, Segment, Button, Icon} from 'semantic-ui-react'
import {Link} from 'react-router'
import {pick} from 'lodash'

export default class Settings extends Component {
  state = {
    busy: true
  }

  componentDidMount() {
    void this.load()
  }

  async load() {
    const {user} = await api.get('user/me')
    if (user) {
      user.busy = false
      this.setState(user)
    }
  }

  onChange = (e, {name, value}) => {
    this.setState({[name]: value})
  }

  onSubmit = async(e, {formData}) => {
    e.preventDefault()
    AlertList.clear()
    const {success, error} = await api.send('user/save', {id: api.config.user.id},
      pick(formData, 'email', 'forename', 'surname', 'skype', 'facebook',
        'vkontakte', 'twitter', 'google', 'odnoklassniki', 'about', 'phone'))
    if (success) {
      AlertList.show({
        success: true,
        content: t('Profile saved'),
        timeout: true
      })
      browserHistory.push('/cabinet')
    }
    else {
      AlertList.show({
        error: true,
        content: error.message,
        onDismiss: true
      })
    }
  }

  walletsButton() {
    if ('blockio' !== api.config.money.systems[0] || api.config.money.systems.length > 1) {
      return <Link to="/wallets">
        <Icon name="money"/>
        <span>{t('Wallets')}</span>
      </Link>
    }
  }

  render() {
    return <Segment className="page settings">
      <h1>{t('Settings')}</h1>
      <Link to="/password">
        <Icon name="key"/>
        <span>{t('Change password')}</span>
      </Link>
      {this.walletsButton()}
      <Form onSubmit={this.onSubmit} loading={this.state.busy}>
        <Avatar
          avatar={this.state.avatar}
          success={() => this.load()}/>
        <Form.Group widths='equal'>
          <Form.Input
            label="Имя"
            name="forename"
            value={this.state.forename || ''}
            onChange={this.onChange}
          />
          <Form.Input
            label="Фамилия"
            name="surname"
            value={this.state.surname || ''}
            onChange={this.onChange}
          />
        </Form.Group>
        <Form.Group widths='equal'>
          <Form.Input
            label="Email"
            name="email"
            value={this.state.email || ''}
            onChange={this.onChange}
          />
          <Form.Input
            label="Phone"
            name="phone"
            className="phone"
            value={this.state.phone || ''}
            onChange={this.onChange}
          />
        </Form.Group>
        <Form.Group widths='equal'>
          <Form.Input
            label="Skype"
            name="skype"
            className="skype"
            value={this.state.skype || ''}
            onChange={this.onChange}
          />
          <Form.Input
            label="Facebook"
            name="facebook"
            className="facebook"
            value={this.state.facebook || ''}
            onChange={this.onChange}
          />
          <Form.Input
            label={t('VK')}
            name="vkontakte"
            className="vkontakte"
            value={this.state.vkontakte || ''}
            onChange={this.onChange}
          />
        </Form.Group>
        <Form.Group widths='equal'>
          <Form.Input
            label="Twitter"
            name="twitter"
            className="twitter"
            value={this.state.twitter || ''}
            onChange={this.onChange}
          />
          <Form.Input
            label="Google"
            name="google"
            className="google"
            value={this.state.google || ''}
            onChange={this.onChange}
          />
          <Form.Input
            label={t('Odnoklassniki')}
            name="odnoklassniki"
            className="odnoklassniki"
            value={this.state.odnoklassniki || ''}
            onChange={this.onChange}
          />
        </Form.Group>
        <Form.TextArea
          label={t('About me')}
          name="about"
          className="about"
          value={this.state.about || ''}
          onChange={this.onChange}/>
        <div className="control">
          <Button positive>{t('Save')}</Button>
        </div>
      </Form>
    </Segment>
  }
}
