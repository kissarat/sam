import React from 'react'
import {map} from 'lodash'
import t from '../t'
import {types, statuses} from '../finance/enums.jsx'

export function Select({name, label, items, change}) {
  return <select onChange={e => change(name, e.target.value)}>
    <option value="all">{label}</option>
    {map(items, (text, value) => <option key={value} value={value}>{text}</option>)}
  </select>
}

export default function Filters({change}) {
  return <div className="filters">
    <Select
      name="type"
      label={t('Choose the type...')}
      change={change}
      items={types}/>
    <Select
      name="status"
      label={t('Choose the status...')}
      change={change}
      items={statuses}/>
  </div>
}
