import Address from './address.jsx'
import AlertList from '../widget/alert.jsx'
import api from '../connect/api.jsx'
import Create from './create.jsx'
import FormComponent from '../widget/form.jsx'
import Internal from './internal.jsx'
import React, {Component} from 'react'
import t from '../t'
import {Button} from 'semantic-ui-react'
import {Link} from 'react-router'
import {map, isObject} from 'lodash'

export default class Payment extends FormComponent {
  state = {
    type: '',
    address: '',
    amount: 0
  }

  cancel = () => this.setState({type: ''})

  changeType(type) {
    return () => this.setState({type})
  }
  
  create() {
    switch (this.state.type) {
      case 'create':
        return <Create
          amount={this.props.amount}
          cancel={this.cancel}
          success={this.submit}/>

      case 'internal':
        return <Internal cancel={this.cancel}/>

      default:
        return <div className="control">
          <Button
            positive
            type="button"
            content={t('Pay')}
            onClick={this.changeType('create')}/>
          <Button
            color="blue"
            type="button"
            content={t('Transfer')}
            onClick={this.changeType('internal')}/>
          <Link to="/withdraw">{t('Withdraw')}</Link>
        </div>
    }
  }

  submit = async state => {
    AlertList.clear()
    if ('blockio' === state.system) {
      this.setState(state)
    }
    else {
      const amount = Math.round(state.amount.replace(/,/g, '.') * api.config.money.scale)
      if (amount > 0) {
        const {success, url, address, data} = await api.send(state.system + '/pay', {amount})
        if (success) {
          if (url) {
            if (isObject(data)) {
              api.createForm(url, data).submit()
            }
            else {
              return location.href = url
            }
          }
          else {
            this.setState({address})
          }
          this.setState({create: false})
          // location.href = url
        }
      }
      else {
        AlertList.show({
          error: true,
          content: t('Enter amount')
        })
      }
    }
  }

  render() {
    if (this.state.address) {
      return <Address {...this.state} />
    }
    return <div className="widget payment">{this.create()}</div>
  }
}
