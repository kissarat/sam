import api from '../connect/api.jsx'
import React, {Component} from 'react'
import {Segment, Dimmer, Loader} from 'semantic-ui-react'

export default class Address extends Component {
  state = {
    address: ''
  }

  async componentWillMount() {
    const {address} = await api.get('blockio/address')
    if (address) {
      this.setState({address})
    }
  }

  address() {
    return <div className="btc-address">
      <span className="label">{t('BTC address')}:</span>&nbsp;
      <span className="value">{this.state.address}</span>
    </div>
  }

  render() {
    return <Segment className="btc-address-container">
      <Loader inverted active={!this.state.address}>
        <Dimmer/>
      </Loader>
      {this.state.address ? this.address() : ''}
      {this.props.amount ? <span className="rate btc">{this.props.amount}</span> : ''}
    </Segment>
  }
}
