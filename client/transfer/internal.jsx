import AlertList from '../widget/alert.jsx'
import api from '../connect/api.jsx'
import FormComponent from '../widget/form.jsx'
import React, {Component} from 'react'
import {browserHistory} from 'react-router'
import {Button, Form} from 'semantic-ui-react'

export default class Internal extends FormComponent {
  state = {}

  async submit() {
    AlertList.clear()
    this.setState({busy: true})
    const {status, transfer, error} = await api.send('transfer/send', {
      nick: this.state.nick,
      amount: this.state.amount.replace(/,/g, '.') * api.config.money.scale
    })
    switch (status) {
      case 402:
        AlertList.show({
          error: true,
          content: t('Insufficient funds'),
          onDismiss: true
        })
        break
      case 404:
        AlertList.show({
          error: true,
          content: t('User {nick} not found', {nick: this.state.nick}),
          onDismiss: true
        })
        break
      default:
        if (transfer) {
          AlertList.show({
            success: true,
            content: t('Successfully'),
            onDismiss: true
          })
          if ('function' === typeof this.props.cancel) {
            this.props.cancel()
          }
          else {
            browserHistory.push('/finance')
          }
        }
        else {
          AlertList.show({
            error: true,
            content: error.message || t('Unknown error'),
            onDismiss: true
          })
        }
    }
    this.setState({busy: false})
  }

  render() {
    return <Form onSubmit={this.onSubmit} loading={this.state.busy}>
      <Form.Group>
        <Form.Input
          label={t('Amount')}
          type="text"
          value={this.state.amount}
          onChange={this.onChange}
          name="amount"/>
        <Form.Input
          label={t('Receiver')}
          name="nick"/>
      </Form.Group>
      <div className="control">
        <Button positive content={t('Transfer')}/>
        <Button
          color="red"
          type="button"
          content={t('Cancel')}
          onClick={this.props.cancel}/>
      </div>
    </Form>
  }
}
