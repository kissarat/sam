import FormComponent from '../widget/form.jsx'
import React, {Component} from 'react'
import t from '../t'
import {Button, Form} from 'semantic-ui-react'
import {map, first} from 'lodash'
import {systems, dictionaryToOptions} from '../finance/enums.jsx'

export default class Create extends FormComponent {
  state = {
    system: '',
    btc: 0
  }

  componentWillMount() {
    this.setState({system: first(Object.keys(systems))})
  }

  change(name, value) {
    const isBitcoin = 'amount' === name && value > 0 && 'blockio' === this.state.system
       && window.ExchangeRate && ExchangeRate.USDBTC
    this.setState({
      [name]: value,
      btc: isBitcoin ? Math.ceil(value * ExchangeRate.USDBTC * 10000) / 10000 : 0
    })
  }

  async submit() {
    this.setState({busy: true})
    await this.props.success(this.state)
    this.setState({busy: false})
  }

  render() {
    const options = dictionaryToOptions(systems)
    return <Form onSubmit={this.onSubmit} loading={this.state.busy}>
      <Form.Group>
        <Form.Input
          name="amount"
          label={t('Amount')}
          value={this.state.amount || this.props.amount || ''}
          onChange={this.onChange}/>
        <Form.Select
          name="system"
          label={t('Payment system')}
          onChange={this.onChange}
          value={this.state.system}
          options={options}/>
      </Form.Group>
      {this.state.btc ? <div className="rate btc">{this.state.btc}</div> : ''}
      <div className="control">
        <Button positive content={t('Pay')}/>
        <Button
          color="red"
          type="button"
          content={t('Cancel')}
          onClick={this.props.cancel}/>
      </div>
    </Form>
  }
}
