import api from '../connect/api.jsx'
import Filters from './filters.jsx'
import Money from '../widget/money.jsx'
import Payment from './payment.jsx'
import React from 'react'
import t from '../t'
import TablePage from '../widget/table-page.jsx'
import {Table, Segment, Icon} from 'semantic-ui-react'
import {types, statuses} from '../finance/enums.jsx'

export default class TransferList extends TablePage {
  rows() {
    return this.state.transfers.map(row => <Table.Row
      key={row.id}
      positive={api.config.user.id === row.to}
      negative={api.config.user.id === row.from}>
      <Table.Cell>{row.id}</Table.Cell>
      <Table.Cell>{new Date(row.created).toLocaleString(t.language()).replace(/\s+/g, '\u00a0')}</Table.Cell>
      <Table.Cell>{api.config.user.id === row.to ? row.from_nick : row.to_nick}</Table.Cell>
      <Money as={Table.Cell} value={row.amount}/>
      <Table.Cell>{types[row.type]}</Table.Cell>
      <Table.Cell>
        {'success' === row.status ? <Icon name="check circle"/> : ''}
        <span>{statuses[row.status]}</span>
        {'canceled' === row.status ? <Icon name="warning sign"/> : ''}
      </Table.Cell>
      <Table.Cell>{row.text ? t(row.text, row.vars ? row.vars : row) : (row.wallet ? t('Wallet {wallet}', row) : null)}</Table.Cell>
    </Table.Row>)
  }

  filter = (name, value) => {
    this.setState({[name]: value})
    setTimeout(this.load)
  }

  render() {
    return <Segment>
      <h1>{t('Finances')}</h1>
      <div className="controls">
        <Payment {...this.props.params}/>
        <Filters change={this.filter}/>
      </div>
      <Table compact="very" className="transfer-list">
        <Table.Header>
          {this.paginator(7)}
          <Table.Row>
            <Table.Cell>{t('ID')}</Table.Cell>
            <Table.Cell>{t('Time')}</Table.Cell>
            <Table.Cell>{t('User')}</Table.Cell>
            <Table.Cell>{t('Amount')}</Table.Cell>
            <Table.Cell>{t('Type')}</Table.Cell>
            <Table.Cell>{t('Status')}</Table.Cell>
            <Table.Cell>{t('Note')}</Table.Cell>
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.rows()}</Table.Body>
      </Table>
    </Segment>
  }
}
