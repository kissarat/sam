import React, {Component} from 'react'
import t from './t'

export class NotFound extends Component {
  render() {
    return <div className="page error not-found">
      <img
        src="http://vk-mm.com/images/not-found.png"
        alt={t('Page not found')}/>
    </div>
  }
}

export class Unavailable extends Component {
  render() {
    return <div className="page error unavailable">
      <img
        src="http://vk-mm.com/images/unavailable.jpg"
        alt={t('Server is not available')}   />
    </div>
  }
}

export class Development extends Component {
  render() {
    return <div className="page error development">
      <img
        src="http://borntofly.biz.ua/access_denied.jpg"
        alt={t('Under development')}
      />
    </div>
  }
}
