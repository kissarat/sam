import api from '../connect/api.jsx'
import buy from './buy.jsx'
import Cart from './cart.jsx'
import React, {Component} from 'react'
import Visit from '../connect/visit.jsx'
import {Button} from 'semantic-ui-react'
import {Link} from 'react-router'
import {map, isEqual} from 'lodash'
import {NotFound} from '../page.jsx'

export default class Product extends Component {
  state = {
    found: true,
    busy: true
  }

  componentWillReceiveProps(props) {
    if (!isEqual(this.props.params, props.params)) {
      this.load(this.props.params)
    }
  }

  componentDidMount() {
    this.load(this.props.params)
  }

  async load(params) {
    if (this.unregister instanceof Function) {
      await this.unregister()
    }
    params = params.splat ? {path: params.splat} : params
    const {article} = await api.get('article/get', params)
    this.setState({
      found: !!article,
      busy: false
    })
    if (article) {
      this.setState(article)
    }
    Visit.register(this, params.splat || '/product/' + params.id)
  }

  render() {
    if (false === this.state.found) {
      return <NotFound/>
    }
    const edit = api.config.user.admin
      ? <Link to={'/edit/' + this.state.id}>Редактировать #{this.state.id}</Link> : ''
    return <article className="page product">
      <div className="column margin">
        <div className="gallery">
          <div className="image" style={{backgroundImage: `url("${this.state.image}")`}}></div>
        </div>
        <div className="titled">
          <h1>{this.state.name}</h1>
          {edit}
          <div className="column">
            <div className="text margin" dangerouslySetInnerHTML={{__html: this.state.text}}/>
            <div className="panel buy margin">
              <div>
                <strong>Цена</strong>
                <div className="money">{this.state.price / 100}</div>
                <strong style={{display: 'none'}}>Продавец</strong>
                <div className="seller" style={{display: 'none'}}>{this.state.user}</div>
                <strong style={{display: 'none'}}>Оплата</strong>
                <div className="payment" style={{display: 'none'}}>mviktor.com</div>
                <strong style={{display: 'none'}}>Подарки</strong>
                <div className="prizes" style={{display: 'none'}}>mviktor.com</div>
              </div>
              <Button.Group>
                <Button className="buy" onClick={() => buy(this.state)}>Купить</Button>
              </Button.Group>
            </div>
          </div>
        </div>
      </div>
    </article>
  }
}
