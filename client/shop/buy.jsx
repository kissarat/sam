import AlertList from '../widget/alert.jsx'
import api from '../connect/api.jsx'
import t from '../t'
import {browserHistory} from 'react-router'

export default async function buy(params) {
  const {success, status, need, article} = await api.send('cart/buy', params)
  if (success) {
    AlertList.show({
      content: t('{name} bought successfully', article),
      positive: true,
      timeout: true
    })
    browserHistory.push('/finance')
  }
  else if (402 === status) {
    AlertList.show({
      content: t('Insufficient funds'),
      negative: true,
      timeout: 30
    })
    browserHistory.push('/pay-' + (need / api.config.money.scale))
  }
}
