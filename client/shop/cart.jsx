import AlertList from '../widget/alert.jsx'
import api from '../connect/api.jsx'
import buy from './buy.jsx'
import React, {Component} from 'react'
import {browserHistory, Link} from 'react-router'
import {Dimmer, Loader, Segment, Form, Table, Icon} from 'semantic-ui-react'
import {map, pick, isEqual} from 'lodash'

export default class Cart extends Component {
  state = {
    busy: true,
    items: []
  }

  static add(id) {
    return api.send('item/add', {article: id})
  }

  componentWillReceiveProps(props) {
    if (!isEqual(this.props.params, props.params)) {
      this.load(this.props.params)
    }
  }

  componentDidMount() {
    this.load(this.props.params)
  }

  async load(params, busy = true) {
    if (busy) {
      this.setState({busy})
    }
    const items = await api.get('item/index', params)
    items.forEach(a => a.name = a.name.slice(0, 36))
    this.setState({
      items,
      busy: false
    })
  }

  async changeAmount(cart, article, amount) {
    await api.send('item/update', {cart, article}, {amount})
    return this.load(this.props.params, false)
  }

  onSubmit = (e, {formData}) => {
    e.preventDefault()
    buy()
    // return this.buy({id: this.state.items[0].cart}, formData.about ? formData : null)
  }

  async remove(params) {
    const {success} = await api.del('item/remove', params)
    if (success) {
      return this.load(this.props.params)
    }
  }

  async clear() {
    const {success} = await api.del('cart/remove')
    if (success) {
      browserHistory.push('/product/list/0')
    }
  }

  async buy(params, data) {
    const {success, status} = await api.send('cart/buy', params, data)
    if (success) {
      AlertList.show({
        content: `Вы совершили покупку на сумму ${this.summary()}`,
        positive: true,
        timeout: true
      })
      browserHistory.push('/product/list/0')
    }
    else if (402 === status) {
      AlertList.show({
        content: t('Insufficient funds'),
        negative: true,
        timeout: 30
      })
    }
  }

  summary() {
    return this.state.items.reduce((a, b) => b.price * b.amount + a, 0) / 100
  }

  rows() {
    return this.state.items.map(p => {
      const key = [p.cart, p.article, p.price].join('_')
      return <Table.Row key={key}>
        <Table.Cell>{p.name}</Table.Cell>
        <Table.Cell>
          <Form.Input
            type="number"
            name={key}
            min="1"
            value={+p.amount}
            onChange={e => this.changeAmount(p.cart, p.article, +e.target.value)}/>
        </Table.Cell>
        <Table.Cell className="money">{p.price / 100}</Table.Cell>
        <Table.Cell>
          <Icon name="trash outline" size="large" onClick={() => this.remove(pick(p, 'cart', 'article'))}/>
        </Table.Cell>
      </Table.Row>
    })
  }

  render() {
    const summary = this.summary()
    return <Segment className="page cart">
      <h1>Корзина</h1>
      <Dimmer inverted active={this.state.busy}>
        <Loader/>
      </Dimmer>
      <Form onSubmit={this.onSubmit}>
        <Form.Button
          onClick={this.clear}
          type="button"
          disabled={this.state.items.length <= 0}
          negative>
          Очистить корзину
        </Form.Button>
        <Table>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Названия</Table.HeaderCell>
              <Table.HeaderCell>Количество</Table.HeaderCell>
              <Table.HeaderCell>Цена</Table.HeaderCell>
              <Table.HeaderCell/>
            </Table.Row>
          </Table.Header>
          <Table.Body>{this.rows()}</Table.Body>
          <Table.Footer>
            <Table.Row>
              <Table.HeaderCell>Сумма</Table.HeaderCell>
              <Table.HeaderCell>{this.state.items.reduce((a, b) => b.amount + a, 0)}</Table.HeaderCell>
              <Table.HeaderCell
                className="money">{summary}</Table.HeaderCell>
              <Table.HeaderCell/>
            </Table.Row>
          </Table.Footer>
        </Table>
        <Form.TextArea
          disabled={summary <= 0}
          label="Заметка"
          name="about"
          autoHeight/>
        <Form.Button
          disabled={summary <= 0}
          positive>
          Купить
        </Form.Button>
      </Form>
    </Segment>
  }
}
