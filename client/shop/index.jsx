import AlertList from '../widget/alert.jsx'
import api from '../connect/api.jsx'
import buy from './buy.jsx'
import Cart from './cart.jsx'
import Paginator from '../widget/paginator.jsx'
import React, {Component} from 'react'
import {Button, Dimmer, Loader, Icon} from 'semantic-ui-react'
import {Link, browserHistory} from 'react-router'
import {pick, defaults, isMatch} from 'lodash'

export default class ProductList extends Component {
  state = {
    offset: 0,
    limit: 12,
    count: 0,
    busy: false,
    products: []
  }

  componentWillReceiveProps(props) {
    if (!isMatch(this.state.page, props.params)) {
      this.loadPage(props.params)
    }
  }

  componentDidMount() {
    this.loadPage({count: 1, ...this.props.params})
  }

  async loadPage(page) {
    this.setState({busy: true})
    page = defaults(pick(page, 'offset', 'count', 'from'), pick(this.state, 'limit'))
    const {articles, count} = await api.get('article/page', {type: 'product', ...page})
    if (count) {
      page.count = count
    }
    this.setState({
      products: articles,
      busy: false,
      ...page
    })
  }

  changeOffset = offset => browserHistory.push('/product/list/' + offset)

  async add(p) {
    return void buy({id: p.id})
    const {success} = await Cart.add(p.id)
    if (success) {
      AlertList.show({
        content: `«${p.name}» добавлен в корзину`,
        icon: 'shopping bag',
        positive: true,
        timeout: true
      })
    }
  }

  list() {
    return this.state.products.map((p) => {
      const amount = p.price / 100
      const image = p.image || '/images/blank-product.png'
      return <li key={p.id} className="item product">
        <div className="image" style={{backgroundImage: `url("${image}")`}}></div>
        <h2>{p.name}</h2>
        <p>{p.short}</p>
        <strong className="money">{amount}</strong>
        <div>
          <span className="BTC">{Math.ceil(amount * ExchangeRate.USDBTC * 10000) / 10000}฿,</span>
          &nbsp;
          <span className="RUB">{Math.ceil(amount * ExchangeRate.USDRUB)}₽</span>
          &nbsp;или&nbsp;
          <span className="UAH">{Math.ceil(amount * ExchangeRate.USDUAH)}₴</span>
        </div>
        <Button.Group>
          <Button className="view" as={Link} to={'/product/' + p.id}>Просмотр »</Button>
          <Button className="buy" type="button" onClick={() => this.add(p)}>Купить</Button>
        </Button.Group>
      </li>
    })
  }

  paginator() {
    if (this.state.count > this.state.limit) {
      return <Paginator {...this.state} size={11} changeOffset={this.changeOffset}/>
    }
  }

  render() {
    return <div className="page product-list">
      <h1>Магазин</h1>
      <Link to="/cart" className="basket">
        <Icon name="shopping basket"/>
        <span>Корзина</span>
      </Link>
      <Dimmer inverted active={this.state.busy}>
        <Loader/>
      </Dimmer>
      <ul>{this.list()}</ul>
      {this.paginator()}
    </div>
  }
}
