// const Watch = require('./watch')
const webpack = require('webpack');
const package_json = require('../package.json')
const {map, pick} = require('lodash')
// const CompressionPlugin = require('compression-webpack-plugin')

const mode = {}
for (const name of (process.env.SAM || '').split(',')) {
  mode[name] = true
}

const babel = {
  test: /\.jsx$/,
  exclude: /(node_modules)/,
  loader: 'babel-loader',
  query: {
    presets: [
      'react',
    ],
    plugins: [
      'transform-class-properties',
      'transform-decorators-legacy',
      'transform-object-rest-spread',
    ]
  }
}

const config = {
  mode: mode.prod ? 'production' : 'development',
  // entry: __dirname + '/entry.jsx',
  output: {
    // path: __dirname + '/../public',
    filename: 'inbisoft.js'
  },
  watchOptions: {
    ignored: /node_modules/
  },
  module: {
    rules: [
      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=10000&minetype=application/font-woff"
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader"
      },
      // {
      //   test: /\.json$/,
      //   loader: 'json-loader'
      // },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
      babel
    ]
  },
  resolve: {},
  plugins: []
}

if (mode.dev) {
  config.devtool = 'source-map'
}


// if (mode.autoreload) {
//   config.plugins.push(new Watch())
// }

if (mode.usage) {
  const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
  config.plugins.push(new BundleAnalyzerPlugin({
    analyzerMode: 'server',
    analyzerHost: '127.0.0.1',
    analyzerPort: 8888,
  }))
}

if (mode.prod) {
  console.log('PRODUCTION')
  babel.query.presets.push(
    'es2015',
    'stage-1'
  )
  config.plugins.push(
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new webpack.optimize.CommonsChunkPlugin({
      children: true,
      async: true,
    }),
    new webpack.optimize.UglifyJsPlugin({
      cacheFolder: '/tmp',
      debug: false,
      minimize: true,
      sourceMap: false,
      beautify: false,
      comments: false,
      compress: {
        sequences: true,
        booleans: true,
        loops: true,
        unused: true,
        warnings: false,
        drop_console: true,
        unsafe: true,
      },
      output: {
        comments: false
      }
    }),
    // new CompressionPlugin({
    //   asset: "[path].gz[query]",
    //   algorithm: "gzip",
    //   test: /\.(js|html)$/,
    //   threshold: 10240,
    //   minRatio: 0.8
    // }),
    new webpack.BannerPlugin({
      banner: map(pick(package_json, 'name', 'version', 'url', 'email', 'author'), (v, k) => `@${k} ${v}`).join('\n'),
      entryOnly: false
    })
  )
}

module.exports = config
