import api from '../connect/api.jsx'
import React, {Component} from 'react'
import {Link} from 'react-router'
import {List, Segment, Dimmer, Loader} from 'semantic-ui-react'

export default class ArticleList extends Component {
  state = {
    busy: true,
    articles: []
  }

  async componentDidMount() {
    const {articles} = await api.get('article/index')
    if (articles instanceof Array) {
      this.setState({
        busy: false,
        articles
      })
    }
  }

  list() {
    return this.state.articles.map(a => <List.Item key={a.id} className={'item ' + a.path}>
      <List.Content>
        <List.Header as={Link} to={a.path ? '/' + a.path : '/view/' + a.id}>{a.name}</List.Header>
        <List.Description>{a.short}</List.Description>
      </List.Content>
    </List.Item>)
  }

  render() {
    return <Segment className="articles">
      <Dimmer active={this.state.busy} inverted>
        <Loader/>
      </Dimmer>
      <h1>Статьи</h1>
      <Link className="button" to="/create">Создать</Link>
      <List>{this.list()}</List>
    </Segment>
  }
}
