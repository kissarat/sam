import React, {Component} from 'react'
import {Form} from 'semantic-ui-react'
import {keyBy} from 'lodash'

export default class CategoryList extends Component {
  state = {}

  async componentDidMount() {
    const {articles} = await api.get('article/index', {
      type: 'cat',
      select: 'id,name'
    })
    this.setState({busy: false})
    if (articles) {
      const options = articles.map(a => ({
        key: a.id,
        value: a.id,
        text: a.name,
      }))
      // categories.unshift({key: 'empty', value: '', text: '- Категория не выбрана -'})
      const list = keyBy(articles, 'id')
      // if (this.props.list instanceof Function) {
      //   this.props.list(list)
      // }
      this.setState({list, options})

    }
  }

  render() {
    if (this.state.options) {
      return <Form.Select
        className="category-list widget"
        type="text"
        name="category"
        label="Категории"
        value={this.props.value}
        onChange={(e, {value}) => this.props.change(this.state.list[value])}
        options={this.state.options}
      />
    }
    else {
      return <div className="category-list widget empty"/>
    }
  }
}
