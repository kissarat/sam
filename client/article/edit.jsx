import api from '../connect/api.jsx'
import Menu from '../widget/menu.jsx'
import React, {Component} from 'react'
import t from '../t'
import {Form, Button, Icon} from 'semantic-ui-react'
import {Link, browserHistory} from 'react-router'
import {pick, map, clone} from 'lodash'
import {translitURL} from './utils.jsx'
import ReactQuil from 'react-quill'
import 'react-quill/dist/quill.snow.css'

const types = [
  {key: 'article', value: 'article', text: 'Статья'},
  {key: 'page', value: 'page', text: 'Страница'}
]

const editor = {
  modules: {
    toolbar: [
      [{ 'header': [1, 2, false] }],
      ['bold', 'italic', 'underline','strike', 'blockquote'],
      [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
      ['link', 'image'],
      ['clean']
    ],
  },

  formats: [
    'header',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'list', 'bullet', 'indent',
    'link', 'image'
  ],
}

export class ArticleEdit extends Component {
  state = {
    price: 0
  }

  componentWillReceiveProps(props) {
    if (props.params.id && this.props.params.id !== props.params.id) {
      this.setState({busy: true})
      this.loadArticle(props.params.id)
    }
  }

  componentWillMount() {
    if (this.props.params.id) {
      this.setState({busy: true})
    }
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.loadArticle(this.props.params.id)
    }
  }

  async loadArticle(id) {
    const {article, relations} = await api.get('article/get', {id, relation: 1})
    this.setState({busy: false})
    if (article) {
      article.relations = relations
      this.setState(article)
    }
    return article
  }

  pathFromName = () => {
    this.setState({path: translitURL(this.state.name)})
  }

  onChange = (e, {name, value}) => {
    this.setState({[name]: value})
  }

  onSubmit = async e => {
    e.preventDefault()
    const data = clone(this.state)
    if (this.state.relations instanceof Array) {
      data.relations = this.state.relations.map(r => r.id)
    }
    const result = await api.send('article/save', pick(this.state, 'id'), data)
    if (result.article && result.article.id > 0) {
      browserHistory.push('/edit/' + result.article.id)
    }
  }

  onChangePrice = e => {
    this.setState({price: e.target.value * 100})
  }

  shortCount() {
    if ('string' === typeof this.state.short) {
      return <div className="short-count">{this.state.short.trim().length}</div>
    }
  }

  type() {
    const options = map({
          page: t('Page'),
          product: t('Product'),
          cat: t('Category'),
        },
        (text, key) => ({value: key, key, text}))

    return <Form.Select
        name="type"
        label={t('Type')}
        onChange={this.onChange}
        value={this.state.type}
        options={options}
    />
  }

  addCategory = relation => {
    this.state.relations.push(relation)
    this.setState({relations: this.state.relations})
  }

  price() {
    if ('product' === this.state.type) {
      return <div className="price">
        <Form.Input
            type="text"
            label={t('Cost')}
            name="price"
            required
            value={(this.state.price || 0) / 100}
            onChange={this.onChangePrice}
        />
      </div>
    }
  }

  breadcrumb() {
    let breadcrumb = [
      [t('Articles'), '/articles']
    ]
    if (this.props.params.id) {
      const url = this.state.path ? '/' + this.state.path : '/view/' + this.props.params.id
      breadcrumb.push([this.state.name, url])
    }
    return Menu.normalize(breadcrumb)
  }

  view() {
    const id = this.props.params.id
    if (id) {
      return <Link className="btn btn-primary" to={'/view/' + id}>{t('View')}</Link>
    }
  }

  relations() {
    if (this.state.relations instanceof Array) {
      return this.state.relations.map((r, i) => <div className="relation has" key={r.id} id={r.path || r.id}>
        <span>{r.name}</span>
        <Icon name="close" onClick={() => this.setState({relations: this.state.relations.splice(i, 1)})}/>
      </div>)
    }
  }

  render() {
    return <Form className="article-editor" onSubmit={this.onSubmit} loading={this.state.busy}>
      <h1>{t('Editing an article')}</h1>
      <Form.Input
          type="text"
          label={t('Name')}
          name="name"
          required
          value={this.state.name || ''}
          onChange={this.onChange}
      />
      {this.relations()}
      {this.price()}
      <Form.Input
          name="short"
          label={t('Short description')}
          onChange={this.onChange}
          value={(this.state.short || '').replace(/\s+/g, ' ')}
      />
      <Form.Select
          name="type"
          label={t('Type')}
          onChange={this.onChange}
          options={types}
          value={this.state.type || 'page'}
      />
      <Form.Input
          name="image"
          label={t('Image')}
          onChange={this.onChange}
          value={this.state.image || ''}
      />
      {this.shortCount()}
      <Form.Field>
        <label>{t('The URL where the article will be available')}</label>
        <Form.Input
            type="text"
            name="path"
            value={this.state.path || ''}
            onChange={this.onChange}
        />
        <Form.Button type="button" onClick={this.pathFromName}>{t('From the title')}</Form.Button>
      </Form.Field>
      <Form.Field>
        <label>{t('Text')}</label>
        <ReactQuil value={this.state.text || ''}
                   onChange={text => this.setState({text})}
                   theme="snow"
                   modules={editor.modules}
                   formats={editor.formats}/>
      </Form.Field>
      <div className="control">
        <Button type="submit">{t('Save')}</Button>
        {this.view()}
      </div>
    </Form>
  }
}
