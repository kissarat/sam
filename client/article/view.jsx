import api from '../connect/api.jsx'
import React, {Component} from 'react'
import {Link, browserHistory} from 'react-router'
import {NotFound} from '../page.jsx'

export default class View extends Component {
  remove = async () => {
    if (confirm('Вы уверены, что хотите удалить статью?')) {
      await api.send('article/remove', {id: this.state.article.id}, null, 'DELETE')
      browserHistory.push('/articles')
    }
  }

  async load(props) {
    if (props.params) {
      const params = props.params.splat ? {path: props.params.splat} : props.params
      const {article} = await api.get('article/get', params)
      if (article) {
        this.setState({article})
      }
    }
  }

  async componentWillReceiveProps(props) {
    this.load(props)
  }

  componentDidMount() {
    this.load(this.props)
  }

  render() {
    if (this.state && this.state.article) {
      const article = this.state.article
      let path = 'article'
      if (article.path) {
        path += ' ' + this.state.path
      }
      return <article className={path}>
        <h1 className="name">
          {article.name}
          <Link to={'/edit/' + article.id} className="fa fa-edit"/>
          <button type="button" onClick={this.remove}>Удалить</button>
        </h1>
        <i className="short">{article.short}</i>
        <div className="text" dangerouslySetInnerHTML={{__html: article.text}}/>
      </article>
    }
    else {
      return <NotFound/>
    }
  }
}
