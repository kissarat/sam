const charsMap = {
  'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh',
  'з': 'z', 'и': 'i', 'й': 'j', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n',
  'о': 'o', 'п': 'p', 'р': 'r', 'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h',
  'ц': 'c', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh', 'ы': 'y', 'э': 'e', 'ю': 'yu', 'я': 'ya',
  'ґ': 'g', 'є': 'ye', 'ї': 'yi'
}

'0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('').forEach(function (c) {
  charsMap[c] = c
})

const regex = new RegExp('[' + Object.keys(charsMap) + ']', 'g')

export function translitURL(text) {
  return text
    .toLowerCase()
    .replace(regex, s => charsMap[s] ? charsMap[s] : '-')
    .replace(/[-\s]+/g, '-')
    .replace(/[^\w\-]/, '')
}
