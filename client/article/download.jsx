import React, {Component} from 'react'

export default class Download extends Component {
  render() {
    return <div className="page download">
      <h1>Inbisoft MLBot v1.1.0 beta</h1>
      <ul>
        <li><a href="http://app.inbisoft.com/serve/download/mlbot-windows-64bit">Windows 64 bit</a></li>
        <li><a href="http://app.inbisoft.com/serve/download/mlbot-windows-32bit">Windows 32 bit</a></li>
        <li><a href="http://app.inbisoft.com/serve/download/mlbot-mac">macOS</a></li>
        <li><a href="http://app.inbisoft.com/serve/download/mlbot-linux-64bit">Linux 64 bit (7zip-архив)</a></li>
        <li><a href="http://app.inbisoft.com/serve/download/mlbot-linux-32bit">Linux 32 bit (7zip-архив)</a></li>
      </ul>
    </div>
  }
}
