const api = require('./connect/api.jsx').default
const React = require('react')
const {boot, isClient, DEBUG} = require('../common/globals')
const {render} = require('react-dom')
const {Unavailable, Development} = require('./page.jsx')
const {currencies} = require('./finance/enums.jsx')

if (isClient) {
  require('../client/polyfill')
  // if (DEBUG) {
  //   require('../client/watch-client')()
  // }
}

export async function bootstrap() {
  // try {
    const config = await api.handshake()
    if (api.config.user.id) {
      const {result} = await api.get('transfer/exchange')
      const ex = {}
      for (const x of result) {
        if (!ex[x.from]) {
          ex[x.from] = {}
        }
        ex[x.from][x.to] = x.rate
      }
      api.config.exchange = ex

      // for (const key in result) {
      //   currencies[key] = result[key][key]
      // }
    }
    const rootElement = document.getElementById('app')
    if (config.maintenance) {
      return void render(<Development/>, rootElement)
    }
    for (const listener of boot.listeners) {
      listener(config)
    }
  // }
  // catch (err) {
  //   console.error(err)
  //   render(<Unavailable/>, document.getElementById('app'))
  //   throw err
  // }
}

export function setup(router) {
  return new Promise(function (resolve, reject) {
    document.addEventListener('DOMContentLoaded', async function () {
      bootstrap()
        .then(function () {
          resolve(render(router, document.getElementById('app')))
        })
        .catch(reject)
    })
  })
}
