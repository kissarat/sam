import {each} from 'lodash'

export async function load(url) {
  const res = await fetch('/serve/web/' + btoa(url))
  if (res.ok) {
    const text = await res.text()
    const doc = document.implementation.createHTMLDocument("New Document")
    doc.documentElement.innerHTML = text
    return doc
  }
  return false
}

export async function extract(url, selectors) {
  const doc = await load(url)
  if (doc) {
    const vars = {}
    each(selectors, function (selector, name) {
      const el = doc.querySelector(selector)
      if (el) {
        vars[name] = el.innerHTML.trim()
      }
    })
    return vars
  }
  return false
}

export async function extractMany(urls, selectors) {
  const results = {}
  for (let i = 0; i < urls.length; i++) {
    const url = urls[i]
    const result = await extract(url, selectors)
    if (result) {
      results[url] = result
    }
  }
  return results
}

export async function geektimes(last, amount) {
  let a = await extractMany(_.range(last - amount, last + 1)
    .map(i => `https://geektimes.ru/post/${i}/`), {name: 'h1 span', text: '.content.html_format'})
  a = _.toArray(a).filter(b => b.text)
    .map(b => ({name: b.name.slice(0, 96), text: b.text.slice(0, 16200)}))
  return a
}
