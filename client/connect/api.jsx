/**
 * Last modified: 18.06.06 14:30:48
 * Hash: d6a1eedc1ea2d4690f358957ec4a39516077ff0c
 */

import 'whatwg-fetch'
import qs from '../base/urlencoded.jsx'
// import routes from '../../routes.json'
import {browserHistory} from 'react-router'
import {announce, isClient} from '../../common/globals'
import {each, isEmpty, isObject} from 'lodash'

const routesMap = {}
// each(routes, function (module, moduleName) {
//   each(module, function (encodedAction, actionName) {
//     routesMap[moduleName + '/' + actionName] = encodedAction
//   })
// })

const TOKEN_AGE = 3 * 24 * 3600
localStorage.removeItem('sam')

const headers = {
  Accept: 'application/json',
  'Content-Type': 'application/json; charset=utf-8',
  // Author: 'Taras Labiak <kissarat@gmail.com>'
}

export class API {
  constructor() {
    this.prefix = '/serve'
  }

  get token() {
    const t = /sam=(\w+)/.exec(document.cookie)
    return t ? t[1] : null
  }

  set token(value) {
    document.cookie = `sam=${value}; path=/; max-age=` + (value ? TOKEN_AGE : 0)
  }

  get config() {
    return this._config
  }

  set config(value) {
    if (value.token) {
      this.token = value.token.id
    }
    return this._config = value
  }

  async handshake() {
    const params = {
      name: navigator.userAgent,
      expires: new Date(Date.now() + TOKEN_AGE * 1000).toISOString(),
    }
    return this.config = await this.send('token/handshake', {time: Date.now()}, params)
  }

  buildURL(url, params) {
    if ('/' === url[0]) {
      const part = url.slice(1)
      const found = routesMap[part]
      if (found) {
        url = '/' + found
      }
    }
    else {
      url = routesMap[url] || url
      const token = this.token
      url = token
        ? `/~${token}/${url}`
        : '/' + url
    }
    if (!isEmpty(params)) {
      url += '?' + qs.stringify(params)
    }
    return url
  }

  async handleResponse(res) {
    if (401 === res.status) {
      return void browserHistory.push('/login')
    }
    const json = await res.json()
    if (!this.responses) {
      this.responses = []
    }
    if (json.result instanceof Array) {
      this.responses.push(json)
    }
    return json
  }

  get isAdmin() {
    return this.config && this.config.user && this.config.user.admin
  }

  async fetch(url, options = {}) {
    if (!options.headers) {
      options.headers = {}
    }
    options.credentials = 'include'
    if (window.crypto && crypto.getRandomValues instanceof Function) {
      const array = new Uint8Array(16)
      crypto.getRandomValues(array)
      options.headers.id = Array.from(array).map(n => n.toString(16)).join('')
    }
    // if (this.isAdmin) {
    //   options.headers.auth = 'Token ' + this.token
    // }
    return this.handleResponse(await fetch(url, options))
  }

  async get(url, params) {
    url = this.prefix + this.buildURL(url, params)
    return this.fetch(url, {headers})
  }

  async send(url, params, data, method = 'POST') {
    if (!data) {
      data = params
      params = null
    }
    url = this.buildURL(url, params)
    const options = {method, headers}
    if (!isEmpty(data) && true !== data) {
      options.body = JSON.stringify(data)
    }
    return this.fetch(this.prefix + url, options)
  }

  async del(url, params) {
    url = this.prefix + this.buildURL(url, params)
    return this.fetch(url, {method: 'DELETE', headers})
  }

  get entities() {
    return this.config.entities
  }

  async logout(pushLoginPage = false) {
    if (pushLoginPage) {
      const config = await this.send('user/logout')
      if (config.success || 401 === config.code) {
        if (config.success) {
          this.config = config
        }
        return browserHistory.push('/login')
      }
      return false
    }
    else {
      return this.send('user/logout')
    }
  }

  checkAuthorized = () => {
    if (!this.config.user || this.config.user.guest) {
      browserHistory.push('/login')
    }
  }

  async getInformer(load) {
    const {user} = await api.get('user/get')
    if (load || !this._user) {
      ['balance', 'payment', 'withdraw', 'accrue'].forEach(function (name) {
        user[name] = user[name] / api.config.money.scale
      })
      this.informer = user
    }
    return this.informer
  }

  beacon(url, params) {
    if (isClient && navigator.sendBeacon instanceof Function) {
      url = this.prefix + this.buildURL(url, params)
      return navigator.sendBeacon(url)
    }
    else {
      console.error('sendBeacon is not supported')
    }
  }

  _upload(xhr) {
    xhr.open('POST', `/serve/~${this.token}/upload/` + xhr.file.name)
    setTimeout(() => xhr.send(xhr.file), 0)
    return xhr
  }

  upload(files) {
    const requests = []
    for (let i = 0; i < files.length; i++) {
      const xhr = new XMLHttpRequest()
      xhr.file = files[i]
      requests.push(xhr)
    }

    let i = 0
    const next = () => {
      if (i < requests.length) {
        const xhr = this._upload(requests[i++])
        xhr.addEventListener('load', function () {
          xhr.responseJSON = JSON.parse(xhr.responseText)
          const e = new CustomEvent('done')
          xhr.dispatchEvent(e)
          next()
        })
      }
    }

    if (requests.length > 0) {
      next()
    }
    return requests
  }

  promiseUpload(files) {
    return Promise.all(this.upload(files)
      .map(xhr => new Promise(resolve => xhr.addEventListener('load', () => resolve(xhr)))))
  }

  hasPaymentSystem() {
    for (const system of arguments) {
      if (api.config.money.systems.indexOf(system) < 0) {
        return false
      }
    }
    return true
  }

  getRate(from, to = from) {
    const x = this.config.exchange
    if (x[from] && x[from][to]) {
      return x[from][to]
    }
    if (x[to] && x[to][from]) {
      return 1 / x[to][from]
    }
    throw new Error(`Unknown rate pair ${from} ${to}`)
  }

  getScale(currency) {
    return Math.log10(this.getRate(currency))
  }

  isEnabled(name) {
    return !this.config[name] || false !== this.config[name].enabled
  }

  createForm(url, params) {
    const form = document.getElementById('empty-form')
    form.setAttribute('action', url)
    for (const name in params) {
      const input = document.createElement('input')
      input.setAttribute('type', 'hidden')
      input.setAttribute('name', name)
      input.value = params[name]
      form.appendChild(input)
    }
    return form
  }

  getFrontURL(name, params) {
    const c = this.config.front
    let url = c.origin + (c[name] ? c[name] : name)
    if (isObject(params)) {
      url += '?' + qs.stringify(params)
    }
    return url
  }

  get lastResponse() {
    if (this.responses instanceof Array) {
      return this.responses[this.responses.length - 1]
    }
  }

  get textarea() {
    const textarea = document.getElementById('json')
    if (textarea) {
      textarea.value = JSON.stringify(this.lastResponse, null, '  ')
    }
    return textarea
  }
}

function repeatEvery(ms, cb, ...args) {
  return repeatEvery[ms] = setInterval(cb, ms, ...args)
}

// repeatEvery(1000, function () {
//   if (api.responses instanceof Array) {

// for (const json of api.responses) {

// }
// }
// })

const api = new API()
api.repeatEvery = repeatEvery
announce({api})
export default api
