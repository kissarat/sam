function getServer() {
  const server = io('/');
  server.isEnabled = true

  server.on('chat.ping', function(socket){
    console.log('chat.pong', socket)
  });

  server.request = function (collection, options) {
    server.emit(collection.getStorageName(), options)
  }
}

export default {
  isEnabled: false
}
