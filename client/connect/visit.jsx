import {merge, defaults, pick, each, isObject} from 'lodash'
import api from './api.jsx'

const Visit = {
  reportVisit() {
    return api.beacon('visit/trace', {
      url: this.url,
      spend: Date.now() - this.startTime
    })
  },

  componentWillUnmount() {
    this.unregister()
  }
}

const Registry = {}

Visit.register = async function (target, url) {
  if (!(target.unregister instanceof Function)) {
    defaults(target, Visit)
  }
  target.url = url
  target.startTime = Date.now()
  Registry[target.url] = target
}

Visit.unregister = function () {
  delete Registry[this.url]
  return this.reportVisit()
}

addEventListener('beforeunload', function () {
  each(Registry, v => v.unregister())
})

export default Visit
