import api from '../connect/api.jsx'
import React, {Component} from 'react'
import Refresh from '../widget/refresh.jsx'
import {isEmpty, omit} from 'lodash'
import {Segment, Table, Dimmer, Loader} from 'semantic-ui-react'
import t from '../t'

const consts = {
  token: {
    create: t('New visitor'),
    login: t('Login'),
    logout: t('Logout'),
  },

  handshake: {
    update: t('Visit')
  },

  user: {
    create: t('Signup'),
    update: t('Profile update'),
  },

  transfer: {
    update: t('Payment received'),
  }
}

export default class Log extends Component {
  state = {
    busy: false
  }

  async load() {
    this.setState({busy: true})
    const params = api.config.user.admin ? {} : {user: api.config.user.id}
    const {records} = await api.get('log/index', params)
    if (records instanceof Array) {
      this.setState({records})
    }
    this.setState({busy: false})
  }

  componentDidMount() {
    void this.load()
  }

  rows() {
    if (this.state.records instanceof Array) {
      return this.state.records.map(row => {
        const time = new Date(row.id / 1000000).toLocaleString()
        const action = consts[row.entity] && consts[row.entity][row.action]
          ? consts[row.entity][row.action]
          : `${row.entity} ${row.action}`
        const nick = api.config.user.admin
          ? <Table.Cell className="nick">{row.user ? `${row.nick} (${row.user})` : ''}</Table.Cell>
          : ''
        const data = api.config.user.admin ? <Table.Cell className="data">
            <pre>{isEmpty(omit(row.data, 'id'))
              ? row.data && row.data.id
              : JSON.stringify(row.data, null, ' ').split('\n').map(a => a.slice(0, 32)).join('\n')}</pre>
          </Table.Cell> : ''
        return <Table.Row key={row.id} id={row.id}>
          <Table.Cell className="time">{time}</Table.Cell>
          <Table.Cell className="action">{action}</Table.Cell>
          <Table.Cell className="ip">{row.ip}</Table.Cell>
          {nick}
          {data}
        </Table.Row>
      })
    }
  }

  render() {
    return <Segment className="page log">
      <Refresh refresh={() => this.load()}/>
      <h1>{t('Journal')}</h1>
      <Dimmer inverted active={this.state.busy}>
        <Loader/>
      </Dimmer>
      <Table compact="very">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>{t('Time')}</Table.HeaderCell>
            <Table.HeaderCell>{t('Action')}</Table.HeaderCell>
            <Table.HeaderCell>IP</Table.HeaderCell>
            {api.config.user.admin ? <Table.HeaderCell>{t('User')}</Table.HeaderCell> : ''}
            {api.config.user.admin ? <Table.HeaderCell>{t('Data')}</Table.HeaderCell> : ''}
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.rows()}</Table.Body>
      </Table>
    </Segment>
  }
}
