module.exports = function watchClient() {
  if (+localStorage.getItem('debug')) {
    const eventSource = new EventSource('http://localhost:7001/')

    var offline = false

    function reloading() {
      document.body.innerHTML = '<div class="webpack reloading">Please Wait...</div>'
      // setTimeout(() => location.reload(), 20000)
    }

    eventSource.addEventListener('start', reloading)
    eventSource.addEventListener('online', function () {
      if (offline) {
        reloading()
        offline = false
      }
    })

    eventSource.addEventListener('reload', function () {
      location.reload()
    })

    eventSource.addEventListener('error', function (e) {
      offline = true
      document.body.innerHTML = '<div class="webpack offline">offline</div>'
    })
  }
}
