import React, {Component} from 'react'
import api from '../connect/api.jsx'
import {Icon} from 'semantic-ui-react'

export default class Avatar extends Component {
  state = {}

  onChange = async e => {
    if (e.target.files.length > 0) {
      this.setState({avatar: 'https://s-media-cache-ak0.pinimg.com/originals/0b/93/09/0b9309cf2dd079c998a5414e32a04618.gif'})
      const [xhr] = await api.promiseUpload(e.target.files)
      const r = JSON.parse(xhr.responseText)
      await api.send('user/save', {id: api.config.user.id}, {avatar: r.url})
      await this.props.success()
    }
  }

  render() {
    let avatar = this.state.avatar || this.props.avatar
    avatar = avatar ? {backgroundImage: `url("${avatar}")`} : {}
    return <div className="avatar edit"
      style={avatar}
      onClick={() => this.refs.file.click()}>
      <input type="file" ref="file" onChange={this.onChange} style={{display: 'none'}}/>
      <Icon name="photo" size="large" color="grey"/>
    </div>
  }
}
