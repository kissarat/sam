import React, {Component} from 'react'
import t from '../t'
import {isObject} from 'lodash'

export default class Language extends Component {
  changeLanguage = e => {
    localStorage.language = e.target.value
    location.reload()
  }

  render() {
      return <select className="choose-language" value={t.language()} onChange={this.changeLanguage}>
        <option key="ru" value="ru">Русский</option>
        <option key="en" value="en">English</option>
        <option key="it" value="it">Italiano</option>
      </select>
  }
}
