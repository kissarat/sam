/**
 * Last modified: 18.06.07 19:06:21
 * Hash: e952b13c4e6f6c9dbfda9cb7751b385683fc8f63
 */

import React, {Component} from 'react'
import {Dimmer, Loader} from 'semantic-ui-react'
import {debounce, clone} from 'lodash'
import api from '../connect/api.jsx'
import Paginator from './paginator.jsx'

export class Search extends Component {
  componentWillMount() {
    this.setState({value: ''})
  }

  change = debounce(value => this.props.change(value), 200)

  render() {
    return <input value={this.state.value}
                  type="search"
                  placeholder="Поиск..."
           onChange={e => this.setState({value: e.target.value}) || this.change(e.target.value)}/>
  }
}

export default class TablePage extends Component {
  state = {offset: 0, limit: 64, items: [], busy: true, order: {id: -1}}

  componentDidMount() {
    this.load()
  }

  changeOffset = offset => this.load({offset})

  async loadItems(params = {}) {
    this.setState({busy: true})
    const ol = ['offset', 'limit', 'order']
    if ('function' === typeof this.getFilterParams) {
      Object.assign(params, this.getFilterParams())
    }
    for (const key of ol) {
      if (!params[key]) {
        params[key] = this.state[key]
      }
    }
    const query = clone(params)
    const order = []
    for(let key in params.order) {
      if (params.order[key] < 0) {
        key = '-' + key
      }
      order.push(key)
    }
    query.order = order.join(',')
    if (!isFinite(this.state.count)) {
      query.count = 1;
    }
    const {result, count, error} = await api.get(this.props.uri || this.uri, query)
    if (result) {
      const state = {
        busy: false,
        items: result
      }
      if (count >= 0) {
        state.count = count
      }
      for (const key of ol) {
        state[key] = params[key]
      }
      return state
    }
    else if (error && error.message) {
      throw new Error(error.message)
    }
  }

  load = async (params = {}) => {
    const state = await this.loadItems(params)
    this.setState(state)
  }

  dimmer() {
    return <Dimmer inverted active={this.state.busy}>
      <Loader/>
    </Dimmer>
  }

  paginator() {
    if (this.state.count > 0 && this.state.count > this.state.limit) {
      return <Paginator
        {...this.state}
        changeOffset={this.changeOffset}
      />
    }
  }
}
