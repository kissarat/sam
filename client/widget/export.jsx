import React, {Component} from 'react'
import t from '../t'

function Workbook(sheets) {
  for (const name in sheets) {
    const sheet = sheets[name]
    if (sheet instanceof Array) {
      sheets[name] = sheet[0] instanceof Array
        ? XLSX.utils.aoa_to_sheet(sheet)
        : XLSX.utils.json_to_sheet(sheet)
    }
  }
  this.SheetNames = Object.keys(sheets)
  this.Sheets = sheets
  this.writeStream = XLSX.write(this, Workbook.DefaultOptions)
}

Workbook.DefaultOptions = {
  bookType: 'xlsx',
  bookSST: true,
  type: 'binary'
}

Workbook.prototype = {
  toBlob(name) {
    if (!name) {
      name = location.hostname + '_' + new Date().toLocaleDateString('ru') + '.xlsx'
    }
    const buffer = new ArrayBuffer(this.writeStream.length)
    const dataArray = new Uint8Array(buffer)
    for (let i = 0; i < this.writeStream.length; i++) {
      dataArray[i] = this.writeStream.charCodeAt(i) & 0xFF
    }
    return new Blob([dataArray], {type: "application/octet-stream"}, name)
  },

  toURL(name) {
    return URL.createObjectURL(this.toBlob(name))
  },

  save(name) {
    return open(this.toURL(name))
  }
}

export default class Export extends Component {
  state = {
    blobURL: null
  }

  onClick = () => this.setState({
    blobURL: new Workbook(this.props.getSheets()).toURL()
  })

  refDownload = anchor => anchor.click()

  button() {
    if (this.state.blobURL) {
      const name = (this.props.name || 'date')
        .replace('date', new Date().toLocaleDateString('ru').replace(/\./g, '-'))
      return <a href={this.state.blobURL} download={name + '.xlsx'} ref={this.refDownload}>{t('Download')}</a>
    }
    else {
      return <button type="button" onClick={this.onClick}>{t('Export to {format}', {format: 'Excel'})}</button>
    }
  }

  render() {
    return <div className="widget export">
      <div className="format xlsx">
        {this.button()}
      </div>
    </div>
  }
}
