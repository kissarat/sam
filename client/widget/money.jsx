import React from 'react'
import api from '../connect/api.jsx'

export default function Money({as = 'span', className = '', value, zero, filter, currency = 'USD'}) {
  className += currency.toLocaleLowerCase()
  value = +value
  if (value || zero) {
    const original = value / api.getRate(currency)
    if ('function' === typeof filter) {
      value = filter(value).toString()
    }
    else {
      value = original.toFixed(api.getScale(currency))
    }
    let [major, minor] = value.split('.')
    const parts = []
    if (+major && 0 !== major) {
      major = major
        .split('')
        .reverse()
        .map((a, i) => 0 === i % 3 && i > 0 ? a + ' ' : a)
        .reverse()
        .join('')
        .trim()
      parts.push(React.createElement('span', {key: 'major', className: 'major'}, major))
    }
    if (+minor && 0 !== minor) {
      parts.push(React.createElement('span', {key: 'minor', className: 'minor'}, minor))
    }
    return React.createElement(as, {key: 'money', className: className + ' money'}, parts)
  }
  else {
    return React.createElement(as)
  }
}
