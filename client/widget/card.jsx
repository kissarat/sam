import React from 'react'
import {Card, Image} from 'semantic-ui-react'

const PersonCard = p => (
  <Card>
    <Card.Content>
      {p.avatar ?
        <Image floated='right' size='mini' src='http://semantic-ui.com/images/avatar/large/steve.jpg'/> : ''}
      <Card.Header>
      {p.surname ? <h3>{p.forename} {p.surname}</h3> : ''}
        {p.nick}
      </Card.Header>
      {p.status ? <Card.Meta>{p.status}</Card.Meta> : ''}
      {p.skype ? <Card.Description><Icon name="skype"/>{p.skype}</Card.Description> : ''}
    </Card.Content>
  </Card>
)

export default PersonCard
