import React, {Component} from 'react'

export function FieldGroup(props) {
  return <div controlId={props.id}>
      <div>{props.label}</div>
      <div {...props} />
      {props.help && <div>{props.help}</div>}
    </div>
}
