import React, {Component} from 'react'
import {map, identity, each} from 'lodash'
import {announce} from '../../common/globals'
import {Message} from 'semantic-ui-react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'

export class AlertList extends Component {
  state = {}

  componentWillMount() {
    AlertList.one = this
  }

  componentWillUnmount() {
    AlertList.one = false
  }

  alert = (options, key) => {
    if (options) {
      const dismiss = () => this.setState({[key]: false})
      if (true === options.onDismiss || options.timeout) {
        options.onDismiss = dismiss
      }
      if (options.timeout) {
        setTimeout(dismiss, 'number' === typeof options.timeout ? options.timeout * 1000 : 5000)
        delete options.timeout
      }
      return <Message {...options}/>
    }
  }

  static clear() {
    const state = {}
    each(AlertList.one.state, function (value, key) {
      state[key] = false
    })
    AlertList.one.setState(state)
  }

  static show(options) {
    options.key = options.id || ((options.type || '') + Date.now())
    AlertList.one.setState({[options.key]: options})
  }

  render() {
    const alerts = map(this.state, this.alert).filter(identity)
    return <ReactCSSTransitionGroup
      transitionName="alert"
      transitionEnterTimeout={500}
      transitionLeaveTimeout={300}
      id="alert-list">{alerts}</ReactCSSTransitionGroup>
  }
}

export default AlertList

announce({AlertList})
