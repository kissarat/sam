import React, {Component} from 'react'
import {Menu, Icon} from 'semantic-ui-react'
import {range} from 'lodash'

export default class Paginator extends Component {
  componentWillReceiveProps(props) {
    this.receiveParams(props)
  }

  hotkeys = e => {
    if (e.altKey) {
      switch (e.key) {
        case 'ArrowLeft':
          return this.open(this.state.page - 1)
        case 'ArrowRight':
          return this.open(this.state.page + 1)
      }
    }
  }

  componentWillMount() {
    this.receiveParams(this.props)
    window.addEventListener('keyup', this.hotkeys)
  }

  componentWillUnmount() {
    window.removeEventListener('keyup', this.hotkeys)
  }

  receiveParams({offset, limit, count, size = 5}) {
    if (this.state && this.state.size) {
      size = this.state.size
      // if (this.props.chan)
    }
    const total = Math.ceil(count / limit)
    const page = Math.floor(offset / limit) + 1
    const left = Math.max(Math.min(page - Math.floor(size / 2), total - (size - 1)), 1)
    const right =  Math.min(Math.max(page + Math.floor(size / 2), size), total)
    const numbers = range(left, right + 1)
    this.setState({
      page,
      numbers,
      total
    })
  }

  open(n) {
    if (n > 0 && n <= this.state.total) {
      this.props.changeOffset((n - 1) * this.props.limit)
    }
  }

  items() {
    return this.state.numbers.map(n => <Menu.Item
      active={n === this.state.page}
      key={n}
      onClick={() => this.open(n)}
      as='a'>
      {n}
    </Menu.Item>)
  }

  render() {
    return <Menu pagination className="widget paginator">
      <Menu.Item as='a' icon>
        <Icon
          name='left chevron'
          onClick={() => this.open(1)}
          disabled={this.state.page < 1}/>
      </Menu.Item>
      {this.items()}
      <Menu.Item as='a' icon>
        <Icon
          name='right chevron'
          onClick={() => this.open(Math.max(this.state.total - 2, 1))}
          disabled={this.state.total < 5}/>
      </Menu.Item>
      <Menu.Item>{this.props.count}</Menu.Item>
      <Menu.Item>
        <input value={this.state.size || this.props.size} onChange={e => this.setState({size: e.target.value})}/>
      </Menu.Item>
    </Menu>
  }
}
