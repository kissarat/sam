import React, {Component} from 'react'
import api from '../connect/api.jsx'

export default class Form extends Component {
  onChange = (e, {name, value}) => {
    this.change(name || e.target.getAttribute('name'), value || e.target.value)
  }

  change(name, value) {
    this.setState({[name]: value})
  }

  onSubmit = e => {
    e.preventDefault()
    this.submit()
  }

  submit() {
    throw new Error('Unimplemented ' + this.constructor.name)
  }

  send(url, params) {
    const data = {}
    for(const key in this.state) {
      if ('_' !== key[0]) {
        data[key] = this.state[key]
      }
    }
    return params
      ? api.send(url, params, data)
      : api.send(url, data)
  }
}
