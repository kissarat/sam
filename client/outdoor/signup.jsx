import api from '../connect/api.jsx'
import React, {Component} from 'react'
import RecoveryButton from './recovery-button.jsx'
import t from '../t'
import {Form, Segment, Button, Icon} from 'semantic-ui-react'
import {Link, browserHistory} from 'react-router'
import {pick} from 'lodash'

export default class Signup extends Component {
  state = {}

  componentWillReceiveProps(props) {
    void this.setReferralFromProps(props)
  }

  componentWillMount() {
    void this.setReferralFromProps(this.props)
  }

  async setReferralFromProps(props) {
    if (props.params.nick && props.params.nick !== localStorage.getItem('ref')) {
      const params = {
        nick: props.params.nick,
        unique: 1
      }
      const visit = /visit=(\d+)/.exec(location.search)
      if (visit && visit[1] > 0) {
        params.id = visit[1]
      }
      const {user} = await api.get('visit/ref', params)
      if (user) {
        this.setState({ref: user.nick})
        localStorage.setItem('ref', user.nick)
      }
    }
    const ref = localStorage.getItem('ref')
    if (ref) {
      this.setState({ref})
    }
  }

  onSubmit = async(e, {formData}) => {
    e.preventDefault()
    this.setState({
      error: false,
      busy: true
    })
    if (this.state.ref) {
      formData.ref = this.state.ref
    }
    for (const name of ['nick', 'email', 'password']) {
      const value = formData[name]
      if (value) {
        formData[name] = value.trim()
      }
    }
    const {success, error} = await api.send('user/signup', formData)
    if (success) {
      browserHistory.push('/login')
    }
    else if (error) {
      const state = {busy: false}
      if ('INVALID_UNIQUE' === error.status) {
        state.error = t('A user with this login is already registered')
      }
      else if (error.invalid) {
        if ('nick' in error.invalid) {
          state.error = t('The login can contain letters of the Latin alphabet (a-z), numbers (0-9), underscore (_)')
        }
        else if ('email' in error.invalid) {
          state.error = t('Wrong email address format')
        }
        else if ('password' in error.invalid) {
          state.error = t('Password must contain at least 6 characters')
        }
        else {
          state.error = t('Unknown input data validation error')
        }
      }
      else if ('invalid_unique' === error.status) {
        if ('nick' === error.name) {
          state.error = t('A user with this login is already registered')
        }
        else if ('email' === error.name) {
          state.error = t('A user has already been registered at this email address')
        }
        else {
          state.error = t('This user already exists')
        }
      }
      else {
        state.error = t(error.message) || t('Unknown error')
      }
      this.setState(state)
    }
  }

  onChange = (e, {name, value}) => {
    this.setState({[name]: value})
  }

  error() {
    if (this.state.error) {
      return <div className="error">
        <Icon name="warning circle"/>
        {this.state.error}
      </div>
    }
  }

  sponsor() {
    if (this.state.ref) {
      return <div className="sponsor">
        <span className="label">
          <Icon name="user"/>
          {t('Your sponsor')}:
        </span>&nbsp;
        <strong className="value">{this.state.ref}</strong>
      </div>
    }
  }

  render() {
    return <Segment padded piled raised className="page login">
      <h1>{t('Signup')}</h1>
      {this.error() || this.sponsor()}
      <Form loading={this.state.busy} className="page signup" onSubmit={this.onSubmit}>
        <Form.Input
          name="nick"
          type="text"
          label={t('Username')}
          title="Допустимыми является латинская буква, число или символ _"
          value={this.state.nick || ''}
          onChange={this.onChange}
        />
        <Form.Input
          name="password"
          type="password"
          label={t('Password')}
          title="Введите больше 6-ти и меньше 128-ка символов"
          value={this.state.password || ''}
          onChange={this.onChange}
        />
        <Form.Input
          name="email"
          type="email"
          label="Email"
          value={this.state.email || ''}
          onChange={this.onChange}
        />
        <div className="control">
          <Button secondary>{t('Signup')}</Button>
          <Link to='/login'>{t('Login')}</Link>
          <RecoveryButton
            login={this.state.email || this.state.nick}
            error={error => this.setState({error})}
          />
        </div>
      </Form>
    </Segment>
  }
}
