import AlertList from '../widget/alert.jsx'
import api from '../connect/api.jsx'
import React, {Component} from 'react'
import t from '../t'
import {Icon} from 'semantic-ui-react'
import {isObject} from 'lodash'
import Language from '../widget/language.jsx'

export default class Outdoor extends Component {
  state = {
    user: null,
    bayer: false
  }

  async load() {
    const resex = /\/ref\/([\w_]+)/.exec(location.pathname)
    const nick = resex ? resex[1] : localStorage.getItem('ref')
    if ('undefined' === nick) {
      console.warn('Nick is "undefined"')
    }
    if (nick && 'undefined' !== nick) {
      const params = {nick}
      if ('goldenlife' === api.config.id) {
        params.buyer = 1
      }
      const state = await api.get('user/get', params)
      if (isObject(state.user) && state.user.id > 0) {
        this.setState(state)
      }
    }
  }

  componentWillMount() {
    if ('/login' === location.pathname) {
      this.setState({user: null})
    }
    else {
      void this.load()
    }
  }

  componentWillReceiveProps() {
    if ('/login' === location.pathname) {
      this.setState({user: null})
    }
    else if (!this.state.user) {
      void this.load()
    }
  }

  header() {
    if ('goldenlife' === api.config.id) {
      return <div className="header">
        <div className="logo">
          <img src="/images/goldenlife-logo.png"/>
        </div>
        <div className="best">
          {t('The best project for earning Bitcoin on the Internet in {year}', {
            year: new Date().getFullYear()
          })}
        </div>
      </div>
    }
  }

  social(p) {
    const urls = []
    for (const name of ['facebook', 'vkontakte', 'twitter', 'google', 'odnoklassniki']) {
      // console.log(name, p[name], p)
      //  &&/^https?\/\//.test(url)
      const url = p[name]
      const icon = 'vkontakte' === name ? 'vk' : name
      if (url) {
        urls.push(<a key={name} target="_blank" href={url} className={name}>
          <Icon name={icon} size="large"/>
        </a>)
      }
    }
    return urls
  }

  sponsor() {
    if (this.state.user && ('goldenlife' !== api.config.id || this.state.buyer)) {
      const p = this.state.user
      const hasName = p.forename || p.surname
      return <div className="card sponsor somebody">
        {this.header()}
        <article>
          <div className="short">
            <div className="avatar-container">
              <div className="avatar" style={p.avatar ? {backgroundImage: `url("${p.avatar}")`} : {}}></div>
            </div>
            <div className="name-container">
              <h2 className={hasName ? 'name something' : 'name nothing'}>
                {hasName ? p.forename + '\u00A0' + p.surname : t('Noname')}</h2>
              <div className="your-sponsor">{t('Your sponsor')}</div>
            </div>
          </div>
          <div
            className={p.about ? 'about something' : 'about nothing'}>{
            p.about ||
            t('goldenlife' === api.config.id
              ? 'Join my team and earn Bitcoin on the Internet. I will help you earn money, build a structure and become successful.'
              : 'Your sponsor left no description')
          }</div>
          <div className="contacts">
            {p.phone ? <div className="phone">
                <Icon name="phone" size="large"/>
                <span className="value">{p.phone}</span>
              </div> : ''}
            {p.email ? <div className="email">
                <Icon name="mail" size="large"/>
                <span className="value">{p.email}</span>
              </div> : ''}
            {p.skype ? <div className="skype">
                <Icon name="skype" size="large"/>
                <span className="value">{p.skype}</span>
              </div> : ''}
          </div>
        </article>
        <div className="footer">
          <div className="social">{this.social(p)}</div>
          <div className="signup-now">
            <span className="text">{t('Sign up now')}</span>
            <Icon name="arrow right"/>
          </div>
        </div>
      </div>
    }
    return <div className="card sponsor empty">{t('You do not use the reflink')}</div>
  }

  render() {
    return <div className="layout outdoor" data-path={location.pathname}>
      <Language/>
      <div className="container">
        <div className="left">
          {this.sponsor()}
        </div>
        <div className="right">
          <AlertList/>
          <main>{this.props.children}</main>
        </div>
      </div>
      <footer>
        <a className="payeer banner" href="https://payeer.com/?partner=3293636&utm_source=inbisoft.com" target="_blank">
          <img src="https://payeer.com/bitrix/templates/difiz/img/banner/ru/468x60.gif?utm_source=inbisoft.com" alt="Payeer"/>
        </a>
      </footer>
    </div>
  }
}
