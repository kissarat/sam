import React from 'react'
import FormComponent from '../base/form-component.jsx'
import {Segment, Form, Icon} from 'semantic-ui-react'
import {API} from '../connect/api.jsx'
import {browserHistory} from 'react-router'

export default class Recovery extends FormComponent {
  state = {}

  componentWillMount() {
    this.api = new API(this.props.params.code)
  }

  componentWillReceiveProps(props) {
    this.api = new API(props.params.code)
  }

  error() {
    if (this.state.error) {
      return <div className="error">
        <Icon name="warning circle"/>
        {this.state.error}
      </div>
    }
  }

  async submit() {
    const {success, error} = await this.api.send('user/recovery', this.state, false, 'PUT')
    if (success) {
      browserHistory.push('/login')
    }
    else if (error && 'string' === typeof error.message) {
      this.setState({error: error.message})
    }
    else {
      this.setState({error: 'Неизвестная ошибка'})
    }
  }

  render() {
    return <Segment className="page recovery">
      <h1>Восстановление пароля</h1>
      {this.error()}
      <Form onSubmit={this.onSubmit}>
        <Form.Input
          label="Новый пароль"
          name="password"
          type="password"
          value={this.state.password}
          onChange={this.onChange}
        />
        <Form.Button>Изменить</Form.Button>
      </Form>
    </Segment>
  }
}
