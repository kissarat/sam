import api from '../connect/api.jsx'
import FormComponent from '../base/form-component.jsx'
import React, {Component} from 'react'
import Recaptcha from 'react-recaptcha'
import RecoveryButton from './recovery-button.jsx'
import t from '../t'
import {Form, Button, Segment, Icon} from 'semantic-ui-react'
import {Link, browserHistory} from 'react-router'

export default class Login extends FormComponent {
  state = {}

  async submit() {
    this.setState({
      busy: true,
      error: false
    })
    const s = this.state
    for(const name of ['login', 'password']) {
      const value = s[name]
      if (value) {
        s[name] = value.trim()
      }
    }
    const data = {
      [s.login && s.login.indexOf('@') >= 0 ? 'email' : 'nick']: s.login,
      password: s.password
    }
    const {success, error} = await api.send('user/login', data)
    const newState = {
      busy: false
    }
    if (success) {
      api.config = await api.handshake()
      browserHistory.push('/cabinet')
    }
    else if (error) {
      if ('ABSENT' === error.status) {
        newState.error = t('Wrong login or password')
      }
      else {
        newState.error = t(error.message) || t('Unknown error')
      }
      this.setState(newState)
    }
  }

  error() {
    if (this.state.error) {
      return <div className="error">
        <Icon name="warning circle"/>
        {this.state.error}
      </div>
    }
  }

  recaptcha() {
    console.log.apply(this, arguments)
  }

  render() {
    return <Segment padded piled raised className="page login">
      <h1>{t('Login')}</h1>
      {this.error()}
      <Form onSubmit={this.onSubmit} loading={this.state.busy}>
        <Form.Input
          name="login"
          label={t('Username or Email')}
          title="Latin letter, number or symbol _ is allowed"
          value={this.state.login || ''}
          onChange={this.onChange}
        />
        <Form.Input
          name="password"
          type="password"
          label={t('Password')}
          title="Enter more than 6 or less 128 characters"
          value={this.state.password || ''}
          onChange={this.onChange}
        />
        <div className="control">
          <Button
            secondary
            type="submit">
            {t('Login')}
          </Button>
          <Link to='/signup'>{t('Signup')}</Link>
          <RecoveryButton
            login={this.state.login}
            success={m => void alert(m) || this.setState({error: false})}
            error={error => this.setState({error})}
          />
        </div>
      </Form>
    </Segment>
  }
}
