import React, {Component} from 'react'
import api from '../connect/api.jsx'
import {Button} from 'semantic-ui-react'

export default class RecoveryButton extends Component {
  state = {}

  send = async e => {
    e.preventDefault()
    this.setState({busy: true})
    const params = {[this.props.login.indexOf('@') > 0 ? 'email' : 'nick']: this.props.login}
    const {success, user, error} = await api.send('user/recovery', params)
    if (success) {
      (this.props.success instanceof Function ? this.props.success : alert)
      ('Письмо с ссылкой на восстановления пароля было отправлено вам на email')
    }
    else if (error && error.message) {
      this.props.error(error.message)
    }
    this.setState({busy: false})
  }

  render() {
    return <Button
      loading={this.state.busy}
      onClick={this.send}
      disabled={!this.props.login}
      className="recovery"
      secondary>Восстановить доступ</Button>
  }
}
