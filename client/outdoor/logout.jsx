import React, {Component} from 'react'
import Form from '../widget/form.jsx'
import {browserHistory} from 'react-router'
import api from '../connect/api.jsx'

export default class Logout extends Form {
  componentWillMount() {
    api.logout(true)
  }

  render() {
    return <div className="page logout">Осуществляется выход...</div>
  }
}
