import api from '../connect/api.jsx'
import React, {Component} from 'react'
import t from '../t'
import {Dimmer, Loader, Table, Button, Form, Segment} from 'semantic-ui-react'
import {Link} from 'react-router'

export default class Feedback extends Component {
  state = {
    articles: [],
    busy: true,
    create: false
  }

  componentDidMount() {
    void this.load()
  }

  async load() {
    this.setState({busy: true})
    const params = {
      type: 'ticket'
    }
    if (!api.config.user.admin) {
      params.user = api.config.user.id
    }
    const {articles} = await api.get('article/index', params)
    this.setState({
      busy: false,
      articles
    })
  }

  onSubmit = async(e, {formData}) => {
    e.preventDefault()
    formData.type = 'ticket'
    await api.send('article/save', formData)
    this.setState({create: false})
    void this.load()
  }

  create() {
    if (this.state.create) {
      return <Form onSubmit={this.onSubmit}>
        <Form.Input
          label={t('Subject')}
          type="text"
          name="name"/>
        <Form.TextArea
          name="text"/>
        <Button positive>{t('Create')}</Button>
        <Button color="red" type="button" onClick={() => this.setState({create: false})}>{t('Cancel')}</Button>
      </Form>
    }
    else {
      return <Button
        primary
        type="button"
        onClick={() => this.setState({create: true})}>
        {t('Create')}
      </Button>
    }
  }

  rows() {
    return this.state.articles.map(row => <Table.Row id={row.path} key={row.id}>
      <Table.Cell>{new Date(row.created).toLocaleString()}</Table.Cell>
      <Table.Cell><Link to={'/ticket/' + row.path}>{row.path}</Link></Table.Cell>
      <Table.Cell>{row.name}</Table.Cell>
      <Table.Cell>{row.nick}</Table.Cell>
      <Table.Cell>{row.email}</Table.Cell>
      <Table.Cell>{row.skype}</Table.Cell>
    </Table.Row>)
  }

  render() {
    return <Segment.Group className="page feedback">
      <Segment>
        <h1>{t("Feedback")}</h1>
        {this.create()}
      </Segment>
      <Segment>
        <Dimmer inverted active={this.state.busy}>
          <Loader/>
        </Dimmer>
        <Table>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>{t('Created')}</Table.HeaderCell>
              <Table.HeaderCell>{t('Ticket')}</Table.HeaderCell>
              <Table.HeaderCell>{t('Subject')}</Table.HeaderCell>
              <Table.HeaderCell>{t('Username')}</Table.HeaderCell>
              <Table.HeaderCell>Email</Table.HeaderCell>
              <Table.HeaderCell>Skype</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>{this.rows()}</Table.Body>
        </Table>
      </Segment>
    </Segment.Group>
  }
}
