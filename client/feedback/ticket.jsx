import React, {Component} from 'react'
import {Dimmer, Loader, Icon, Form, Segment} from 'semantic-ui-react'
import {Link} from 'react-router'
import api from '../connect/api.jsx'
import {pick} from 'lodash'
import t from '../t'

class TicketForm extends Component {
  onSubmit = async(e, {formData}) => {
    e.preventDefault()
    const data = {
      ...formData,
      ...pick(this.props, 'article', 'parent')
    }
    await api.send('message/save', data)
    this.props.load(data)
  }

  render() {
    return <Form onSubmit={this.onSubmit}>
      <Form.TextArea name="text"/>
      <Form.Button positive>{t('Write')}</Form.Button>
    </Form>
  }
}

export default class Ticket extends Component {
  state = {
    messages: [],
    busy: true,
    create: false
  }

  componentWillReceiveProps(props) {
    this.load(props.params)
  }

  componentDidMount() {
    this.load(this.props.params)
  }

  async load(params) {
    this.setState({busy: true})
    const state = await api.get('article/ticket', params)
    if (!state.article) {
      state.article = false
    }
    state.busy = false
    this.setState(state)
  }

  article() {
    if (false === this.state.article) {
      return <h1>{t('Ticket {path} not found', this.props.params)}</h1>
    }
    else if (this.state.article) {
      const article = this.state.article
      return <Segment>
        <h1>{article.name}</h1>
        <h2>
          {t('Ticket')}&nbsp;
          <input
            size="18"
            readOnly
            value={article.path}/>
          &nbsp;{t('Created').toLocaleLowerCase()}&nbsp;
          {new Date(article.created).toLocaleString()}
        </h2>
        <p>{article.text}</p>
      </Segment>
    }
  }

  messages() {
    if (!this.state.article) {
      return
    }
    return <Segment className="messages">
      <Link to="/feedback">
        <Icon name="arrow circle outline left"/>
        {t('Back to Tickets')}
      </Link>
      <ul>
        {this.state.messages.map(m => <li key={m.id}>
          <div className="about">
            {m.user.admin ? <Icon name="doctor" color="red" size="large"/> : ''}
            <strong>{m.user.forename + ' ' + m.user.surname}</strong>
            &nbsp;
            <i>{new Date(m.created).toLocaleString()}</i>
          </div>
          <p>{m.text}</p>
        </li>)}
      </ul>
      <TicketForm
        article={this.state.article.id}
        load={() => this.load(this.props.params)}/>
    </Segment>
  }

  render() {
    return <Segment.Group className="page ticket">
      <Dimmer inverted active={this.state.busy}>
        <Loader/>
      </Dimmer>
      {this.article()}
      {this.messages()}
    </Segment.Group>
  }
}