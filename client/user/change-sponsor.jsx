import api from '../connect/api.jsx'
import React, {Component} from 'react'
import {Form, Icon} from 'semantic-ui-react'

export default class ChangeSponsor extends Component {
  state = {value: ''}

  onChange = async(e, {value}) => {
    this.setState({value})
  }

  save = async() => {
    const {user} = await api.get('user/get', {nick: this.state.value})
    if (user) {
      const {success} = await api.send('user/save', {id: this.props.user.id}, {parent: user.id})
      if (success) {
        this.props.success(this.props.user, user)
      }
    }
  }

  saveButton() {
    if (this.state.value && (this.state.value != this.props.user.sponsor)) {
      return <Icon name="save" onClick={this.save}/>
    }
  }

  render() {
    return <div className="change-sponsor">
      <Form.Input
        value={this.state.value || this.props.user.sponsor}
        onChange={this.onChange}
      />
      {this.saveButton()}
    </div>
  }
}
