import api from '../connect/api.jsx'
import ChangeSponsor from './change-sponsor.jsx'
import Money from '../widget/money.jsx'
import React from 'react'
import TablePage, {Search} from '../widget/table-page.jsx'
import {AlertList} from '../widget/alert.jsx'
import {map, pick} from 'lodash'
import {currencies} from '../finance/enums.jsx'
import {Icon, Segment, Table} from 'semantic-ui-react'
import Settings from './settings.jsx'

export default class UserList extends TablePage {
  uri = 'user/index'

  componentWillMount() {
    this.setState({user: null, value: ''})
  }

  sponsor(user) {
    if (user.parent > 0) {
      return <ChangeSponsor
        user={user}
        success={this.load}/>
    }
  }

  getFilterParams() {
    return pick(this.state, 'search')
  }

  async login(user) {
    if (api.isEnabled('front')) {
      open(api.getFrontURL('authorize', {nick: user.nick, token: api.token}))
    }
    else {
      const {success} = await api.send('token/save', {id: api.token}, {user: user.id})
      if (success) {
        location.pathname = '/password'
      }
    }
  }

  async buy(user) {
    const {success, transfer} = await api.send('transfer/create', {
      type: 'payment',
      status: 'success',
      to: user.id,
      amount: 200
    })
    if (success) {
      const {success, node} = await api.send('knee/buy', {user: user.id})
      if (success) {
        AlertList.show({
          success: true,
          content: `Пользователю ${user.nick} был оплачен счет #${transfer.id} и открыта площадка #${node.id}`,
          onDismiss: true
        })
        this.load()
      }
    }
  }

  buyButton(user) {
    if (user.client > 0) {
      return <span>{user.client}</span>
    }
    else {
      return <Icon
        name="shop"
        size="large"
        title="Купить площадку"
        onClick={() => this.buy(user)}/>
    }
  }

  async activate(u) {
    this.setState({busy: true})
    await api.send('mail/activate', {
      id: u.id,
      email: u.email
    })
    this.setState({busy: false})
  }

  rows() {
    return map(this.state.items, u => {
      let name = u.nick
      if (u.forename) {
        name = <span>{u.nick}&nbsp;({u.forename}&nbsp;{u.surname})</span>
      }
      return <Table.Row key={u.id}>
        <Table.Cell className="id">
          {u.children > 0 ?
            <span>{u.id}&nbsp;<a className="graph" href={'/graph.html#referral/' + u.id} target="_blank">
            <Icon name="external"/>
          </a></span> : u.id}
        </Table.Cell>
        <Table.Cell className="nick">
          <Icon
            onClick={() => this.activate(u)}
            name={'new' === u.type ? 'mail outline' : 'mail'}
            title={'new' === u.type ? 'Зарегистрирован' : 'Подтвержден'}/>
          <span className="value">{name}</span>
        </Table.Cell>
        <Table.Cell className="email">{u.email} <strong>{u.skype}</strong></Table.Cell>
        {Object.keys(currencies).map(c => <Money key={c} as={Table.Cell} value={u[c.toLowerCase()]} currency={c}/>)}
        <Table.Cell className="sponsor">
          {this.sponsor(u)}
        </Table.Cell>
        <Table.Cell className="action">
          <Icon name="setting"
                onClick={() => this.setState({user: u})}/>
          <a href={api.getFrontURL('authorize', {nick: u.nick, token: api.token})}
             title="Зайти в кабинет"
             target="_blank">
            <Icon
              name="sign in"
              size="large"/>
          </a>
        </Table.Cell>
      </Table.Row>
    })
  }

  table() {
    return <div>
      <Search change={search => this.load({search})}/>
      <div style={{position: 'relative'}}>
        {this.dimmer()}
        {this.paginator()}
        <Table compact="very">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>ID</Table.HeaderCell>
              <Table.HeaderCell>Логин и имя</Table.HeaderCell>
              <Table.HeaderCell>Email и Skype</Table.HeaderCell>
              {Object.keys(currencies).map(c => <Table.HeaderCell key={c}>{c}</Table.HeaderCell>)}
              <Table.HeaderCell>Спонсор</Table.HeaderCell>
              <Table.HeaderCell/>
            </Table.Row>
          </Table.Header>
          <Table.Body>{this.rows()}</Table.Body>
        </Table>
      </div>
    </div>
  }

  settings() {
    return <Settings {...this.state.user}
                     back={() => this.setState({user: null})}/>
  }

  // <Link to="/generate">Генерировать тестовых пользователей</Link>

  render() {
    return <Segment className="page users">
      <h1>Управления пользовтелями</h1>
      {this.state.user ? this.settings() : this.table()}
    </Segment>
  }
}
