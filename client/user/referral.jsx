import api from '../connect/api.jsx'
import Export from '../widget/export.jsx'
import React, {Component} from 'react'
import Refresh from '../widget/refresh.jsx'
import t from '../t'
import {Link} from 'react-router'
import {range} from 'lodash'
import {Segment, Dimmer, Loader, Icon} from 'semantic-ui-react'

export default class Referral extends Component {
  state = {
    referrals: []
  }

  componentDidMount() {
    this.load(this.props.params.level)
  }

  componentWillReceiveProps(props) {
    if (this.props.params.level !== props.params.level) {
      this.load(+props.params.level)
    }
  }

  async load(level = this.props.params.level) {
    this.setState({referrals: [], busy: true})
    const params = {view: 'referral'}
    if (+level) {
      params.level = level
    }
    const {referrals} = await api.get('user/structure', params)
    if (referrals instanceof Array) {
      this.setState({referrals, busy: false})
    }
  }

  async createTestLicense(id) {
    this.setState({busy: true})
    const {user} = await api.send('user/sync', {id},
      {mlbot: new Date(Date.now() + 24 * 3600 * 1000).toISOString()})
    await
      this.load()
  }

  trial(p) {
    if ('inbisoft' === api.config.id && (api.config.user.admin || api.config.user.leader)) {
      return p.mlbot
        ? <span className="time">{new Date(p.mlbot).toLocaleString()}</span>
        : <Icon onClick={() => this.createTestLicense(p.id)} name="lab" title="Пробный период"/>
    }
  }

  getSheets = () => ({
    user: this.state.referrals.map(u => {
      const mlbot = new Date(u.mlbot)
      return {
        ID: u.id,
        [t('Username')]: u.nick,
        Skype: u.skype,
        Email: u.email,
        [t('Level')]: u.level,
        [t('Surname')]: u.surname,
        [t('Forename')]: u.forename,
        MLBot: mlbot.getTime() > Date.now() ? mlbot.toLocaleString() : null
      }
    })
  })

  referrals() {
    return this.state.referrals.map(p => <div
      key={p.id}
      className={p.mlbot && new Date(p.mlbot).getTime() - 7 * 24 * 3600 * 1000 > Date.now() ? 'person card mlbot' : 'person card'}>
      <div className="avatar" style={p.avatar ? {backgroundImage: `url("${p.avatar}")`} : {}}/>
      <div className="info">
        <b>{p.nick} {this.trial(p)}</b>
        {p.surname ? <div className="name">{p.forename} {p.surname}</div> : ''}
        {p.skype ? <i><Icon name="skype"/>{p.skype}</i> : ''}
        <div>
          <a className="email" href={'email:' + p.email}>{p.email}</a>
        </div>
      </div>
    </div>)
  }

  levels() {
    return ('inbisoft' === api.config.id ? range(1, 8) : range(1, 6)).map(number => <Link
      key={number}
      to={'/referrals/' + number}>
      {t('Level {number}', {number})}
    </Link>)
  }

  render() {
    return <Segment className="page referrals">
      <Refresh refresh={() => this.load()}/>
      <Dimmer inverted active={this.state.busy}>
        <Loader/>
      </Dimmer>
      <h1>{this.props.params.level > 0 ? t('Referrals of level {number}', {number: this.props.params.level}) : t('Referrals')}</h1>
      <Export name={'referrals-' + (this.props.params.level || 'all') + '-date'} getSheets={this.getSheets}/>
      <a className="graph"
         href={'/graph.html#referral/' + api.config.user.id}
         target="_blank">
        <Icon name="external"/>
        {t('Tree of referrals')}
      </a>
      <div className="levels">
        <Link to="/referrals">{t('All')}</Link>
        {this.levels()}
      </div>
      <div className="list">
        {this.referrals()}
      </div>
    </Segment>
  }
}
