import React from 'react'
import {Button, Input} from 'semantic-ui-react'
import FormComponent from '../base/form-component.jsx'
import {pick} from 'lodash'

const num = '0123456789'
const alnum = 'abcdefghijklmnopqrstuvwxyz' + num

function generate(number, chars) {
  const a = new Uint8Array(number)
  crypto.getRandomValues(a)
  const string = []
  for (const n of a) {
    string.push(chars[n % chars.length])
  }
  return string.join('')
}

export default class Settings extends FormComponent {
  state = {
    supported: false,
    password: '',
    pin: ''
  }

  componentWillMount() {
    this.setState({supported: 'object' === typeof crypto})
  }

  async componentDidMount() {
    const {user} = await api.get('user/get', {id: this.props.id})
    if (user) {
      this.setState(pick(user, 'email', 'perfect', 'payeer', 'advcash'))
    }
  }

  async save(name) {
    const {success, error} = await api.send(['password', 'pin'].indexOf(name) >= 0 ? 'user/password' : 'user/save',
      {id: this.props.id}, {[name]: this.state[name]})
    if (success) {
      alert('Сохранено')
    }
    else if (error && error.message) {
      alert(error.message)
    }
  }

  async ban() {
    const {success} = await api.send('user/ban', {id: this.props.id})
    if (success) {
      alert(`Пользователь ${this.props.nick} заблокирован. Чтобы он мог войти в кабинет измените ему пароль`)
    }
  }

  field(name, label) {
    if (this.state[name]) {
      return <div className="field">
        <label>{label}</label>
        <Input placeholder={label} name={name} value={this.state[name]}
               onChange={this.onChange}/>
        <Button onClick={() => this.save(name)}>Изменить</Button>
      </div>
    }
    return ''
  }

  name() {
    const n = ((this.props.forename || '') + ' ' + (this.props.surname || '')).trim()
    return this.props.nick + (n ? ', ' + n : '')
  }

  render() {
    return <div className="page user-settings">
      <div className="about">
        <span>Пользователь:</span>&nbsp;
        <strong>{this.name()}</strong>
      </div>
      <div className="field">
        <label>Пароль</label>
        <Input placeholder="Пароль" name="password" value={this.state.password} onChange={this.onChange}/>
        <Button disabled={!this.state.supported}
                onClick={() => this.setState({password: generate(32, alnum)})}>Сгенерировать</Button>
        <Button disabled={!this.state.password}
                onClick={() => this.save('password')}>Изменить</Button>
      </div>
      <div className="field">
        <label>Транзакционный пароль</label>
        <Input placeholder="Транзакционный пароль" name="pin" value={this.state.pin} onChange={this.onChange}/>
        <Button disabled={!this.state.supported}
                onClick={() => this.setState({pin: generate(4, num)})}>Сгенерировать</Button>
        <Button disabled={!this.state.pin}
                onClick={() => this.save('pin')}>Изменить</Button>
      </div>
      {this.field('email', 'Email')}
      {this.field('perfect', 'Perfect Money')}
      {this.field('payeer', 'Payeer')}
      {this.field('advcash', 'AdvCash')}
      <Button onClick={this.props.back}>Назад</Button>
      <Button className="ban" onClick={() => this.ban()} negative={true}>Заблокировать</Button>
    </div>
  }
}
