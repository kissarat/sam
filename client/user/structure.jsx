import api from '../connect/api.jsx'
import React, {Component} from 'react'
import t from '../t'
import {Dimmer, Loader, Icon} from 'semantic-ui-react'

export class Children extends Component {
  state = {
    busy: true,
    users: []
  }

  componentWillMount() {
    void this.load(this.props)
  }

  componentWillReceiveProps(props) {
    void this.load(props)
  }

  async load(params) {
    if (!+params.parent) {
      params = {parent: api.config.user.id}
    }
    const {users} = await api.get('user/children', params)
    if (users) {
      this.setState({
        busy: false,
        users
      })
    }
  }

  openFn({id, count}) {
    return () => {
      if (count > 0) {
        const user = this.state.users.find(u => id === u.id)
        user.isOpen = !user.isOpen
        this.setState({users: this.state.users})
      }
    }
  }

  rows() {
    return this.state.users.map(p => {
      let name = p.nick
      if (p.surname) {
        name += ` (${p.surname} ${p.forename})`
      }
      const openButton = p.isOpen
        ? <Icon name="minus"/>
        : (p.count > 0 ? <Icon name="plus"/> : '')
      return <div key={p.id} className={'tree-node' + (p.count <= 0 ? ' childless' : '')}>
        <div className="info" onClick={this.openFn(p)}>
          <div className="open-icon">{openButton}</div>
          <div className="name">{name}</div>
          <div className="created">{new Date(p.created).toLocaleDateString()}</div>
          {p.count > 0 ? <div className="count">{p.count}</div> : ''}
        </div>
        {p.isOpen ? <Children parent={p.id}/> : ''}
      </div>
    })
  }

  render() {
    return <div className="widget user-children">
      <Dimmer inverted active={this.state.busy}>
        <Loader/>
      </Dimmer>
      {this.rows()}
    </div>
  }
}

export default function Structure() {
  return <div className="page structure">
    <h1>{t('Structure')}</h1>
    <Children/>
  </div>
}
