import api from '../connect/api.jsx'
import Money from '../widget/money.jsx'
import React, {Component} from 'react'
import t from '../t'
import {isObject} from 'lodash'
import {Segment, Statistic, Table} from 'semantic-ui-react'
import {systems, currencies} from '../finance/enums.jsx'

const names = {
  payment: 'Пополнения',
  pending: 'Ожидания подтверждения',
  withdraw: 'Вывод',
  accrue: 'Начисления',
  buy: 'Куплено на сумму',
  balance: 'На балансах',
  users: 'Зарегистрировано',
  users_day: 'Зарегистрировано за сутки',
  visitors: 'Посетителей',
  visitors_day: 'Посетителей за сутки',
  new_visitors_day: 'Новых посетителей за сутки',
  activities_day: 'Посещений за сутки',
  referral_day: 'Переходов по рефcсылке за сутки',
}

const money = ['payment', 'pending', 'withdraw', 'accrue', 'balance', 'buy']

export default class Statistics extends Component {
  state = {
    statistics: {},
    referrals: [],
    transfer_day: [],
    transfer_week: [],
    transfer_statistics: [],
    balances: {}
  }

  componentDidMount() {
    this.load()
    // this.interval = setInterval(this.load, 30000)
  }

  componentWillUnmount() {
    // clearInterval(this.interval)
  }

  load = async () => {
    const r = await api.get('home/statistics', {referrals: 1})
    if (isObject(r.statistics)) {
      this.setState(r)
    }
    this.loadItems('transfer_day')
    this.loadItems('transfer_week')
    this.loadItems('transfer_statistics')
    const {result} = await api.get('admin/balance')
    if (isObject(result)) {
      this.setState({balances: result})
    }
    // window._state = this.state
  }

  async loadItems(view) {
    const {result} = await api.get('admin/statistics', {view})
    if (result instanceof Array) {
      const items = []
      for (const currency in currencies) {
        const item = {currency}
        for (const type of ['payment', 'withdraw', 'accrue', 'bonus']) {
          const t = result
              .find(t => type === t.type && currency === t.currency)
          item[type] = t ? t.amount / currencies[t.currency] : 0
          // if (t) {
          //   console.log(type, t.currency, t.amount, currencies[t.currency], t)
          // }
        }
        items.push(item)
      }
      this.setState({[view]: items})
    }
  }

  statistics() {
    const properties = []
    for (const key in this.state.statistics) {
      const value = this.state.statistics[key]
      if (names[key]) {
        properties.push(<Statistic key={key} className={!value ? 'zero' : ''}>
          <Statistic.Value>{money.indexOf(key) >= 0
              ? <Money currency="USD" value={value} zero={0}
                       filter={v => Math.ceil(v / 100)}/> : value || 0}</Statistic.Value>
          <Statistic.Label>{names[key]}</Statistic.Label>
        </Statistic>)
      }
    }
    return properties
  }

  referrals() {
    return this.state.referrals.map(function ({nick, count}) {
      return <tr key={nick}>
        <td>{nick}</td>
        <td>{count}</td>
      </tr>
    })
  }

  items(view) {
    return this.state[view].map(t => <Table.Row key={t.system}>
      <Table.HeaderCell>{t.currency}</Table.HeaderCell>
      <Table.Cell>{t.payment}</Table.Cell>
      <Table.Cell>{t.withdraw}</Table.Cell>
      <Table.Cell>{t.accrue}</Table.Cell>
      <Table.Cell>{t.bonus}</Table.Cell>
    </Table.Row>)
  }

  balances() {
    const bs = []
    for(const currency in currencies) {
      bs.push(<div key={currency}>
        <Money value={this.state.balances[currency]} currency={currency} key={currency}/>
      </div>)
    }
    return bs
  }

  render() {
    const time = new Date()
    return <Segment.Group horizontal className="page page-statistics">
      <Segment as="dl">
        <dt>{t('Global')}</dt>
        <dd>
          <button onClick={() => api.send('/log/reset')}>Reset Login Attempts</button>
        </dd>
        <dt>{t('Statistics')}</dt>
        <dd>{this.statistics()}</dd>
        <dt>{t('Time')}</dt>
        <dd className="block-time">
          <div className="server">
            <span className="label">{t('Server')}</span>
            <span
                className="value">{this.state.time
                ? new Date(this.state.time).toLocaleString('ru') : t('Unknown')}</span>
          </div>
          <div className="local">
            <span className="label">{t('Local')}</span>
            <span className="value">{time.toLocaleString('ru')}</span>
          </div>
          <div className="utc">
            <label>{t('UTC time')}</label>
            <input defaultValue={time.toISOString()}/>
          </div>
        </dd>
        <dt>Балансы</dt>
        <dd>{this.balances()}</dd>
        <dt>За день</dt>
        <dd>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.Cell/>
                <Table.HeaderCell>Платежи</Table.HeaderCell>
                <Table.HeaderCell>Выводы</Table.HeaderCell>
                <Table.HeaderCell>Начисления</Table.HeaderCell>
                <Table.HeaderCell>Партнерские</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{this.items('transfer_day')}</Table.Body>
          </Table>
        </dd>
        <dt>За неделю</dt>
        <dd>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.Cell/>
                <Table.HeaderCell>Платежи</Table.HeaderCell>
                <Table.HeaderCell>Выводы</Table.HeaderCell>
                <Table.HeaderCell>Начисления</Table.HeaderCell>
                <Table.HeaderCell>Партнерские</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{this.items('transfer_week')}</Table.Body>
          </Table>
        </dd>
        <dt>За все время</dt>
        <dd>
          <Table>
            <Table.Header>
              <Table.Row>
                <Table.Cell/>
                <Table.HeaderCell>Платежи</Table.HeaderCell>
                <Table.HeaderCell>Выводы</Table.HeaderCell>
                <Table.HeaderCell>Начисления</Table.HeaderCell>
                <Table.HeaderCell>Партнерские</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>{this.items('transfer_statistics')}</Table.Body>
          </Table>
        </dd>
      </Segment>
      <Segment className="referrals">
        <h2>Тор переходов по рефссылках</h2>
        <table>
          <tbody>{this.referrals()}</tbody>
        </table>
      </Segment>
    </Segment.Group>
  }
}
