/**
 * Last modified: 18.06.06 05:26:15
 * Hash: 153486459006e7451e68cfada574f06c365765c8
 */

import AlertList from '../widget/alert.jsx'
import React, {Component} from 'react'
import {items} from '../widget/menu.jsx';
import {Menu, Segment} from 'semantic-ui-react'

const menu = [
  ['Статистика', '/statistics'],
  ['Выплаты', '/approve'],
  ['Пользователи', '/users'],
  ['Финансы', '/transfers'],
  ['Журнал', '/journal'],
  ['Ограничения по IP', '/restrict'],
]

export default class Admin extends Component {
  componentWillMount() {
    document.getElementById('common-style').href = '/styles/admin.css'
    this.menu = menu.slice()
  }

  render() {
    return <div className="layout admin-panel">
      <Menu attached='top' pointing>{items(this.menu)}</Menu>
      <Segment attached='bottom'>
        <AlertList/>
        <div className="content">
          {this.props.children}
        </div>
      </Segment>
    </div>
  }
}
