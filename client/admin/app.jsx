/**
 * Last modified: 18.10.14 03:19:23
 * Hash: 181b062fa89b40ca6a0d8d8e94ba5c8e1ec37d88
 */

import AlertList from '../widget/alert.jsx'
import React, {Component} from 'react'
import {items} from '../widget/menu.jsx';
import {Menu, Segment} from 'semantic-ui-react'

const menu = [
  ['Статистика', '/statistics'],
  ['Пользователи', '/users'],
  ['Финансы', '/transfers'],
  ['Журнал', '/journal'],
  ['Ограничения по IP', '/restrict'],
]

export default class Admin extends Component {
  componentWillMount() {
    document.getElementById('common-style').href = '/styles/admin.css'
    this.menu = menu.slice()
  }

  render() {
    return <div className="layout admin-panel">
      <Menu attached='top' pointing>{items(this.menu)}</Menu>
      <Segment attached='bottom'>
        <AlertList/>
        <div className="content">
          {this.props.children}
        </div>
      </Segment>
    </div>
  }
}
