import api from '../connect/api.jsx'
import FormComponent from '../base/form-component.jsx'
import React, {Component} from 'react'
import {Segment, Table, Icon, Form} from 'semantic-ui-react'
import {pick} from 'lodash'

export default class Restrict extends FormComponent {
  state = {
    text: '',
    rows: []
  }

  componentWillMount() {
    void this.load()
  }

  async load() {
    this.setState({
      id: '',
    })
    const {restricts} = await api.get('restrict/index', {user: api.config.user.id})
    if (restricts instanceof Array) {
      this.setState({rows: restricts})
    }
  }

  save = async() => {
    if (!this.state.text) {
      delete this.state.text
    }
    const {success} = await api.send('restrict/save', pick(this.state, 'id', 'text'))
    if (success) {
      await this.load()
    }
  }

  async remove(id) {
    const {success} = await api.del('restrict/remove', {id})
    if (success) {
      await this.load()
    }
  }

  rows() {
    return this.state.rows.map(row => <Table.Row key={row.id}>
      <Table.Cell>{row.id}</Table.Cell>
      <Table.Cell>{row.text}</Table.Cell>
      <Table.Cell><Icon name="trash" onClick={() => this.remove(row.id)}/></Table.Cell>
      <Table.Cell>{new Date(row.created).toLocaleString()}</Table.Cell>
    </Table.Row>)
  }

  render() {
    return <Segment className="restrict">
      <h1>Ограничения по IP</h1>
      <Table>
        <Table.Body>
          <Table.Row>
            <Table.Cell>
              <Form.Input
                name="id"
                placeholder="IP"
                value={this.state.id}
                onChange={this.onChange}/>
            </Table.Cell>
            <Table.Cell>
              <Form.Input
                name="text"
                placeholder="Кто"
                value={this.state.text}
                onChange={this.onChange}/>
            </Table.Cell>
            <Table.Cell>
              <Icon name="plus" onClick={this.save}/>
            </Table.Cell>
            <Table.Cell/>
          </Table.Row>
          {this.rows()}
        </Table.Body>
      </Table>
    </Segment>
  }
}
