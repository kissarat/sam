import AlertList from '../widget/alert.jsx'
import api from '../connect/api.jsx'
import Money from '../widget/money.jsx'
import React, {Component} from 'react'
import {systems} from '../finance/enums.jsx'
import {Button, Checkbox, Icon, Segment, Table} from 'semantic-ui-react'
import t from '../t'

export default class Approve extends Component {
  state = {
    rows: [],
    busy: false
  }

  componentWillMount() {
    void this.load()
  }

  async load() {
    const {result} = await api.get('transfer/index', {view: 'withdrawable'})
    if (result instanceof Array) {
      this.setState({rows: result})
    }
  }

  checker = id => e => {
    const row = this.state.rows.find(t => id === t.id)
    row.selected = !row.selected && !!row.system
    this.setState({rows: this.state.rows})
  }

  checkAll(selected) {
    for (const row of this.state.rows) {
      row.selected = selected && !!row.system
    }
    this.setState({rows: this.state.rows})
  }

  getEnabledWithdraws(system, currency) {
    let rows = this.state.rows.filter(t => t.selected && t.system === system)
    if (currency) {
      rows = rows.filter(t => t.currency === currency)
    }
    return rows
  }

  submit = async () => {
    if (this.state.busy) {
      return
    }
    const now = Date.now()
    this.setState({busy: true})
    system: for (const system of ["perfect", "payeer", "advcash"]) {
      for (const withdraw of this.getEnabledWithdraws(system)) {
        if (api.hasPaymentSystem(system)) {
          const params = {id: withdraw.id}
          const {success, error} = 'success' === withdraw.will
            ? await api.send(system + '/send', params)
            : await api.send('transfer/save', params, {
              status: 'canceled',
              text: t('Insufficient funds')
            })
          if (success) {
            this.setState({rows: this.state.rows.filter(t => t.id !== withdraw.id)})
          }
          else {
            withdraw.text = error.message
            AlertList.show({
              id: now,
              negative: false,
              content: `ID ${withdraw.id}: ` + withdraw.text,
              onDismiss: true
            })
            this.setState({rows: this.state.rows})
          }
        }
        else {
          AlertList.show({
            id: system,
            negative: false,
            content: t('{name} is disabled', {name: systems[system]}),
            onDismiss: true
          })
          continue system
        }
      }
    }
    if (api.hasPaymentSystem('blockio')) {
      for (const currency of ['BTC', 'LTC', 'DOGE']) {
        const ids = this.getEnabledWithdraws('blockio', currency).map(t => t.id)
        if (ids.length > 0) {
          const {success, transfers, transaction, error} = await api.send('blockio/approve', {ids, currency})
          console.log(transaction, transfers)
          if (success) {
            const ids = transfers.filter(t => t.id)
            this.setState({rows: this.state.rows.filter(t => ids.indexOf(t.id) < 0)})
            AlertList.show({
              positive: true,
              content: `${currency} transaction ID: ` + transaction.id,
              onDismiss: true
            })
          }
          else {
            AlertList.show({
              error: true,
              content: error.message,
              onDismiss: true
            })
          }
        }
      }
    }
    this.setState({busy: false})
  }

  async cancel(id) {
    const {success} = await api.send('transfer/save', {id}, {
      status: 'canceled',
      // text: this.state.text || null
    })
    if (success) {
      this.setState({rows: this.state.rows.filter(t => id !== t.id)})
    }
  }

  rows() {
    return this.state.rows.map(row => <Table.Row key={row.id} negative={'canceled' === row.will}>
      <Table.Cell><Checkbox checked={row.selected} onChange={this.checker(row.id)}/></Table.Cell>
      <Table.Cell>{row.id}</Table.Cell>
      <Table.Cell>{row.from_nick}</Table.Cell>
      <Table.Cell>{'USD' === row.currency ? systems[row.system] || 'Неизвестно' : row.currency}</Table.Cell>
      <Money as={Table.Cell} value={row.amount} currency={row.currency}/>
      <Table.Cell data-name="wallet">{row.wallet}</Table.Cell>
      <Table.Cell data-name="time">{new Date(row.created).toLocaleString()}</Table.Cell>
      <Table.Cell data-name="text">{t(row.text) || ('Balance: ' + (row.remain / api.getRate(row.currency)))}</Table.Cell>
      <Table.Cell data-name="ip">{row.ip}</Table.Cell>
      <Table.Cell data-name="action">
        <Icon name="cancel" onClick={() => this.cancel(row.id)}/>
      </Table.Cell>
    </Table.Row>)
  }

  render() {
    return <Segment className="page approve">
      <h1>Подтверждения платежей</h1>
      <Button type="button" content="Все" onClick={() => this.checkAll(true)}/>
      <Button type="button" content="Ничего" onClick={() => this.checkAll(false)}/>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell/>
            <Table.HeaderCell>ID</Table.HeaderCell>
            <Table.HeaderCell>Кому</Table.HeaderCell>
            <Table.HeaderCell>Платежка</Table.HeaderCell>
            <Table.HeaderCell>Сумма</Table.HeaderCell>
            <Table.HeaderCell>Кошелек</Table.HeaderCell>
            <Table.HeaderCell>Создано</Table.HeaderCell>
            <Table.HeaderCell>Замечания</Table.HeaderCell>
            <Table.HeaderCell>IP</Table.HeaderCell>
            <Table.HeaderCell/>
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.rows()}</Table.Body>
      </Table>
      <Button
        type="button"
        positive content="Оплатить"
        onClick={this.submit}
        loading={this.state.busy}
        disabled={this.state.busy || !this.state.rows.some(t => t.selected)}/>
    </Segment>
  }
}
