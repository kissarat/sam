import Approve from './approve.jsx'
import ArticleList from '../article/index.jsx'
import Ethereum from './ethereum.jsx'
import Log from '../log/index.jsx'
import React from 'react'
import RequestList from './request/index.jsx'
import Restrict from './restrict.jsx'
import Statistics from './statistics.jsx'
import TransferEdit from '../finance/edit.jsx'
import TransferList from '../finance/index.jsx'
import UserList from '../user/index.jsx'
import View from '../article/view.jsx'
import {ArticleEdit} from '../article/edit.jsx'
import {Route} from 'react-router'

const adminRoute = <Route>
  <Route path="journal" component={Log}/>
  <Route path="users" component={UserList}/>
  <Route path="statistics" component={Statistics}/>
  <Route path="articles" component={ArticleList}/>
  <Route path="restrict" component={Restrict}/>
  <Route path="approve" component={Approve}/>
  <Route path="create" component={ArticleEdit}/>
  <Route path="transfers" component={TransferList}/>
  <Route path="ethereum/:currency/:id" component={Ethereum}/>
  <Route path="ethereum/:currency" component={Ethereum}/>
  <Route path="edit/:id" component={ArticleEdit}/>
  <Route path="requests" component={RequestList}/>
  <Route path="transfer/create" component={TransferEdit}/>
  <Route path="transfer/edit/:id" component={TransferEdit}/>
  <Route path="view/:id" component={View}/>
  <Route path="*" component={View}/>
</Route>

export default adminRoute
