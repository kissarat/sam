import React from 'react'
import TablePage from '../widget/table-page.jsx'
import {Segment, Table, Button} from 'semantic-ui-react'
import api from '../connect/api.jsx'
import {pick} from 'lodash'

export default class Ethereum extends TablePage {
  uri = 'ethereum/load'

  componentWillMount() {
    this.setState({transactions: [], balance: -1, fee: -1})
  }

  async componentDidMount() {
    void this.load(this.props.params)
  }

  async componentWillReceiveProps(props) {
    if (this.state.currency !== props.params.currency) {
      void this.load(props.params)
    }
  }

  getCurrency(props = this.props) {
    return props.params.currency
  }

  showBalance() {
    return this.state.balance > 0
  }

  loadDefaults(params) {
    return this.load(Object.assign({
      balance: this.state.balance,
      currency: this.state.currency
    }, params))
  }

  async loadItems(params) {
    try {
      const state = await TablePage.prototype.loadItems.call(this, params)
      state.currency = params.currency
      if (params.balance >= 0) {
        state.balance = params.balance
      }
      if (state.items instanceof Array) {
        state.items.sort((a, b) => a.amount - b.amount)
        state.sum = state.items.reduce((a, b) => a + +b.amount, 0)
        if (!(this.state.fee >= 0)) {
          const {result} = await api.get('ethereum/fee', {currency: params.currency})
          state.fee = result * Math.pow(10, -18 + api.getScale(params.currency))
        }
      }
      return state
    }
    catch (ex) {
      alert(ex.toString())
      this.setState({busy: false})
    }
    return {}
  }

  async loadTransaction({id}) {
    if (id > 0) {
      const {transfer} = await api.get('transfer/get', {id})
      if (transfer) {
        this.setState({
          target: transfer.wallet,
          amount: transfer.amount / api.getRate(this.getCurrency())
        })
      }
    }
  }

  rows() {
    return this.state.items.map(w => <Table.Row
      key={w.id}
      active={w === this.state.current}
      onClick={() => this.setState({current: w})}>
      <Table.Cell className="eth-address">
        <a target="_blank" href={'https://etherscan.io/address/' + w.id}>{w.id}</a>
      </Table.Cell>
      <Table.Cell>{w.nick}</Table.Cell>
      {this.showBalance() ?
        <Table.Cell className="percent">{(100 * w.amount / this.state.sum).toFixed(1)}</Table.Cell> : null}
      {this.showBalance() ? <Table.Cell className={this.getCurrency().toLowerCase()}>
        {w.amount / api.getRate(this.getCurrency())}
      </Table.Cell> : null}
      {this.showBalance() ? <Table.Cell
        className="usd">{(w.amount * api.getRate(this.getCurrency(), 'USD') / api.getRate(this.getCurrency())).toFixed(2)}</Table.Cell> : null}
    </Table.Row>)
  }

  send = async () => {
    const unloadParams = {
      id: this.state.current.id,
      currency: this.getCurrency()
    }
    if ('0' !== localStorage.ethereum_password) {
      unloadParams.password = 'default'
    }
    let r = await api.send('ethereum/unlock', unloadParams)
    if (r.success) {
      r = await api.send('ethereum/send', {
        from: this.state.current.id,
        to: this.state.target,
        currency: this.getCurrency(),
        amount: Math.round(this.state.amount * api.getRate(this.getCurrency()))
      })
      this.setState({transactions: this.state.transactions.concat([r.result])})
      if (r.success) {
        if (this.props.params && this.props.params.id > 0) {
          await api.send('transfer/save', {id: this.props.params.id}, {status: 'success'})
        }
        this.setState({current: null})
      }
    }
  }

  form() {
    return <form className={this.state.current ? 'active' : ''}>
      <input placeholder="Wallet"
             className="receiver-wallet"
             value={this.state.target || ''}
             onChange={e => this.setState({target: e.target.value})}
             size="46"/>
      <input placeholder="Amount"
             value={this.state.amount || ''}
             onChange={e => this.setState({amount: e.target.value})}/>
      <button type="button" onClick={this.send}>Send</button>
    </form>
  }

  transactions() {
    return this.state.transactions.map(t => <div key={t}>
      <a target="blank" href={'https://etherscan.io/tx/' + t}>{t}</a>
    </div>)
  }

  render() {
    const dollar = api.getRate(this.getCurrency()) / api.getRate(this.getCurrency(), 'USD')
    return <Segment className="page ethereum">
      <h1>Ethereum (<span className="usd">{api.getRate(this.getCurrency(), 'USD').toFixed(2)}</span>)</h1>
      {this.dimmer()}
      {this.paginator()}
      {this.form()}
      <Button onClick={() => this.loadDefaults({balance: this.state.fee + 1})}>Загрузить балансы</Button>
      {this.transactions()}
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>
              Fee:&nbsp;
              <span className={this.getCurrency().toLowerCase()}>
                {((this.state.fee || 0) / api.getRate(this.getCurrency())).toFixed(api.getScale(this.getCurrency()))}
                </span>&nbsp;
              (<span className="usd">{(this.state.fee || 0) / dollar}</span>)
            </Table.HeaderCell>
            <Table.HeaderCell>Пользователь</Table.HeaderCell>
            {this.showBalance() ? <Table.HeaderCell>Доля</Table.HeaderCell> : null}
            {this.showBalance() ? <Table.HeaderCell className={this.getCurrency().toLowerCase()}>
              {(this.state.sum || 0) / api.getRate(this.getCurrency())}
            </Table.HeaderCell> : null}
            {this.showBalance() ?
              <Table.HeaderCell className="usd">{(this.state.sum || 0) / dollar}</Table.HeaderCell> : null}
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.rows()}</Table.Body>
      </Table>
    </Segment>
  }
}
