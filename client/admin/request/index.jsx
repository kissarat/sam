import React, {Component} from 'react'
import api from '../../connect/api.jsx'
import {map, isObject} from 'lodash'
import {Segment, Form, Icon, Table} from 'semantic-ui-react'
import TablePage from '../../widget/table-page.jsx'
import RequestEdit from './edit.jsx'

const scheduled = []

export default class RequestList extends TablePage {
  uri = 'request/index'

  componentWillMount() {
    this.setState({current: -1})
  }

  textarea(r) {
    if (r.id === this.state.current) {
      return <Form.TextArea
        autoHeight
        value={'application/json' === r.type ? JSON.stringify(JSON.parse(r.data), null, '  ') : r.data}/>
    }
    else {
      return r.data ? r.data.slice(0, 120) : ''
    }
  }

  select(r) {
    this.setState({[r.id]: this.state[r.id] ? false : r})
  }

  selected() {
    const selection = []
    for(let id in this.state) {
      if (id > 0) {
        selection.push(this.state[id])
      }
    }
    return selection
  }

  async execute(req) {
    scheduled.push(req)
    if (scheduled.length <= 1) {
      while (req = scheduled.shift()) {
        let res
        try {
          req.active = true
          this.select(req)
          res = await api.send('/' === req.url[0] ? req.url.slice(1) : req.url, JSON.parse(req.data))
          if (res.status < 400) {
            console.log(res)
          }
          else {
            // console.error(res)
          }
        }
        catch (ex) {
          console.error(ex)
          res = {status: 500}
        }
        req.active = false
        req.status = res.status
        this.select(req)
      }
    }
    await this.load()
  }

  refresh(r) {
    if (!r.active) {
      this.execute(r)
    }
    this.select(r)
  }

  async remove(r) {
    const {success} = await api.del('request/remove', {id: +r.id})
    if (success) {
      return this.load()
    }
  }

  rows() {
    return this.state.items.map(r => <Table.Row
      key={r.id}
      active={!!this.state[r.id]}
      positive={r.status < 400}
      negative={r.status >= 400}>
      <Table.Cell>
        <Form.Checkbox
          checked={!!this.state[r.id]}
          onChange={() => this.select(r)}/>
      </Table.Cell>
      <Table.Cell>{r.id}</Table.Cell>
      <Table.Cell>
        <Icon
          name="refresh"
          loading={r.active}
          onClick={() => this.refresh(r)}/>
      </Table.Cell>
      <Table.Cell>{new Date(r.time).toLocaleTimeString()}</Table.Cell>
      <Table.Cell>{r.ip}</Table.Cell>
      <Table.Cell>{'GET' === r.method ? <a href={r.url}>r.url</a> : r.url}</Table.Cell>
      <Table.Cell className={'POST'=== r.method ? 'method post' : 'method get'}>{r.method}</Table.Cell>
      <Table.Cell
        className={'string' === typeof r.agent ? 'agent' : 'agent null'}>{r.agent ? r.agent.slice(0, 40) : ''}</Table.Cell>
      <Table.Cell
        className={'string' === typeof r.data ? 'data ' : 'data null'}
        onClick={() => this.setState({current: r.id})}>
        {this.textarea(r)}
      </Table.Cell>
      <Table.Cell>
        <Icon
          name="trash"
          onClick={() => this.remove(r)}/>
      </Table.Cell>
    </Table.Row>)
  }

  render() {
    return <Segment className="page request-list">
      <h1>Запросы {this.state.time ? new Date(this.state.time).toLocaleString() : ''}</h1>
      {this.dimmer()}
      {this.paginator()}
      <Table compact="very">
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell/>
            <Table.HeaderCell>ID</Table.HeaderCell>
            <Table.HeaderCell/>
            <Table.HeaderCell>Time</Table.HeaderCell>
            <Table.HeaderCell>IP</Table.HeaderCell>
            <Table.HeaderCell>URL</Table.HeaderCell>
            <Table.HeaderCell>Method</Table.HeaderCell>
            <Table.HeaderCell>Agent</Table.HeaderCell>
            <Table.HeaderCell>Body</Table.HeaderCell>
            <Table.HeaderCell/>
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.rows()}</Table.Body>
      </Table>
    </Segment>
  }
}
