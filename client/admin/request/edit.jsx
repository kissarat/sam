import React, {Component} from 'react'
import {TextArea} from 'semantic-ui-react'
import {pick} from 'lodash'

export default class RequestEdit extends Component {
  selected() {
    const items = []
    for (let request of this.props.selected) {
      request = pick(request, 'id', 'url', 'method', 'type', 'data')
      if (request.data) {
        if ('application/json' === request.type) {
          request.data = JSON.parse(request.data)
        }
      }
      else {
        delete request.type
        delete request.data
      }
      items.push(request)
    }
    // this.setState({items})
    return 'return ' + items
  }

  onChange = e => {

  }

  render() {
    return <TextArea onChange={this.onChange} value={JSON.stringify(this.selected(), null, '  ')} autoHeight/>
  }
}
