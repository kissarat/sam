import React, {Component} from 'react'
import Form from '../widget/form.jsx'
import {FormGroup, ControlLabel, ButtonToolbar, Button} from 'react-bootstrap'
import {FieldGroup} from '../widget/field.jsx'
import 'whatwg-fetch'
import api from '../connect/api.jsx'
import {Link, browserHistory} from 'react-router'
import {pick} from 'lodash'

class UploadProgress extends Component {
  state = {
    loaded: 0,
    total: 0
  }

  componentWillReceiveProps({xhr}) {
    if (!xhr.handled) {
      xhr.addEventListener('progress', e => {
        this.setState({
          loaded: +e.loaded,
          total: +e.total
        })
      })
      xhr.addEventListener('done', e => {
        this.setState(xhr.responseJSON)
      })
      xhr.handled = true
    }
  }

  componentWillMount() {
    this.componentWillReceiveProps(this.props)
  }

  render() {
    if (this.state.mime && 0 === this.state.mime.indexOf('image/')) {
      return <div className="widget upload-image-preview">
        <div className="image" style={{backgroundImage: `url("${this.state.url}")`}}/>
      </div>
    }
    else {
      return <div className="widget upload-progress">
        <progress value={this.state.loaded} max={this.state.total}/>
        <span>{this.props.xhr.file.name}</span>
      </div>
    }
  }
}

export default class Upload extends Component {
  state = {
    xhrs: []
  }

  changeFile = e => {
    const xhrs = api.upload(e.target.files)
    this.setState({xhrs: this.state.xhrs.concat(xhrs)})
  }

  xhrs() {
    return this.state.xhrs.map((xhr, i) => <UploadProgress key={i} xhr={xhr}/>)
  }

  render() {
    return <div className="widget upload">
      <input multiple type="file" accept="image/*" onChange={this.changeFile}/>
      <div>{this.xhrs()}</div>
    </div>
  }
}
