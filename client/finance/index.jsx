import api from '../connect/api.jsx'
import React from 'react'
import Refresh from '../widget/refresh.jsx'
import t from '../t'
import {each, pick, map, defaults, filter, toArray, chunk, isEmpty, isObject} from 'lodash'
import {Link} from 'react-router'
import Filters from '../transfer/filters.jsx'
import {Message, Icon, Table, Form, Segment} from 'semantic-ui-react'
import {types, statuses} from './enums.jsx'
import TablePage from '../widget/table-page.jsx'

function converter(format) {
  return function (options) {
    if ('string' === typeof options) {
      options = {label: options}
    }
    return defaults(options, {format})
  }
}

function getValue(fn, row, name, defaultValue) {
  // console.log(fn, row, name, defaultValue)
  if ('function' === typeof fn) {
    return fn(row[name], row, name)
  }
  else if (fn) {
    return fn
  }
  return defaultValue
}

const format = {
  time: converter(function time(t) {
    return t ? new Date(t).toLocaleString().replace(/\s+/g, '\u00a0') : ''
  }),
  money: function (label) {
    return {
      label,
      className(value, row) {
        return row.currency.toLowerCase()
      },
      format(value, row) {
        return value / api.getRate(row.currency)
      }
    }
  },
  enumerable: converter(function (value) {
    return this.items[value] || value
  }),
  text: converter(function (text, row) {
    return t(text, row.vars || row)
  })
}

const columns = {
  id: 'ID',
  from_nick: t('Written off'),
  to_nick: 'Насчитано',
  amount: format.money(t('Amount')),
  type: format.enumerable({
    label: 'Тип',
    items: types
  }),
  status: format.enumerable({
    label: t('Status'),
    items: statuses
  }),
  created: format.time(t('Created')),
  text: format.text(t('Note')),
  time: format.time(t('Changed')),
  ip: 'IP',
  wallet: {
    label: t('Wallet'),
    format(v, row) {
      if ('ETH' === row.currency) {
        return <a target="blank" href={'https://etherscan.io/address/' + v}>{v}</a>
      }
      return v
    }
  },
  txid: {
    label: t('Transaction'),
    format(v, row) {
      if ('ETH' === row.currency) {
        return <a target="blank" href={'https://etherscan.io/tx/' + v}>{v}</a>
      }
      return v
    }
  },
}

each(columns, function (options, name) {
  if (!options) {
    return console.warn('Undefined options', name)
  }
  if ('string' === typeof options) {
    columns[name] = options = {
      label: options,
      format: a => a
    }
  }
  options.name = name
  if (['id', 'from_nick', 'to_nick', 'type', 'status', 'amount', 'text', 'created', 'wallet'].indexOf(name) >= 0) {
    options.visible = true
  }
})

export default class Finance extends TablePage {
  uri = 'transfer/index'

  componentWillMount() {
    const state = {
      create: this.props.params.amount && false !== this.state.create,
      size: 10,
      now: Date.now()
    }
    each(columns, o => {
      let value = !!o.visible
      if (this.isFilter(o)) {
        value = ''
      }
      state[o.name] = value
    })
    this.setState(state)
    // setInterval(this.interval, 300)
  }

  interval = () => {
    // this.setState({now: Date.now()})
  }

  selected() {
    return toArray(filter(columns, c => this.boolean(c.name)))
  }

  headers() {
    const array = this.selected().map(function (o) {
      return <Table.HeaderCell key={o.name} className={o.name}>
        {o.label}
      </Table.HeaderCell>
    })
    array.push(<Table.HeaderCell key="actions"/>)
    return array
  }

  async remove(id) {
    await api.del('transfer/remove', {id})
    this.load()
  }

  ethereum(row) {
    if ('withdraw' === row.type && 'success' !== row.type && ['ETH', 'ETC'].indexOf(row.currency) >= 0) {
      return <Link to={`/ethereum/${row.currency}/` + row.id} title="Сделать перевод"><Icon name="money"/></Link>
    }
  }

  rows() {
    return this.state.items.map(row => {
      const created = new Date(row.created).getTime()
      const angle = Math.round(this.state.now + created / (100000 * 2)) % 360
      // console.log(angle, row.created)
      const color = `hsl(${angle},80%,70%)`
      const cells = this.selected().map(o => <Table.Cell
          key={o.name}
          className={getValue(o.className, row, o.name, o.format.name)}
          data-name={o.name}
          style={{color: 'created' === o.name ? color : 'black'}}>
        {o.format(row[o.name], row)}
      </Table.Cell>)
      if (api.config.user.admin) {
        cells.push(<Table.Cell key="delete" className="actions">
          <Link to={"/transfer/edit/" + row.id}><Icon name="edit"/></Link>
          <Icon name="trash" onClick={() => this.remove(row.id)}/>
          {this.ethereum(row)}
        </Table.Cell>)
      }
      return <Table.Row onClick={() => this.onRow(row)} key={row.id} data-type={row.id}>{cells}</Table.Row>
    })
  }

  isFilter(o) {
    return false
    return (isObject(o.items) && !isEmpty(o.items)) || o.search
  }

  onCheck = (e, {name, checked}) => {
    checked = !!checked
    let value
    if (checked) {
      value = this.isFilter(columns[name]) ? '' : !!checked
    }
    else {
      value = checked
    }
    this.setState({[name]: value})
    if ('all' === name) {
      setTimeout(this.load, 0)
    }
  }

  boolean(name) {
    const value = this.state[name]
    return true === value || '' === value || !!value
  }

  admin() {
    if (api.config.user.admin) {
      return <Form.Checkbox
          name="all"
          checked={this.state.all}
          label={t('All users')}
          onChange={this.onCheck}
      />
    }
  }

  columnsSelection() {
    // const only = ['type', 'status', 'ip', 'wallet', 'txid']
    const selected = []
    each(columns, o => {
      let group = <Form.Checkbox
          name={o.name}
          key={o.name}
          checked={this.boolean(o.name)}
          label={o.label}
          onChange={this.onCheck}
      />
      if (['type', 'status'].indexOf(o.name) >= 0) {
        let input
        const change = (e, {value}) => this.filter(o.name, value || true)
        if (o.items) {
          const options = map(o.items, (value, key) => ({key, value: key, text: value}))
          options.unshift({key: 0, value: '', text: ''})
          input = <Form.Select size="mini" options={options} onChange={change}/>
        }
        else {
          input = <Form.Input size="mini"/>
        }
        group = <Form.Group className="filter" key={o.name}>
          {group}
          {input}
        </Form.Group>
      }
      selected.push(group)
    })
    const chunks = chunk(selected, 2).map((c, i) => <Form.Group key={i} widths='equal'>{c}</Form.Group>)
    return <Form className="select-columns">
      {this.admin()}
      <h4>{t('Columns')}</h4>
      {chunks}
    </Form>
  }

  form() {
    if (this.props.params && this.props.params.id) {
      return <Message
          success
          icon="dollar"
          content={t('Payment #{id} received', this.props.params)}
      />
    }
    else {
      return <div>
      </div>
    }
  }

  filter = (name, value) => this.setState({[name]: value}, this.load)

  getFilterParams() {
    const params = {}
    for (const name of ['type', 'status']) {
      const v = this.state[name]
      if (v && true !== v) {
        params[name] = v
      }
    }
    window._state = this.state
    return params
  }

  onRow = row => {
    if (api.textarea instanceof Element && api.lastResponse) {
      const json = api.lastResponse.result.find(t => t.id === row.id)
      this.setState({selectedRow: json})
      api.textarea.value = JSON.stringify(json, null, '  ')
      api.textarea.scrollIntoView()
    }
  }

  render() {
    return <Segment.Group className="page transfer-list">
      <Refresh refresh={this.load}/>
      <Link to="/transfer/create">{t('Create')}</Link>
      <Segment.Group horizontal>
        <Segment>
          <h1>{t('Finances')}</h1>
          {this.form()}
          {this.paginator()}
          <textarea id="json" style={{display: 'none'}}/>
        </Segment>
        <Segment>{this.columnsSelection()}</Segment>
      </Segment.Group>
      <Segment>
        {this.dimmer()}
        <Table compact="very">
          <Table.Header>
            <Table.Row>
              {this.headers()}
            </Table.Row>
          </Table.Header>
          <Table.Body>{this.rows()}</Table.Body>
        </Table>
      </Segment>
    </Segment.Group>
  }
}
