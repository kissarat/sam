import t from '../t'
import {map} from 'lodash'

const types = {
  payment: t('Payment'),
  internal: t('Transfer'),
  withdraw: t('Withdrawal'),
  buy: t('Purchase'),
  bonus: t('Qualification'),
  earn: t('Income'),
  accrue: t('Accrue'),
  support: t('Support'),
}

const statuses = {
  created: t('Created'),
  success: t('Successfully'),
  canceled: t('Canceled'),
  fail: t('Error'),
  pending: t('Pending'),
  virtual: t('Virtual'),
}

const systems = {
  perfect: 'Perfect Money',
  advcash: 'AdvCash',
  blockio: 'Bitcoin',
  ethereum: 'Ethereum',
  payeer: 'Payeer',
  nix: 'NixMoney',
}

const currencies = {
  BTC: 1000000,
  DOGE: 10,
  ETC: 100000,
  ETH: 100000,
  LTC: 100000,
  USD: 100,
  XRP: 100,
}

function only(object, allowed) {
  for (const key in object) {
    if (allowed.indexOf(key) < 0) {
      delete object[key]
    }
  }
}

function dictionaryToOptions(items) {
  const options = items instanceof Array
    ? items.map(c => ({key: c, value: c, text: c}))
    : map(items, (value, key) => ({key, value: key, text: value}))
  options.unshift({key: 0, value: '', text: ''})
  return options
}

export {types, statuses, systems, currencies, dictionaryToOptions, only}
