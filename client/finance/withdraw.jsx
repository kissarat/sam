import AlertList from '../widget/alert.jsx'
import api from '../connect/api.jsx'
import FormComponent from '../base/form-component.jsx'
import React, {Component} from 'react'
import t from '../t'
import {browserHistory} from 'react-router'
import {Form, Button} from 'semantic-ui-react'
import {map, first} from 'lodash'
import {systems, dictionaryToOptions} from './enums.jsx'

export default class Withdraw extends FormComponent {
  state = {
    amount: '0.004',
    wallet: '',
    text: ''
  }

  async componentWillMount() {
    const {user} = await api.get('user/get')
    if (user && 'number' === typeof user.balance) {
      const state = {amount: (user.balance / api.config.money.scale).toString()}
      state.system = first(Object.keys(systems))
      this.setState(state)
    }
  }

  submit = async() => {
    AlertList.clear()
    if ('string' !== typeof this.state.amount) {
      return AlertList.show({
        error: true,
        content: t('The amount must be more than {amount}', {amount: api.config.money.withdraw.min / api.config.money.scale}),
        onDismiss: true
      })
    }
    const r = await api.send(('future-money' === api.config.id) ? 'blockio/withdraw' : 'transfer/withdraw', {
      text: this.state.text.trim() || null,
      wallet: this.state.wallet,
      amount: (this.state.amount.replace(/,/g, '.')) * api.config.money.scale,
      // type: 'withdraw'
    })
    if (r.success) {
        AlertList.show({
          success: true,
          content: t('Successfully'),
          onDismiss: true
        })
        browserHistory.push('/finance')
    }
    else if (r.error) {
      AlertList.show({
        error: true,
        content: t(r.error.message, r.error),
        onDismiss: true
      })
    }
    else {
      alert(t('Unknown error'))
    }
  }

  render() {
    const options = dictionaryToOptions(systems)
    return <Form onSubmit={this.onSubmit}>
      <h1>{t('Withdrawal')}</h1>
      <div>
        <Form.Input
          name="amount"
          label={t('Amount')}
          value={this.state.amount || ''}
          required
          onChange={this.onChange}
        />
        <Form.Select
          name="system"
          label={t('Payment system')}
          onChange={this.onChange}
          value={this.state.system || ''}
          options={options}/>
        <Form.Input
          name="wallet"
          label={t('Wallet')}
          disabled={'inbisoft' === api.config.id}
          value={this.state.wallet || ''}
          onChange={this.onChange}
        />
        <Form.Input
          name="text"
          label={t('Note')}
          value={this.state.text || ''}
          onChange={this.onChange}
        />
      </div>
      <div className="control">
        <Button positive>{t('Request a withdrawal')}</Button>
      </div>
    </Form>
  }
}
