import api from '../connect/api.jsx'
import React, {Component} from 'react'
import {Button, Form, Segment} from 'semantic-ui-react'
import {each, pick, map} from 'lodash'
import {types, statuses, currencies, dictionaryToOptions} from './enums.jsx'
import {browserHistory} from 'react-router'

export default class TransferEdit extends Component {
  state = {}

  title() {
    const id = this.props.params.id
    return id
      ? 'Редактирования платежа #' + id
      : 'Создания платежа'
  }

  onChange = (e, {name, value}) => {
    this.setState({[name]: value})
  }

  load = async () => {
    const {transfer} = await api.get('transfer/get', {id: this.props.params.id})
    if (transfer) {
      if (transfer.from) {
        const {user} = await api.get('user/get', {id: transfer.from})
        transfer.from_nick = user.nick
      }
      if (transfer.to) {
        const {user} = await api.get('user/get', {id: transfer.to})
        transfer.to_nick = user.nick
      }
      transfer.amount = transfer.amount / api.getRate(transfer.currency)
      this.setState(transfer)
    }
  }

  componentDidMount() {
    if (this.props.params.id) {
      this.load()
    }
  }

  componentWillReceiveProps(props) {
    if (props.params.id) {
      setTimeout(this.load, 0)
    }
  }

  onSubmit = async(e, {formData}) => {
    e.preventDefault()
    if (formData.from_nick) {
      const {user} = await api.get('user/get', {nick: formData.from_nick})
      formData.from = user.id
    }
    else {
      formData.from = null
    }
    if (formData.to_nick) {
      const {user} = await api.get('user/get', {nick: formData.to_nick})
      formData.to = user.id
    }
    else {
      formData.to = null
    }
    formData = pick(formData, 'from', 'to', 'amount', 'type', 'status', 'wallet', 'currency')
    formData.amount = Math.floor(formData.amount.replace(/,/g, '.') * api.getRate(this.state.currency))
    each(formData, function (value, key) {
      if (!value || ('string' === typeof value && !value.trim())) {
        formData[key] = null
      }
    })
    const {success} = this.props.params.id
      ? await api.send('transfer/save', {id: this.props.params.id}, formData)
      : await api.send('transfer/create', formData)
    if (success) {
      browserHistory.push('/transfers')
    }
  }

  wallet() {
    if (['payment', 'withdraw'].indexOf(this.state.type) >= 0) {
      return <Form.Input
        name="wallet"
        label="Кошелек"
        value={this.state.wallet || ''}
        onChange={this.onChange}
      />
    }
  }

  render() {
    return <Segment className="page finance-edit">
      <h1>{this.title()}</h1>
      <Form onSubmit={this.onSubmit}>
        <div>
          <Form.Input
            name="from_nick"
            label="Списать"
            value={this.state.from_nick || ''}
            onChange={this.onChange}
          />
          <Form.Input
            name="to_nick"
            label="Насчитать"
            value={this.state.to_nick || ''}
            onChange={this.onChange}
          />
          <Form.Input
            name="amount"
            label="Сумма"
            value={this.state.amount || 0}
            onChange={this.onChange}
          />
          <Form.Select
            name="currency"
            label="Валюта"
            options={dictionaryToOptions(Object.keys(currencies))}
            value={this.state.currency || 0}
            onChange={this.onChange}
          />
          <Form.Select
            name="type"
            label="Тип"
            options={dictionaryToOptions(types)}
            value={this.state.type || ''}
            onChange={this.onChange}
          />
          <Form.Select
            name="status"
            label="Статус"
            options={dictionaryToOptions(statuses)}
            value={this.state.status || ''}
            onChange={this.onChange}
          />
          {this.wallet()}
        </div>
        <div className="control">
          <Button>Сохранить</Button>
        </div>
      </Form>
    </Segment>
  }
}
