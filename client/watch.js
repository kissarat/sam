const http = require('http')

const requests = []

function Watch() {
  this.serve()
}

Watch.prototype = {
  serve() {
    const server = http.createServer(function (req, res) {
      res.writeHead(200, {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'text/event-stream',
      })
      if ('GET' === req.method) {
        requests.push(res)
        res.write('event: online\ndata: {}\n\n')
      }
      else {
        res.end()
      }
    })

    server.listen(7001)
  },

  apply(compiler) {
    compiler.plugin("compile", () => requests.forEach(res => res.write('event: start\ndata: {}\n\n')))
    compiler.plugin("done", () => requests.forEach(res => res.write('event: reload\ndata: {}\n\n')))
  }
}

module.exports = Watch
