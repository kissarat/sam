import React, {Component} from 'react'

/**
 * @property {object} props
 * @property {object} state
 * @property {function} setState
 */
export default class FormComponent extends Component {
  onSubmit = e => {
    e.preventDefault()
    if (this.submit instanceof Function) {
      this.submit()
    }
    else {
      console.error('Not submittable')
    }
  }

  onChange = e => {
    const changes = {[e.target.getAttribute('name')]: e.target.value}
    this.setState(changes)
  }
}
