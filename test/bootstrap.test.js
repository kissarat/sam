const db = require('../server/db')

before(function (done) {
  db.connect()
    .then(function () {
      done()
    })
    .catch(done)
})

require('./unit/token.test')
require('./unit/user.test')
require('./unit/item.test')
