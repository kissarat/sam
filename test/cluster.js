const cluster = require('cluster')
const numCPUs = require('os').cpus().length

var port = 8000
var i = port + 1

console.log('start' + i)
if (cluster.isMaster) {
  for (; i < port + numCPUs; i++) {
    cluster.fork()
    console.log(i)
  }
}
else if (cluster.isWorker) {
  console.log('hello' + i)
}
else {
  console.log('something else')
}
cluster.on('fork', function () {
  console.log('forl', i)
})
setTimeout(function () {
  console.log('setTimeout', i)
}, 50)