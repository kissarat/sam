const db = require('schema-db')
const assert = require('assert')
const rex = require('../../server/request')
const User = require('../../server/entities/user')
const modules = require('../../server/modules')
const {random} = require('lodash')

describe('item', function () {
  const req = Object.create(rex)
  req.headers = {ip: '127.0.0.4'}

  it('add', function (done) {
    const data = {
      amount: random(1, 10)
    }
    db.table('user').orderBy(db.raw('random()')).limit(1)
      .then(function ([user]) {
        req.user = new User(user)
        return db.table('article').orderBy(db.raw('random()')).limit(1)
      })
      .then(function ([article]) {
        data.article = article.id
        return modules.item.add.call(req, data)
      })
      .then(function (item) {
        assert.strictEqual(data.article, item.article)
        done()
      })
      .catch(done)
  })
})
