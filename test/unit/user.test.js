const assert = require('assert')
const Token = require('../../server/entities/token')
const modules = require('../../server/modules')
const rex = require('../../server/request')
const rs = require('randomstring')
const db = require('schema-db')
const bcrypt = require('promised-bcrypt')
const {pick, isObject} = require('lodash')
const {lorem} = require('faker')
const {hostname} = require('os')

function findToken(where = '"user" is null') {
  return db.table('token')
    .where(db.raw(where))
    .orderBy(db.raw('random()'))
    .limit(1)
    .then(function ([token]) {
      const req = Object.create(rex)
      req.headers = {ip: '127.0.0.3'}
      req.token = new Token(token)
      return req
    })
}

function generateUser() {
  const nick = lorem.word() + '_' + rs.generate(4)
  return {
    nick,
    password: rs.generate(12),
    email: nick + '@yopmail.com'
    // email: 'kissarat@gmail.com'
  }
}

describe('user', function () {
  before(function (done) {
    const data = {
      id: rs.generate(4),
      name: 'test-' + hostname(),
      expires: new Date(2030, 1)
    }
    db.table('token').insert(data)
      .then(function () {
        done()
      })
      .catch(done)
  })

  it('signup', function (done) {
    findToken()
      .then(function (req) {
        const data = generateUser()
        return modules.user.signup.call(req, data)
          .then(function (user) {
            assert.equal('number', typeof user.id)
            assert.ok(user.id > 0)
            assert.equal(data.nick, user.nick)
            return db.table('token').where({user: user.id, type: 'password'})
              .then(function (tokens) {
                // assert.strictEqual(tokens.length, 1)
                const token = tokens[0]
                assert.strictEqual(token.user, user.id)
                return bcrypt.compare(data.password, token.id)
              })
          })
          .then(function (equal) {
            assert.ok(equal)
            done()
          })
      })
      .catch(done)
  })

  it('logout', function (done) {
    findToken('"user" is not null')
      .then(function (req) {
        return modules.user.logout.call(req)
          .then(function (token) {
            assert.notStrictEqual(token.found, false)
            assert.strictEqual(token.success, true)
            assert.strictEqual(token.id, req.token.id)
            assert.strictEqual(token.expires.getTime(), req.token.expires.getTime())
            assert.ok(!token.user)
            done()
          })
      })
      .catch(done)
  })

  it('login', function (done) {
    findToken()
      .then(function (req) {
        const data = generateUser()
        return db.table('user').insert(pick(data, ['nick', 'email']), ['id', 'created'])
          .then(function ([user]) {
            return bcrypt.hash(data.password)
              .then(function (passwordHash) {
                return db.table('token').insert({
                  id: passwordHash,
                  user: user.id,
                  type: 'password',
                  expires: new Date(2030, 0)
                })
              })
              .then(function () {
                return modules.user.login.call(req, data)
              })
              .then(function (token) {
                assert.strictEqual(token.success, true)
                assert.ok(isObject(token.user))
                assert.strictEqual(token.user.id, user.id)
                assert.strictEqual(token.user.nick, data.nick)
                done()
              })
          })
      })
      .catch(done)
  })
})
