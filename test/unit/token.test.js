const assert = require('assert')
const token = require('../../server/modules/token')
const rex = require('../../server/request')
const rs = require('randomstring')
const db = require('schema-db')

describe('token', function () {
  const req = Object.create(rex)
  req.headers = {ip: '127.0.0.2'}

  it('create', function (done) {
    const t = {
      expires: new Date(2030, 1),
      name: rs.generate(240)
    }
    token.create.call(req, t)
      .then(function (data) {
        assert.ok(data.success)
        assert.equal(t.name, data.name)
        assert.equal(t.expires.getTime(), data.expires.getTime())
        assert.ok(/^\w{24}$/.test(data.id))
        done()
      })
      .catch(done)
  })

  it('reset', function (done) {
    const t = {
      expires: new Date(2030, 1),
      name: rs.generate(240)
    }
    db.table('token')
      .where(db.raw("type = 'plain'"))
      .orderBy(db.raw('random()'))
      .limit(1)
      .then(function (r) {
        return token.reset.call(req, {id: r[0].id}, t)
      })
      .then(function (data) {
        assert.ok(data.success)
        assert.equal(t.name, data.name)
        assert.equal(t.expires.getTime(), data.expires.getTime())
        assert.ok(/^\w{24}$/.test(data.id))
        done()
      })
      .catch(done)
  })

  it('delete', function (done) {
    db.table('token')
      .where(db.raw("type = 'plain'"))
      .orderBy(db.raw('random()'))
      .limit(1)
      .then(function (r) {
        return token.delete.call(req, {id: r[0].id})
      })
      .then(function (data) {
        assert.ok(data.success)
        assert.ok(/^\w{24}$/.test(data.id), 'Invalid ID ' + data.id)
        done()
      })
      .catch(done)
  })
})
