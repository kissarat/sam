async function run() {
  return new Promise(function (resolve) {
    setTimeout(() => resolve(5), 3000)
  })
}

async function main() {
  console.log(await run())
}

main()
