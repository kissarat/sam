const bitcoin = require('bitcoin-promise')

const client = new bitcoin.Client({
  host: process.argv[process.argv.length - 1],
  port: 8332,
  user: 'taras',
  pass: 'eenu0fuu5Chiesamo7phae1ahch3sha7',
  timeout: 120000,
  ssl: false
});

// console.log(Object.keys(require('bitcoin').Client.prototype).join('\n'))

async function run() {
  // console.log(await client.getPeerInfo())
  // console.log(await client.getGenerate())
  // console.log(await client.listAccounts())
  // console.log(await client.getAccountAddress(''))
  // console.log(await client.getTransaction('294625ea61beb232453c29bec765edad554609fdc0e9a6b372c217159b6741f5'))
  // console.log(await client.getTransaction('51a2e222b68758b0791156f7eb3fe456a07c6fb37c5a50247011d1a415f63460'))
  // console.log(await client.getTransaction('48136ff658dd0769f0d699d1492bbc45c166d09769db5289a6e0e1506ac94512'))
  // console.log(await client.validateAddress('3AWQfkkpoYu7T85EEMvj4pdQdi5bqZQWwJ'))
  // console.log(await client.listReceivedByAccount())
  // console.log(await client.setTxFee(0.00000000))
  // console.log(await client.getInfo())
  // console.log(await client.getConnectionCount())
  console.log(await client.sendToAddress('2NC8q4SYUeSt1z4WbjKeUJx4gSCPY7dAAxN', 0.1))
  console.log(await client.getBalance('*', 6))
  const addresses = await client.getAddressesByAccount('')
  for(const address of addresses) {
    console.log(address, await client.getReceivedByAddress(address))
  }
  // console.log(await client.sendToAddress('2NBx3dTESgyp9stRq8EtXv2t8WX5WCC2CC1', 0.08))

  // console.log(await client.sendMany('', {
  //   '2N3XRq8zSRMm4dSQdGE2SHuDZvwhssFqAz5': 0.0010,
  //   '2NEKZejTY38Zv1M9Ahs4v6vSbcDYzPGG4LA': 0.0010,
  //   '2NGKTfUFRfxp62jTnYtntaVDpwDeVkBKwXX': 0.00276000,
    // '2N6jUPCW3SjYSn3BszzqBcTq9eHEj9Scoga': 0.25,
    // '2Muij3gWAihxcuzU9qGtjuqDyrzz77gDjC7': 0.30,
    // '2NBx3dTESgyp9stRq8EtXv2t8WX5WCC2CC1': 0.35,
    // '2NAtMW98YKKzeTHu52oFs9Li8RzCJEYMHQ3': 0.40,
    // '2Mvgf18QnAnYXz9QQGqr7S1GpqAoyr11u1J': 0.45,
    // '2NAiupCa2tUSj2eBacMkHBG7tMebxigvAci': 0.50,
    // '2N8saH4W55ECmbYp6hpsoRmDMHuH8Msk2Ld': 0.55,

    // '2MxhHmGJNE5gspg1WYKypLPyd2h5Sort1hU': 0.60,
    // '2N2MHCwwjUcgDVBYoSpxiWfF8eAukkpiHD8': 0.65,
    // '2NFNP3V3Va8NY1inhxyoWj5Yi4pKk2GYCKB': 0.70,
    // '2MtJu1WR5gYkhH5ytxKmYTrdyP4rPxBb8e4': 0.75,
  // }), 'Hello World from Taras Labiak <kissarat@gmail.com>')
  // console.log(await client.getNewAddress(''))
  // console.log(await client.getInfo())
}

run()
