module.exports = {
  id: 'july',
  name: 'July',
  enabled: false,

  dir: __dirname,

  database: {
    id: 'july',
  },

  money: {
    scale: 100,
    currency: 'USD'
  },

  domains: [],

  telegram: {
    enabled: false
  },

  remote: {
    enabled: true
  },

  nix: {
    enabled: true
  },

  perfect: {
    enabled: true
  },

  payeer: {
    enabled: true
  }
}
