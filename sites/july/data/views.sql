CREATE OR REPLACE VIEW token_user AS
  SELECT
    t.created,
    t.handshake,
    NULL :: JSON     AS hard,
    t.id,
    t.ip,
    t.name,
    t.time,
    t.type,
    u.email,
    u.nick,
    u.admin,
    FALSE            AS leader,
    u.avatar,
    u.parent,
    row_to_json(u.*) AS "user"
  FROM token t
    LEFT JOIN "user" u ON t.user = u.id
  WHERE expires > CURRENT_TIMESTAMP;

CREATE OR REPLACE VIEW user_about AS
  SELECT
    id,
    type,
    nick,
    forename,
    surname,
    admin,
    FALSE AS leader,
    skype,
    avatar,
    ''    AS facebook,
    ''    AS vkontakte,
    ''    AS twitter,
    ''    AS google,
    ''    AS odnoklassniki,
    ''    AS about
  FROM "user";

CREATE OR REPLACE VIEW need AS
  SELECT
    n.id,
    n.user,
    (n.amount * p.percent) AS amount
  FROM node n
    JOIN program p ON n.program = p.id
  WHERE
    NOT EXISTS(
        SELECT 1
        FROM transfer t1
        WHERE type = 'debit' AND status = 'success' AND t1.node = n.id
        LIMIT 1
    )
    AND (
      SELECT CURRENT_TIMESTAMP - coalesce(max(t2.created), n.created) >= INTERVAL '1 day'
      FROM transfer t2
      WHERE type = 'accrue' AND status = 'success' AND t2.node = n.id
    );

CREATE OR REPLACE VIEW referral AS
  WITH RECURSIVE r(id, "nick", parent, email, root, level, forename, surname, skype, avatar) AS (
    SELECT
      id,
      nick,
      parent,
      email,
      id AS root,
      0  AS level,
      forename,
      surname,
      skype,
      avatar
    FROM "user"
    UNION
    SELECT
      u.id,
      u.nick,
      u.parent,
      u.email,
      r.root,
      r.level + 1 AS level,
      u.forename,
      u.surname,
      u.skype,
      u.avatar
    FROM "user" u
      JOIN r ON u.parent = r.id
    WHERE r.level < 50
  )
  SELECT
    id,
    nick,
    parent,
    email,
    root,
    level,
    forename,
    surname,
    skype,
    avatar
  FROM r
  ORDER BY root, level, id;

CREATE OR REPLACE VIEW informer AS
  SELECT
    b.*,
    coalesce((SELECT sum(amount)
              FROM "transfer" a
              WHERE a.type = 'payment' AND a.status = 'success' AND a.to = b.id), 0)            AS payment,
    coalesce((SELECT sum(amount)
              FROM "transfer" a
              WHERE a.type = 'withdraw' AND a.status = 'success' AND a.to = b.id), 0)           AS withdraw,
    coalesce((SELECT sum(amount)
              FROM "transfer" a
              WHERE a.type IN ('accrue', 'bonus') AND a.status = 'success' AND a.to = b.id), 0) AS accrue,
    coalesce((SELECT sum(amount)
              FROM "transfer" a
              WHERE a.type IN ('accrue', 'bonus') AND a.status = 'success' AND a.to = b.id
                    AND created > CURRENT_TIMESTAMP -
                                  INTERVAL '1 day'), 0)                                         AS accrue_day,
    (SELECT count(*)
     FROM "visit" v
     WHERE v.url = '/ref/' || u.nick)                                                           AS visit,
    (SELECT count(*)
     FROM "user" r
     WHERE r.parent = u.id)                                                                     AS invited,
    u.nick,
    u.type,
    u.surname,
    u.forename,
    u.skype,
    u.avatar,
    u.email,
    to_json(p.*)                                                                                AS parent
  FROM balance b
    JOIN "user" u ON b.id = u.id
    LEFT JOIN user_about p ON u.parent = p.id;

CREATE OR REPLACE VIEW sponsor AS
  WITH RECURSIVE r(id, "nick", parent, root, level) AS (
    SELECT
      id,
      nick,
      parent,
      id AS root,
      0  AS level
    FROM "user"
    UNION
    SELECT
      u.id,
      u.nick,
      u.parent,
      r.root,
      r.level + 1 AS level
    FROM "user" u
      JOIN r ON u.id = r.parent
    WHERE r.level < 50
  )
  SELECT
    id,
    nick,
    parent,
    root,
    level
  FROM r
  ORDER BY root, level, id;

CREATE OR REPLACE VIEW statistics AS
  SELECT
    (SELECT sum(amount)
     FROM transfer
     WHERE type = 'payment' AND status = 'success')                        AS payment,
    (SELECT sum(amount)
     FROM transfer
     WHERE type = 'payment' AND status =
                                'pending')                                 AS pending,
    (SELECT sum(amount)
     FROM transfer
     WHERE type = 'withdraw' AND status =
                                 'success')                                AS withdraw,
    (SELECT sum(amount)
     FROM transfer
     WHERE type = 'accrue' AND status =
                               'success')                                  AS accrue,
    (SELECT sum(amount)
     FROM transfer
     WHERE type = 'credit' AND status =
                               'success')                                  AS buy,
    (SELECT count(*)
     FROM
         "user")                                                           AS users,
    (SELECT count(*)
     FROM "user" ud
     WHERE ud.created > CURRENT_TIMESTAMP -
                        INTERVAL '1 day')                                  AS users_day,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires >
                                CURRENT_TIMESTAMP)                         AS visitors,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires > CURRENT_TIMESTAMP
           AND handshake > CURRENT_TIMESTAMP -
                           INTERVAL '1 day')                               AS visitors_day,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires > CURRENT_TIMESTAMP
           AND handshake > CURRENT_TIMESTAMP -
                           INTERVAL '1 day')                               AS new_visitors_day,
    (SELECT count(*)
     FROM log
     WHERE entity = 'handshake' AND action = 'update'
           AND to_timestamp(id / (1000 * 1000 * 1000)) > CURRENT_TIMESTAMP -
                                                         INTERVAL '1 day') AS activities_day,
    (SELECT count(*)
     FROM visit
     WHERE url LIKE '/ref/%' AND visit.created > CURRENT_TIMESTAMP -
                                                 INTERVAL '1 day')         AS referral_day,
    (SELECT sum(balance)
     FROM balance)                                                         AS balance;
