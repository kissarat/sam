CREATE TYPE user_type AS ENUM ('new', 'native', 'special');

CREATE TABLE "user" (
  id       SERIAL PRIMARY KEY,
  nick     VARCHAR(24) NOT NULL,
  email    VARCHAR(48) NOT NULL,
  "admin"  BOOLEAN     NOT NULL DEFAULT FALSE,
  skype    VARCHAR(54),
  avatar   VARCHAR(192),
  surname  VARCHAR(48),
  forename VARCHAR(48),
  created  TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time     TIMESTAMP,
  type     user_type   NOT NULL,
  gavatar  INT,
  secret   CHAR(60),
  pin      CHAR(4),
  parent   INT REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE,
  perfect  VARCHAR(10),
  nix      VARCHAR(20),
  payeer   VARCHAR(20)
);
CREATE UNIQUE INDEX user_nick
  ON "user" (lower(nick));
CREATE UNIQUE INDEX user_email
  ON "user" (lower(email));

INSERT INTO "user" (type, nick, email, admin) VALUES ('native', 'admin', 'admin@inbisoft.com', TRUE);

CREATE TYPE token_type AS ENUM ('server', 'browser', 'app', 'code', 'password');

CREATE TABLE token (
  id        VARCHAR(240) PRIMARY KEY,
  "user"    INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "type"    token_type NOT NULL DEFAULT 'browser',
  created   TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time      TIMESTAMP,
  handshake TIMESTAMP,
  expires   TIMESTAMP  NOT NULL,
  ip        INET,
  name      VARCHAR(240)
);

CREATE TYPE transfer_type AS ENUM ('internal', 'payment', 'withdraw', 'accrue', 'bonus', 'credit', 'debit', 'support');

CREATE TABLE program (
  id       SMALLINT PRIMARY KEY,
  name     VARCHAR(32) NOT NULL,
  percent  FLOAT       NOT NULL,
  period   INTERVAL    NOT NULL
);

INSERT INTO program VALUES
  (1, 'Week', 0.005, '1 week'),
  (2, 'Month', 0.0065, '1 month');

CREATE TABLE node (
  id      SERIAL PRIMARY KEY,
  "user"  INT       NOT NULL REFERENCES "user" (id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  program INT       NOT NULL REFERENCES program (id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  amount  INT       NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time    TIMESTAMP
);
