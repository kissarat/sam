CREATE OR REPLACE FUNCTION buy(_program_id INT, user_id INT, _amount INT)
  RETURNS INT AS $$
DECLARE
  _created     TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
  _program     RECORD;
  _node_id     INT;
  _transfer_id INT;
  _user        RECORD;
BEGIN
  SELECT
    id,
    name
  FROM program
  WHERE id = _program_id
  INTO _program;

  SELECT
    id,
    nick,
    parent
  FROM "user"
  WHERE id = user_id
  INTO _user;

  IF _program IS NULL
  THEN
    RETURN -404;
  END IF;

  IF (SELECT balance - _amount < 0
      FROM balance
      WHERE id = user_id)
  THEN
    RETURN -402;
  END IF;

  INSERT INTO node ("user", "program", amount, created)
  VALUES (user_id, _program.id, _amount, _created)
  RETURNING id
    INTO _node_id;

  INSERT INTO transfer ("type", "status", "from", amount, created, node, "text", vars)
  VALUES (
    'credit', 'success', user_id, _amount, _created, _node_id,
    'Opening {name}',
    json_build_object('name', _program.name)
  )
  RETURNING id
    INTO _transfer_id;

  INSERT INTO transfer ("type", "status", "to", amount, created, node, "text", vars)
    VALUES (
      'bonus',
      'success',
      _user.parent,
      _amount * 0.1,
      _created,
      _node_id,
      'Referral bonus from {nick}',
      json_build_object('from', user_id, 'nick', _user.nick)
  );

  RETURN _transfer_id;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION accrue()
  RETURNS VOID AS $$
DECLARE
  _time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;
BEGIN
  INSERT INTO transfer ("type", "status", "to", amount, created, node)
    SELECT
      'accrue',
      'success',
      "user",
      amount,
      _time,
      id
    FROM need;
END
$$ LANGUAGE plpgsql;
