import React, {Component} from 'react'
import t from '../t'
import api from '../../../../client/connect/api.jsx'
import FormComponent from "../../../../client/base/form-component.jsx";
import {AlertList} from "../../../../client/widget/alert.jsx";
import {browserHistory} from "react-router";

export class Program extends FormComponent {
  state = {}

  async submit() {
    const {status, need, error} = await api.send('program/open', {
      id: this.props.id,
      amount: this.state.amount * 100
    })
    if (error && error.message) {
      AlertList.show({
        negative: true,
        content: t(error.message),
        onDismiss: true
      })
    }
    else if (402 === status) {
      AlertList.show({
        negative: true,
        content: t('Insufficient funds'),
        onDismiss: true
      })
      browserHistory.push('/pay/' + (need / 100))
    }
  }

  render() {
    return <div className="widget program">
      <h2>{this.props.name}</h2>
      <form onSubmit={this.onSubmit}>
        <input
          placeholder={t('Amount')}
          type="number"
          name="amount"
          min="10"
          onChange={this.onChange}
          required/>
        <button>Open</button>
      </form>
    </div>
  }
}

export default class ProgramList extends Component {
  state = {}

  async componentWillMount() {
    const {programs} = await api.get('program/index')
    if (programs) {
      this.setState({programs})
    }
  }

  list() {
    if (this.state.programs instanceof Array) {
      return this.state.programs.map(p => <Program key={p.id} {...p}/>)
    }
    else {
      return <span>Loading...</span>
    }
  }

  render() {
    return <div className="page program-list">
      <h1>{t('Deposits')}</h1>
      <div className="list">{this.list()}</div>
    </div>
  }
}
