import AlertList from '../../../../client/widget/alert.jsx'
import Language from '../../../../client/widget/language.jsx'
import React, {Component} from 'react'
import Menu from '../../../../client/widget/menu.jsx'
import t from '../t'


const items = [
  ['Back Office', '/cabinet', 'suitcase'],
  ['Deposit', '/deposits', 'shopping basket'],
  ['Structure', '/structure', 'users'],
  ['Finances', '/finance', 'dollar'],
  ['Banners', '/banner', 'tv'],
  ['Settings', '/settings', 'setting'],
  ['Logout', '/logout', 'sign out'],
]

for(const item of items) {
  item[0] = t(item[0])
}

export default class JulyLayout extends Component {
  render() {
    return <div className="layout cabinet">
      <Menu items={items}/>
      <div className="main">
        <header>
          <Language/>
        </header>
        <AlertList/>
        <main>{this.props.children}</main>
      </div>
    </div>
  }
}
