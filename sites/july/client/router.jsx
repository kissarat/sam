import api from '../../../client/connect/api.jsx'
import JulyLayout from './cabinet/layout.jsx'
import JulyMain from './cabinet/main.jsx'
import Login from '../../../client/outdoor/login.jsx'
import Logout from '../../../client/outdoor/logout.jsx'
import Outdoor from '../../../client/outdoor/app.jsx'
import Password from '../../../client/settings/password.jsx'
import ProgramList from "./bank/program-list.jsx"
import React from 'react'
import Recovery from '../../../client/outdoor/recovery.jsx'
import Settings from '../../../client/settings/index.jsx'
import Signup from '../../../client/outdoor/signup.jsx'
import Structure from '../../../client/user/structure.jsx'
import TransferList from "../../../client/transfer/index.jsx"
import WalletSettings from '../../../client/settings/wallets.jsx'
import Withdraw from "../../../client/finance/withdraw.jsx"
import {adminRoute} from '../../../client/admin/router.jsx'
import {IndexRedirect, Router, Route, browserHistory} from 'react-router'

export const routes = <Route path='/'>
  <IndexRedirect to="cabinet"/>
  <Route component={Outdoor}>
    <Route path='login' component={Login}/>
    <Route path='signup' component={Signup}/>
    <Route path='ref/:nick' component={Signup}/>
    <Route path='recovery/:code' component={Recovery}/>
  </Route>
  <Route onEnter={api.checkAuthorized}>
    <Route component={JulyLayout}>
      <Route path='cabinet' component={JulyMain}/>
      <Route path='structure' component={Structure}/>
      <Route path='deposits' component={ProgramList}/>
      <Route path='finance' component={TransferList}/>
      <Route path='withdraw' component={Withdraw}/>
      <Route path="settings" component={Settings}/>
      <Route path="password" component={Password}/>
      <Route path="wallets" component={WalletSettings}/>
      <Route path="pay-:amount" component={TransferList}/>
    </Route>
    {adminRoute}
  </Route>
  <Route path='logout' component={Logout}/>
</Route>

export const router = <Router history={browserHistory}>{routes}</Router>
