import 'babel-core/register'
import 'babel-polyfill'
import {setup} from '../../../client/main.jsx'
import {router} from './router.jsx'
import {boot} from '../../../common/globals'
import {systems, only} from '../../../client/finance/enums.jsx'

boot(function () {
  only(systems, ['perfect', 'nix', 'payeer'])
})

void setup(router)
