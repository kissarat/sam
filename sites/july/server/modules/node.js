module.exports = {
  $close: {method: 'POST', auth: {}},
  async close({id}) {
    id = +id
    if (!(id > 0)) {
      return {status: 400}
    }
    const node = await this.table('node')
      .where('id', id)
      .first()
    if (!node) {
      return {status: 404}
    }
    if (!this.user.admin && node.user !== this.user.id) {
      return {status: 403}
    }
    const [transfer] = await this.table('transfer')
      .insert({
        type: 'debit',
        status: 'success',
        to: node.user,
        amount: node.amount,
        node: node.id
      },
      ['id', 'created'])
    return {
      success: true,
      node,
      transfer
    }
  },

  $accrue: {method: 'GET', auth: {admin: true}},
  async accrue() {
    await this.sql('SELECT accrue()')
    return {success: true}
  }
}
