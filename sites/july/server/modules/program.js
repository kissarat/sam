module.exports = {
  $open: {method: 'POST', auth: {}},
  async open({id, amount}) {
    if (!(amount >= 1000)) {
      return {status: 400, error: {message: 'The amount must be great than $10'}}
    }
    if (0 !== amount % 100) {
      return {status: 400, error: {message: 'The amount should be multiples of $1'}}
    }
    const user_id = this.user.id
    const r = await this.sql('SELECT buy(?, ?, ?) as transfer_id',
      [id, user_id, amount])
    const transfer_id = r.rows[0].transfer_id
    if (transfer_id > 0) {
      return {success: true}
    }
    else {
      if (-402 === transfer_id) {
        const {balance} = await this.table('balance')
          .where({id: user_id})
          .first()
        return {
          status: 402,
          need: amount - balance,
          balance
        }
      }
      else {
        return {status: -transfer_id}
      }
    }
  },

  async index() {
    const programs = await this.table('program')
      .orderBy('id')
    return {programs}
  }
}
