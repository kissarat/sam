module.exports = {
  origin: 'http://admin.softroom.pro',
  id: 'ladder',
  name: 'Ladder',
  enabled: false,
  locals: {
    pretty: true
  },

  dir: __dirname,

  database: {
    id: 'ladder',
    report: false,
    pool: {min: 0, max: 1},
    connection: {
      host: 'localhost',
      database: 'october',
      user: 'october'
    },
  },

  money: {
    scale: 100,
    currency: 'EUR'
  },

  front: {
    enabled: true,
    authorize: '/user/authorize',
    origin: 'https://cash-ladder.com'
  },

  restrict: {
    enabled: false
  },

  blockio: {
    enabled: false,
  },

  domains: [
    'admin-ladder.softroom.pro'
  ]
}
