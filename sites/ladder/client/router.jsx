import Admin from '../../../client/admin/app.jsx'
import adminRouter from '../../../client/admin/router.jsx'
import api from '../../../client/connect/api.jsx'
import Login from '../../../client/outdoor/login.jsx'
import Logout from '../../../client/outdoor/logout.jsx'
import MatrixNodeList from './matrix-nodes.jsx'
import NodeList from './node/index.jsx'
import Outdoor from '../../../client/outdoor/app.jsx'
import ProgramList from './programs.jsx'
import React from 'react'
import Signup from '../../../client/outdoor/signup.jsx'
import {IndexRedirect, Router, Route, browserHistory} from 'react-router'

class LadderAdmin extends Admin {
  componentWillMount() {
    super.componentWillMount();
    this.menu.push(
        ['Очередь', '/queue'],
        ['Ступени', '/programs'],
        ['Статьи', '/articles']
    )
  }
}

export const routes = <Route path='/'>
  <IndexRedirect to="cabinet"/>
  <Route component={Outdoor}>
    <Route path='login' component={Login}/>
    <Route path='signup' component={Signup}/>
  </Route>
  <Route component={LadderAdmin} onEnter={api.checkAuthorized}>
    <Router path='nodes' component={NodeList}/>
    <Router path='queue' component={MatrixNodeList}/>
    <Router path='programs' component={ProgramList}/>
    {adminRouter}
  </Route>
  <Route path='logout' component={Logout}/>
</Route>

export const router = <Router history={browserHistory}>{routes}</Router>
