import 'babel-core/register'
import 'babel-polyfill'
import {setup} from '../../../client/main.jsx'
import {router} from './router.jsx'
import {boot} from '../../../common/globals'
import {currencies} from '../../../client/finance/enums.jsx'

boot(function () {
  currencies.EUR = 100
})

void setup(router)
