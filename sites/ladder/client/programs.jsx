import React from 'react'
import {Segment, Table, Icon} from 'semantic-ui-react'
import TablePage from '../../../client/widget/table-page.jsx'
import api from '../../../client/connect/api.jsx'


export default class ProgramList extends TablePage {
  uri = 'program/index'

  componentWillMount() {
    this.setState({order: {id: 1}})
  }

  async change(id, data) {
    await api.send('program/direction', {id}, data)
    this.load()
  }

  direction(row) {
    if (0 === row.start) {
      return <Table.Cell className="direction choose">
        <div className={-1 === row.direction ? 'active' : ''}
             onClick={() => this.change(row.id, {direction: -1})}>назад
        </div>
        <div className={0 === row.direction ? 'active' : ''}
             onClick={() => this.change(row.id, {direction: 0})}>вперед
        </div>
      </Table.Cell>
    }
    else {
      return <Table.Cell className="direction choose"/>
    }
  }

  items() {
    return this.state.items.map(r => <Table.Row key={r.id}>
      <Table.Cell>{r.id}</Table.Cell>
      <Table.Cell className="start choose">
        <div className={1 === r.start ? 'active' : ''}
             onClick={() => this.change(r.id, {start: 1})}>начало
        </div>
        <div className={0 === r.start ? 'active' : ''}
             onClick={() => this.change(r.id, {start: 0})}>середина
        </div>
        <div className={-1 === r.start ? 'active' : ''}
             onClick={() => this.change(r.id, {start: -1})}>конец
        </div>
      </Table.Cell>
      {this.direction(r)}
      <Table.Cell>{new Date(r.changed).toLocaleString()}</Table.Cell>
      <Table.Cell>
        <a className="graph" href={'/graph.html#matrix/' + r.id} target="_blank">
          <Icon name="external"/>
        </a>
      </Table.Cell>
    </Table.Row>)
  }

  render() {
    return <Segment className="page program-list">
      <h1>Ступени</h1>
      {this.dimmer()}
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Ступень</Table.HeaderCell>
            <Table.HeaderCell>Старт</Table.HeaderCell>
            <Table.HeaderCell>Направление</Table.HeaderCell>
            <Table.HeaderCell>Изменено</Table.HeaderCell>
            <Table.HeaderCell>Граф</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.items()}</Table.Body>
      </Table>
    </Segment>
  }
}
