import api from '../../../client/connect/api.jsx'
import React from 'react'
import TablePage, {Search} from '../../../client/widget/table-page.jsx'
import {Segment, Table, Icon, Button} from 'semantic-ui-react'

export default class MatrixNodeList extends TablePage {
  uri = 'node/index'

  getFilterParams() {
    return {view: 'user_matrix_node'}
  }

  async up(id) {
    await api.send('node/up', {id})
    return this.load()
  }

  async remove(id) {
    await api.send('node/remove', {id}, null, 'DELETE')
    return this.load()
  }

  items() {
    return this.state.items.map(item => <Table.Row key={item.id}>
      <Table.Cell>{item.id}</Table.Cell>
      <Table.Cell>{item.program}</Table.Cell>
      <Table.Cell>{item.nick}</Table.Cell>
      <Table.Cell>
        {item.count > 0
            ? <span>{item.count}&nbsp;<a className="graph" href={'/graph.html#matrix/' + item.id} target="_blank">
            <Icon name="external"/>
          </a></span>
            : <Button type="button" onClick={() => this.remove(item.id)}>Удалить из очереди</Button>}
      </Table.Cell>
      <Table.Cell>{new Date(item.created).toLocaleString()}</Table.Cell>
      <Table.Cell>
        {new Date(item.expires).toLocaleString()}
        &nbsp;
        <Icon name="refresh" onClick={() => this.update(item.id)}/>
      </Table.Cell>
      <Table.Cell>
        {item.parent > 0 ?
            <span>{item.parent}&nbsp;
              <a className="graph" href={'/graph.html#matrix/' + item.parent} target="_blank">
                <Icon name="external"/>
              </a>
            </span> : ''}
      </Table.Cell>
    </Table.Row>)
  }

  render() {
    return <Segment className="page node-index">
      <h1>Investments</h1>
      {this.dimmer()}
      {this.paginator()}
      <Search change={search => this.load({search})}/>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>ID</Table.HeaderCell>
            <Table.HeaderCell>Ступень</Table.HeaderCell>
            <Table.HeaderCell>Пользователь</Table.HeaderCell>
            <Table.HeaderCell>Дочерних</Table.HeaderCell>
            <Table.HeaderCell>Открыт</Table.HeaderCell>
            <Table.HeaderCell>Истекает</Table.HeaderCell>
            <Table.HeaderCell>Родительский</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.items()}</Table.Body>
      </Table>
    </Segment>
  }
}
