module.exports = {
  $call: {auth: {admin: true}},

  index() {
    return this.table('program_direction')
  },

  $direction: {method: 'POST'},
  direction({id}, data) {
    data.changed = new Date().toISOString()
    return this.table('program')
        .where('id', +id)
        .update(data)
  }
}
