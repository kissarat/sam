const views = [
  'matrix_node',
  'node_summary',
  'user_matrix_node',
  'user_node_summary'
]

module.exports = {
  $accrue: {auth: {nick: 'cron'}},
  async accrue() {
    await this.sql('SELECT accrue()')
    return {success: true}
  },

  $up: {method: 'POST', auth: {admin: true}},
  up({id}) {
    return this.table('node')
        .where('id', +id)
        .update({expires: this.sql("CURRENT_TIMESTAMP + interval '7 days'")})
  },

  $index: {auth: {admin: true}},
  index({view, search}) {
    if (views.indexOf(view) < 0) {
      return {status: 400}
    }
    const q = this.table(view)
    if ('string' === typeof search && (search = search.trim())) {
      for(const token of search.split(/[+\s]+/g)) {
        for(const key of ['nick']) {
          q.orWhere(key, 'ilike', `%${token}%`)
        }
      }
    }
    return q
  },

  $remove: {method: 'DELETE', auth: {admin: true}},
  remove({id}) {
    return this.table('node')
        .where('id', +id)
        .delete()
  }
}
