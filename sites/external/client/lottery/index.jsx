/**
 * Last modified: 18.07.14 23:05:36
 * Hash: a88615b391761faac123385a514923a47aa0b6b5
 */

import api from '../../../../client/connect/api.jsx'
import FormComponent from '../../../../client/base/form-component.jsx'
import moment from 'moment'
import React from 'react'
import t from "../../../../client/t";
import TablePage from '../../../../client/widget/table-page.jsx'
import {Button, Form, Icon, Input, Segment, Table} from 'semantic-ui-react'
import {browserHistory, Link} from 'react-router';
import {pick} from 'lodash';

// <Checkbox name="win" toggle checked={this.state.win} onChange={this.onChange}/>
// <label>только выигрыши</label>

class LotteryFilter extends FormComponent {
  state = {
    number: 0,
    win: false
  }

  submit() {
    const state = {
      number: +this.state.number,
      win: this.state.win ? 1 : 0
    }
    if (state.number >= 0 && Number.isInteger(state.number)) {
      this.props.filter(this.state)
    }
  }

  render() {
    return <Form className="filter" onSubmit={this.onSubmit}>
      <label>Розыгрыш:</label>
      <Input type="number" name="number" value={this.state.number} onChange={this.onChange}/>
      <Button>Применить</Button>
    </Form>
  }
}

export default class Lottery extends TablePage {
  uri = 'lottery/index'
  // state = {
  //   order: {id: -1}
  // }

  getFilterParams() {
    return pick(this.state, 'number')
  }

  async award(r, n) {
    const {success} = await api.send('lottery/award', {id: r.id, n})
    if (success) {
      await this.load()
    }
  }

  async remove(r) {
    const {success} = await api.del('lottery/remove', {id: r.id})
    if (success) {
      await this.load()
    }
  }

  action(r) {
    if (r.win > 0) {
      return <div>
        <span className="number">{r.n}</span>&nbsp;
        <span className="place-text">место</span>&nbsp;
        <span className="money usd">{r.win / 100}</span>
      </div>
    }
    return <div>
      <span className="prizes">
        <span className="prize first" onClick={() => this.award(r, 1)}>1</span>
        <span className="prize second" onClick={() => this.award(r, 2)}>2</span>
        <span className="prize third" onClick={() => this.award(r, 3)}>3</span>
      </span>
      <span className="actions">
        <Icon name="edit" onClick={() => browserHistory.push('/lottery/' + r.id)}/>
        <Icon name="trash" onClick={() => this.remove(r)}/>
      </span>
    </div>
  }

  items() {
    return this.state.items.map(r => <Table.Row key={r.id} id={r.id}>
      <Table.Cell>{r.id}</Table.Cell>
      <Table.Cell>{r.nick}</Table.Cell>
      <Table.Cell>{r.number}</Table.Cell>
      <Table.Cell>{r.amount}</Table.Cell>
      <Table.Cell>{moment(r.created).format('YY.MM.DD hh:mm:ss')}</Table.Cell>
      <Table.Cell>{this.action(r)}</Table.Cell>
    </Table.Row>)
  }

  render() {
    return <Segment className="page lottery">
      <h1>Лотерея</h1>
      {this.dimmer()}
      <Link to="/lottery/create">{t('Create')}</Link>
      <LotteryFilter filter={state => this.load(state)}/>
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>ID</Table.HeaderCell>
            <Table.HeaderCell>Пользователь</Table.HeaderCell>
            <Table.HeaderCell>Розыгрыш</Table.HeaderCell>
            <Table.HeaderCell>Билетов</Table.HeaderCell>
            <Table.HeaderCell>Время</Table.HeaderCell>
            <Table.HeaderCell>Действия</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.items()}</Table.Body>
      </Table>
    </Segment>
  }
}
