import api from '../../../../client/connect/api.jsx'
import FormComponent from '../../../../client/base/form-component.jsx'
import React from 'react'
import {Button, Form, Segment} from 'semantic-ui-react'
import {browserHistory} from "react-router";

export default class LotteryEdit extends FormComponent {
  state = {
    sponsor: '',
    number: 1,
    amount: 1
  }

  async load({id}) {
    id = +id
    if (id > 0) {
      const {result} = await api.get('lottery/get', {id})
      if (result) {
        this.setState(result)
      }
    }
  }

  componentDidMount() {
    void this.load(this.props.params)
  }

  componentWillReceiveProps(props) {
    void this.load(props.params)
  }

  async submit() {
    const lottery = {
      nick: this.state.nick,
      number: +this.state.number,
      amount: +this.state.amount
    }
    const {success} = this.props.params && this.props.params.id > 0
      ? await api.send('lottery/save', {id : this.props.params.id}, lottery)
      : await api.send('lottery/save', lottery)
    if (success) {
      browserHistory.push('/lottery')
    }
  }

  get isEdit() {
    return this.props.params.id > 0
  }

  render() {
    return <Segment className="page lottery-edit">
      <h1>{this.isEdit ? 'Редактирования билетов #' + this.props.params.id : 'Создание билетов'}</h1>
      <Form className="filter" onSubmit={this.onSubmit}>
        <Form.Input
          name="nick"
          label="Пользователь"
          value={this.state.nick}
          onChange={this.onChange}
        />
        <Form.Input
          name="number"
          label="Розыгрыш"
          value={this.state.number || 1}
          onChange={this.onChange}
        />
        <Form.Input
          name="amount"
          label="Билетов"
          value={this.state.amount || 1}
          onChange={this.onChange}
        />
        <Button>{this.isEdit ? 'Сохранить' : 'Создать'}</Button>
      </Form>
    </Segment>
  }
}
