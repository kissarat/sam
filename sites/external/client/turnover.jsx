import React from 'react'
import TablePage from '../../../client/widget/table-page.jsx'
import Money from '../../../client/widget/money.jsx'
import {Segment, Table} from 'semantic-ui-react'
import {currencies} from '../../../client/finance/enums.jsx'

function object(array) {
  const result = {}
  for (const currency in currencies) {
    result[currency] = array instanceof Array
        ? array.filter(t => t.currency === currency).reduce((a, t) => a + t.amount, 0) : 0
  }
  return result
}

function header() {
  return Object.keys(currencies).map(currency => <th key={currency}>{currency}</th>)
}

function money(item) {
  return Object.keys(currencies).map(currency =>
      <Money as={Table.Cell} value={item[currency]} currency={currency}/>)
}

export default class Turnover extends TablePage {
  uri = 'user/turnover'

  componentWillMount() {
    this.setState({order: {consolidated: -1}})
  }

  items() {
    return this.state.items.map(function (item) {
      return <Table.Row key={item.id} className={item.status}>
        <Table.Cell>{item.id}</Table.Cell>
        <Table.Cell>{item.nick}</Table.Cell>
        {money(object(item.buy))}
        {money(object(item.first))}
        {money(object(item.five))}
        {money(object(item.all))}
        <Table.Cell>{item.referral}</Table.Cell>
        <Table.Cell>{item.team}</Table.Cell>
        {money(object(item.withdraw))}
        {money(object(item.accrue))}
        <Table.Cell>{item.sponsor}</Table.Cell>
      </Table.Row>
    })
  }

  render() {
    return <Segment className="page node-index">
      <h1>Оборот</h1>
      {this.dimmer()}
      {this.paginator()}
      <Table>
        <Table.Header>
          <Table.Row>
            <th rowSpan={2}>ID</th>
            <th rowSpan={2}>Пользователь</th>
            <th colSpan={3}>Вклад</th>
            <th colSpan={3}>Первая</th>
            <th colSpan={3}>Пятая</th>
            <th colSpan={3}>Всех</th>
            <th rowSpan={2}>Рефералы</th>
            <th rowSpan={2}>Команда</th>
            <th colSpan={3}>Вывод</th>
            <th colSpan={3}>Заработано</th>
            <th rowSpan={2}>Спосор</th>
          </Table.Row>
          <Table.Row>
            {header()}
            {header()}
            {header()}
            {header()}
            {header()}
            {header()}
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.items()}</Table.Body>
      </Table>
    </Segment>
  }
}
