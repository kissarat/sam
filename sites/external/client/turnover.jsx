/**
 * Last modified: 18.07.21 20:10:19
 * Hash: a437e6de8c703fbea89c7eb5917db06d6109ace8
 */

import Money from '../../../client/widget/money.jsx'
import React from 'react'
import TablePage from '../../../client/widget/table-page.jsx'
import {currencies} from '../../../client/finance/enums.jsx'
import {pick, size} from 'lodash';
import {Segment, Table} from 'semantic-ui-react'

const COUNT = size(currencies)

function sum(array) {
  const result = {}
  for (const currency in currencies) {
    result[currency] = array instanceof Array
        ? array.filter(t => t.currency === currency).reduce((a, t) => a + t.amount, 0) : 0
  }
  return result
}

function header() {
  return Object.keys(currencies).map(currency => <th key={currency}>{currency}</th>)
}

function money(item) {
  return Object.keys(currencies).map(currency =>
      <Money
        key={item.id} as={Table.Cell}
        value={item[currency]}
        currency={currency}/>)
}

export default class Turnover extends TablePage {
  uri = 'user/turnover'

  componentWillMount() {
    this.setState({order: {consolidated: -1}})
  }

  items() {
    return this.state.items.map(function (item) {
      return <Table.Row key={item.id} className={item.status}>
        <Table.Cell>{item.id}</Table.Cell>
        <Table.Cell>{item.nick}</Table.Cell>
        {money(sum(item.buy))}
        {money(sum(item.first))}
        {money(sum(item.all))}
        <Table.Cell>{item.referral}</Table.Cell>
        <Table.Cell>{item.team}</Table.Cell>
        {money(sum(item.withdraw))}
        {money(sum(item.accrue))}
        <Table.Cell>{item.sponsor}</Table.Cell>
      </Table.Row>
    })
  }

  render() {
    return <Segment className="page node-index">
      <h1>Оборот</h1>
      {this.dimmer()}
      {this.paginator()}
      <Table>
        <Table.Header>
          <Table.Row>
            <th rowSpan={2}>ID</th>
            <th rowSpan={2}>Пользователь</th>
            <th colSpan={COUNT}>Вклад</th>
            <th colSpan={COUNT}>Первая</th>
            <th colSpan={COUNT}>Всех</th>
            <th rowSpan={2}>Рефералы</th>
            <th rowSpan={2}>Команда</th>
            <th colSpan={COUNT}>Вывод</th>
            <th colSpan={COUNT}>Заработано</th>
            <th rowSpan={2}>Спосор</th>
          </Table.Row>
          <Table.Row>
            {header()}
            {header()}
            {header()}
            {header()}
            {header()}
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.items()}</Table.Body>
      </Table>
    </Segment>
  }
}
