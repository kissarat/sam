/**
 * Last modified: 01.01.01 00:00:00
 * Hash: 0000000000000000000000000000000000000000
 */

import React, {Component} from 'react'
import {Checkbox, Header, Segment} from 'semantic-ui-react'
import api from '../../../client/connect/api.jsx'

const locales = [
  {id: 'ru', name: 'Русский'},
  {id: 'en', name: 'English'}
]

const systems = [
  {
    "name": "Perfect Money",
    "currency": "USD",
    "system": "perfect"
  },
  {
    "name": "Payeer",
    "currency": "USD",
    "system": "payeer"
  },
  {
    "name": "AdvCash",
    "currency": "USD",
    "system": "advcash"
  },
  {
    "name": "Bitcoin",
    "currency": "BTC",
    "system": "blockio"
  },
  {
    "name": "Ethereum",
    "currency": "ETH",
    "system": "ethereum"
  },
  {
    "name": "Ethereum Classic",
    "currency": "ETC",
    "system": "ethereum"
  },
  {
    "name": "Litecoin",
    "currency": "LTC",
    "system": "blockio"
  },
  {
    "name": "Dogecoin",
    "currency": "DOGE",
    "system": "blockio"
  }
]

export default class Settings extends Component {
  state = {
    busy: false,
    locales,
    systems,
  }

  componentDidMount() {
    this.setState({busy: true})
    this.load()
  }

  async load() {
    const systems = this.state.systems
    const {result} = await api.get('settings/list', {key: 'systems'})
    let r = await api.get('settings/list', {key: 'currencies'})
    for (const system of systems) {
      system.enable = result.indexOf(system.system) >= 0 && r.result.indexOf(system.currency) >= 0
    }
    const locales = this.state.locales
    r = await api.get('settings/list', {key: 'locales'})
    for (const locale of locales) {
      locale.enable = r.result.indexOf(locale.id) >= 0
    }
    this.setState({systems, locales, busy: false})
  }

  async changeList(key, value, enable) {
    const {success} = await api.get(enable ? 'settings/push' : 'settings/del', {key, value})
    return success
  }

  async change(s) {
    if (this.busy) {
      return
    }
    this.setState({busy: true})
    if (s.id) {
      await this.changeList('locales', s.id, !s.enable)
    }
    else {
      s.enable = !s.enable
      const systems = this.state.systems
      systems.find(a => a.name === s.name).enable = s.enable
      const isUSD = 'USD' === s.currency
      const count = systems.filter(a => a.enable && (isUSD ? s.currency === a.currency : s.system === a.system)).length
      await this.changeList(isUSD ? 'systems' : 'currencies', isUSD ? s.system : s.currency, s.enable)
      if (count === (s.enable ? 1 : 0)) {
        await this.changeList(isUSD ? 'currencies' : 'systems', isUSD ? s.currency : s.system, s.enable)
      }
    }
    await this.load()
  }

  getList(list) {
    return list.map((s, i) => <div className="field" key={i}>
      <span>{s.name}</span>
      <Checkbox toggle checked={s.enable} onChange={() => this.change(s)}/>
    </div>)
  }

// <Dimmer inverted active={this.state.busy}>
// <Loader/>
// </Dimmer>

  render() {
    return <Segment.Group horizontal className="page settings">
      <Segment>
        <Header>Платежные системы</Header>
        <div className="list">
          {this.getList(this.state.systems)}
        </div>
      </Segment>
      <Segment>
        <Header>Языки</Header>
        <div className="list">
          {this.getList(this.state.locales)}
        </div>
      </Segment>
    </Segment.Group>
  }
}
