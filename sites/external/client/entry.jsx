/**
 * Last modified: 18.11.02 03:07:42
 * Hash: 2fbf14ccd61d7dd6bae528fec0391e11ad67e138
 */

import 'babel-core/register'
import 'babel-polyfill'
import {setup} from '../../../client/main.jsx'
import {router} from './router.jsx'
import {boot} from '../../../common/globals'
import {currencies, types} from '../../../client/finance/enums.jsx'

boot(function () {
  // types.lottery = 'Покупка билетов'
  // types.win = 'Выигрыш'
  Object.assign(currencies, {
    USD: 100,
    BTC: 1000000,
    DOGE: 10,
    // ETC: 100000,
    LTC: 100000,
    // XRP: 100,
  })
})

void setup(router)
