/**
 * Last modified: 18.07.10 07:21:51
 * Hash: f70ea45311082c3b1b9dedb2e0edc9959cb5d45f
 */

const config = require('../../../client/webpack-default')

config.entry = __dirname + '/entry.jsx'
config.output.filename = 'main.js'
config.output.path = __dirname + '/../public/scripts'

module.exports = config
