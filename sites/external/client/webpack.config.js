const config = require('../../../client/webpack-default')

config.entry = __dirname + '/entry.jsx'
config.output.filename = 'main.js'
config.output.path = __dirname + '/../public/scripts'

module.exports = config
