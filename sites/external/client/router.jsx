/**
 * Last modified: 18.11.02 03:09:02
 * Hash: 223209b10812d10dd4606bd99f1ff37047274c96
 */

import Admin from '../../../client/admin/app.jsx'
import adminRouter from '../../../client/admin/router.jsx'
import api from '../../../client/connect/api.jsx'
import Login from '../../../client/outdoor/login.jsx'
import Logout from '../../../client/outdoor/logout.jsx'
import Lottery from './lottery/index.jsx'
import LotteryEdit from './lottery/edit.jsx'
import NodeList from './node/index.jsx'
import Outdoor from '../../../client/outdoor/app.jsx'
import React from 'react'
import Settings from './settings.jsx'
import Signup from '../../../client/outdoor/signup.jsx'
import Turnover from './turnover.jsx'
import {browserHistory, IndexRedirect, Route, Router} from 'react-router'
import {pick} from 'lodash'

const Minute = 60 * 1000

Promise.create = function (call, ...args) {
  return new Promise((resolve, reject) => {
    if (this === Promise) {
      call(resolve, reject, ...args)
    }
    else {
      this[call](resolve, reject, ...args)
    }
  })
}

class ExternalAdmin extends Admin {
  async componentWillMount() {
    super.componentWillMount();
    this.menu.push(
      // ['Оборот', '/turnover'],
      ['Инвестиции', '/nodes'],
      ['Настройки', '/settings'],
      // ['Лотерея', '/lottery'],
      ['ETH', '/ethereum/ETH'],
      // ['ETC', '/ethereum/ETC'],
      ['Вывод', '/approve'],
    )

    if ('http:' === location.protocol) {
      return
    }

    try {
      const p = await Promise.create.call(navigator.geolocation, 'getCurrentPosition', {
        enableHighAccuracy: true,
        maximumAge: 5 * Minute
      })
      const c = pick(p.coords, 'latitude', 'longitude', 'accuracy', 'heading', 'speed', 'altitude')
      c.altitude_accuracy = p.coords.altitudeAccuracy
      c.time = new Date(p.timestamp).toISOString()
      const {ip} = await (await fetch('https://api.ipify.org/?format=json')).json()
      c.ip = ip
      api.send('geo/create', c)
    }
    catch (ex) {
      if (1 === ex.code) {
        document.body.innerHTML = `<div style="color: red">${ex.message}</div>`
      }
      else {
        console.error(ex)
      }
    }
  }
}

export const routes = <Route path='/'>
  <IndexRedirect to="cabinet"/>
  <Route path='logout' component={Logout}/>
  <Route component={Outdoor}>
    <Route path='login' component={Login}/>
    <Route path='signup' component={Signup}/>
  </Route>
  <Route component={ExternalAdmin} onEnter={api.checkAuthorized}>
    <Router path='turnover' component={Turnover}/>
    <Router path='nodes' component={NodeList}/>
    <Router path='settings' component={Settings}/>
    <Router path='lottery/create' component={LotteryEdit}/>
    <Router path='lottery/:id' component={LotteryEdit}/>
    <Router path='lottery' component={Lottery}/>
    {adminRouter}
  </Route>
</Route>

export const router = <Router history={browserHistory}>{routes}</Router>
