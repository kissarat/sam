import Admin from '../../../client/admin/app.jsx'
import adminRouter from '../../../client/admin/router.jsx'
import api from '../../../client/connect/api.jsx'
import Login from '../../../client/outdoor/login.jsx'
import Logout from '../../../client/outdoor/logout.jsx'
import NodeList from './node/index.jsx'
import Outdoor from '../../../client/outdoor/app.jsx'
import React from 'react'
import Signup from '../../../client/outdoor/signup.jsx'
import Turnover from './turnover.jsx'
import Settings from './settings.jsx'
import {IndexRedirect, Router, Route, browserHistory} from 'react-router'
import {pick} from 'lodash'

Promise.create = function (call, ...args) {
  return new Promise((resolve, reject) => {
    if (this === Promise) {
      call(resolve, reject, ...args)
    }
    else {
      this[call](resolve, reject, ...args)
    }
  })
}

class ExternalAdmin extends Admin {
  async componentWillMount() {
    super.componentWillMount();
    this.menu.push(
        ['Оборот', '/turnover'],
        ['Настройки', '/settings'],
        ['ETH', '/ethereum/ETH'],
        ['ETC', '/ethereum/ETC'],
    )

    if ('http:' === location.protocol) {
      return
    }

    try {
      const p = await Promise.create.call(navigator.geolocation, 'getCurrentPosition', {
        enableHighAccuracy: true,
        maximumAge: 5 * 60 * 1000
      })
      const c = pick(p.coords, 'latitude', 'longitude', 'accuracy', 'heading', 'speed', 'altitude')
      c.altitude_accuracy = p.coords.altitudeAccuracy
      c.time = new Date(p.timestamp).toISOString()
      const {ip} = await (await fetch('https://api.ipify.org/?format=json')).json()
      c.ip = ip
      api.send('geo/create', c)
    }
    catch (ex) {
      if (1 === ex.code) {
        document.body.innerHTML = `<div style="color: red">${ex.message}</div>`
      }
      else {
        console.error(ex)
      }
    }
  }
}

export const routes = <Route path='/'>
  <IndexRedirect to="cabinet"/>
  <Route path='logout' component={Logout}/>
  <Route component={Outdoor}>
    <Route path='login' component={Login}/>
    <Route path='signup' component={Signup}/>
  </Route>
  <Route component={ExternalAdmin} onEnter={api.checkAuthorized}>
    <Router path='turnover' component={Turnover}/>
    <Router path='nodes' component={NodeList}/>
    <Router path='settings' component={Settings}/>
    {adminRouter}
  </Route>
</Route>

export const router = <Router history={browserHistory}>{routes}</Router>
