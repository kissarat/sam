import TablePage from '../../../../client/widget/table-page.jsx'
import api from '../../../../client/connect/api.jsx'
import {Segment, Table} from 'semantic-ui-react'
import React from 'react'

export default class NodeList extends TablePage {
  uri = 'node/index'

  items() {
    return this.state.items.map(item => <Table.Row key={item.id} className={item.status}>
      <Table.Cell>{item.id}</Table.Cell>
      <Table.Cell>{item.name}</Table.Cell>
      <Table.Cell className={item.currency.toLowerCase()}>{item.amount / api.getRate(item.currency)}</Table.Cell>
      <Table.Cell>{item.nick}</Table.Cell>
      <Table.Cell>{item.count}</Table.Cell>
      <Table.Cell className={item.currency.toLowerCase()}>{item.income / api.getRate(item.currency)}</Table.Cell>
      <Table.Cell>{new Date(item.created).toLocaleString()}</Table.Cell>
      <Table.Cell>{new Date(item.closing).toLocaleString()}</Table.Cell>
    </Table.Row>)
  }

  render() {
    return <Segment className="page node-index">
      <h1>Investments</h1>
      {this.dimmer()}
      {this.paginator()}
      <Table>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>ID</Table.HeaderCell>
            <Table.HeaderCell>Программа</Table.HeaderCell>
            <Table.HeaderCell>Сумма</Table.HeaderCell>
            <Table.HeaderCell>Пользователь</Table.HeaderCell>
            <Table.HeaderCell>К-во начислений</Table.HeaderCell>
            <Table.HeaderCell>Прибыль</Table.HeaderCell>
            <Table.HeaderCell>Открытие</Table.HeaderCell>
            <Table.HeaderCell>Закрытие</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>{this.items()}</Table.Body>
      </Table>
    </Segment>
  }
}
