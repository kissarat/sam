/**
 * Last modified: 18.07.15 18:37:00
 * Hash: 3693c18afe460c1b672c945d3f5b7b2779a0819d
 */

const awards = {
  1: 0.5,
  2: 0.2,
  3: 0.1
}

const bonuses = {
  1: 0.05,
  2: 0.03,
  3: 0.02
}

module.exports = {
  $call: {auth: {admin: true}},

  index({number, win}) {
    let q = this.table('admin_lottery')
    if (number > 0) {
      q = q.where({number: +number})
    }
    if (win > 0) {
      q = q.whereSQL('win not null')
    }
    return q
  },

  async get({id}) {
    const result = await this.table('lottery')
      .where({id})
      .first()
    if (!result) {
      return {status: 404}
    }
    const user = await this.table('user')
      .where({id: result.user})
      .first()
    result.nick = user.nick
    return {result}
  },

  async save({id}, data) {
    id = +id
    if (!(data.number > 0 || data.amount > 0)) {
      return {status: 400}
    }
    const user = await this.table('user')
      .where({nick: data.nick})
      .first()
    if (user) {
      data.user = user.id
      delete data.nick
    }
    else {
      return {status: 404}
    }
    let lottery = await this.table('lottery')
      .where({
        user: data.user,
        number: data.number
      })
      .first()
    if (lottery) {
      [lottery] = await this.table('lottery')
        .where({id: lottery.id})
        .update(data, ['id'])
    }
    else {
      [lottery] = await this.table('lottery')
        .insert(data, ['id'])
    }
    return {
      success: lottery && lottery.id > 0,
      lottery
    }
  },

  $remove: {method: 'DELETE'},
  async remove({id}) {
    const lottery_transfers = await this.table('lottery_transfer')
      .where({lottery: id})
    const [lottery] = await this.table('lottery')
      .where({id}).del(['id'])
    const transfers = []
    for (const {transfer} of lottery_transfers) {
      const t = await this.table('transfer')
        .where({id: transfer})
        .del(['id'])
      transfers.push(...t)
    }
    await this.table('lottery_transfer')
      .where({lottery: id})
      .del()
    return {
      success: true,
      lottery_transfers,
      transfers,
      lottery
    }
  },

  async award({id, n}) {
    id = +id
    n = +n
    if (!(id > 0 || n > 0)) {
      return {
        status: 400,
        error: {
          message: 'Invalid arguments'
        }
      }
    }
    const lottery = await this.table('lottery')
      .where({id})
      .first()
    if (!lottery || !(lottery.id > 0)) {
      return {status: 404}
    }
    if ('number' !== typeof awards[n]) {
      return {status: 400}
    }
    const previous = await this.table('admin_lottery')
      .where({
        number: lottery.number,
        n
      })
      .first(['id'])
    if (previous && previous.id > 0) {
      await this.table('transfer')
        .where({
          node: previous.id,
          type: 'win',
          status: 'success',
          n
        })
        .del()
      await this.table('transfer')
        .where({
          node: previous.id,
          type: 'bonus',
          status: 'success',
          n
        })
        .del()
    }
    const [{sum: fund}] = await this.table('lottery')
      .where({number: +lottery.number})
      .sum('amount')
    if (!(fund > 0)) {
      return {status: 402}
    }
    if (!(awards[n] > 0)) {
      return {status: 400}
    }

    const amount = Math.floor(100 * fund * awards[n])
    const bonusAmount = Math.floor(100 * fund * bonuses[n])
    await this.log('lottery', 'award', {id, n})
    // console.log(lottery, n, amount)
    const [prize] = await this.table('transfer')
      .insert({
          to: lottery.user,
          type: 'win',
          status: 'success',
          node: lottery.id,
          n,
          amount,
          currency: 'USD'
        },
        ['id'])
    // console.log(prize)
    if (prize) {
      const user = await this.table('user').where({id: lottery.user}).first()
      let bonuses
      const hasParent = user.parent > 0
      if (user && hasParent) {
        bonuses = await this.table('transfer')
          .insert({
              to: user.parent,
              type: 'bonus',
              status: 'success',
              node: lottery.id,
              n,
              amount: bonusAmount,
              currency: 'USD'
            },
            ['id'])
      }
      const bonus = bonuses && bonuses.length > 0 ? bonuses[0] : null
      return {
        success: !hasParent || !!bonus,
        prize,
        bonus
      }
    }
    return {success: false}
  }
}
