/**
 * Last modified: 18.06.03 16:36:36
 * Hash: ce432463a623b14515e4068ef208c261b2461740
 */

module.exports = {
  $call: {auth: {admin: true}},

  index() {
    return this.table('lottery')
  }
}
