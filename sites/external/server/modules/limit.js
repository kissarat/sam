const {pick} = require('lodash')

const fields = ['date', 'currency', 'program']

module.exports = {
  $call: {auth: {admin: true}},

  index() {
    return this.table('date_limit')
  },

  $create: {method: 'POST'},
  async create(data) {
    const result = await this.table('limit')
      .insert(data, fields)
    return {success: true, result}
  },

  $save: {method: 'POST'},
  async save({date}, data) {
    let result = []
    for(const limit of data) {
      const where = pick(limit, fields)
      where.date = date
      result = result.concat(await this.table('limit')
        .where(where)
        .update(pick(limit, 'date', 'amount'), fields))
    }
    return {success: true, result}
  },

  $remove: {method: 'DELETE'},
  async remove({date}) {
    const result = await this.table('limit')
        .where({date})
        .del(fields)
    return {success: true, result}
  }
}
