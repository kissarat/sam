module.exports = {
  $accrue: {auth: {nick: 'cron'}},
  async accrue() {
    await this.sql('SELECT accrue()')
    return {success: true}
  },

  $index: {auth: {admin: true}},
  index() {
    return this.table('node_summary')
  }
}
