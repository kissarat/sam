module.exports = {
  $call: {auth: {admin: true}},

  async get({key}) {
    const result = await this.site.redis.get(key)
    return {result}
  },

  async set({key, value}) {
    return {success: 'OK' === await this.site.redis.set(key, value)}
  },

  async list({key}) {
    const result = await this.site.redis.lrange(key, 0, -1)
    return {result}
  },

  async push({key, value}) {
    const result = await this.site.redis.rpush(key, value);
    return {success: result > 0, result}
  },

  async del({key, value}) {
    const result = await this.site.redis.lrem(key, 1, value)
    return {success: result > 0, result}
  }
}
