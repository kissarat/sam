module.exports = {
  origin: 'http://localhost',
  id: 'external',
  name: 'External',
  enabled: false,
  locals: {
    pretty: true
  },

  dir: __dirname,

  database: {
    id: 'external',
    report: false,
    pool: {min: 0, max: 1}
  },

  remote: {
    enabled: true,
    clients: {
      perfect: {
        uri: 'https://perfectmoney.is/acct/confirm.asp',
        method: 'POST',
      },
      advcash: {
        uri: 'https://wallet.advcash.com/wsm/merchantWebService',
        method: 'POST',
        headers: {
          'content-type': 'application/xml'
        }
      }
    }
  },

  advcash: {
    enabled: true,
  },

  money: {
    scale: 100,
    currency: 'USD'
  },

  front: {
    enabled: true,
    authorize: '/user/authorize'
  },

  restrict: {
    enabled: true
  },

  blockio: {
    enabled: true,
    keys: {
      BTC: '78ee-f08f-ed34-e151',
      LTC: '9e32-9760-a81b-8c3a',
      DOGE: '4642-56c7-8843-cddc'
    }
  },

  redis: {
    enabled: true
  },

  domains: [
    'admin.ai-logist.com'
  ]
}
