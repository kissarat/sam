/**
 * Last modified: 18.07.10 07:21:51
 * Hash: 82478386d69fc3658480420a578546bf91b78294
 */

module.exports = {
  id: 'external',
  name: 'External',
  enabled: false,
  locals: {
    pretty: true
  },

  dir: __dirname,

  database: {
    id: 'external',
    report: false,
    pool: {min: 0, max: 1}
  },

  remote: {
    enabled: true,
    clients: {
      perfect: {
        uri: 'https://perfectmoney.is/acct/confirm.asp',
        method: 'POST',
      },
      advcash: {
        uri: 'https://wallet.advcash.com/wsm/merchantWebService',
        method: 'POST',
        headers: {
          'content-type': 'application/xml'
        }
      }
    }
  },

  advcash: {
    enabled: true,
  },

  money: {
    scale: 100,
    currency: 'USD'
  },

  front: {
    enabled: true,
    authorize: '/user/authorize'
  },

  restrict: {
    enabled: true
  },

  blockio: {
    enabled: true,
    keys: {
    }
  },

  redis: {
    enabled: true
  },

  domains: [
  ]
}
