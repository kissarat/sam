CREATE VIEW token_user AS
  SELECT
    t.created,
    t.id,
    t.ip,
    t.time,
    t.type,
    t.expires,
    u.email,
    u.nick,
    u.admin,
    u.avatar,
    u.parent,
    row_to_json(u.*) AS "user"
  FROM token t
    LEFT JOIN "user" u ON t.user = u.id;

CREATE OR REPLACE VIEW statistics AS
  SELECT
    coalesce((SELECT sum(usd)
              FROM transfer_rate
              WHERE type = 'payment' AND status = 'success'), 0) :: BIGINT  AS payment,
    coalesce((SELECT sum(usd)
              FROM transfer_rate
              WHERE type = 'payment' AND status = 'pending'), 0) :: BIGINT  AS pending,
    coalesce((SELECT sum(usd)
              FROM transfer_rate
              WHERE type = 'withdraw' AND status = 'success'), 0) :: BIGINT AS withdraw,
    coalesce((SELECT sum(usd)
              FROM transfer_rate
              WHERE type = 'accrue' AND status = 'success'), 0) :: BIGINT   AS accrue,
    coalesce((SELECT sum(usd)
              FROM transfer_rate
              WHERE type = 'buy' AND status = 'success'), 0) :: BIGINT      AS buy,
    (SELECT count(*)
     FROM "user")                                                           AS users,
    (SELECT count(*)
     FROM "user" ud
     WHERE ud.created > CURRENT_TIMESTAMP -
                        INTERVAL '1 day')                                   AS users_day,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires >
                                CURRENT_TIMESTAMP)                          AS visitors,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires > CURRENT_TIMESTAMP
           AND handshake > CURRENT_TIMESTAMP -
                           INTERVAL '1 day')                                AS visitors_day,
    0                                                                       AS new_visitors_day,
    0                                                                       AS activities_day,
    (SELECT count(*)
     FROM visit
     WHERE url LIKE '/ref/%' AND visit.created > CURRENT_TIMESTAMP -
                                                 INTERVAL '1 day')          AS referral_day,
    0                                                                       AS balance;
