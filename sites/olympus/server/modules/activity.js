const _ = require('lodash');

module.exports = {
  $call: {admin: true},

  $index: {method: 'GET'},
  index() {
    return this.table('activity')
  },

  $edit: {method: 'POST'},
  async edit({id}, data) {
    id = +id;
    data = _.omit(data, 'id');
    let t = this.table('activity');
    t = id > 0 ? t.update({id}, data) : t.insert(data);
    return {
      success: true,
      activity: await t.returning('id')
    }
  },
}
