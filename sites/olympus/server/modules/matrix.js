/**
 * Last modified: 18.09.16 06:27:29
 * Hash: 74125515c0c39bf29e3bf2e1365d7fd4632ace8a
 */

module.exports = {
  $call: {auth: {admin: true}},

  $index: {method: 'GET'},
  index({program, active, nick}) {
    program = +program
    active = +active
    let q = this.table('matrix_node')
    if (program > 0) {
      q = q.where({program})
    }
    if (active > 0) {
      q = q.where({active: true})
    }
    if (nick && nick.trim()) {
      q = q.where({nick: nick.trim()})
    }
    return q
  },

  $qualification: {method: 'POST'},
  async qualification({id}, {number}) {
    const result = await this.table('node')
        .where({id: +id})
        .update({qualified: +number})
        .returning(['id', 'qualified'])
    return {
      success: 1 === result.length,
      result
    }
  }
}
