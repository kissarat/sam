/**
 * Last modified: 18.06.06 05:22:11
 * Hash: f76471c1036510c15911232bb4fd35e6de59f629
 */

module.exports = {
  origin: 'http://local-admin-olympus.administer.tech',
  id: 'olympus',
  name: 'olympus',
  enabled: false,
  locals: {
    pretty: true
  },

  dir: __dirname,

  database: {
    id: 'olympus',
    report: false,
    pool: {min: 0, max: 1}
  },

  remote: {
    enabled: true,
    clients: {
      perfect: {
        uri: 'https://perfectmoney.is/acct/confirm.asp',
        method: 'POST',
      },
      advcash: {
        uri: 'https://wallet.advcash.com/wsm/merchantWebService',
        method: 'POST',
        headers: {
          'content-type': 'application/xml'
        }
      }
    }
  },

  advcash: {
    enabled: true,
  },

  money: {
    scale: 100,
    currency: 'USD'
  },

  front: {
    enabled: true,
    authorize: '/user/authorize'
  },

  restrict: {
    enabled: false
  },

  redis: {
    enabled: true
  },

  domains: [
    'local-admin-olympus.administer.tech',
    'test-admin-olympus.administer.tech',
    'sadok.ocean-of-ethereum.com',
  ]
}
