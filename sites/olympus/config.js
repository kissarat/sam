/**
 * Last modified: 18.07.27 20:57:06
 * Hash: fcc2ee92d554b1ae80eb540935f18e7189f6d56e
 */

module.exports = {
  id: 'olympus',
  name: 'olympus',
  enabled: false,
  locals: {
    pretty: true
  },

  dir: __dirname,

  database: {
    id: 'olympus',
    report: false,
    pool: {min: 0, max: 1}
  },

  remote: {
    enabled: true,
    clients: {
      perfect: {
        uri: 'https://perfectmoney.is/acct/confirm.asp',
        method: 'POST',
      },
      advcash: {
        uri: 'https://wallet.advcash.com/wsm/merchantWebService',
        method: 'POST',
        headers: {
          'content-type': 'application/xml'
        }
      }
    }
  },

  advcash: {
    enabled: true,
  },

  money: {
    scale: 100,
    currency: 'USD'
  },

  front: {
    enabled: true,
    authorize: '/user/authorize'
  },

  restrict: {
    enabled: false
  },

  redis: {
    enabled: true
  },

  domains: [
  ]
}
