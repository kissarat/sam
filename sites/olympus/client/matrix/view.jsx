import React, {Component} from 'react'
import api from '../../../../client/connect/api.jsx'
import {Segment} from "semantic-ui-react";

export default class MatrixView extends Component {
  getUrl() {
    return api.getFrontURL(`/matrix/${api.config.user.nick}/` + this.props.params.id, {
      'access-token': api.token,
      'direct': 1
    })
  }

  render() {
    return <Segment className="matrix view">
      <iframe src={this.getUrl()}/>
    </Segment>
  }
}
