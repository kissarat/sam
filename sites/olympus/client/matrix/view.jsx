import React, {Component} from 'react'
import api from '../../../../client/connect/api.jsx'
import {Segment} from "semantic-ui-react";

export default class MatrixView extends Component {
  getUrl() {
    return api.getFrontURL(`/matrix/${api.config.user.nick}/15`, {'access-token': api.token})
  }

  render() {
    return <Segment className="matrix view">
      <iframe src={this.getUrl()}></iframe>
    </Segment>
  }
}
