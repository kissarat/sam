import React from 'react'
import api from '../../../../client/connect/api.jsx'
import {Segment, Table} from "semantic-ui-react"
import TablePage from "../../../../client/widget/table-page.jsx"
import {Link} from "react-router"

export default class MatrixList extends TablePage {
  uri = 'matrix/index'

  load = async (params = {}) => {
    const state = await this.loadItems(params)
    console.log(state)
    const {result} = await api.get('program/index')
    if (result instanceof Array) {
      const programs = {}
      for (const program of result) {
        programs[program.id] = program
      }
      state.programs = programs
      this.setState(state)
    }
  }

  rows() {
    return this.state.items.map(item => {
      const program = this.state.programs[item.program]
      return <Table.Row>
        <Table.Cell>{item.id}</Table.Cell>
        <Table.Cell>{item.nick}</Table.Cell>
        <Table.Cell>{program.name}</Table.Cell>
        <Table.Cell>{item.count} / {program.size}</Table.Cell>
        <Table.Cell><Link to={'/matrix/' + item.id}>view</Link></Table.Cell>
      </Table.Row>
    })
  }

  render() {
    if (this.state.programs) {
      return <Segment className="matrix index">
        {this.dimmer()}
        <h1>Матрицы</h1>
        <Table>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>ID</Table.HeaderCell>
              <Table.HeaderCell>Пользователь</Table.HeaderCell>
              <Table.HeaderCell>Программа</Table.HeaderCell>
              <Table.HeaderCell>Заполнение</Table.HeaderCell>
              <Table.HeaderCell/>
            </Table.Row>
          </Table.Header>
          <Table.Body>{this.rows()}</Table.Body>
        </Table>
      </Segment>
    }
    return this.dimmer()
  }
}
