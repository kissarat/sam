/**
 * Last modified: 18.09.16 06:29:29
 * Hash: c44f5bdcdc31beae65d42e29fea9fa0cad97b886
 */

import api from '../../../../client/connect/api.jsx'
import React from 'react'
import TablePage from "../../../../client/widget/table-page.jsx"
import {Link} from "react-router"
import {pick} from "lodash";
import {Segment, Table, Dropdown, Checkbox, Input} from "semantic-ui-react"

export default class MatrixList extends TablePage {
  uri = 'matrix/index'

  componentWillMount() {
    this.setState({
      program: 0,
      active: 0,
      nick: ''
    })
  }

  load = async (params = {}) => {
    const state = await this.loadItems(params)
    if (!this.state.programs) {
      const {result} = await api.get('program/index')
      if (result instanceof Array) {
        const programs = {}
        for (const program of result) {
          programs[program.id] = program
        }
        state.programs = programs
      }
    }
    this.setState(state)
  }

  getFilterParams() {
    return pick(this.state, 'program', 'active', 'nick')
  }

  getPrograms() {
    const options = [{
      value: 0,
      text: 'Все'
    }]
    for (const key in this.state.programs) {
      const p = this.state.programs[key]
      options.push({
        value: p.id,
        text: p.name
      })
    }
    return options
  }

  change(state) {
    this.setState(state, () => this.load())
  }

  toggleActive() {
    this.setState({active: this.state.active > 0 ? 0 : 1}, () => this.load())
  }

  async saveQualification(id, number) {
    const {success} = await api.send('matrix/qualification', {id}, {number: number > 2 ? 2 : number})
    if (success) {
      const node = this.state.items.find(n => n.id === id)
      node.qualified = number
      this.setState({items: this.state.items})
    }
  }

  rows() {
    return this.state.items.map(item => {
      const program = this.state.programs[item.program]
      return <Table.Row key={item.id} className={item.active ? 'open' : 'closed'}>
        <Table.Cell>{item.id}</Table.Cell>
        <Table.Cell>{item.nick}</Table.Cell>
        <Table.Cell>{program.name}</Table.Cell>
        <Table.Cell>{item.count} <span className="max-members-number">/ {program.size}</span></Table.Cell>
        <Table.Cell>
          <input value={item.qualified > 0 ? item.qualified : 0}
                 onChange={e => this.saveQualification(item.id, +e.target.value)}/>
        </Table.Cell>
        <Table.Cell><Link to={'/matrix/' + item.id}>view</Link></Table.Cell>
      </Table.Row>
    })
  }

  render() {
    if (this.state.programs) {
      return <Segment className="matrix index">
        {this.dimmer()}
        <h1>Матрицы</h1>
        <div className="filters">
          <Dropdown
              placeholder="Тип"
              options={this.getPrograms()}
              value={this.state.program}
              onChange={(e, {value}) => this.change({program: value})}/>
          <Input
              placeholder="Пользователь"
              value={this.state.nick}
              onChange={(e, {value}) => this.change({nick: value})}/>
          <div className="only-active">
            Только открытые
            <Checkbox toggle checked={this.state.active > 0} onChange={() => this.toggleActive()}/>
          </div>
        </div>
        <Table>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>ID</Table.HeaderCell>
              <Table.HeaderCell>Пользователь</Table.HeaderCell>
              <Table.HeaderCell>Программа</Table.HeaderCell>
              <Table.HeaderCell>Заполнение</Table.HeaderCell>
              <Table.HeaderCell>Квалификация</Table.HeaderCell>
              <Table.HeaderCell/>
            </Table.Row>
          </Table.Header>
          <Table.Body>{this.rows()}</Table.Body>
        </Table>
      </Segment>
    }
    return this.dimmer()
  }
}
