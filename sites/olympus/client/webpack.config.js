/**
 * Last modified: 18.06.06 05:24:22
 * Hash: 4cae436351a74b4ee70ed4ad9e08bddfdb38ba46
 */

const config = require('../../../client/webpack-default')

config.entry = __dirname + '/entry.jsx'
config.output.filename = 'main.js'
config.output.path = __dirname + '/../public/scripts'

module.exports = config
