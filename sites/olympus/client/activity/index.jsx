import api from '../../../../client/connect/api.jsx'
import React from 'react'
import TablePage from "../../../../client/widget/table-page.jsx"
import {Link} from "react-router"
import {pick} from "lodash";
import {Segment, Table, Dropdown, Checkbox, Input} from "semantic-ui-react"

export default class ActivityList extends TablePage {
  uri = 'activity/index'
}
