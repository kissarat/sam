/**
 * Last modified: 18.06.06 05:22:11
 * Hash: f66e9852ef6aeaa37f7e482f930aecb67539df5d
 */

import 'babel-core/register'
import 'babel-polyfill'
import {setup} from '../../../client/main.jsx'
import {router} from './router.jsx'
// import {boot} from '../../../common/globals'
// import {currencies} from '../../../client/finance/enums.jsx'

// boot(function () {
// })

void setup(router)
