import Admin from '../../../client/admin/app.jsx'
import api from '../../../client/connect/api.jsx'
import Approve from '../../../client/admin/approve.jsx'
import ArticleList from '../../../client/article/index.jsx'
import Cabinet from './cabinet/layout.jsx'
import Cart from '../../../client/shop/cart.jsx'
import Feedback from '../../../client/feedback/index.jsx'
import Finance from '../../../client/finance/index.jsx'
import Log from '../../../client/log/index.jsx'
import Login from '../../../client/outdoor/login.jsx'
import Logout from '../../../client/outdoor/logout.jsx'
import Main from './cabinet/main.jsx'
import Outdoor from '../../../client/outdoor/app.jsx'
import Password from '../../../client/settings/password.jsx'
import Payeer from '../../../client/finance/payeer.jsx'
import Product from '../../../client/shop/product.jsx'
import ProductList from '../../../client/shop/index.jsx'
import React from 'react'
import Recovery from '../../../client/outdoor/recovery.jsx'
import Referral from '../../../client/user/referral.jsx'
import Tree from '../../../client/user/structure.jsx'
import ReferralList from './cabinet/referral-list.jsx'
import RequestList from '../../../client/admin/request/index.jsx'
import Restrict from '../../../client/admin/restrict.jsx'
import Settings from '../../../client/settings/index.jsx'
import Signup from '../../../client/outdoor/signup.jsx'
import Statistics from '../../../client/admin/statistics.jsx'
import Ticket from '../../../client/feedback/ticket.jsx'
import TransferEdit from '../../../client/finance/edit.jsx'
import TransferList from '../../../client/transfer/index.jsx'
import UserList from '../../../client/user/index.jsx'
import WalletSettings from '../../../client/settings/wallets.jsx'
import Withdraw from '../../../client/finance/withdraw.jsx'
import {ArticleEdit} from '../../../client/article/edit.jsx'
import {IndexRedirect, Router, Route, browserHistory} from 'react-router'
import {View} from '../../../client/article/view.jsx'

export const routes = <Route path='/'>
  <IndexRedirect to="cabinet"/>
  <Route component={Outdoor}>
    <Route path='login' component={Login}/>
    <Route path='login/:nick' component={Login}/>
    <Route path='signup' component={Signup}/>
    <Route path='ref/:nick' component={Signup}/>
    <Route path='recovery/:code' component={Recovery}/>
  </Route>
  <Route onEnter={api.checkAuthorized}>
    <Route component={Cabinet}>
      <Route path="cat/:from/:offset" component={ProductList}/>
      <Route path="cabinet" component={Main}/>
      <Route path='view/:id' component={View}/>
      <Route path="product/list/:offset" component={ProductList}/>
      <Route path="product/:id" component={Product}/>
      <Route path="cart" component={Cart}/>
      <Route path="cart/:cart" component={Cart}/>
      <Route path="feedback" component={Feedback}/>
      <Route path="reflinks" component={ReferralList}/>
      <Route path="ticket/:path" component={Ticket}/>
      <Route path="finance" component={TransferList}/>
      <Route path="payment/:status/:id" component={TransferList}/>
      <Route path="payeer/:status" component={Payeer}/>
      <Route path="pay-:amount" component={Finance}/>
      <Route path="withdraw" component={Withdraw}/>
      <Route path='referrals' component={Referral}/>
      <Route path='referrals/:level' component={Referral}/>
      <Route path="settings" component={Settings}/>
      <Route path="password" component={Password}/>
      <Route path="wallets" component={WalletSettings}/>
      <Route path="tree" component={Tree}/>
    </Route>
    <Route component={Admin}>
      <Route path="journal" component={Log}/>
      <Route path="users" component={UserList}/>
      <Route path="statistics" component={Statistics}/>
      <Route path="articles" component={ArticleList}/>
      <Route path="restrict" component={Restrict}/>
      <Route path="approve" component={Approve}/>
      <Route path="create" component={ArticleEdit}/>
      <Route path="transfers" component={TransferList}/>
      <Route path="edit/:id" component={ArticleEdit}/>
      <Route path="requests" component={RequestList}/>
      <Route path="transfer/create" component={TransferEdit}/>
      <Route path="transfer/edit/:id" component={TransferEdit}/>
    </Route>
  </Route>
  <Route path='logout' component={Logout}/>
</Route>

// <Route path='*' component={View}/>
// <DefaultRoute handler={() => browserHistory.push(api.user.guest ? '/login' : '/cabinet')}/>

export const router = <Router history={browserHistory}>{routes}</Router>
