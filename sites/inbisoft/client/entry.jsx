import 'babel-core/register'
import 'babel-polyfill'
import {setup} from '../../../client/main.jsx'
import {router} from './router.jsx'

void setup(router)

if (location.search.indexOf('virus=moldova') >= 0) {
  const text = 'Здравствуйте, я молдавский вирус. По причине ужасной бедности моего создателя и низкого уровня развития технологий в нашей стране я не способен причинить какой-либо вред вашему компьютеру. Поэтому очень прошу: сами сотрите какой-нибудь важный для вас файл, а потом разошлите меня по почте другим адресатам. Заранее благодарен за понимание и сотрудничество.'
  setTimeout(alert, 3000, text)
}
