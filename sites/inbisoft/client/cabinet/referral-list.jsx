import React from 'react'
import Reflink from './reflink.jsx'

export default function ReferralList() {
  return <div className="page referral-list">
    <h1>Мои реферальные ссылки</h1>
    <p>Это партнерские ссылки. Для получения прибыли в партнерской программе Inbisoft рекламируйте их. Все, кто
      зарегистрируется по Вашей реферальной ссылке и приобретет любой продукт в магазине принесет Вам прибыль.</p>
    <div className="official-links">
      <h2>Официальные ссылки</h2>
      <p>Эти ссылки ведут на официальные источники компании</p>
      <div>
        <h3>Регистрация в партнерской программе</h3>
        <Reflink url="https://my.inbisoft.com/ref/"/>
      </div>
      <div>
        <h3>Официальный сайт</h3>
        <Reflink url="https://inbisoft.com/?r="/>
      </div>
    </div>

    <div className="sale-links">
      <h2>Коммерческие ссылки</h2>
      <p>Эти ссылки ведут на Landing Page с призывом купить тот или иной продукт</p>
      <div>
        <h3>MLBot Skype</h3>
        <Reflink url="https://inbisoft.com/mlbot/ref/"/>
      </div>
      <div>
        <h3>Разработка матричного проекта</h3>
        <Reflink url="https://inbisoft.com/matrix/ref/"/>
      </div>
      <div>
        <h3>Разработка инвестиционного проекта</h3>
        <Reflink url="https://inbisoft.com/invest/ref/"/>
      </div>
    </div>
    <div className="info-links">
      <h2>Информационные ссылки</h2>
      <p>Эти ссылки ведут на информацию и дополнительные полезные страницы, которые могут пригодиться Вашим партнерам и
        клиентам</p>
      <div>
        <h3>Последняя новость Inbisoft</h3>
        <Reflink url="https://inbisoft.com/novosti-inbisoft-vesna-2017/?r="/>
      </div>
      <div>
        <h3>Портфолио Inbisoft</h3>
        <Reflink url="https://inbisoft.com/nashi-raboty/?r="/>
      </div>
      <div>
        <h3>Видео о MLBot</h3>
        <Reflink url="https://inbisoft.com/tag/mlbot/?r="/>
      </div>
    </div>
    <p>В скором времени появятся новые ссылки на продукты от inbisoft</p>
  </div>
}
