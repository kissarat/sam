import api from '../../../../client/connect/api.jsx'
import React, {Component} from 'react'
import Reflink from './reflink.jsx'
import t from '../t'
import {Grid, Icon, Progress, Segment, Dimmer, Loader} from 'semantic-ui-react'
import {Link} from 'react-router'
import {types} from '../../../../client/finance/enums.jsx'

export default class InbisoftMain extends Component {
  state = {
    copied: false,
    transfers: false
  }

  componentDidMount() {
    void this.load()
  }

  async load() {
    const {user} = await api.get('user/get', {id: api.config.user.id})
    if (user) {
      // if ('new' === user.type) {
      // AlertList.show({
      //   error: true,
      //   id: 'need-activation',
      //   content: `Ваш аккаунт активирован не полностью!
      //   Вам на почту отправлено письмо с ссылкой активации. Перейдите, пожалуйста, по ссылке в письме для подтверждения почты.
      //   Активация не является обязательной. Вы можете без нее покупать продукты, обращаться в поддержку за помощью, участвовать в партнерской программе и делать что угодно, кроме восстановления пароля и вывода денег.
      //   Если Вы указали неверную почту при регистрации — обратитесь в службу поддержки для ее изменения.`,
      //   onDismiss: true
      // })
      // }
      this.setState(user)
    }
    const transfers = await api.get('transfer/index', {
      user: api.config.user.id,
      limit: 6
    })
    if (transfers instanceof Array) {
      this.setState({transfers})
    }
  }

  onChange = async e => {
    if (e.target.files.length > 0) {
      this.setState({avatar: 'https://s-media-cache-ak0.pinimg.com/originals/0b/93/09/0b9309cf2dd079c998a5414e32a04618.gif'})
      const [xhr] = await api.promiseUpload(e.target.files)
      const r = JSON.parse(xhr.responseText)
      await api.send('user/save', {id: api.config.user.id}, {avatar: r.url})
      await this.load()
    }
  }

  avatar() {
    return <div className="avatar-container huge">
      <div className="avatar huge"
           onClick={() => this.refs.file.click()}
           style={this.state.avatar ? {backgroundImage: `url("${this.state.avatar}")`} : {}}>
        <div className="inner-avatar">
          <input type="file" ref="file" onChange={this.onChange}/>
          <Icon name="photo" size="large" color="grey"/>
          <div className="info">
            <div className="name">{this.state.forename} {this.state.surname}</div>
            <div className="earned">
              <span className="label">{t('Earned')}:</span>
              &nbsp;
              <span className="value money">{this.state.accrue / api.config.money.scale}</span>
            </div>
            <div className="team">
              <div className="partners">
                <div className="label">{t('Number of partners')}</div>
                <div className="value">{this.state.referral}</div>
              </div>
              <div className="sales">
                <div className="label">{t('Number of sales')}</div>
                <div className="value">{this.state.sales}</div>
              </div>
              <div className="turnover">
                <div className="label">{t('Turnover')}</div>
                <div className="value money">{this.state.turnover / api.config.money.scale}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  }

  social(p) {
    const urls = []
    for (const name of ['facebook', 'vkontakte', 'twitter', 'google', 'odnoklassniki']) {
      // console.log(name, p[name], p)
      //  &&/^https?\/\//.test(url)
      const url = p[name]
      if (url) {
        urls.push(<a key={name} target="_blank" href={url} className={name}/>)
      }
    }
    return urls
  }

  sponsor() {
    const p = this.state.parent
    if (p) {
      const hasName = p.forename || p.surname
      return <Segment className="card sponsor somebody">
        <h2 className="title">{t("Sponsor's card")}</h2>
        <div className="info">
          <div className="me">
            <div className="short">
              <div className="avatar-container">
                <div className="avatar" style={p.avatar ? {backgroundImage: `url("${p.avatar}")`} : {}}></div>
              </div>
              <div className="name">
                <h3
                  className={hasName ? 'something' : 'nothing'}>{hasName ? p.forename + '  ' + p.surname : t('Noname')}</h3>
                <div className="nick">{p.nick}</div>
              </div>
            </div>
            <div
              className={p.about ? 'about something' : 'about nothing'}>{p.about || t('Your sponsor left no description')}</div>
          </div>
          <div className="contacts">
            <h3>{t('Contacts')}</h3>
            <div className="list">
              {p.skype ? <div className="skype">
                  <Icon name="skype"/>
                  <span className="value">{p.skype}</span>
                </div> : ''}
              {p.phone ? <div className="phone">
                  <Icon name="phone"/>
                  <span className="value">{p.phone}</span>
                </div> : ''}
              {p.email ? <div className="email">
                  <Icon name="mail"/>
                  <span className="value">{p.email}</span>
                </div> : ''}
            </div>
          </div>
        </div>
        <div className="social">{this.social(p)}</div>
      </Segment>
    }
    return <div className="card sponsor empty">{t('You have no sponsor')}</div>
  }

  transfers() {
    if (this.state.transfers instanceof Array) {
      return this.state.transfers.map(t => <tr key={t.id} className={this.state.id === t.to ? 'positive' : 'negative'}>
        <td>{types[t.type]}</td>
        <td className="money">{t.amount / api.config.money.scale}</td>
        <td>{new Date(t.time || t.created).toLocaleDateString()}</td>
      </tr>)
    }
  }

  render() {
    return <Grid columns="equal" className="page main">
      <Grid.Row className="top">
        <Grid.Column width={10} className="left">
          {this.avatar()}
        </Grid.Column>
        <Grid.Column className="right">
          <div className="panel referral">
            <div className="link referral">
              <h2>{t('Affiliate link')}</h2>
              <Reflink url="https://inbisoft.com/mlbot/ref/"/>
              {this.state.visit > 0 ?
                <div className="visit">{t('{number} referrals', {number: this.state.visit})}</div> : 0}
            </div>
            <div className="next bonus">
              <h3>Слудующая премия</h3>
              <div className="number">
                <span className="value money">16534</span>
                <span className="percent">80%</span>
              </div>
              <Progress percent={80} size="tiny" inverted/>
            </div>
            <div className="earned bonus">
              <h3>Заработано балов</h3>
              <div className="number">
                <span className="value">7521</span>
                <span className="percent">80%</span>
              </div>
              <Progress percent={80} size="tiny"/>
            </div>
          </div>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row className="bottom">
        <Grid.Column width={10} className="left">
          {this.sponsor()}
        </Grid.Column>
        <Grid.Column className="right">
          <Segment className="last-transfers">
            <Dimmer active={false === this.state.transfers} inverted>
              <Loader/>
            </Dimmer>
            <h2>{t('Finances')}</h2>
            <table>
              <tbody>{this.transfers()}</tbody>
            </table>
            <Link className="go-finances" to="/finance">{t('Go to history')}</Link>
          </Segment>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  }
}
