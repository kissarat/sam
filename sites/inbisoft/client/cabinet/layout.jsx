import AlertList from '../../../../client/widget/alert.jsx'
import api from '../../../../client/connect/api.jsx'
import FreshmanList from './freshman-list.jsx'
import Language from '../../../../client/widget/language.jsx'
import React, {Component} from 'react'
import t from '../t'
import {boot} from '../../../../common/globals'
import {LinkItem} from '../../../../client/widget/menu.jsx'
import {Link} from 'react-router'
import {Menu} from 'semantic-ui-react'

const verticalMenu = [
  [t('Back Office'), '/cabinet', 'suitcase'],
  [t('Shop'), '/product/list/0', 'shop'],
  [t('Reflinks'), '/reflinks', 'linkify'],
  [t('Referrals'), '/referrals/1', 'users'],
  // [t('For Investors'), '/product/list/0', 'shop'],
  [t('Finances'), '/finance', 'dollar'],
  // [t('Advertising'), '/advertise', 'signal'],
  [t('Settings'), '/settings', 'setting'],
  // [t('Information'), '/about', 'info'],
  [t('Logout'), '/logout', 'sign out'],
]

boot(function () {
  if (api.config.user.admin) {
    verticalMenu.push([t('Admin Panel'), location.origin + '/statistics', 'users'])
  }
})

const horizontalMenu = [
  // ['MLBot Windows', '/serve/file/download/mlbot-win32'],
  // ['MLBot macOS', '/serve/file/download/mlbot-macos'],
  // ['О нас', '/about'],
  // ['Партнерская программа', '/partners'],
  // ['Как купить?', '/how'],
  // ['Котакты', '/contact-us'],
]

export default class InbisoftLayout extends Component {
  state = {}

  componentDidMount() {
    void this.load()
  }

  async load() {
    const {user} = await api.get('user/get', {id: api.config.user.id})
    if (user) {
      this.setState(user)
    }
  }

  verticalMenu() {
    return <Menu vertical inverted>
      <div className="logo">
        <Link to="/cabinet">
          <img src="/images/logo-inbisoft.png"/>
        </Link>
      </div>
      {verticalMenu.map(([name, url, icon]) => <Menu.Item key={url}>{0 === url.indexOf('http')
        ? <a href={url} target="_blank">{t(name)}</a>
        : <LinkItem name={t(name)} url={url} icon={icon}/>}</Menu.Item>)}
    </Menu>
  }

  horizontalMenu() {
    return <Menu text>
      <Menu.Item>
        <LinkItem className="button feedback" name={t('Feedback')} url="/feedback"/>
        <Menu.Item>
          <Language/>
        </Menu.Item>
        <Menu.Item><a key="win32" href="/serve/file/download/mlbot-win32" target="_blank">MLBot Windows</a></Menu.Item>
        <Menu.Item><a key="macos" href="/serve/file/download/mlbot-macos" target="_blank">MLBot macOS</a></Menu.Item>
        <Menu.Item>
          <a key="linux" href="/serve/file/download/mlbot-linux-32bit" target="_blank">MLBot Linux</a>
        </Menu.Item>
      </Menu.Item>
      <Menu.Menu position="right">
        {horizontalMenu.map(([name, url]) => <Menu.Item key={url}>
          <LinkItem name={name} url={url}/>
        </Menu.Item>)}
      </Menu.Menu>
    </Menu>
  }

  balance() {
    if ('number' === typeof this.state.balance) {
      const balance = this.state.balance / api.config.money.scale
      const accrue = this.state.accrue / api.config.money.scale
      const accrue_day = this.state.accrue_day / api.config.money.scale
      return <div className="financial">
        <div className="balance money" title={t('Balance')}>{balance}</div>
        <div className="income">
          <span className="money" title={t('Earned')}>{accrue}</span>
          <span className="plus money" title={t('Earned in the last 24 hours')}>{accrue_day}</span>
        </div>
      </div>
    }
  }

  render() {
    return <div className="layout cabinet">
      <div className="container">
        {this.verticalMenu()}
        <div className="cabinet-right">
          {this.horizontalMenu()}
          <div className="cabinet-main">
            <div id="main">
              <AlertList/>
              <main>{this.props.children}</main>
            </div>
            <aside>
              <div className="earned">
                {this.balance()}
                <div className="control">
                  <Link className="button pay" to="/pay-22">{t('Pay')}</Link>
                  <Link className="button withdraw inverted" to="/withdraw">{t('Withdraw')}</Link>
                </div>
              </div>
              <FreshmanList/>
            </aside>
          </div>
          <div className="freshman"></div>
        </div>
      </div>
      <div className="advertise">
        <a className="payeer banner" href="//payeer.com/?partner=3293636&utm_source=inbisoft.com" target="_blank">
          <img src="//payeer.com/bitrix/templates/difiz/img/banner/ru/468x60.gif?utm_source=inbisoft.com" alt="Payeer"/>
        </a>
        <a className="leopays banner" href="/serve/file/download/leopays" target="_blank">
          <img src="/images/leopays.png" alt="LaoPays"/>
        </a>
        <a className="club-leader banner" href="/serve/file/download/club-leader" target="_blank">
          <img src="//kab.club-leader.com/assets/banner01.gif" alt="Club Leader"/>
        </a>
        <a className="kaleostra banner" href="/serve/file/download/kaleostra" target="_blank">
          <img src="//kaleostra.com/images/promo/468x60-3.gif" alt="Kaleostra"/>
        </a>
      </div>
    </div>
  }
}
