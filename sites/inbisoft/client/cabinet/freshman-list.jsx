import api from '../../../../client/connect/api.jsx'
import React, {Component} from 'react'
import t from '../t'
import {Feed, Icon} from 'semantic-ui-react'
import {Link} from 'react-router'

export default class FreshmanList extends Component {
  state = {
    referrals: [],
  }

  async componentDidMount() {
    const {referrals} = await api.get('user/structure', {
      view: 'referral',
      root: api.config.user.id,
      limit: 6
    })
    if (referrals) {
      this.setState({referrals})
    }
  }

  render() {
    return <Feed className="widget freshman-list">
      <h2>{t('New partners')}</h2>
      {this.state.referrals.map(u => <Feed.Event key={u.id}>
        <Feed.Label>
          <div className="avatar" style={u.avatar ? {backgroundImage: `url("${u.avatar}")`} : {}}/>
        </Feed.Label>
        <Feed.Content>
          <Feed.Summary>
            <Feed.User>{u.surname ? (u.forename + ' ' + u.surname) : u.nick}</Feed.User>
            {u.skype ? <Feed.Extra>
              <a href={`skype:${u.skype}?add`}>
                <Icon name="skype"/>
                <span>{u.skype}</span>
              </a>
            </Feed.Extra> : ''}
          </Feed.Summary>
        </Feed.Content>
      </Feed.Event>)}
      <Link className="button see-all" to="/referrals/1">{t('See all')}</Link>
    </Feed>
  }
}