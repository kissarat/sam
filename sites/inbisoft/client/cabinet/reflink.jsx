import React, {Component} from 'react'
import api from '../../../../client/connect/api.jsx'

export default class Reflink extends Component {
  state = {copied: false}

  copyReferralURL = () => {
    if (window.clipboardData && clipboardData.setData instanceof Function) {
      clipboardData.setData('URL', this.refs.input.value)
    }
    else if (document.execCommand instanceof Function) {
      this.refs.input.select()
      document.execCommand('copy')
    }
    this.setState({copied: true})
  }

  render() {
    return <div className="widget reflink">
      <input
        readOnly
        ref="input"
        onClick={this.copyReferralURL}
        value={this.props.url + api.config.user.nick}/>
      {this.state.copied
        ? <div className="copied"/>
        : <button type="button" onClick={this.copyReferralURL}>Copy</button>}
    </div>
  }
}
