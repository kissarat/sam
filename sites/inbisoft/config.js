module.exports = {
  origin: 'https://my.inbisoft.com',
  id: 'inbisoft',
  name: 'inbisoft.com',
  enabled: false,

  dir: __dirname,

  database: {
    id: 'inbisoft'
  },

  money: {
    scale: 100,
    currency: 'USD'
  },

  mlbot: {
    server: {
      key: 'sheiceeth7reelohr5cahquah4mokoo2oe1eiz6chuigh0Ta'
    }
  },

  remote: {
    enabled: true
  },

  blockio: {
    enabled: true
  },

  payeer: {
    enabled: true
  },

  perfect: {
    enabled: true
  },

  domains: [
    'inbisoft.local',
    'inbisoft.softroom.pro',
    'my.inbisoft.com',
  ]
}
