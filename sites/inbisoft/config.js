/**
 * Last modified: 18.07.27 20:57:06
 * Hash: fcc2ee92d554b1ae80eb540935f18e7189f6d56e
 */

module.exports = {
  origin: 'https://my.inbisoft.com',
  id: 'inbisoft',
  name: 'inbisoft.com',
  enabled: false,

  dir: __dirname,

  database: {
    id: 'inbisoft'
  },

  money: {
    scale: 100,
    currency: 'USD'
  },

  remote: {
    enabled: true
  },

  blockio: {
    enabled: true
  },

  payeer: {
    enabled: true
  },

  perfect: {
    enabled: true
  },

  domains: [
    'my.inbisoft.com',
  ]
}
