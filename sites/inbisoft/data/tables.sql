INSERT INTO program VALUES
  (1, 0),
  (2, 1::FLOAT4/22);

INSERT INTO reward VALUES
  (1, 1, 0.08),
  (1, 2, 0.01),
  (1, 3, 0.01),
  (1, 4, 0.01),
  (1, 5, 0.02),
  (2, 1, 10::FLOAT4/22),
  (2, 2, 1::FLOAT4/22);

INSERT INTO article (id, type, path, name, price, program) VALUES
  (2, 'cat', 'shop', 'Shop', NULL, NULL),
  (3, 'cat', 'investment', 'Investment', NULL, NULL),
  (4, 'product', 'matrix-project', 'Matrix', 200000, 1),
  (5, 'product', 'affiliate-project', 'Affiliate program', 50000, 1),
  (6, 'product', 'bank-project', 'Investment project', 100000, 1);
