CREATE OR REPLACE VIEW sponsor_leader AS
  SELECT
    root,
    min(level) AS level
  FROM sponsor s
  WHERE s.leader
  GROUP BY root;

CREATE OR REPLACE VIEW under_leader AS
  SELECT
    s.id AS "user",
    s.root,
    r.program,
    r.percent,
    s.level
  FROM sponsor s
    JOIN reward r ON s.level = r.level
    JOIN program p ON r.program = p.id
    LEFT JOIN sponsor_leader l ON s.root = l.root
  WHERE coalesce(s.level < l.level, TRUE);

CREATE OR REPLACE VIEW leader_reward AS
  SELECT
    s.id                      AS "user",
    s.root,
    p.id                      AS program,
    coalesce((
               SELECT sum(rr.percent)
               FROM reward rr
               WHERE rr.program = p.id AND rr.level >= l.level
             ), 0) + p.leader AS percent,
    s.level
  FROM sponsor s
    JOIN sponsor_leader l ON s.root = l.root AND s.level = l.level
    CROSS JOIN program p;

CREATE OR REPLACE VIEW sponsor_reward AS
  SELECT *
  FROM under_leader WHERE level > 0
  UNION
  SELECT *
  FROM leader_reward WHERE level > 0;

