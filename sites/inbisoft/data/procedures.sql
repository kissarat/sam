--========================== buy ============================

CREATE OR REPLACE FUNCTION buy(_user_id INT, _id INT, _ip INET)
  RETURNS INT AS $$
DECLARE
  _article    RECORD;
  _created    TIMESTAMP;
  transfer_id INT;
  _node_id    INT;
  _nick       TEXT;
BEGIN
  SELECT
    price,
    program,
    name
  FROM article
  WHERE id = _id
  INTO _article;

  IF _article IS NULL
  THEN
    RETURN -404;
  END IF;

  SELECT nick
  FROM "user"
  WHERE id = _user_id
  INTO _nick;

  SELECT transfer_send(_user_id, NULL, _article.price, 'buy', _ip)
  INTO transfer_id;

  IF transfer_id > 0
  THEN
    SELECT created
    FROM transfer
    WHERE id = transfer_id
    INTO _created;

    INSERT INTO node ("user", article, created) VALUES (_user_id, _id, _created)
    RETURNING id
      INTO _node_id;

    UPDATE transfer
    SET node = _node_id,
      text   = 'Buying {name}',
      vars   = json_build_object('name', _article.name)
    WHERE id = transfer_id;

    INSERT INTO transfer ("to", amount, "type", status, created, node, "text", vars, ip)
      SELECT
        "user",
        _article.price * percent,
        'accrue',
        'success',
        _created,
        _node_id,
        'Accrue from buying {name} from {from_nick} at level {level}',
        json_build_object('name', _article.name, 'from', _user_id, 'from_nick', _nick, 'level', s.level),
        _ip
      FROM sponsor_reward s
      WHERE s.root = _user_id AND s.program = _article.program
      ORDER BY s."level";
    RETURN _node_id;
  END IF;
  RETURN transfer_id;
END
$$ LANGUAGE plpgsql;

--========================== buy_cart ============================

CREATE OR REPLACE FUNCTION buy_cart(_cart_id INT)
  RETURNS INT AS $$
DECLARE
  _cart    RECORD;
  _amount  INT;
  _id      INT;
  _created TIMESTAMP;
BEGIN
  SELECT *
  FROM cart
  WHERE id = _cart_id
  INTO _cart;

  IF _cart IS NULL
  THEN
    RETURN -404;
  END IF;

  SELECT sum(amount * price) :: INT
  FROM item
  WHERE cart = _cart_id
  INTO _amount;

  SELECT transfer_send(_cart."user", NULL, _amount, 'buy')
  INTO _id;

  IF _id > 0
  THEN
    SELECT created
    FROM transfer
    WHERE id = _id
    INTO _created;

    UPDATE cart
    SET transfer = _id, time = _created
    WHERE id = _cart_id;

    INSERT INTO transfer ("to", amount, type, status, created)
      SELECT
        "to",
        amount,
        type,
        'success',
        _created
      FROM buy_cart
      WHERE cart = _cart_id
      ORDER BY created;
  END IF;
  RETURN _id;
END
$$ LANGUAGE plpgsql;

--========================== buy_mlbot ============================

CREATE OR REPLACE FUNCTION buy_mlbot(_user_id INT, _ip INET)
  RETURNS INT AS $$
DECLARE
  _id      INT;
  _created TIMESTAMP;
BEGIN
  SELECT transfer_send(_user_id, NULL, 2200, 'buy', _ip)
  INTO _id;
  IF _id > 0
  THEN
    SELECT created
    FROM transfer
    WHERE id = _id
    INTO _created;
    INSERT INTO transfer ("to", amount, type, status, created, ip)
      SELECT
        id,
        income,
        'accrue' :: transfer_type,
        'success' :: transfer_status,
        _created,
        _ip
      FROM remuneration
      WHERE root = _user_id
      ORDER BY level ASC;
    UPDATE "user"
    SET mlbot = CURRENT_TIMESTAMP + INTERVAL '2 years'
    WHERE id = _user_id;
  END IF;
  RETURN _id;
END
$$ LANGUAGE plpgsql;
