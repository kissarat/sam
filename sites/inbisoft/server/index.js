const Application = require('../../../server/application')

const app = new Application(require('../../../config'), {
  inbisoft: require('../config')
})

app.run()
