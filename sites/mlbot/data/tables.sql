CREATE TABLE skype (
  id       SERIAL PRIMARY KEY,
  owner    INT         NOT NULL REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  login    VARCHAR(64) NOT NULL,
  password VARCHAR(64) NOT NULL,
  spend    INT,
  size     INT,
  UNIQUE (owner, login)
);

CREATE TABLE skype_message (
  id        BIGSERIAL PRIMARY KEY,
  "from"    VARCHAR(32)   NOT NULL,
  "to"      VARCHAR(32)   NOT NULL,
  "text"    VARCHAR(8192) NOT NULL,
  "created" TIMESTAMP     NOT NULL,
  "time"    TIMESTAMP
);

CREATE TYPE sex AS ENUM ('male', 'female');

CREATE TABLE contact (
  id       SERIAL PRIMARY KEY,
  skype    VARCHAR(32) NOT NULL,
  name     VARCHAR(192),
  sex      sex,
  birthday TIMESTAMP,
  country  CHAR(2),
  city     VARCHAR(192),
  avatar   VARCHAR(192),
  mood     VARCHAR(192),
  site     VARCHAR(192),
  language VARCHAR(24),
  phone    VARCHAR(24),
  about    VARCHAR(8192),
  email    VARCHAR(64),
  created  TIMESTAMP   NOT NULL,
  time     TIMESTAMP
);

CREATE TABLE skype_contact (
  skype   VARCHAR(32) NOT NULL,
  contact VARCHAR(32) NOT NULL,
  time    TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE skype_task (
  id      SERIAL PRIMARY KEY,
  created TIMESTAMP   NOT NULL,
  time    TIMESTAMP   NOT NULL,
  account VARCHAR(32) NOT NULL,
  type    VARCHAR(32) NOT NULL,
  status  SMALLINT    NOT NULL,
  after   TIMESTAMP,
  wait    INT,
  number  SMALLINT,
  repeat  SMALLINT,
  text VARCHAR(8192)
);

CREATE TABLE skype_log (
  id      BIGSERIAL PRIMARY KEY,
  contact VARCHAR(32) NOT NULL,
  created TIMESTAMP   NOT NULL,
  task    INT         NOT NULL,
  status  SMALLINT    NOT NULL,
  number  SMALLINT,
  message VARCHAR(8192)
);
