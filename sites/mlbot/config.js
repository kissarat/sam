const os = require('os')
const {normalize} = require('path')

const dir = __dirname
module.exports = {
  id: 'mlbot',
  dir,
  modules: dir + '/server/modules',

  origin: 'http://app.inbisoft.com',
  domains: [
    'app.inbisoft.com'
  ],

  daemon: {
    enabled: false
  },

  skype: {
    profile: {
      filter: false,
      path: normalize(os.userInfo()['homedir'] + '/skype')
    }
  },

  file: {
    download: {
      path: 'http://download.club-leader.com'
    }
  },

  hard: {
    enabled: true
  },
}
