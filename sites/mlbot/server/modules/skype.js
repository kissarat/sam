const {isObject, pick, values, constant, times, identity, isEmpty, omit} = require('lodash')
const fs = require('fs-extra-promise')
const qs = require('querystring')

function stringify(data, sep, eq = '=') {
  return qs.stringify(data, sep, eq, {
    encodeURIComponent: identity
  })
}

function upsert(table, conflict, data) {
  const keys = Object.keys(data).map(a => `"${a}"`).join(',')
  let _values = values(data)
  const placeholder = times(_values.length, constant('?')).join(',')
  let update = {}
  for (const key in data) {
    if (conflict.indexOf(key) < 0) {
      update[`"${key}"`] = '?'
      _values.push(data[key])
    }
  }
  update = stringify(update, ',')
  let where = {}
  conflict.forEach(function (key) {
    where[`"${key}"`] = '?'
  })
  where = stringify(where, ' AND ')
  // _values = _values.concat(conflict.map(n => data[n]))
  conflict = conflict.map(a => `"${a}"`).join(',')

  const sql = `
  INSERT INTO ${table}(${keys}) VALUES (${placeholder})
  ON CONFLICT (${conflict}) DO UPDATE SET
  ${update}
  `
  console.log(sql, _values)
  return this.site.database.raw(sql, _values)
}

function checkMethod(method) {
  return {
    method,
    auth: {},
    token: {
      type: 'app',
      mlbot: true
    },
  }
}

module.exports = {
  $point: {method: 'GET'},
  point() {
    const time = new Date().toISOString()
    return {time}
  },

  $profile: checkMethod('POST'),
  profile(params, data) {
    if ('string' === typeof params.id && isObject(data)) {
      if (this.config.skype.filter) {
        data.contacts = data.contacts.map(c => c.authorized && !c.blocked)
      }
      let skype = pick(data, 'login', 'password', 'spend')
      skype.owner = this.user.id
      skype.size = data.contacts.length
      return upsert.call(this, 'skype', ['owner', 'login'], skype)
        .then(() => fs.writeFile(`${this.config.skype.profile.path}/${params.id}.json`, JSON.stringify(data)))
        .then(() => ({
          success: true,
          message: 'Account saved'
        }))
    }
    else {
      return {status: 400}
    }
  },

  $accounts: checkMethod(['GET', 'POST']),
  accounts(_1, data) {
    if ('GET' === this.method) {
      return this
        .table('skype')
        .where('owner', this.user.id)
        .orderBy('login')
    }
    else if ('POST' === this.method) {
      if (!(data instanceof Array) || data.length <= 0) {
        return {status: 400}
      }
      data.forEach(account => account.owner = this.user.id)
      return this
        .table('skype')
        .where('owner', this.user.id)
        .del()
        .then(() => this.table('skype').insert(data))
        .then(({rowCount}) => ({
          success: rowCount > 0,
          count: rowCount
        }))
    }
  },

  $hard: {method: 'GET'},
  hard(params) {
    params = pick(params, 'id', 'nick', 'email')
    if (isEmpty(params)) {
      return {status: 400}
    }
    return this.table('user')
      .where(params)
      .update({hard: null, soft: null, time: new Date()})
      .then(count => ({
        status: count > 0 ? 200 : 500,
        success: count > 0
      }))
  },

  $remove: checkMethod('DELETE'),
  remove(params) {
    if (!params) {
      params = {}
    }
    params.owner = this.user.id
    return this
      .table('skype')
      .where(params)
      .del()
      .then(affected => ({success: affected > 0, affected}))
  },

  $get: {auth: {admin: true}, method: 'GET'},
  get({id}) {
    return fs.readJsonAsync(`${this.config.skype.profile.path}/${id}.json`)
  },

  $messages: checkMethod('POST'),
  async messages(params, data) {
    if (!(data instanceof Array) || data.length <= 0) {
      return {status: 400}
    }
    for (const message of data) {
      message.created = new Date(message.created)
    }
    const q = this.table('skype_message')
      .whereRaw('("from" = ? AND "to" = ?) OR ("from" = ? AND "to" = ?)',
        [params.account, params.chat, params.chat, params.account])
      .whereIn('created', data.map(m => m.created))
      .select('id', 'created', 'time')
    console.log(q.toString())
    const found = await q
    const insert = []
    const updated = []
    data.sort((a, b) => a.created.getTime() - b.created.getTime())
    for (const message of data) {
      const previous = found.find(m => message.created.getTime() === new Date(m.created).getTime())
      if (previous) {
        const isUpdated = previous
          && ((!previous.time && message.time) || new Date(message.time).getTime() > new Date(previous.time).getTime())
        if (isUpdated) {
          const updatedMessage = await this.table('skype_message')
            .where('id', previous.id)
            .update(pick(message, 'text', 'time'), ['id', 'time'])
          updated.push(updatedMessage)
        }
      }
      else {
        insert.push(message)
      }
    }

    let created
    if (insert.length > 0) {
      created = await this.table('skype_message')
        .insert(insert, ['id', 'created', 'time'])
    }
    else {
      created = []
    }

    return {
      success: true,
      affected: created.length + updated.length,
      created,
      updated
    }
  },

  $contacts: checkMethod('POST'),
  async contacts(params, data) {
    let _contacts = await this.table('contact')
      .whereIn('skype', data.map(c => c.skype))
      .select('skype')
    _contacts = _contacts.map(c => c.skype)
    const insertContacts = []
    const updatedContacts = []
    const time = new Date()
    for (let contact of data) {
      if (isObject(contact.phones)) {
        contact.phone = contact.phones.mobile || contact.phones.office || contact.phones.home
      }
      if (contact.emails instanceof Array && contact.emails.length > 0) {
        contact.email = contact.emails[0]
      }
      contact = pick(contact, 'about', 'avatar', 'phone', 'birthday', 'city', 'country', 'created', 'language', 'mood',
        'name', 'sex', 'site', 'skype', 'email')
      if (_contacts.indexOf(contact.skype) >= 0) {
        contact.time = time
        const updatedContact = await this.table('contact')
          .where('skype', contact.skype)
          .update(omit(contact, 'skype'), ['id', 'skype', 'time'])
        updatedContacts.push(updatedContact)
      }
      else {
        insertContacts.push(contact)
      }
    }
    const createdContacts = await this.table('contact')
      .insert(insertContacts, ['id', 'skype'])

    let contactList = await this.table('skype_contact')
      .whereIn('contact', data.map(c => c.skype))
      .where('skype', params.account)
      .select('contact')
    contactList = contactList.map(c => c.contact)

    const insertRecords = []
    const updatedList = []
    for (const contact of data) {
      const record = {
        skype: params.account,
        contact: contact.skype
      }
      if (contactList.indexOf(contact.skype) >= 0) {
        const updatedRecord = await this.table('skype_contact')
          .where(record)
          .update({time}, ['contact', 'time'])
        updatedList.push(updatedRecord)
      }
      else {
        record.time = time
        insertRecords.push(record)
      }
    }

    const createdList = await this.table('skype_contact')
      .insert(insertRecords, ['contact', 'time'])

    return {
      success: true,
      affected: createdContacts.length + updatedContacts.length + createdList.length + updatedList.length > 0,
      time,
      contacts: {
        created: createdContacts,
        updated: updatedContacts
      },
      list: {
        created: createdList,
        updated: updatedList
      }
    }
  },

  $task: checkMethod('POST'),
  async task(params, data) {
    params.created = new Date(+params.created)
    if (params.created.getTime() < new Date('2017-04-16').getTime()) {
      return {
        status: 400,
        params,
        error: {
          message: 'Invalid created'
        }
      }
    }
    if ('string' !== typeof params.account || params.account.length < 1) {
      return {status: 400, params}
    }
    if (!(data.log instanceof Array)) {
      return {status: 400, log: data.log}
    }
    const task = pick(data, 'type', 'status', 'text', 'number', 'wait', 'repeat')
    task.time = new Date(data.time)
    if (data.after) {
      task.after = new Date(data.after)
    }
    let rows = await this.table('skype_task')
      .where(params)
      .update(task, ['id', 'created', 'time'])
    if (rows.length <= 0) {
      rows = await this.table('skype_task')
        .insert(merge(task, params), ['id', 'created', 'time'])
    }
    if (1 === rows.length) {
      const updated = rows[0]
      const log = await this.table('skype_log')
        .insert(data.log.map(function (raw) {
          const record = pick(raw, 'type', 'status', 'number', 'message')
          record.task = updated.id
          record.contact = raw.contact.split('~')[1]
          record.created = new Date(raw.created)
          return record
        }),
        ['id', 'created'])
      return {
        success: true,
        task: updated,
        log
      }
    }
    return {
      status: 400,
      tasks: rows,
      error: {
        message: 'Invalid tasks number'
      }
    }
  }
}
