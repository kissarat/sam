const parent = require('../../../../server/modules/user')
const {pick, omit, merge, defaults} = require('lodash')
const bcrypt = require('promised-bcrypt')

module.exports = {
  __proto__: parent,

  _login(params, user) {
    if ('app' === this.token.type && !(new Date(user.mlbot).getTime() > Date.now())) {
      return {
        status: 403,
        error: {
          status: 'ALERT',
          message: 'У вас нету лицензии использвать это приложение'
        }
      }
    }
    return parent._login.call(this, params, user)
  },

  $silver: {
    method: 'POST',
    token: 'server',
    auth: {admin: true}
  },
  silver(data) {
    data = pick(data, 'id', 'nick', 'email', 'secret', 'forename',
      'surname', 'skype', 'time', 'created',
      'type', 'mlbot')
    defaults(data, {
      type: 'leader',
      mlbot: '2020-01-01'
    })
    return this.table('user')
      .where('id', +data.id)
      .update(omit(data, 'id'))
      .then(count => {
        if (count < 1) {
          return this.table('user')
            .insert(data)
            .then(() => ({
              status: 201,
              success: true
            }))
        }
        return {success: true}
      })

  },

  $mlbot: {
    method: 'POST',
    token: 'server',
    auth: {admin: true}
  },
  mlbot(params, data) {
    if ('kissarat' === this.user.nick) {
      data.nick = 'inbisoft_' + data.nick
    }
    else {
      data.type = 'clubleader' === this.user.nick ? 'leader' : this.user.nick
    }

    function _upsert() {
      data = pick(data, 'id', 'nick', 'email', 'secret', 'forename',
        'surname', 'skype', 'time', 'created',
        'type', 'mlbot')
      return this.table('user')
        .where(params)
        .first('id')
        .then(found => {
          if (found) {
            return this.table('user')
              .where(params)
              .update(data, ['id', 'email'])
              .then(([user]) => ({
                status: 200,
                success: true,
                user
              }))
          }
          else {
            return this.table('user')
              .insert(merge(data, params), ['id', 'email'])
              .then(([user]) => ({
                status: 201,
                success: true,
                user
              }))
          }
        })
    }

    if (data.password) {
      return bcrypt.hash(data.password)
        .then(secret => {
          data.secret = secret
          return _upsert.call(this)
        })
    }
    else {
      return _upsert.call(this)
    }
  }
}
