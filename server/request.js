const features = require('./features')
const {HttpError} = require('./error')
const {isObject, slice} = require('lodash')
const {request} = require('express')

module.exports = {
  __proto__: request,

  get modules() {
    return this.site.modules
  },

  get config() {
    return this.site.config
  },

  log(entity, action, data, user, ip) {
    return this.site.database
      .log(entity, action, data, user || (this.user ? this.user.id : null), ip || this.ip)
  },

  table(name) {
    return this.site.database.table(name)
  },

  sql(sql, ...bindings) {
    return this.site.database.raw(sql, ...bindings)
  },

  transaction(cb) {
    return this.site.database.transaction(cb)
  },

  get entities() {
    return this.site.database.entities
  },

  get models() {
    return this.site.database.models
  },

  get ip() {
    return this.headers.ip || this.connection.remoteAddress
  },

  get moduleConfig() {
    return this.config[this.moduleName]
  },

  act(module, action, options, changes) {
    return this.modules[module][action](options, changes)
  },

  raise(code, options) {
    if (code && options) {
      if (isObject(options)) {
        if (!options.error) {
          options = {code, error: options}
        }
      }
      else if ('string' === typeof options) {
        options = {code, error: {message: options}}
      }
    }
    else {
      options = code
      if ('number' === typeof options) {
        options = {code}
      }
      else if ('string' === typeof options) {

      }
    }
    throw new HttpError(options)
  },

  check(name, options) {
    if (this.isEnabled(name)) {
      const checker = this.modules.access[name]
      if ('function' === typeof checker) {
        return checker.call(this, options)
      }
      else {
        this.raise(500, name)
      }
    }
  },

  raiseCheck(name, options) {
    const result = this.check(name, options)
    if (result) {
      this.raise(result)
    }
  },

  invoke(actionName) {
    const result = this.module[actionName].apply(this, slice(arguments, 1))
    return 'function' === typeof result.then ? result : Promise.resolve(result)
  },
}

Object.assign(module.exports, features)
