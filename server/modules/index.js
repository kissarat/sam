module.exports = {
  access: require('./access'),
  admin: require('./admin'),
  advcash: require('./advcash'),
  article: require('./article'),
  blockio: require('./blockio'),
  cart: require('./cart'),
  error: require('./error'),
  ethereum: require('./ethereum'),
  file: require('./file'),
  geo: require('./geo'),
  home: require('./home'),
  log: require('./log'),
  mail: require('./mail'),
  message: require('./message'),
  nix: require('./nix'),
  payeer: require('./payeer'),
  perfect: require('./perfect'),
  report: require('./report'),
  // request: require('./request'),
  restrict: require('./restrict'),
  telegram: require('./telegram'),
  token: require('./token'),
  transfer: require('./transfer'),
  user: require('./user'),
  visit: require('./visit'),
  wallet: require('./wallet'),
}
