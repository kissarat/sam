const fs = require('fs')
const os = require('os')
const path = require('path')

function File(app) {
  app.get('/download/:id', function (req, res) {
    res.promise(File.prototype.download.call(req, {id: req.pathRoutes.slice(2).join('/')}))
  })
  app.all('/upload/:name', this.upload)
}

File.prototype = {
  download({id = this.pathRoutes.slice(2).join('/')}) {
    console.log(id)
    if ('string' !== typeof id) {
      return {
        status: 400,
        error: {
          message: 'Invalid ID'
        }
      }
    }
    return this.table('file')
      .where('id', id)
      .then(([file]) => {
        const respond = () => {
          const location = file.url ||
            (this.config.origin + `/files/${file.id}.` + this.config.file.mime[file.mime].ext[0])
          return {
            status: 302,
            headers: {
              location
            }
          }
        }
        if (file) {
          const userAgent = this.headers['user-agent']
          if (!userAgent || !this.config.file.download.exclude.some(r => r.test(userAgent))) {
            return this.table('download')
              .insert({
                file: id,
                ip: this.ip,
                agent: this.headers['user-agent'],
              })
              .then(respond)
          }
          else {
            respond()
          }
        }
        else {
          return {status: 404}
        }
      })
  },

  upload(req, res) {
    if (req.user.guest) {
      res.transmit({status: 401})
    }
    else {
      const mime = req.headers['content-type']
      const size = +req.headers['content-length']
      const type = req.config.file.mime[mime]
      if (type && size) {
        if (size <= type.size) {
          const now = Date.now()
          const id = now.toString(36) // + req.user.id.toString(36)
          const filename = id + '.' + type.ext[0]
          const tempfilename = path.join(os.tmpdir(), filename)
          const temp = fs.createWriteStream(tempfilename)
          temp.on('error', res.error)
          temp.on('close', function () {
            fs.rename(tempfilename, req.config.file.dir + '/' + filename, async function (err) {
              if (err) {
                res.error(err)
              }
              else {
                const file = {
                  id,
                  mime,
                  size,
                  ip: req.headers.ip,
                  name: req.params.name,
                  user: req.user.id
                }
                await req.table('file').insert(file)
                res.transmit({
                  status: 201,
                  success: true,
                  url: '/files/' + filename,
                  file
                })
              }
            })
          })
          req.pipe(temp)
        }
        else {
          res.transmit({
            status: 413,
            error: {
              message: `Request entity ${mime} too large: ` + size
            }
          })
        }
      }
      else {
        res.transmit({
          status: 415,
          error: {
            message: 'Unsupported media type: ' + type
          }
        })
      }
    }
  }
}

module.exports = File
