const {pick, isEmpty} = require('lodash')

module.exports = {
  $call: {auth: {}},

  $index: {method: 'GET'},
  index(params) {
    if (!this.user.admin && params.user != this.user.id) {
      return {status: 403, error: {message: "You can view your's transfers only"}}
    }
    if (params.view && ['withdrawable', 'transfer_user', 'transfer'].indexOf(params.view) < 0) {
      return {status: 400, error: {message: 'Invalid view'}}
    }
    const q = this.table(params.view || 'transfer_user')
    if (params.user > 0) {
      q.whereRaw(
        '"from" = ? OR "to" = ?',
        [params.user, params.user]
      )
    }
    const where = pick(params, 'from', 'to', 'type', 'status', 'node', 'wallet', 'txid', 'ip', 'currency')
    if (!isEmpty(where)) {
      q.where(where)
    }
    return q
  },

  $send: {method: 'POST'},
  send(data) {
    const amount = +data.amount
    if (!(amount > 0)) {
      return {
        status: 400,
        error: {
          message: 'Invalid amount'
        }
      }
    }
    return this.table('user')
      .where({nick: data.nick})
      .first('id')
      .then(user => {
        if (user) {
          const q = this.sql(`SELECT transfer_send(?, ?, ?, 'internal', ?) as id`,
            [this.user.id, user.id, amount, this.ip])
          return q.then(function ({rows: [{id}]}) {
            const success = id > 0
            const answer = {
              status: success ? 201 : -id,
              success,
              [success ? 'transfer' : 'error']: amount,
              user
            }
            if (success) {
              answer.transfer = {id, amount}
            }
            return answer
          })
        }
        else {
          return {status: 404}
        }
      })
  },

  $get: {method: 'GET', auth: {admin: true}},
  get(params) {
    return this.table('transfer')
      .where(params)
      .first()
      .then(transfer => ({transfer}))
  },

  $create: {method: 'POST', auth: {admin: true}},
  async create(params) {
    const [transfer] = await this.table('transfer')
      .insert(params, ['id', 'type', 'status', 'amount', 'from'])
    const success = transfer && transfer.id > 0
    if (success && 'success' === transfer.status && this.site.telegram) {
      setTimeout(async () => {
        const user = await this.table('user')
          .where('id', params.from)
          .first('nick')
        if (user) {
          this.site.telegram.broadcast(`Пользователь ${user.nick} вывел ${transfer.amount / this.config.money.scale}$`)
        }
      }, 1000)
    }
    return {
      status: success ? 201 : 500,
      success,
      transfer
    }
  },

  $save: {method: 'POST', auth: {admin: true}},
  save(params, data) {
    data.time = new Date()
    return this.table('transfer')
      .where(params)
      .update(data, ['id', 'time'])
      .then(function ([transfer]) {
        const success = transfer && transfer.id > 0
        return {
          status: success ? 200 : 500,
          success,
          transfer
        }
      })
  },

  $remove: {method: 'DELETE', auth: {admin: true}},
  remove(params) {
    return this.table('transfer')
      .where(params)
      .del()
  },

  exchange() {
    return this.table('exchange')
  },

  $rate: {auth: {nick: 'cron'}},
  async rate() {
    const result = await this.site.exchange.download()
    result.success = true
    return result
  },

  $withdraw: {method: 'POST'},
  async withdraw(data) {
    if ('inbisoft' === this.config.id) {
      const user = await this.table('user')
        .where('id', this.user.id)
        .first()
      if (/^U\d+$/.test(user.perfect)) {
        data.wallet = user.perfect
      }
      else {
        return {status: 400, error: {message: 'Specify your wallet in the settings'}}
      }
    }
    else if (!/\w{8,40}/.test(data.wallet)) {
      return {status: 400, error: {message: 'Wallet is required'}}
    }
    data.wallet = data.wallet.replace(/[^\w]/g, '')
    data.amount = parseInt(data.amount)
    if (data.amount >= this.config.money.withdraw.min) {
      const balance = await this.table('balance')
        .where('id', this.user.id)
        .first()
      if (!balance || 'number' !== typeof balance.balance || data.amount > balance.balance) {
        return {status: 400, error: {message: 'Insufficient funds'}}
      }
      const previous = await this.table('transfer')
        .where({
          from: this.user.id,
          type: 'withdraw',
          status: 'created'
        })
        .first('id')
      if (previous) {
        return {
          status: 400, error: {
            id: previous.id,
            message: 'You already have an unverified withdrawal request with ID {id}'
          }
        }
      }
      data.text = 'string' === typeof data.text ? data.text.trim() || null : null
      data = pick(data, 'amount', 'wallet', 'text')
      data.from = this.user.id
      data.ip = this.ip
      data.type = 'withdraw'
      data.status = 'created'
      return this.table('transfer')
        .insert(data, ['id', 'created', 'from', 'amount', 'ip'])
        .then(([transfer]) => ({
          status: transfer.id > 0 ? 201 : 500,
          success: transfer.id > 0 ? 201 : 500,
          transfer
        }))
    }
    return {
      status: 400,
      error: {
        amount: this.config.money.withdraw.min / this.config.money.scale,
        message: 'The amount must be more than {amount}'
      }
    }
  }
}
