const db = require('schema-db')
const request = require('request-promise')
const {createHash} = require('crypto')
const {isEmpty, pick, defaults} = require('lodash')
const {stringify} = require('querystring')

function admin(method = 'GET') {
  return {
    method,
    auth: {admin: true}
  }
}

function invokePayeer(params) {
  defaults(params, {
    account: this.config.payeer.account,
    apiId: this.config.payeer.id,
    apiPass: this.config.payeer.password
  })
  return request({
    method: 'POST',
    url: 'https://payeer.com/ajax/api/api.php',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: stringify(params)
  })
    .then(JSON.parse);
}

function seq(promises) {
  return promises.length > 1
    ? promises[0].then(() => seq(promises.slice(1)))
    : promises[0]
}

async function receive(d) {
  if (isEmpty(d)) {
    return {
      status: 400,
      error: {
        message: 'Empty request'
      }
    }
  }
  else {
    const hash = createHash('sha256')
    const array = [
      d.m_operation_id,
      d.m_operation_ps,
      d.m_operation_date,
      d.m_operation_pay_date,
      d.m_shop,
      d.m_orderid,
      d.m_amount,
      d.m_curr,
      d.m_desc,
      d.m_status,
      this.config.payeer.secret
    ]
    hash.update(array.join(':'), 'utf8')
    const m = {}
    const realAmount = +d.m_amount
    if ('success' != d.m_status) {
      m.error = {
        status: 'INVALID_STATUS',
        message: 'Status must be success'
      }
    }
    else if (!isFinite(realAmount)) {
      m.error = {
        status: 'INVALID_AMOUNT',
        message: 'Invalid amount'
      }
    }
    else if (hash.digest('hex').toUpperCase() !== d.m_sign) {
      m.error = {
        status: 'INVALID_SIGNATURE',
        message: 'Invalid signature'
      }
    }
    else {
      const changes = {
        amount: d.m_amount * this.config.money.scale,
        type: 'payment',
        status: 'success',
        txid: d.m_operation_id,
        time: new Date(),
        vars: {
          ps: +d.m_operation_ps,
          paid: d.m_operation_pay_date
        }
      }
      if (d.client_account) {
        changes.wallet = d.client_account
      }
      const [transfer] = await this.table('transfer')
        .where('id', d.m_orderid)
        .update(changes, ['id', 'txid', 'time', 'vars', 'to'])
      await this.log('transfer', 'approve', transfer, transfer.to)
      return {
        status: 302,
        headers: {
          location: this.config.origin + '/payment/success/' + d.m_orderid
        }
      }
    }
    if (m.error) {
      m.status = 403
      return m
    }
  }
}

module.exports = {
  $pay: {method: 'POST', auth: {admin: true}, amount: 0},
  async pay({amount}) {
    amount = +amount
    const data = {
      to: this.user.id,
      amount,
      type: 'payment',
      status: 'created',
      system: 'payeer',
      ip: this.ip
    }
    const [transfer] = await this.table('transfer')
      .insert(data, ['id', 'created', 'amount', 'ip'])
    // const m_amount = (Math.round(transfer.amount * (1 + this.config.payeer.commission)) / 100).toFixed(2)
    const m_amount = (transfer.amount / this.config.money.scale).toFixed(Math.log10(this.config.money.scale))
    const created = new Date(transfer.created).toISOString()
    const m_desc = new Buffer(`#${transfer.id} $${m_amount} at ${created} from ${transfer.ip}`).toString('base64')
    const d = {
      m_orderid: transfer.id,
      m_shop: this.config.payeer.shop,
      m_amount,
      m_desc,
      m_curr: 'USD',
      lang: 'ru'
    }
    const hash = createHash('sha256')
    const string = [d.m_shop, d.m_orderid, d.m_amount, d.m_curr, d.m_desc, this.config.payeer.secret].join(':')
    hash.update(string, 'utf8')
    d.m_sign = hash.digest('hex').toUpperCase()
    return {
      status: 201,
      success: true,
      url: 'https://payeer.com/merchant/?' + stringify(d),
      transfer
    }
  },

  $status: {auth: {nick: 'payeer'}},
  status: receive,

  $success: {auth: {nick: 'payeer_public'}},
  success: receive,

  $fail: {auth: {nick: 'payeer_public'}},
  fail: receive,

  $history: admin(),
  history(params) {
    params = pick(params, 'sort', 'count', 'from', 'to', 'type', 'append')
    defaults(params, {
      count: 1000
    })
    return invokePayeer.call(this, defaults(params, {
      action: 'history'
    }))
  },

  $get: admin(),
  get({id}) {
    return invokePayeer.call(this, {
      action: 'historyInfo',
      historyId: id
    })
  },

  $balance: admin(),
  balance() {
    return invokePayeer.call(this, {
      action: 'balance',
    })
  },

  $send: admin('POST'),
  async send({id}) {
    id = +id
    let transfer = await this.table('transfer')
      .where('id', id)
      .first()
    if ('success' === transfer.status) {
      return {status: 403}
    }
    const response = await this.site.payeer.send(transfer.wallet, transfer.amount / 100, transfer.id)
    return {response}
  }
}
