module.exports = {
  $call: {auth: {admin: true}},

  $index: {method: 'GET'},
  index(params) {
    return this.table('request')
  },

  $remove: {method: 'DELETE'},
  async remove(params) {
    const requests = await this.table('request')
      .where(params)
      .del(['id', 'time'])
    return {
      success: requests.length > 0,
      requests
    }
  }
}
