/**
 * Last modified: 18.06.03 17:14:25
 * Hash: 8ce7897715f8e1c4ad441d2e70aa24614e3d3784
 */

const bcrypt = require('promised-bcrypt')
const mail = require('./mail')
const rp = require('request-promise')
const rs = require('randomstring')
const tokenModule = require('./token')
const {pick, omit, isEmpty, defaults, toArray} = require('lodash')
const {tree} = require('../structure')
const crypto = require('crypto')

const userFields = ['id', 'nick', 'forename', 'surname', 'skype', 'parent']
const passwordRegex = /^.{6,128}$/

async function gavatar({email}) {
  const hash = crypto.createHash('md5')
  hash.update(email)
  const digest = hash.digest('hex')
  const gavatar = await rp(`https://ru.gravatar.com/${digest}.json`, {
    headers: {
      accept: 'application/json',
      'user-agent': 'Sam'
    }
  })
  return JSON.parse(gavatar).entry[0]
}

module.exports = {
  gavatar,

  $find: {
    columns: ['id', 'nick', 'email', 'admin', 'leader', 'skype', 'avatar', 'created', 'surname', 'forename', 'type', 'time', 'parent'],
    sort: ['id', 'nick', 'email', 'skype', 'created', 'forename', 'surname', 'time', 'parent'],
    query: ['id', 'nick', 'email', 'skype', 'created', 'forename', 'surname', 'time', 'parent'],
    // auth: {}
  },

  async _checkPassword(user, password) {
    if (user.secret) {
      const success = await bcrypt.compare(password, user.secret)
      if (success) {
        const token = await this.token.update({user: user.id})
        token.success = true
        delete user.secret
        delete user.hard
        delete user.soft
        token.user = user
        return token
      }
      return {
        status: 400,
        success,
        error: {
          status: 'ABSENT',
          name: 'password'
        }
      }
    }
    else {
      return {
        status: 403,
        error: {
          message: 'User has no password'
        }
      }
    }
  },

  async _login(params, user) {
    const invalid = this.check('hard', user.hard)
    if (invalid) {
      return invalid
    }
    else {
      const checked = await this.invoke('_checkPassword', user, params.password)
      if (checked.success && !user.hard && this.token.hard) {
        await this.table('user')
          .where('id', user.id)
          .update(pick(this.token, 'hard', 'soft'))
        return checked
      }
      return checked
    }
  },

  async login(params) {
    const userCondition = pick(params, 'nick', 'email')
    if (isEmpty(userCondition)) {
      return {
        status: 400,
        error: {
          status: 'ALERT',
          message: 'Email/логин и пароль не указан'
        }
      }
    }
    if (!params.password) {
      return {
        status: 400,
        error: {
          status: 'ALERT',
          message: 'Пароль не указан'
        }
      }
    }

    const sha256 = crypto.createHash('sha256')
    sha256.update(JSON.stringify(params))
    const request = {
      headers: pick(this.headers, 'ip', 'user-agent', 'content-type', 'content-length'),
      'data-sha256': sha256.digest('hex')
    }
    const users = await this.table('user')
      .where(userCondition)
    if (users.length > 0) {
      const user = users[0]
      if (false !== this.config.restrict.enabled && user.admin) {
        if (!this.ip) {
          return {
            status: 500,
            request,
            error: {message: 'Server Error: No IP is undefined ' + this.ip}
          }
        }
        const q = this.table('ip')
        if (this.config.ip.v4.regex.test(this.ip)) {
          q.whereRaw('id = ? OR id = ?', [this.ip, '::ffff:' + this.ip])
        }
        else {
          q.whereRaw('id', this.ip)
        }
        const rows = await q.count()
        if (!(rows[0].count > 0)) {
          return {
            status: 403,
            request,
            error: {
              message: 'Your cannot access from IP ' + this.ip
            }
          }
        }
      }
      // if (this.config.signup.verify.enabled && 'new' === user.type) {
      //   return {
      //     status: 403,
      //     error: {
      //       message: 'Для входа вы должны подтвердить свой email'
      //     }
      //   }
      // }
      // else {
      const data = await this.invoke('_login', params, user)
      if (data.success) {
        params.token = this.token.id
        await this.log('token', 'login', params, user.id)
      }
      // }
      return data
    }
    return {
      status: 400,
      request,
      error: {
        status: 'ABSENT',
        name: 'login'
      }
    }
  },

  $logout: {auth: {}, method: 'POST'},
  async logout() {
    const r = await this.token.update({user: null})
    await tokenModule.handshake.call(this)
    await this.log('token', 'logout', {token: this.token.id}, this.user.id)
    r.success = true
    return r
  },

  $signup: {method: 'POST'},
  async signup(data) {
    const required = {
      email: /^.*@.*\.\w+$/,
      nick: /^[\w_]+$/,
      password: passwordRegex
    }
    const invalid = {}
    for (const name in required) {
      const value = data[name]
      if (!value || !required[name].test(value)) {
        invalid[name] = value
      }
    }
    if (!isEmpty(invalid)) {
      return {
        status: 400,
        error: {invalid}
      }
    }

    if (this.user.admin && data.nick.indexOf('_test_') === 0) {
      const faker = require('faker')
      data.nick = data.nick.replace('_test_', faker.internet.userName() + '_')
    }
    data.type = 'user'

    const secret = await bcrypt.hash(data.password)
    const _signup = async () => {
      data = pick(data, 'nick', 'email', 'parent')
      data.type = 'email' === this.config.signup.verify.type ? 'new' : 'native'
      data.secret = secret
      try {
        const [user] = await this.table('user')
          .insert(data, ['id', 'type', 'nick', 'created', 'email', 'parent'])
        if (this.config.signup.verify.enabled && 'email' === this.config.signup.verify.type) {
          await mail.activate.call(this, user)
        }
        const result = {
          status: 201,
          success: true,
          user,
        }
        data.token = this.token.id
        await this.log('user', 'create', data, user.id)
        try {
          let gav = await gavatar({email: user.email})
          gav = {
            gavatar: +gav.id,
            avatar: gav.thumbnailUrl
          }
          const u = await this.table('user')
            .where('id', user.id)
            .update(gav, ['gavatar', 'avatar'])
          if (u && u.length > 0) {
            user.gavatar = u.gavatar
            user.avatar = u.avatar
          }
        }
        catch (ex) {
        }

        if (this.site.telegram) {
          setTimeout(async () => {
            const user = await this.table('user_parent')
              .where('id', user.id)
              .first()
            if (user) {
              this.site.telegram.broadcast(`Пользователь ${user.nick} зарегистрировался под ${user.parent_nick}`)
            }
          }, 1000)
        }
        return result
      }
      catch (err) {
        if (this.site.database.UNIQUE_VIOLATION === err.code) {
          return {
            status: 400,
            error: {
              status: 'invalid_unique',
              code: err.code,
              cause: 'database',
              name: err.constraint.replace('user_', '')
            }
          }
        }
        throw err
      }
    }
    if (data.ref) {
      const {id} = await this.table('user')
        .where({nick: data.ref})
        .first('id')
      if (id > 0) {
        data.parent = id
        return _signup()
      }
      else {
        return {
          status: 404,
          error: {
            message: 'Sponsor not found'
          }
        }
      }
    }
    if (!data.parent) {
      data.parent = this.config.signup.root
    }
    return _signup()
  },

  $me: {method: 'GET'},
  async me() {
    return {
      ip: this.ip,
      user: omit(this.user, 'secret', 'hard', 'soft')
    }
  },

  $freshman: {method: 'GET', auth: {admin: true}},
  freshman() {
    return this.table('user')
      .select(userFields)
      .orderBy('created', 'desc')
      .limit(5)
  },

  $get: {method: 'GET', auth: {admin: true}},
  async get(params) {
    const where = pick(params, 'id', 'nick', 'email')
    if (isEmpty(where)) {
      params.id = this.user.id
    }
    // FUCKUP: informer
    const user = await this.table('user')
      .where(where)
      .first()
    if (user) {
      if (params.buyer > 0) {
        const [{count}] = await this.table('node')
          .where('user', user.id)
          .count()
        return {
          user,
          buyer: count > 0
        }
      }
      return {user}
    }
    return {status: 404}
  },

  $tree: {method: 'GET', auth: {}},
  tree(params) {
    if (!this.user.admin) {
      if (+params.id !== this.user.id) {
        return {
          status: 403,
          error: {
            message: 'You can see your structure only'
          }
        }
      }
      if (['referral', 'sponsor'].indexOf(params.view) < 0) {
        return {
          status: 403,
          error: {
            message: 'You can access "referral" or "sponsor"'
          }
        }
      }
    }
    return tree.call(this, params)
  },

  $save: {method: 'POST', auth: {admin: true}},
  async save({id}, data) {
    const fields = {
      advcash: /^U\d{12}$/,
      email: /.*@.*\.\w+/,
      parent: /^\d+$/,
      payeer: /^P\d+$/,
      perfect: /^U\d{7,8}$/,
    }
    const changes = {}
    for (const key in data) {
      const regex = fields[key]
      if (regex instanceof RegExp) {
        let value = data[key]
        if ('number' === typeof value) {
          value = value.toString()
        }
        if (regex.test(value)) {
          changes[key] = value
        }
        else {
          return {
            status: 400,
            error: {message: 'Неправильный формат для ' + key}
          }
        }
      }
      else {
        return {
          status: 400,
          error: {message: key.replace(/</g, '') + ' нельзя изменять'}
        }
      }
    }
    const count = await this.table('user')
      .where({id: +id})
      .update(changes)
    return {
      success: 1 === count,
      count
    }
  },

  $password: {method: 'POST', auth: {admin: true}},
  async password({id}, {password, pin}) {
    if (!(id > 0)) {
      return {
        status: 400,
        error: {
          message: 'Укажите id пользователя'
        }
      }
    }
    if (!passwordRegex.test(password)) {
      return {
        status: 400,
        error: {
          message: 'Пароль должен содержать не меньше 6-ти и не больше 128 символов'
        }
      }
    }
    const changes = {}
    if (password) {
      changes.secret = await bcrypt.hash(password)
    }
    if (pin) {
      changes.pin = pin
    }
    if (isEmpty(changes)) {
      return {
        status: 400,
        error: {
          message: 'Nothing changed'
        }
      }
    }
    changes.time = new Date()
    const [user] = await this.table('user')
      .where({id})
      .update(changes, ['id', 'time', 'created', 'nick'])
    changes.id = +id
    await this.log('user', 'password', changes)
    return {
      success: !!user,
      user
    }
  },

  $ban: {method: 'POST', auth: {admin: true}},
  async ban({id}) {
    const [user] = await this.table('user')
      .where({id})
      .update({secret: null, time: new Date()}, ['id', 'time', 'created', 'nick'])
    let rows = []
    if (user) {
      rows = await this.table('token')
        .where({user: user.id})
        .del()
    }
    await this.log('user', 'ban', {user: +id})
    return {
      success: !!user,
      tokens: rows.length,
      user
    }
  },

  $structure: {method: 'GET', auth: {admin: true}},
  async structure({view, level, limit}) {
    let allowed = ['sponsor', 'referral']
    if (allowed.indexOf(view) < 0) {
      allowed = allowed.join(' or ')
      return {
        status: 400,
        error: {
          view,
          message: `Parameter view must be ${allowed}`
        }
      }
    }
    level = level
      ? level.split('.').map(a => +a)
      : [1, 2, 3, 4, 5]
    const q = this.table(view)
      .where('root', this.user.id)
      .whereIn('level', level)
      .orderBy('level', 'asc')
      .orderBy('id', 'desc')
    if (limit > 0) {
      q.limit(limit)
    }
    const users = await q
    return {['sponsor' === view ? 'sponsors' : 'referrals']: users}
  },

  $children: {method: 'GET', auth: {admin: true}},
  async children({select, parent}) {
    if (parent > 0) {
      const users = await this.table('children')
        .where('parent', +parent)
        .orderBy('id', 'desc')
      return {users}
    }
    else {
      return {status: 400}
    }
  },

  $index: {method: 'GET', auth: {admin: true}},
  index({search}) {
    const q = this.table('admin_user')
    if ('string' === typeof search && (search = search.trim())) {
      for (const token of search.split(/[+\s]+/g)) {
        for (const key of ['nick', 'email', 'surname', 'forename']) {
          q.orWhere(key, 'ilike', `%${token}%`)
        }
      }
    }
    return q
  },

  $verify: {method: 'GET', token: 'code', auth: {admin: true}},
  async verify() {
    await this.table('user')
      .where({
        id: this.user.id,
        type: 'new'
      })
      .update({type: 'native'})
    await this.table('token')
      .where('id', this.token.id)
      .del()
    return {
      status: 302,
      headers: {
        location: this.config.origin + '/login'
      }
    }
  },

  $recovery: {method: ['POST', 'PUT'], auth: {admin: true}},
  async recovery(params) {
    if ('POST' === this.method) {
      const query = pick(params, 'email', 'nick')
      let user = await this.table('user')
        .where(query)
        .first('id', 'nick', 'email', 'type')
      if (!user) {
        return {
          status: 404,
          query,
          error: {
            message: 'Пользователь не найден'
          }
        }
      }
      if ('native' !== user.type) {
        return {
          status: 403,
          error: {
            message: 'Вы не подтвердили email после регистрации, поэтому не можете восстанавливать пароль'
          }
        }
      }
      // if (!this.user.admin && this.user.id != user.id) {
      //   return {
      //     status: 403,
      //     error: {
      //       message: 'Только администратор может послать запрос другому пользователю'
      //     }
      //   }
      // }
      let token = await this.table('token')
        .where({
          type: 'code',
          user: user.id
        })
        .first()
      if (!token) {
        const data = {
          id: rs.generate(this.config.signup.verify.size),
          type: 'code',
          user: user.id,
          expires: new Date(Date.now() + this.config.signup.verify.duration),
          ip: this.ip,
          name: this.headers['user-agent']
        };
        [token] = await this.table('token')
          .insert(data, ['id', 'user', 'created', 'expires', 'ip', 'name'])
      }
      const locals = {
        to: user.email,
        subject: 'Восстановления пароля ' + this.config.name,
        token: token.id
      }
      await this.site.mail.sendTemplate('recovery', locals)
      const success = !!(token && token.id && token.user > 0)
      return {
        status: success ? 201 : 500,
        success,
        user,
        token
      }
    }
    else if ('PUT' === this.method) {
      if (!this.user.admin && 'code' !== this.token.type) {
        return {
          status: 400,
          error: {
            message: 'Ссылка для восстановления пароля не действительная'
          }
        }
      }
      if (!passwordRegex.test(params.password)) {
        return {
          status: 400,
          error: {
            message: 'Пароль слишком короткий'
          }
        }
      }
      // if (params.password)
      const secret = await bcrypt.hash(params.password)
      const [user] = await this.table('user')
        .where('id', this.user.id)
        .update({
            secret,
            time: new Date()
          },
          ['id', 'nick', 'email', 'time'])
      await this.table('token')
        .where('id', this.token.id)
        .del()
      return {
        success: true,
        user,
        token: this.token
      }
    }
    else {
      return {status: 405}
    }
  },

  $sync: {method: 'POST', auth: {leader: true}},
  async sync(params, body) {
    const user = await this.table('user')
      .where(params)
      .first('nick', 'email', 'secret', 'type', 'forename', 'surname', 'mlbot')
    if (!user) {
      return {status: 404}
    }
    defaults(body, user)
    // console.log(body)
    const url = `http://app.inbisoft.com/serve/~${this.config.mlbot.server.key}/user/mlbot?email=` + user.email
    const r = await rp(url, {
      method: 'POST',
      body,
      json: true
    })
    if (200 === r.status) {
      await this.table('user')
        .where(params)
        .update(body)
    }
    return r
  },

  /*
  async soft() {
    const users = await this.table('user')
      .whereRaw('soft IS NOT NULL')
      .select('nick', 'soft')
      .orderBy('id', 'desc')
    const object = {}
    for (const user of users) {
      object[user.nick] = user.soft
    }
    return object
  },
  */

  $import: {method: 'GET', auth: {admin: true}},
  async import({token, hostname, path, select}) {
    const response = await this.site.remote.import(token || this.token.id, hostname, path, {limit: 1000})
    if (response.users) {
      const ids = response.users.map(u => u.id)
      const last = Math.max(...ids)
      const keyed = {}
      select = select.split(',')
      for (const data of response.users) {
        const user = pick(data, select)
        user.parent = data.parent && data.parent.id ? data.parent.id : data.parent
        keyed[data.id] = user
      }
      const existing = (
        await this.table('user')
          .whereIn('id', ids)
          .select('id')
      )
        .map(u => u.id)
      for (const id of existing) {
        await this.table('user')
          .where({id})
          .update(keyed[id])
        delete keyed[id]
      }
      const inserted = toArray(keyed)
      await this.table('user')
        .insert(inserted)
      await this.sql('ALTER SEQUENCE user_id_seq RESTART WITH ' + (last + 1))
      return {
        last,
        count: response.users.length,
        inserted
      }
    }
    return response
  },

  turnover() {
    return this.table('user_turnover')
  }
}
