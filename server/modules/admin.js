module.exports = {
  $call: {auth: {admin: true}},

  statistics({view = 'transfer_statistics'}) {
    if (['transfer_statistics', 'transfer_week', 'transfer_day'].indexOf(view) < 0) {
      return {status: 400}
    }
    return this.table(view)
  },

  async balance() {
    const r = await this.table('all_balance')
    const result = r.reduce((a, b) => (a[b.currency] = b.amount) && a, {})
    return {result}
  },

  future() {
    return this.table('future_json')
  }
}
