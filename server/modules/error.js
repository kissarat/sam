module.exports = {
  //conflict
  409() {
    return {
      status: 'ALERT',
      error: {
        message: 'Вы можете использовать данное приложение только на одном компьютере'
      }
    }
  },

  //server
  500() {
    return {}
  },

  //unknown
  520() {
    return {}
  },

  other() {
    return {
    }
  }
}
