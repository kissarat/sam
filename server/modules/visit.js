const {pick} = require('lodash')

module.exports = {
  $trace: {method: 'POST', token: true},
  trace(data) {
    data = pick(data, 'url', 'spend')
    data.token = this.token.id
    return this.table('visit')
      .insert(data, ['id', 'created'])
      .then(([visit]) => ({status: visit ? 201 : 500, success: !!visit, visit}))
  },

  $redirect: {method: 'GET'},
  redirect({to, nick}) {
    if (nick && to) {
      const data = {
        url: to + '/' + nick,
        ip: this.ip,
        agent: this.headers['user-agent'] || null
      }
      return this.table('visit')
        .insert(data)
        .then(() => ({
          status: 302,
          headers: {
            location: 'http://mlbot.inbisoft.com/'
          }
        }))
    }
    return {status: 400}
  },

  $external: {method: ['GET', 'POST']},
  async external({url, spend, sam}) {
    url = url ? new Buffer(url, 'base64').toString('utf8') : this.headers.referrer
    const regex = /sam=([^;]+)/.exec(this.headers.cookie)
    if (!sam && regex) {
      sam = regex[1]
    }
    const [visit] = await this.table('visit')
      .insert({
          agent: this.headers['user-agent'],
          ip: this.ip,
          spend: +spend || 0,
          token: sam,
          url,
        },
        ['id', 'created', 'ip'])
    return {
      success: visit.id > 0,
      visit
    }
  },

  $ref: {method: 'GET', token: {}},
  async ref({nick, unique}) {
    const user = await this.table('user')
      .where('nick', nick)
      .first('id', 'nick')
    const data = {url: '/ref/' + nick}
    if (this.token.id) {
      data.token = this.token.id
    }
    else {
      data.ip = this.ip
    }

    if (user) {
      if (unique > 0) {
        const visit = await this.table('visit')
          .where(data)
          .first('id', 'created')
        if (visit && visit.id > 0) {
          return {
            success: true,
            status: 200,
            visit,
            user
          }
        }
      }

      data.ip = this.ip
      const [visit] = await this.table('visit')
        .insert(data, ['id', 'created'])
      return {
        success: true,
        status: 201,
        visit,
        user
      }
    }
    else {
      return {status: 404}
    }
  }
}
