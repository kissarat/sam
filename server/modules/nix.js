const perfect = require('./perfect')

module.exports = Object.create(perfect)

module.exports.pay = function (params) {
  params.url = this.config.nix.url
  return perfect.pay.call(this, ...arguments)
}

module.exports.success = async function success({id}, {PAYMENT_BATCH_NUM}) {
  const transfer = await this.table('transfer')
    .where({id, txid: PAYMENT_BATCH_NUM})
    .first()

  if (!transfer) {
    return {
      status: 404,
      error: {
        message: `Transfer ${PAYMENT_BATCH_NUM} not found`
      }
    }
  }

  const status = 'success' === transfer.status ? 'success' : 'fail'

  return {
    status: 301,
    headers: {
      location: this.config.origin + `/payment/${status}/` + transfer.id
    }
  }
}
