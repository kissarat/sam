const {pick, each, omit, isObject} = require('lodash')

module.exports = {
  $routes: {auth: {nick: 'cron'}},
  // $routes: {auth: {admin: true}},
  routes(query = {}) {
    query.access = +query.access
    query.app = +query.app
    const routes = {}
    const modules = query.app ? this.app.modules : this.modules
    each(omit(modules, 'error', 'access', 'home'), function (module, name) {
      const moduleResult = query.access ? pick(module, '$call') : {}
      each(module, function (fn, name) {
        if (fn instanceof Function) {
          const params = /^[\w\s]+\(([^)]*)/.exec(fn.toString())[1]
          const accessName = '$' + name
          const access = module[accessName]
          moduleResult[name] = query.access && access ? {params, access} : params
        }
      })
      routes[name] = moduleResult
    })
    return {routes}
  },

  config() {
    const data = pick(this.config, 'id', 'name', 'maintenance', 'origin', 'money',
      'data', 'http', 'ip', 'signup', 'file', 'front')
    data.money.systems = ['perfect', 'payeer', 'blockio', 'nix', 'advcash']
      .filter(s => this.config[s] && false !== this.config[s].enabled)
    if (this.user.admin) {
      data.money.withdraw.enabled = !!this.config.blockio.secret
    }
    if (isObject(this.config.recaptcha)) {
      data.recaptcha = pick(this.config.recaptcha, 'key')
    }
    // data.database = this.site.config.database.connection
    const time = process.hrtime()
    const now = Date.now()
    data.uptime = time[0] + time[1] / 1000000000
    data.start = new Date(now - data.uptime * 1000).toISOString()
    data.time = new Date(now).toISOString()
    // data.schema = this.site.database.schema
    return data
  },

  // $server: {auth: {nick: 'cron'}},
  $server: {auth: {admin: true}},
  server() {
    return this.site.config
  },

  mirror(query, body) {
    return {
      headers: this.headers,
      params: this.params,
      query,
      body
    }
  },

  schema() {
    const schemas = {}
    const entities = this.site.database.entities
    for(const id of Object.keys(entities).sort()) {
      schemas[id] = entities[id].schema
    }
    return schemas
  },

  $statistics: {auth: {admin: true}},
  async statistics({referrals}) {
    const statistics = await this.table('statistics')
      .first()
    each(statistics, function (value, key) {
      statistics[key] = +(value || 0)
    })
    const r = {statistics}
    if (+referrals) {
      r.referrals = await this.table('top_referrals')
    }
    r.time = new Date()
    return r
  },

  // settings() {
  //   return this.site.getSettings()
  // },

  async proxy(params) {
    let options
    if (params.uri) {
      options = {}
      options.uri = params.uri
      delete params.uri
      options.qs = params
    }
    else if (params.url) {
      options = params
    }
    const resp = await this.site.remote.request(options)
    return JSON.parse(resp)
  },

  status() {
    return {success: true}
  }
}
