const {pick} = require('lodash')

module.exports = {
  error(params, data) {
    data = pick(data, 'url', 'name', 'message', 'stack')
    if (data.stack instanceof Array) {
      data.stack = data.stack.join('\n')
    }
    return this.table('report')
      .insert(data, 'id')
      .then(function ([id]) {
        id = +id
        return {
          success: id > 0,
          id
        }
      })
  }
}
