module.exports = {
  $call: {auth: {}},

  $save: {method: 'POST'},
  save({id}, data) {
    data.user = this.user.id
    let q = this.table('message')
    q = id
      ? q.where('id', id).update(data, ['id'])
      : q.insert(data, ['id', 'created'])
    return q.then(([message]) => {
      if (message) {
        if (!this.user.admin) {
          setTimeout(() => this.site.telegram.feedback(`${this.config.origin}/ticket/${data.path} ${data.text}`), 1000)
        }
        return {
          status: id ? 200 : 201,
          success: message.id > 0,
          message
        }
      }
      return {status: 404}
    })
  },
}
