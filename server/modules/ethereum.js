const _ = require('lodash')
const fs = require('fs')

function Ethereum(app, site) {
}

Ethereum.prototype = {
  $load: {method: 'GET', auth: {admin: true}},
  async load({currency, balance}) {
    const time = new Date().toISOString()
    const addresses = await this.site.ethereum.invoke(currency, 'personal_listAccounts')
    // TODO: load wallets
    // await this.table('wallet').del()
    const com = currency.toLowerCase()
    const users = await this.table('user').whereIn(com, addresses)
    let accounts = []
    for (const id of addresses) {
      const user = users.find(u => id === u[com])
      accounts.push({id, currency, time, system: 'ethereum', nick: user ? user.nick : null})
    }
    if (balance > 0) {
      balance = +balance
      let result = []
      for(const account of accounts) {
        let amount = await this.site.ethereum.invoke(currency, 'eth_getBalance', account.id, 'latest')
        amount = this.site.ethereum.fromWei(amount)
        if (amount >= balance) {
          account.amount = amount
          result.push(account)
        }
      }
      accounts = result
    }
    // await this.table('wallet').insert(result.map(t => _.omit(t, 'nick')))
    return {success: true, result: accounts}
  },

  /*
  $wallets: {method: 'GET', auth: {admin: true}},
  async wallets({min = 0}) {
    const q = this.table('wallet')
        .where('system', 'ethereum')
    if (min > 0) {
      const fee = this.site.ethereum.fromWei(await this.site.ethereum.getFee())
      q.whereRaw('amount >= ?', [+min - fee])
    }
    return {result: await q}
  },
*/
  $unlock: {method: 'POST', auth: {admin: true}},
  async unlock({currency, id, password, seconds}) {
    const success = await this.site.ethereum
      .invoke(currency, 'personal_unlockAccount', id,
        'default' === password ? this.site.ethereum.password : password || '', seconds || null)
    return {success}
  },

  $send: {method: 'POST', auth: {admin: true}},
  async send({currency, from, to, amount, password}) {
    const estimateGas = await this.site.ethereum.estimateGas(currency)
    const gasPrice = await this.site.ethereum.gasPrice(currency)
    const value = this.site.ethereum.toWei(amount)
    // console.log(estimateGas, gasPrice, value, '0x' + (value - estimateGas * gasPrice).toString(16))
    const result = await this.site.ethereum
      .invoke(currency, 'eth_sendTransaction', {
        from,
        to,
        value: '0x' + (value - estimateGas * gasPrice).toString(16),
        gas: estimateGas,
        // data: ETH_CONTRACT_EMPTY
      })
    return {success: true, result}
  },

  // $fee: {method: 'GET', auth: {admin: true}},
  async fee({currency}) {
    return {result: await this.site.ethereum.getFee(currency)}
  },

  $estimateGas: {method: 'GET', auth: {admin: true}},
  async estimateGas() {
    return {result: await this.site.ethereum.estimateGas()}
  },

  $gasPrice: {method: 'GET', auth: {admin: true}},
  async gasPrice() {
    return {result: await this.site.ethereum.gasPrice()}
  },
}

module.exports = Ethereum
