const bcrypt = require('promised-bcrypt')
const fs = require('fs')
const Translation = require('../../common/i18n/translation')
const {isObject, values, pick, sample} = require('lodash')

const Keyboard = {
  Login: ['Other login'],
  Cabinet: ['Balance', 'Income', 'Team', 'Logout'],
  Boolean: ['Yes', 'No']
}

function admin(method) {
  return {
    method,
    auth: {admin: true}
  }
}

module.exports = {
  $chat: admin('GET'),
  chat({id}) {
    return this.site.telegram.getChat(id)
  },

  $me: admin('GET'),
  me() {
    return this.site.telegram.getMe()
  },

  $send: admin('POST'),
  send(params) {
    return this.site.telegram.sendMessage(params)
  },

  $broadcast: admin('POST'),
  broadcast({text}) {
    return this.site.telegram.broadcast(text)
  },

  $hook: {auth: {nick: 'telegram'}},
  async hook({message}) {
    // const time = new Date()
    const isMessage = isObject(message) && isObject(message.from) && isObject(message.chat)
      && 'number' === typeof message.from.id
      && 'string' === typeof message.text
    if (isMessage) {
      const rawReplica = message.text.trim()
      const userRepilca = rawReplica
        .replace(/\s+/g, ' ')
        .toLocaleLowerCase()
      const text = []
      let buttons = []
      let t
      let user = await this.table('user_telegram')
        .where('telegram', message.from.id)
        .first('id', 'email', 'nick', 'admin', 'surname', 'forename', 'context')
      if (!user || !user.context) {
        await this.table('telegram')
          .insert({id: message.from.id})
        user = {telegram: message.from.id, context: {}}
      }
      let translation = Translation.findTranslation(userRepilca)
      if (translation) {
        user.context.language = translation.language
      }
      else if (!user.context.language) {
        text.push(values(Translation.multitranslate('Choose language')).join('. ') + '.')
        buttons = ['Русский', 'English']
      }
      if (user.context.language) {
        if (!translation) {
          translation = Translation.list[user.context.language]
        }
        t = translation.toFunction()
        const replica = translation.inverse(userRepilca)
        if (/(fuck|блядь|хуй|пизда|залупа|лох)/.test(replica)) {
          text.push(t("I'm not Siri, I don't understand profanity"))
        }
        if (0 === 'reset'.indexOf(replica) || 'logout' === replica) {
          user.context = pick(user.context, 'language', 'registered')
          await this.table('user')
            .where('telegram', message.from.id)
            .update({telegram: null})
          text.push(t('You have successfully logged out!'))
          buttons = Keyboard.Login
        }
        if ('other login' === replica) {
          user.context.nick = null
          user.context.loginAsked = false
        }
        if ('boolean' === typeof user.context.registered) {
          if (user.context.nick) {
            if (user.id) {
              switch (replica) {
                case 'profile':
                case 'balance':
                  const {balance} = await this.table('balance')
                    .where('id', user.id)
                    .first('balance')
                  text.push(t('Balance of user {nick} is {amount}', {
                    nick: user.nick,
                    amount: (balance || 0) / this.config.money.scale
                  }))
                  text.push(t('Name') + ': ' + user.forename + ' ' + user.surname)
                  text.push('Email: ' + user.email)
                  buttons = Keyboard.Cabinet
                  break

                case 'income':
                case 'last income':
                  const {rows} = await this.sql(`
                  SELECT t.amount, t.created, u.nick
                  FROM transfer t JOIN "user" u ON (t.vars ->> 'from')::INT = u.id
                  WHERE status = 'success' AND "to" = ?`, [user.id])

                  if (rows.length > 0) {
                    for (const transfer of rows) {
                      // console.log(transfer)
                      text.push(t('{amount} at {time} from {nick}', {
                        amount: transfer.amount / this.config.money.scale,
                        time: new Date(transfer.created).toLocaleTimeString('ru'),
                        nick: transfer.nick
                      }))
                    }
                  }
                  else {
                    text.push(t('Nothing found'))
                  }
                  buttons = Keyboard.Cabinet
                  break

                case 'referrals':
                case 'team':
                  const referrals = await this.table('referral')
                    .where('root', user.id)
                    .orderBy('id', 'desc')
                    .select('nick', 'surname', 'forename')
                    .limit(10)
                  if (referrals.length > 0) {
                    for (const r of referrals) {
                      let txt = r.nick
                      if (r.forename || r.surname) {
                        name = (r.forename + ' ' + r.surname).replace(/\s+/, ' ')
                        txt += ` (${name})`
                      }
                      text.push(txt)
                    }
                  }
                  else {
                    text.push(t('Nothing found'))
                  }
                  buttons = Keyboard.Cabinet
                  break

                default:
                  text.push(t(sample([
                    "Unknown command",
                    "Use the menu",
                    "Sorry, but I was not created for this",
                    "Use the menu below to control me",
                    "I'm too dumb to understand you",
                    "Operate the menu items below to give me tasks",
                    "I'm still young and inexperienced. I advise you to order me through the menu",
                    "Well, we can spend a thousand hours, but if you do not understand me, just click on the menu",
                    "I don't know what to do",
                    "What would you like?"
                  ])))
                  buttons = Keyboard.Cabinet
                  void this.log('telegram', 'received', {text: rawReplica}, user.id)
              }
            }
            else {
              const found = await this.table('user')
                .where('nick', user.context.nick)
                .first('id', 'secret')
              if (await bcrypt.compare(rawReplica, found.secret)) {
                const data = {telegram: message.from.id}
                if (!user.forename && message.from.first_name) {
                  data.forename = message.from.first_name
                }
                if (!user.surname && message.from.last_name) {
                  data.surname = message.from.last_name
                }
                await this.table('user')
                  .where('id', found.id)
                  .update(data)
                text.push(t('Welcome!'))
                buttons = Keyboard.Cabinet
              }
              else {
                text.push(t('Wrong password'))
                buttons = Keyboard.Login
              }
            }
          }
          else {
            if (user.context.loginAsked) {
              const q = this.table('user')
              if (rawReplica.indexOf('@') > 0) {
                q.where('email', rawReplica)
              }
              else {
                q.where('nick', rawReplica)
              }
              const found = await q.first('id', 'nick')
              if (found) {
                user.context.nick = found.nick
                text.push(t('Enter password for user {nick}', found))
              }
              else {
                text.push(t('User not found'))
              }
              buttons = Keyboard.Login
            }
            else {
              text.push(t('Enter username or email'))
              user.context.loginAsked = true
            }
          }
        }
        else {
          switch (replica) {
            case 'yes':
              user.context.registered = true
              text.push(t('Enter username or email'))
              user.context.loginAsked = true
              buttons = Keyboard.Login
              break
            case 'no':
              user.context.registered = false
              text.push(t('You can register at {url}', {url: this.config.origin + '/signup'}))
              buttons = Keyboard.Login
              break
            default:
              text.push(t('Are you already registered at {url}?', {url: this.config.origin}))
              buttons = Keyboard.Boolean
          }
        }
      }
      // console.log(message.from.id, user.context)
      await this.table('telegram')
        .where('id', message.from.id)
        .update({context: user.context})
      const data = {
        chat_id: message.from.id,
        disable_web_page_preview: true,
        disable_notification: true,
        text: text.join('\n')
      }
      if (buttons.length > 0) {
        data.reply_markup = JSON.stringify({
          keyboard: [buttons.map(text => ({text: t instanceof Function ? t(text) : text}))],
          one_time_keyboard: true,
          resize_keyboard: false
        })
      }

      this.site.telegram.sendMessage(data)
      return {
        status: 200,
        data
      }
    }
    fs.writeFileSync(`/tmp/${this.config.id}-${Date.now()}.json`, JSON.stringify(arguments[0], null, '\t'))
    return {status: 400}
  }
}
