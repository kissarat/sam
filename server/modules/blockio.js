const request = require('request-promise')
const {pick, merge, isEmpty} = require('lodash')

module.exports = {
  $balance: {method: 'GET', auth: {admin: true}},
  async balance() {
    const {status, data} = await this.site.blockio.get('get_balance')
    const success = 'success' === status
    return {
      status: success ? 200 : 500,
      success,
      data
    }
  },

  $rates: {method: 'GET'},
  async rates({amount}) {
    const r = await this.site.blockio.getRates()
    if (amount > 0) {
      amount = +amount
      const amounts = {}
      for(const key in r.rates) {
        amounts[key] = +((r.rates[key] * amount).toPrecision(5))
      }
      return {amounts}
    }
    return r
  },

  $address: {method: 'POST', auth: {}},
  async address() {
    const {address} = await this.site.blockio.getAddress(this.user.nick)
    const rows = await this.table('user')
      .where('id', this.user.id)
      .update({blockio: address}, ['id', 'time', 'blockio'])
    await this.log('blockio', 'address', {address})
    return {
      success: true,
      address,
      user: rows[0]
    }
  },

  $pay: {method: 'POST', auth: {}},
  async pay({amount}) {
    if (!Number.isInteger(amount) || !(amount > 0)) {
      return {
        status: 400,
        error: {
          amount,
          message: 'Invalid parameter "amount"'
        }
      }
    }
    const data = await this.site.blockio.getAddress(this.user.nick)
    data.success = data.status < 300
    const ip = this.ip
    if (data.success) {
      const transfer = {
        type: 'payment',
        status: 'created',
        amount,
        to: this.user.id,
        wallet: data.address,
        ip
      }
      const transfers = await this.table('transfer')
        .insert(transfer, ['id', 'created'])
      if (1 === transfers.length) {
        data.transfer = merge(transfers[0], transfer)
        return data
      }
      return merge(data, {
        status: 500,
        transfers,
        error: {
          message: 'Transfer not found'
        }
      })
    }
    data.status = 500
    return data
  },

  $withdraw: {method: 'POST', auth: {admin: true}},
  withdraw(params) {
    params = pick(params, 'amount', 'wallet')
    params.amount = +params.amount
    if (!(params.amount >= 400)) {
      return {
        status: 400,
        error: {
          message: 'Сумма выплаты должна быть больше 0.004 ฿'
        }
      }
    }
    const regex = this.config.blockio.test
      ? /^[2mn][1-9A-HJ-NP-Za-km-z]{26,35}$/
      : /^[13][a-km-zA-HJ-NP-Z1-9]{25,34}$/
    if (!regex.test(params.wallet)) {
      return {
        status: 400,
        error: {
          message: 'Неверный формат кошелька'
        }
      }
    }
    const getBalance = () => this.table('balance')
      .where({id: this.user.id})
      .first('balance')
    const insufficientFunds = (need, balance) => {
      const scale = this.config.money.scale
      return {
        status: 402,
        need,
        error: {
          message: `Недостаточно средств. Учитывая комиссию \
            ${need / scale} ฿ сети Bitcoin на вашем счету должно быть не меншьше ${(balance + need) / scale} ฿`
        }
      }
    }
    return getBalance()
      .then(({balance}) => {
        const need = balance - params.amount
        if (need < 0) {
          return insufficientFunds(-need, balance)
        }
        else {
          return this.site.blockio.estimateFee(params.amount / this.config.money.scale, params.wallet)
            .then(r => {
              if (!r) {
                return {
                  status: 500,
                  error: {
                    message: 'На данный момент вы не можете создать запрос на вывод денег, ' +
                    'попробуйте сделать это позже или обратитесь в техническую поддержку'
                  }
                }
              }
              if (r.fee) {
                const fee = Math.ceil(r.fee * this.config.money.scale)
                return getBalance()
                  .then(({balance}) => {
                    const need = balance - params.amount - fee
                    if (need < 0) {
                      return insufficientFunds(-need, balance)
                    }
                    else {
                      return this.table('transfer')
                        .insert({
                            type: 'withdraw',
                            status: 'created',
                            amount: Math.floor(params.amount + fee),
                            wallet: params.wallet,
                            to: this.user.id,
                            ip: this.ip
                          },
                          ['id', 'created', 'amount', 'ip'])
                        .then(([transfer]) => ({
                          status: 201,
                          success: true,
                          transfer
                        }))
                    }
                  })
              }
              else {
                return {
                  status: 500,
                  error: {
                    message: r.message
                  }
                }
              }
            })
        }
      })
  },

  $hook: {auth: {nick: 'blockio'}},
  async hook(post_data) {
    const {delivery_attempt, notification_id, type, data, created_at} = post_data
    console.log({
      procedure: 'blockio/hook',
      method: this.method,
      headers: this.headers,
      post_data
    })
    if ('ping' === type) {
      return {
        status: 200,
        type: 'pong'
      }
    }

    if (!('address' === type && delivery_attempt && notification_id)) {
      return {
        status: 200,
        error: {
          message: 'Unknown request'
        }
      }
    }

    await this.site.blockio.save({
      id: data.txid,
      amount: data.balance_change,
      fee: 0,
      network: data.network,
      created: new Date(created_at * 1000),
      confirmations: data.confirmations,
    })

    if (this.config.blockio.network === data.network) {
      const scale = this.config.blockio.exchange || this.config.money.scale
      const amount = Math.floor(data.balance_change * scale)
      if (amount < 0) {
        return {
          error: {
            message: 'Negative amount'
          }
        }
      }
      const query = {
        txid: data.txid,
        type: 'payment'
      }
      const changes = {
        status: data.confirmations >= this.config.blockio.confirmations ? 'success' : 'pending',
        amount,
        system: 'blockio',
        ip: this.ip
      }
      let transfer = await this.table('transfer')
        .where(query)
        .first()

      if (!transfer) {
        transfer = await this.table('transfer')
          .where({
            wallet: data.address,
            type: 'payment',
            status: 'created'
          })
          .first()
      }

      const user = await this.table('user')
        .where('blockio', data.address)
        .first()
      if (!transfer || (transfer && !transfer.user)) {
        if (user) {
          changes.to = user.id
        }
      }

      let result
      const returning = ['id', 'time', 'to', 'status', 'wallet', 'txid']
      if (changes.to) {
        if (transfer) {
          if ('pending' === transfer.status || 'created' === transfer.status) {
            changes.time = new Date()
            result = await this.table('transfer')
              .where(query)
              .update(changes, returning)
            if ('success' === changes.status && this.site.telegram) {
              setTimeout(() => {
                  this.site.telegram.broadcast(`Payment ${+data.balance_change}฿ from ${user.nick} #${result[0].id}`)
                },
                1000)
            }
          }
          else {
            result = true
          }
        }
        else {
          changes.wallet = data.address
          result = await this.table('transfer')
            .insert(merge(query, changes), returning)
        }
      }

      const success = result instanceof Array ? result.length > 0 : !!result

      const isAnomaly = transfer && (
          (transfer.wallet && transfer.wallet !== data.address) ||
          (transfer.txid && transfer.txid !== data.txid)
        )
      if (!success || isAnomaly) {
        await this.log('blockio', isAnomaly ? 'anomaly' : type, post_data)
      }

      return {
        status: 200,
        success,
        transfer: result[0]
        // error: {
        //   message: 'Webhook Not Implemented'
        // }
      }
    }
  },

  $approve: {method: 'POST', auth: {admin: true}},
  async approve({ids, currency}) {
    // TODO: global
    const currencies = {
      BTC: 1000000,
      LTC: 100000,
      DOGE: 10,
    }
    // console.log(this.config.blockio)
    if (!this.config.blockio.secret) {
      return {status: 403, error: {message: 'Withdraw is disabled'}}
    }
    if (!(ids instanceof Array && ids.every(id => id > 0))) {
      return {status: 400, error: {message: 'Invalid ids'}}
    }
    const time = new Date()
    const transfers = await this.table('withdrawable')
      .whereIn('id', ids)
      .orderBy('id')
    const amounts = {}
    for (const transfer of transfers) {
      transfer.amount = (transfer.amount / currencies[currency]).toFixed(6)
      if ('success' === transfer.will) {
        amounts[transfer.wallet] = transfer.amount
      }
      for (const key in transfer) {
        if (!transfer[key]) {
          delete transfer[key]
        }
      }
    }
    await this.log('transfer', 'approve', {time, ids, transfers})
    if (transfers.length <= 0) {
      return {status: 400, error: {message: 'No withdraws found with ids: ' + ids.join(',')}}
    }
    let transaction = false
    if (!isEmpty(amounts)) {
      const r = await this.site.blockio.withdraw(currency, amounts, transfers.filter(t => 'success' === t.will).map(t => t.id))
      if ('success' !== r.status) {
        console.error(r)
        return {
          status: 500,
          error: {
            message: 'fail' === r.status && r.data && 'string' === typeof r.data.error_message
              ? r.data.error_message
              : 'Invalid blockio response ' + r
          }
        }
      }
      transaction = {
        id: r.data.txid,
        amount: '-' + r.data.amount_sent,
        fee: '-' + r.data.network_fee,
        network: r.data.network,
        created: time
      }
      // FUCKUP:
      // await this.site.blockio.save(transaction)

    }
    let updated = []
    for (const status of ['success', 'canceled']) {
      const update_transfers = transfers.filter(t => status === t.will)
      if (update_transfers.length > 0) {
        const changes = {status, time}
        if ('success' === status) {
          changes.txid = transaction.id
        }
        else {
          changes.text = 'Insufficient funds'
        }
        const _updated = await this.table('transfer')
          .whereIn('id', update_transfers.map(t => t.id))
          .update(changes, ['id', 'type', 'status', 'wallet', 'from', 'text', 'created', 'time', 'amount'])
        updated = updated.concat(_updated)
      }
    }
    if (this.site.telegram) {
      setTimeout(async() => {
          const strings = [`Withdraw ${-transaction.amount} ${transaction.network} transaction ${transaction.id}:`]
          const transfers = await this.table('transfer')
            .whereIn('id', ids)
            .whereRaw("type = 'withdraw' AND status = 'success'")
          for (const t of transfers) {
            strings.push(t.amount / this.config.money.scale + ` ${transaction.network} ${t.from_nick} to ` + t.wallet)
          }
          this.site.telegram.broadcast(strings.join('\n'))
        },
        1000)
    }
    return {
      success: true,
      transaction,
      transfers: updated
    }
  },

  $about: {method: 'GET', auth: {admin: true}},
  about() {
    const names = ['test', 'version', 'confirmations', 'network']
    if (this.config.dev) {
      names.push('key')
    }
    const info = pick(this.config.blockio, names)
    info.withdraw = this.config.money.withdraw
    info.withdraw.enabled = info.withdraw.enabled && !!this.config.blockio.secret
    return info
  },

  $receive: {method: 'GET', auth: {nick: 'cron'}},
  async receive() {
    const {data, status} = await this.site.blockio.getTransactions('received')
    if ('success' === status) {
      const time = new Date()
      const transfers = await this.table('transfer_user')
        .whereIn('txid', data.txs.map(t => t.txid))
        .select('id', 'status', 'txid', 'wallet', 'from_nick')
      const recipients = data.txs.map(t => t.amounts_received[0].recipient)
        .filter(a => !transfers.find(t => a === t.wallet))
      const users = recipients.length > 0
        ? await this.table('user')
          .whereIn('blockio', recipients)
          .select('id', 'nick', 'blockio')
        : []
      const inserts = []
      const updates = []
      const unidentified = []
      // let scale = this.config.money.scale
      // if ('BTC' !== this.config.money.currency) {
      //   const {rates} = await this.site.blockio.getRates()
      //   scale *= rates[this.config.money.currency]
      // }
      const scale = 1000000
      if (scale > 0) {
        for (const tx of data.txs) {
          const created = new Date(tx.time * 1000)
          const amountString = tx.amounts_received[0].amount
          const amount = +amountString
          const transfer = transfers.find(t => t.txid === tx.txid)
          const isConfirmed = tx.confirmations >= this.config.blockio.confirmations
          // if (tx.confirmations < this.config.blockio.confirmations * 2) {
          const transaction = {
            id: tx.txid,
            amount: amountString,
            created,
            network: data.network,
            fee: 0,
            confirmations: tx.confirmations
          }
          if (transfer) {
            transaction.time = time
          }
          await this.site.blockio.save(transaction)
          // }
          if (transfer) {
            if (isConfirmed && 'pending' === transfer.status) {
              updates.push(transfer.id)
            }
          }
          else {
            const wallet = tx.amounts_received[0].recipient
            const u = users.find(u => wallet === u.blockio)
            if (u) {
              inserts.push({
                to: u.id,
                type: 'payment',
                status: isConfirmed ? 'success' : 'pending',
                txid: tx.txid,
                wallet,
                amount: Math.floor(amount * scale),
                currency: 'BTC',
                system: 'blockio'
              })
            }
            else {
              console.error(`User with wallet ${wallet} not found for ${tx.txid} transaction`)
              unidentified.push(tx)
            }
          }
        }
        const response = {
          status: 200,
          success: true,
          affected: -1,
          unidentified,
          inserted: [],
          updated: []
        }
        const returning = ['id', 'amount', 'status', 'to', 'wallet', 'txid']
        if (updates.length > 0) {
          response.updated = await this.table('transfer')
            .whereIn('id', updates)
            .update({
                status: 'success',
                time
              },
              returning)
        }
        if (inserts.length > 0) {
          response.inserted = await this.table('transfer')
            .insert(inserts, returning)
          if (response.inserted.length > 0) {
            response.status = 201
          }

          if (this.site.telegram) {
            setTimeout(() => {
                  const scale = this.site.exchange.scale('BTC')
                  this.site.telegram.broadcast(inserts.map(function (t) {
                    const user = users.find(u => u.id === t.to)
                    return user
                        ? `Payment ${t.amount / scale} BTC by ${user.nick} #` + t.id
                        : `User ${t.to} finding error`;
                  }))
                },
                1000)
          }
        }
        response.affected = response.updated.length + response.inserted.length
        return response
      }
      else {
        return {
          status: 500,
          error: {
            scale,
            message: 'Invalid money scale'
          }
        }
      }
    }
    else {
      throw {status: 500, data}
    }
  }
}
