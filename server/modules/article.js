const {each, pick, omit} = require('lodash')
const salo = require('salo')
const request = require('request-promise')
const rs = require('randomstring')

function Article(app, site) {
  if (!site) {
    app.get('/web/:url', Article.prototype.external)
  }
}

const columns = ['id', 'path', 'image', 'type', 'name', 'short', 'price', 'time', 'created']

Article.prototype = {
  get(params) {
    return this.table('article')
      .where(pick(params, 'id', 'path'))
      .then(([article]) => {
        if (article) {
          if (params.relation) {
            return this.table('relation')
              .innerJoin('article', 'relation.from', 'article.id')
              .where('to', article.id)
              .select(columns)
              .then(function (relations) {
                return {article, relations}
              })
          }
          return {article}
        }
        return {status: 404}
      })
  },

  $save: {auth: {admin: true}, method: 'POST'},
  save(params, data) {
    if ('ticket' === data.type) {
      data.path = rs.generate(16)
    }
    each(data, function (value, key) {
      data[key] = value || null
    })
    const relations = data.relations
    data = pick(data, 'path', 'image', 'type', 'name', 'short', 'price', 'accrue', 'text')
    if (params.id) {
      data.time = new Date()
    }
    else {
      data.user = this.user.id
    }
    let q = this.table('article')
    q = params.id
      ? q.where(params).update(data, ['id', 'path', 'time'])
      : q.insert(data, ['id', 'path', 'created'])
    return q.then(([article]) => {
      if (article) {
        const to = article.id
        const result = {
          status: params.id ? 200 : 201,
          success: to > 0,
          article
        }
        if ('ticket' === data.type && this.site.telegram) {
          setTimeout(() => this.site.telegram.feedback(`📰${data.name} ${this.config.origin}/ticket/${data.path}\n${data.text}`), 1000)
        }
        if (relations) {
          return this.sql('DELETE FROM relation WHERE "to" = ?', to)
            .then(() => this.table('relation').insert(relations.map(from => ({from, to}))))
            .then(() => result)
        }
        return result
      }
      return {status: 404}
    })
  },

  $many: {auth: {admin: true}},
  many(data) {
    return this.table('article').insert(data)
  },

  $index: {method: 'GET', auth: {}},
  async index(params = {}) {
    if (!this.user.admin) {
      params.user = this.user.id
    }
    const q = this.table('article_author')
      .where(omit(params, 'select'))
    if (params.select) {
      q.select(params.select.split(','))
    }
    const articles = await q
      .orderBy('priority', 'desc')
      .orderBy('id', 'desc')
    return {articles}
  },

  $page: {method: 'GET', columns},
  page(params) {
    const q = this.table('article')
    if (params.from) {
      q.innerJoin('relation', 'article.id', 'relation.to')
      q.where('relation.from', +params.from)
    }
    if (params.type) {
      params.select = params.select.filter(c => 'type' !== c)
    }
    q.where(pick(params, 'type', 'from'))
    const countQuery = q.clone()
    return q
      .whereRaw('active')
      .select(params.select)
      .offset(params.offset || 0)
      .limit(params.limit || 12)
      .orderBy('priority', 'desc')
      .orderBy('id', 'desc')
      .then(articles => {
        if (articles.length >= 12 && params.count) {
          return countQuery.count()
            .then(function ([{count}]) {
              return {count: +count, articles}
            })
        }
        return {articles}
      })
  },

  $external: {auth: {admin: true}},
  external(req, res) {
    request(new Buffer(req.params.url, 'base64').toString())
      .then(function (r) {
        res.send(r)
      })
      .catch(function (err) {
        res
          .status(err.status)
          .end()
      })
  },

  $ticket: {method: 'GET'},
  ticket(params) {
    if (!this.user.admin) {
      params.user = this.user.id
    }
    return this.table('article')
      .where(params)
      .then(([article]) => {
        if (article) {
          return this.table('message_user')
            .where({article: article.id})
            .then(messages => ({article, messages}))
        }
        return {status: 404}
      })
  },

  $remove: {method: 'DELETE'},
  remove({id}) {
    return this.table('article')
        .where('id', +id)
        .delete()
  }
}

module.exports = Article
