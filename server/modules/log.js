const {omit, isObject} = require('lodash')

module.exports = {
  $call: {auth: {admin: true}},

  $index: {method: 'GET'},
  async index(params) {
    if (!this.user.admin && this.user.id != params.user) {
      return {status: 403}
    }

    const q = this.table(params.user ? 'log' : 'user_log')
        .where(omit(params, 'limit', 'offset', 'before', 'after'))
    if (params.before) {
      q.whereRaw('id <= ?', [params.before])
    }
    if (params.after) {
      q.whereRaw('id > ?', [params.after])
    }
    const page = {
      offset: params.offset || 0,
      limit: params.limit || 300
    }
    const records = await q
        .orderBy('id', 'desc')
        .limit(page.limit)
        .offset(page.offset)
    for (const record of records) {
      record.id = +record.id
      const isPasswordChange = record.data && record.data.old && record.data.new && record.data.repeat
      const isSecret = isPasswordChange || ('token' === record.entity && ('login' === record.action || 'logout' === record.action))
          || ('user' === record.entity && 'create' === record.action)
      if (isSecret && record.data) {
        record.data = {id: isPasswordChange ? 'Изменение пароля' : (isObject(record.data.token) ? record.data.token.id : record.data.token)}
      }
      record.created = new Date(record.id / (1000 * 1000)).toISOString()
    }
    const data = {
      page,
      records
    }
    if (this.config.dev) {
      data.sql = q.toString()
    }
    return data
  },

  $reset: {method: 'POST'},
  async reset() {
    await this.table('log')
        .where({entity: 'auth', action: 'invalid'})
        .where('id', '>', (Date.now() - 16 * 60 * 1000) * (1000 * 1000))
        .del()
    return {success: true}
  }
}
