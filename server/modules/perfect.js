const qs = require('querystring')
const {pick} = require('lodash')
const crypto = require('crypto')

async function receiveStatus(params, postData) {
  if ('POST' === this.moduleConfig.method) {
    params = postData
  }

  const isNixStatus = 'nix' === this.moduleName && 'status' === this.actionName

  const validate = {
    PAYMENT_ID: /^\d+$/,
    PAYEE_ACCOUNT: /^U\d+$/,
    PAYMENT_AMOUNT: /^\d+(\.\d{2})?$/,
    PAYMENT_UNITS: /^USD$/,
    PAYMENT_BATCH_NUM: /^\d+$/,
    PAYER_ACCOUNT: /^U\d+$/,
    TIMESTAMPGMT: /^\d+$/,
    V2_HASH: /^[0-9A-Z]+$/,
  }
  for (const key in validate) {
    const value = params[key]
    if (!value || !validate[key].test(value)) {
      return {
        status: 400,
        error: {
          key,
          message: `Invalid ${key} parameter`
        }
      }
    }
  }
  const string = [
    params.PAYMENT_ID,
    params.PAYEE_ACCOUNT,
    params.PAYMENT_AMOUNT,
    params.PAYMENT_UNITS,
    params.PAYMENT_BATCH_NUM,
    params.PAYER_ACCOUNT,
    this.moduleConfig.secret,
    params.TIMESTAMPGMT,
  ]
    .join(':')
  const md5 = crypto.createHash('md5')
  md5.update(string)
  const hash = md5.digest('hex').toUpperCase()
  if (hash === params.V2_HASH) {
    const data = {
      amount: params.PAYMENT_AMOUNT * 100,
      wallet: params.PAYER_ACCOUNT,
      txid: params.PAYMENT_BATCH_NUM,
      time: new Date(params.TIMESTAMPGMT * 1000).toISOString(),
      status: isNixStatus ? 'success' : this.actionName
    }
    if (this.site.telegram) {
      setTimeout(() => this.site.telegram.broadcast(
        `Оплата \$${params.PAYMENT_AMOUNT} пользователем ${params.USER || 'unknown user'} №${params.PAYMENT_ID}`), 1000)
    }
    await this.log('transfer', 'approve', {perfect: params}, params.USER_ID)
    const [transfer] = await this.table('transfer')
      .whereRaw(`type = 'payment' AND id = ?`, [params.PAYMENT_ID])
      .update(data, ['id', 'amount', 'wallet', 'to', 'txid', 'time', 'created', 'status'])
    if (transfer) {
      if (isNixStatus) {
        return {success: true}
      }
      return {
        status: 301,
        headers: {
          location: this.config.origin + `/payment/${transfer.status}/` + transfer.id
        }
      }
    }
    else {
      return {status: 404}
    }
  }
  else {
    return {
      status: 400,
      error: {
        message: 'Invalid payment signature'
      }
    }
  }
}

module.exports = {
  $pay: {method: 'POST', auth: {}},
  async pay({amount, url = 'https://perfectmoney.is/api/step1.asp'}) {
    if (!(amount > 0)) {
      return {status: 400, error: {message: 'Invalid amount'}}
    }
    const transfer = {
      type: 'payment',
      status: 'created',
      to: this.user.id,
      ip: this.ip,
      system: this.moduleName,
      amount
    }
    const [r] = await this.table('transfer')
      .insert(transfer, ['id', 'ip', 'created', 'amount', 'wallet', 'to', 'time'])
    const {nick, email} = await this.table('user')
      .where('id', r.to)
      .first('nick', 'email')
    const {balance} = await this.table('balance')
      .where('id', r.to)
      .first('balance')
    const special = page => this.config.origin + `/serve/${this.moduleName}/${page}?` + qs.stringify(pick(r, 'id', 'to'))
    amount = (r.amount / 100).toFixed(2)
    const data = {
      BAGGAGE_FIELDS: 'USER USER_ID BALANCE CREATED IP EMAIL',
      BALANCE: (r.amount / 100).toFixed(2),
      EMAIL: email,
      IP: this.ip,
      NOPAYMENT_URL: special('fail'),
      PAYEE_ACCOUNT: this.moduleConfig.wallet,
      PAYEE_NAME: this.config.name,
      PAYMENT_AMOUNT: amount,
      PAYMENT_ID: r.id,
      PAYMENT_UNITS: 'USD',
      PAYMENT_URL: special('success'),
      PAYMENT_URL_METHOD: this.moduleConfig.method,
      STATUS_URL: special('status'),
      SUGGESTED_MEMO: `$${amount} payment ${r.id} from ${this.user.nick} at ${this.ip}`,
      CREATED: new Date(r.created).toISOString(),
      USER: nick,
      USER_ID: r.to,
    }
    const response = {
      status: 201,
      success: true,
      transfer: r,
      url
    }
    if ('GET' === this.moduleConfig.method) {
      response.url += '?' + qs.stringify(data)
    }
    else {
      response.data = data
    }
    return response
  },

  success: receiveStatus,
  fail: receiveStatus,
  status: receiveStatus,

  $send: {method: 'POST', auth: {admin: true}},
  async send({id}) {
    id = +id
    let transfer = await this.table('transfer')
      .where('id', id)
      .first()
    if ('success' === transfer.status) {
      return {status: 403}
    }
    try {
      const txid = await this.site[this.moduleName].send(transfer.wallet, transfer.amount / 100, id)
      const data = {
        txid,
        ip: this.ip,
        time: new Date(),
        status: 'success'
      };
      [transfer] = await this.table('transfer')
        .where('id', id)
        .update(data, ['id', 'txid', 'time', 'ip', 'amount'])
      return {success: true, transfer}
    }
    catch (ex) {
      await this.table('transfer')
        .where('id', id)
        .update({text: ex.message})
      throw ex
    }
  }
}
