function admin(method) {
  return {
    method,
    auth: {admin: true}
  }
}

module.exports = {
  $index: admin('GET'),
  async index() {
    const restricts = await this.table('ip')
      .orderBy('created', 'desc')
    return {restricts}
  },

  $save: admin('POST'),
  async save(data) {
    data.ip = this.ip
    data.editor = this.user.id
    data.created = new Date()
    const restricts = await this.table('ip')
      .insert(data, ['id', 'created'])
    await this.log('ip', 'create', data)
    return {
      success: restricts.length > 0,
      affected: restricts.length,
      restricts
    }
  },

  $remove: admin('DELETE'),
  async remove({id}) {
    const r = await this.table('ip')
      .where('id', id)
      .del()
    return {
      success: true,
      affected: r.rowCount
    }
  },

  $gain: {method: 'GET', auth: {nick: 'ip'}},
  async gain({text}) {
    if (text) {
      const data = {
        id: this.ip,
        ip: this.ip,
        editor: this.user.id,
        time: new Date()
      }
      await this.table('ip')
        .where('text', text)
        .update(data)
      return {
        status: 302,
        headers: {
          location: '/login'
        }
      }
    }
    else {
      return {success: false, status: 400, text}
    }
  }
}
