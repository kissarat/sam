const rs = require('randomstring')
const {pick, omit, isEmpty} = require('lodash')

function generateToken() {
  return rs.generate({
    length: 24,
    charset: 'alphabetic'
  })
}

const tokenTypes = ['app']

module.exports = {
  async create(params) {
    params = pick(params, 'expires', 'name', 'type', 'hard', 'soft')
    if (this.headers && this.ip) {
      params.ip = this.ip
    }
    params.id = generateToken()
    if (params.token && tokenTypes.indexOf(params.type) < 0) {
      return {
        status: 403,
        error: {
          message: `Token of type ${tokenTypes.join(', ')} supported`
        }
      }
    }
    const [r] = await this.table('token').insert(params, ['id', 'time', 'expires', 'name', 'type'])
    if (this.isEnabled('hard') && !this.user.guest && params.hard) {
      return this.user.update({hard: params.hard})
    }
    await this.log('token', 'create', r)
    r.success = true
    return r
  },

  $reset: {auth: {admin: true}},
  reset({id}, params) {
    params.id = generateToken()
    params.time = new Date().toISOString()
    return this.table('token')
      .where('id', id)
      .update(pick(params, 'id', 'time', 'expires', 'name'), ['id', 'time', 'created', 'expires', 'name'])
      .then(([r]) => {
        if (r) {
          return this.log('token', 'reset', {id: r.id}).then(function () {
            r.success = true
            return r
          })
        }
        else {
          return {status: 404}
        }
      })
  },

  delete({id}) {
    return this.table('token')
      .where('id', id)
      .returning(['id', 'time', 'created'])
      .del()
      .then(([r]) => {
        if (r) {
          return this.log('token', 'delete', {id: r.id}).then(function () {
            r.success = true
            return r
          })
        }
        else {
          return {status: 404}
        }
      })
  },

  handshake() {
    const data = this.modules.home.config.call(this)

    const send = data => {
      if (this.isEnabled('daemon')) {
        return this.table('daemon')
          .where(this.raw(
            'expires > CURRENT_TIMESTAMP AND (type is null OR type = ?)',
            [data.token.type]
          ))
          .then(function (daemons) {
            if (daemons.length > 0) {
              data.daemons = daemons
            }
            return data
          })
      }
      else {
        return data
      }
    }

    if (this.token.id) {
      const handshake = this.token.handshake
      const changes = {handshake: new Date()}
      if (this.ip !== this.token.ip) {
        changes.ip = this.ip
      }
      if (this.headers['user-agent'] !== this.token.name) {
        changes.name = this.headers['user-agent']
      }
      if (!isEmpty(omit(changes, 'handshake'))) {
        changes.time = new Date()
      }
      return this.check('hard') || this.table('token')
          .where('id', this.token.id)
          .update(changes)
          .then(() => {
            data.token = pick(this.token, 'id', 'type', 'name', 'expires', 'hard', 'mlbot', 'ip')
            data.user = pick(this.user, 'id', 'type', 'nick', 'surname', 'forename', 'skype', 'avatar', 'admin', 'leader')
            const lastTokenUpdate = handshake || this.token.time || this.token.created
            if (changes.ip || changes.name || Date.now() - new Date(lastTokenUpdate).getTime() > 5 * 60 * 1000) {
              return this.log('handshake', 'update', {id: this.token.id})
                .then(() => send(data))
            }
            return send(data)
          })
    }
    else {
      return this.modules.token.create.call(this, this.body)
        .then(function (token) {
          data.token = token
          data.user = {guest: true}
          return send(data)
        })
    }
  },

  $save: {method: 'POST', auth: {admin: true}},
  save(params, changes) {
    return this.table('token')
      .where(params)
      .update(changes, ['id'])
      .then((rows) => ({
        success: rows.length > 0
      }))
  }
}
