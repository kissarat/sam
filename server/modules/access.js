const {isObject, isMatch, difference, omit, isEmpty} = require('lodash')

function plural(array) {
  return array instanceof Array ? array : [array]
}

const extra = ['select', 'sort', 'limit', 'offset', 'search']

module.exports = {
  auth({admin, leader, nick}) {
    if (this.user.guest) {
      return {status: 401}
    }
    const isForbidden = ((leader && !(this.user.leader || this.user.admin)) || (admin && !this.user.admin))
      || nick && nick !== this.user.nick
    if (isForbidden) {
      return {
        status: 403,
        error: {
          message: 'Forbidden'
        }
      }
    }
  },

  method(names) {
    if (!plural(names).includes(this.method)) {
      return {status: 405}
    }
  },

  token(names) {
    if (true === names) {
      if (!this.token.id) {
        return {
          status: 403,
          error: {
            token: true,
            message: 'You cannot access this action without token'
          }
        }
      }
    }
    else if (!(isObject(names)
        ? isMatch(this.token, names)
        : plural(names).includes(this.token.type))) {
      return {
        status: 403,
        error: {
          token: true,
          message: 'Invalid token'
        }
      }
    }
  },

  columns(allow, params) {
    if ('string' === typeof params.select) {
      params.select = params.select.split(/[.,]/g)
      const columns = difference(params.select, allow)
      if (columns.length > 0) {
        return {
          status: 400,
          error: {
            columns,
            message: 'Deny columns'
          }
        }
      }
    }
    else {
      params.select = allow
    }
  },

  query(allow, params) {
    const query = omit(params, allow.concat(extra))
    if (!isEmpty(query)) {
      return {
        status: 400,
        query
      }
    }
  },

  amount(value, params) {
    if (!(params.amount > value)) {
      return {status: 400, error: {message: 'Invalid amount'}}
    }
  },

  hard(hardInfo = this.user.hard) {
    if ('app' === this.token.type && this.token.hard && hardInfo
      && !isMatch(this.token.hard, hardInfo)) {
      return {status: 409}
    }
  },

  sort(allow, params) {
    if ('string' === typeof params.sort) {
      const sort = {}
      for(let column of params.sort.split(',')) {
        const order = '-' === column[0] ? -1 : 1
        if (order < 0) {
          column = column.slice(1)
        }
        if (allow.indexOf(column) >= 0) {
          sort[column] = order
        }
        else {
          return {
            status: 400,
            error: {
              message: `Sorting by column '${column}' is not allow`
            }
          }
        }
      }
      params.sort = sort
    }
  },
}
