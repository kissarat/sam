const {sync} = require('./user')

module.exports = {
  $call: {auth: {}},

  $mlbot: {method: 'POST'},
  async mlbot() {
    const {rows} = await this.sql('SELECT buy_mlbot(?, ?) as status', [this.user.id, this.ip])
    const status = rows[0].status
    const success = status > 0
    const data = {
      status: success ? 201 : -status,
      success
    }
    if (402 !== data.status) {
      await sync.call(this, {id: this.user.id}, {mlbot: '2020-01-01'})

      if (this.site.telegram) {
        setTimeout(() => this.site.telegram.broadcast(`Поздравляем пользователя ${this.user.nick} с приобретением MLBot!`), 1000)
      }

      const sponsors = await this.table('sponsor_reward')
        .where('root', this.user.id)
      data.notifications = sponsors.map(s => ({
        user: s.id,
        text: '+ $' + (s.percent * 2200) / this.config.money.scale
      }))

      return data
    }
    else {
      const {rows} = await this.sql('SELECT 2200 - balance as need FROM balance WHERE id = ?', [this.user.id])
      data.need = +rows[0].need
      return data
    }
  },

  $buy: {method: 'POST'},
  async buy({id}) {
    const article = await this.table('article')
      .where('id', +id)
      .first('id', 'path', 'name', 'type', 'price')
    const {rows: [{status}]} = await this.sql('SELECT buy(?, ?, ?) as status', [this.user.id, article.id, this.ip])
    const success = status > 0
    const data = {
      status: success ? 201 : -status,
      success,
      article
    }
    if (402 === data.status) {
      const {rows} = await this.sql('SELECT ? - balance as need FROM balance WHERE id = ?', [article.price, this.user.id])
      data.need = +rows[0].need
    }
    else {
      data.article = await this.table('article')
        .where('id', article.id)
        .first('id', 'path', 'name', 'price')
      if ('mlbot' === data.article.path) {
        await sync.call(this, {id: this.user.id}, {mlbot: '2020-01-01'})
      }

      if (this.site.telegram) {
        setTimeout(() => this.site.telegram.broadcast('Congratulations to the user {nick} with the purchase of {name}!', {
          nick: this.user.nick,
          name: article.name
        }))
      }
    }

    return data
  }
}