const {createHash} = require('crypto')

module.exports = {
  $send: {method: 'POST', auth: {admin: true}},
  async send({amount, wallet, id}) {
    const now = new Date().toISOString()
    const config = this.config.advcash
    function getToken() {
      const string = config.password + ':' + now
              .replace(/(\d{4})-(\d{2})-(\d{2})T(\d{2}).*/, '$1$2$3:$4')
      const hash = createHash('sha256')
      hash.update(string)
      return hash.digest('hex').toUpperCase()
    }

    let transfer
    if (id) {
      transfer = await this.table('transfer').where('id', +id).first()
      console.log(transfer)
      if (!transfer) {
        return {status: 404}
      }
      amount = +transfer.amount
      wallet = transfer.wallet
    }
    if (!(amount >= 10)) {
      return {status: 400, error: {message: 'Invalid amount ' + amount}}
    }
    if (wallet) {
      wallet = wallet.trim().replace(/\s+/g, '')
    }
    if (!/^U\d{12}$/.test(wallet)) {
      return {status: 400, error: {message: 'Invalid wallet ' + wallet}}
    }
    const token = getToken()
    const usdAmount = (amount / 100).toFixed(2)
    const xml = `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
      xmlns:wsm="http://wsm.advcash/">
      <soapenv:Header/>
      <soapenv:Body>
      <wsm:sendMoney>
      <arg0>
        <apiName>${config.id}</apiName>
        <authenticationToken>${token}</authenticationToken>
        <accountEmail>${config.email}</accountEmail>
      </arg0>
      <arg1>
        <amount>${usdAmount}</amount>
        <currency>USD</currency>
        <walletId>${wallet}</walletId>
        <savePaymentTemplate>false</savePaymentTemplate>
      </arg1>
      </wsm:sendMoney>
      </soapenv:Body>
      </soapenv:Envelope>`
    const r = await this.site.remote.clients.advcash.request({body: xml})
    const match = /<return>([\w-]+)<\/return>/.exec(r)
    if (match) {
      if (transfer) {
        await this.table('transfer')
            .where('id', +id)
            .update({
              txid: match[1],
              status: 'success',
              time: now,

            })
      }
      return {
        success: true,
        result: match[1]
      }
    }
    else {
      return {success: false}
    }
  }
}
