const rs = require('randomstring')

module.exports = {
  $call: {auth: {admin: true}},

  $send: {method: 'POST'},
  send(params) {
    return this.site.mail.send(params)
  },

  $activate: {method: 'POST'},
  async activate({id, email}) {
    const t = {
      id: rs.generate(this.config.signup.verify.size),
      type: 'code',
      user: id,
      expires: new Date(Date.now() + this.config.signup.verify.duration),
      ip: this.ip,
      name: this.headers['user-agent']
    }
    const locals = {
      to: email,
      subject: 'Активируйте учетную запись ' + this.config.name,
      token: t.id
    }
    const [token] = await this.table('token')
        .insert(t, ['id', 'created', 'ip', 'name'])
    const mail = await this.site.mail.sendTemplate('signup', locals)
    return {
      success: true,
      token,
      mail
    }
  }
}
