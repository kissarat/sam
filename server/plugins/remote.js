const request = require('request-promise')
const merge = require('deepmerge')

function Remote(config) {
  this.rpcId = 0
  this.config = config
}

Remote.prototype = {
  request(options) {
    return request(options ? merge(options, this.config) : this.config)
  },

  get(url) {
    this.request({url})
  },

  async import(token, hostname, path, qs) {
    const data = await this.request({
      uri: `https://${hostname}/serve/~${token}/${path}`,
      qs
    })
    return JSON.parse(data)
  },

  async procedureCall(method, params = []) {
    const r = await this.request({
      method: 'POST',
      timeout: 15000,
      body: {
        jsonrpc: '2.0',
        id: ++this.rpcId,
        method,
        params
      },
      json: true
    })
    if ('result' in r) {
      return r.result
    }
    else {
      throw new Error(JSON.stringify(r))
    }
  }
}

function RemotePlugin({config}) {
  config = config.remote
  Remote.call(this, config)
  this.clients = {}
  for (const name in this.config.clients) {
    this.clients[name] = new Remote(this.config.clients[name])
  }
}

RemotePlugin.prototype = Object.create(Remote.prototype)

module.exports = RemotePlugin
