function Perfect() {

}

Perfect.prototype = {
  get config() {
    return this.site.config.perfect
  },

  async send(wallet, amount, id) {
    // console.log(wallet, amount, id)
    const qs = {
      AccountID: this.config.id,
      PassPhrase: this.config.password,
      Payer_Account: this.config.wallet,
      Payee_Account: wallet,
      Amount: amount
    }
    if (id) {
      qs.PAYMENT_ID = id
    }
    const response = await this.site.remote.clients.perfect.request({qs})
    const s = response.toString()
    const m = /<td>PAYMENT_BATCH_NUM<.td><td>(\d+)<.td>/.exec(s)
    if (m) {
      return +m[1]
    }
    else {
      const m = /Error: (.*)/.exec(s)
      if (m) {
        throw new Error(m[1])
      }
    }
  }
}

module.exports = Perfect
