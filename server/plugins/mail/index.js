const nodemailer = require('nodemailer')
const fs = require('fs')
const pug = require('pug')
const {pick, merge} = require('lodash')

function Mail({config}) {
  this.auth = config.mail.auth
  this.transporter = nodemailer.createTransport(config.mail)
  this.locals = {
    origin: config.origin
  }
  this.templates = {}
  const templateDir = __dirname + '/templates'
  fs.readdirSync(templateDir).forEach(filename => {
    const match = /^([^.]+)\.pug$/.exec(filename)
    if (match) {
      this.templates[match[1]] = pug.compileFile(templateDir + '/' + filename)
    }
  })
  // console.log(config)
}

Mail.prototype = {
  send(options) {
    options.from = this.auth.user
    // fs.writeFileSync(`/tmp/${options.to}.html`, options.html)
    // console.log(options)
    const transporter = this.transporter
    return new Promise(function (resolve, reject) {
      transporter.sendMail(options, function (err, res) {
        if (err) {
          reject(err)
        }
        else {
          resolve(res)
        }
      })
    })
  },

  sendTemplate(name, options) {
    const locals = merge(this.locals, options)
    options = pick(options, 'to', 'subject')
    options.html = this.templates[name](locals)
    return this.send(options)
  }
}

module.exports = Mail
