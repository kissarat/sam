const request = require('request-promise')
const {clone} = require('lodash')
const {substitute} = require('../../common/i18n/translation')

module.exports = class Telegram {
  get config() {
    return this.site.config.telegram
  }

  invoke(method, qs) {
    return request({
      uri: `https://api.telegram.org/bot${this.config.token}/` + method,
      qs,
      json: true
    })
  }

  broadcast(text, vars) {
    const params = clone(this.config.broadcast)
    params.text = substitute(text, vars)
    return this.invoke('sendMessage', params)
  }

  feedback(text) {
    const params = clone(this.config.feedback)
    params.text = text
    return this.invoke('sendMessage', params)
  }

  getChat(chat_id) {
    return this.invoke('getChat', {chat_id})
  }

  getMe() {
    return this.invoke('getMe')
  }

  sendMessage(params) {
    return this.invoke('sendMessage', params)
  }
}
