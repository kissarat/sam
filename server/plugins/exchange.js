function Exchange(app) {

}

Exchange.prototype = {
  to(from, to, amount = 1) {
    return this[from][to] * amount
  },

  rate(currency, amount = 1) {
    return this.to(currency, currency, amount)
  },

  scale(currency) {
    return Math.log10(this.rate(currency))
  },

  async load() {
    for(const {from, to, rate} of await this.site.table('exchange')) {
      if (!this[from]) {
        this[from] = {}
      }
      this[from][to] = rate
    }
  },

  async download() {
    const {BTC, ETH} = await this.site.remote.request({
      uri: 'https://min-api.cryptocompare.com/data/price',
      qs: {fsym: 'USD', tsyms: 'BTC,ETH'},
      json: true
    })
    const time = new Date().toISOString()
    const exchange = [
      ['BTC', 'USD', 1/BTC],
      ['ETH', 'USD', 1/ETH],
      ['USD', 'BTC', BTC],
      ['USD', 'ETH', ETH],
      ['BTC', 'ETH', ETH/BTC],
      ['ETH', 'BTC', BTC/ETH],
    ]
    for(const [from, to, rate] of exchange) {
      await this.site.table('exchange')
        .where({from, to})
        .update({rate, time})
    }
    return {BTC, ETH, time}
  }
}

module.exports = Exchange
