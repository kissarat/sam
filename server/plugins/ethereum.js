function Ethereum() {
}

Ethereum.prototype = {
  toWei(amount) {
    return amount * Math.pow(10, 18 - this.site.exchange.scale('ETH'))
  },

  fromWei(amount) {
    return Math.floor(amount * Math.pow(10, this.site.exchange.scale('ETH') - 18))
  },

  invoke(currency, method, ...params) {
    if (['ETH', 'ETC'].indexOf(currency) < 0) {
      throw new Error('Invalid currency: ' + currency)
    }
    // console.log(method, params)
    return this.site.remote.clients[currency].procedureCall(method, params)
  },

  gasPrice(currency) {
    return this.invoke(currency, 'eth_gasPrice')
  },

  estimateGas(currency) {
   // return this.invoke('eth_estimateGas', {})
   return '0x' + (21000).toString(16)
  },

  async getFee(currency) {
    return (await this.gasPrice(currency)) * (await this.estimateGas(currency))
  }
}

module.exports = Ethereum
