const {defaults} = require('lodash/core')

function Nix() {

}

Nix.prototype = {
  get config() {
    return this.site.config.nix
  },

  async send(wallet, amount, id) {
    const qs = {
      PASSPHRASE: this.config.password,
      PAYER_ACCOUNT: this.config.wallet,
      PAYEE_ACCOUNT: wallet,
      AMOUNT: amount
    }
    if (id) {
      qs.MEMO = '#' + id
    }
    const options = defaults(this.config.send, {
      method: 'POST',
      uri: 'http://dev.nixmoney.com/send',
      qs
    })
    // console.log(options)
    const response = await this.site.remote.request(options)
    const s = response.toString()
    const m = /<input name='PAYMENT_BATCH_NUM' type='hidden' value='(\d+)'/.exec(s)
    if (m) {
      return +m[1]
    }
    else {
      const m = /<input name='ERROR' type='hidden' value='([^']+)'>/.exec(s)
      if (m) {
        throw new Error(m[1])
      }
    }
  }
}

module.exports = Nix
