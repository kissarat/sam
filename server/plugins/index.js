module.exports = {
  remote: require('./remote'),
  blockio: require('./blockio'),
  exchange: require('./exchange'),
  ethereum: require('./ethereum'),
  mail: require('./mail'),
  nix: require('./nix'),
  payeer: require('./payeer'),
  perfect: require('./perfect'),
  telegram: require('./telegram'),
  redis: require('./redis'),
}
