const request = require('request-promise')
const {merge, clone, omit} = require('lodash')
const Official = require('block_io')

const FAKE_EXCHANGE_RATE = {
  "network": "BTC",
  "prices": [{"price": "6946.57", "price_base": "USD", "exchange": "coinbase", "time": 1509622657}, {
    "price": "6874.0",
    "price_base": "USD",
    "exchange": "bitfinex",
    "time": 1509622660
  }]
}

const cache = {
  getRates: {
    time: 0
  }
}

class Debounce {
  constructor(delay) {
    this.delay = delay
    this.last = 0
    this.queue = []
    this.run = this.run.bind(this)
  }

  wait() {
    if (this.delay > 0) {
      const now = Date.now()
      const delta = now - this.last
      if (delta < this.delay) {
        this.timer = setInterval(this.run, this.delay)
        return new Promise(resolve => this.queue.push(resolve))
      }
    }
  }

  run() {
    if (this.queue.length > 0) {
      const now = Date.now()
      if ((now - this.last) >= this.delay) {
        this.last = now
        const resolve = this.queue.shift()
        if ('function' === typeof resolve) {
          resolve()
        }
        else {
          console.error('Debounce.run: resolve is not function', resolve)
        }
      }
    }
    else {
      clearInterval(this.timer)
      console.log('Debounce.run clearInterval')
    }
  }
}

class Cache {
  constructor(methods) {
    Cache.mix(this, methods)
  }

  static mix(self, methods) {
    self.cache = {}
    for (const m of methods) {
      const original = self[m];
      self[m] = function () {
        let cache = self.cache[m]
        const now = Date.now()
        if (!cache || now - cache.time >= 300 * 1000) {
          cache = original.apply(self, arguments)
          cache.time = now
          self.cache[m] = cache
        }
        return cache
      }
    }
  }
}

function BlockIoApi({config}) {
  merge(this, config.blockio)
  this.debounce = new Debounce(this.delay || 0)
  Cache.mix(this, ['getRates'])
}

function getAddressFromResponse(r) {
  console.log(arguments[0])
  if (r.data && r.data.addresses instanceof Array) {
    const address = r.data.addresses[0]
    address.success = true
    if ('success' === r.status) {
      address.status = 200
    }
    return address
  }
  else {
    return r
  }
}

BlockIoApi.prototype = {
  async internal(method, param) {
    if (!BlockIoApi[this.key]) {
      BlockIoApi[this.key] = new Official(this.key, this.secret, this.version)
    }
    const official = BlockIoApi[this.key]
    if ('string' === typeof method) {
      if (Official.prototype[method] instanceof Function) {
        await this.debounce.wait()
        return new Promise(resolve => official[method](param, function (err, r) {
          resolve(err || r)
        }))
      }
      return {
        status: 500,
        error: {
          method,
          error: 'Unknown method'
        }
      }
    }
    return official
  },

  async get(currency, method, qs = {}) {
    try {
      if (['BTC', 'LTC', 'DOGE'].indexOf(currency) < 0) {
        throw new Error(currency)
      }
      if (!qs.api_key) {
        qs.api_key = this.keys[currency]
      }
      await this.debounce.wait()
      const r = await this.site.remote.request({
        uri: `https://www.block.io/api/v2/${method}/`,
        qs
      })
      return JSON.parse(r)
    }
    catch (err) {
      if (404 === err.statusCode) {
        return err
      }
      throw err
    }
  },

  getTransactions(params) {
    if ('string' === typeof params) {
      params = {type: params}
    }
    return this.get('get_transactions', params)
  },

  async getRates() {
    const rates = {}
    if (this.site.config.blockio.test) {
      const r = await this.site.remote.request({uri: 'https://blockchain.info/ticker'})
      const currencies = JSON.parse(r)
      for (const key in currencies) {
        rates[key] = currencies[key].buy
      }
    }
    else {
      const {data} = await this.get('get_current_price')
      for (const {price, price_base} of data.prices) {
        rates[price_base] = Math.min(price, rates[price_base] || 1000 * 1000)
      }
    }
    return {rates}
  },

  createAddress(label) {
    return this.internal('get_new_address', {label}).then(r => {
      if ('success' === r.status) {
        r.data.status = 201
        r.data.success = true
        return r.data
      }
      else {
        r.status = 500
        return r
      }
    })
  },

  unarchiveAddress(label) {
    return this.internal('unarchive_address', {label}).then(getAddressFromResponse)
  },

  getAddress(label) {
    return this.unarchiveAddress(label)
        .then(data => data.success ? data : this.createAddress(label))
  },

  estimateFee(amount, wallet) {
    return this.internal('get_network_fee_estimate', {
      amounts: amount,
      to_addresses: wallet
    })
        .then(function (r) {
          console.log(r)
          if ('success' === r.status) {
            return {fee: +r.data.estimated_network_fee}
          }
        })
  },

  async withdraw(currency, addresses, nonce) {
    const params = {
      priority: 'low',
      amounts: [],
      to_addresses: []
    }
    let sum = 0
    for (const address in addresses) {
      params.to_addresses.push(address)
      const amount = addresses[address]
      params.amounts.push(amount)
      sum += +amount
    }
    const estimate = clone(params)
    estimate.amounts = estimate.amounts.join(',')
    estimate.to_addresses = estimate.to_addresses.join(',')
    let r = await this.get(currency, 'get_network_fee_estimate', estimate)
    if (!r || 'success' !== r.status) {
      return r
    }
    const percent = (sum - r.data.estimated_network_fee) / sum
    params.amounts = params.amounts.map(a => (percent * a).toFixed(5))
    params.pin = this.secret
    // if (nonce) {
    //   if (nonce instanceof Array) {
    //     nonce = nonce.join('')
    //   }
    //   params.nonce = nonce
    // }
    console.log('WITHDRAW:' + JSON.stringify([params.to_addresses, params.amounts]))
    params.amounts = params.amounts.join(',')
    params.to_addresses = params.to_addresses.join(',')
    r = await this.get(currency, 'withdraw', params)
    if (r instanceof Error) {
      throw r
    }
    console.log(JSON.stringify(r, null, '\t'))
    return r
  },

  async save(data) {
    let transaction = await this.site.database.table('blockchain_transaction')
        .where('id', data.id)
        .first()
    if (transaction) {
      // if (!data.time) {
      //   data.time = new Date()
      // }
      const transactions = await this.site.database.table('blockchain_transaction')
          .where('id', data.id)
          .update(omit(data, 'id'))
      return transactions[0]
    }
    else {
      if (!data.created) {
        data.created = new Date()
      }
      const transactions = await this.site.database.table('blockchain_transaction')
          .insert(data, ['created', 'amount', 'fee'])
      return transactions[0]
    }
  }
}

BlockIoApi.from = function (self) {
  return new BlockIoApi(self)
}

module.exports = BlockIoApi
