const querystring = require('querystring')

function Payeer() {

}

Payeer.prototype = {
  get config() {
    return this.site.config.payeer
  },

  async request(params) {
    params.account = this.config.account
    params.apiId = this.config.id
    params.apiPass = this.config.password
    // console.log(params)
    const response = await this.site.remote.request({
      method: 'POST',
      url: 'https://payeer.com/ajax/api/api.php',
      body: querystring.stringify(params),
      headers: {
        accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded'
      },
    })
    return JSON.parse(response)
  },

  send(wallet, amount, id) {
    const data = {
      curOut: 'USD',
      curIn: 'USD',
      action: 'transfer',
      sum: amount,
      to: wallet
    }
    if (id) {
      data.comment = '#' + id
    }
    return this.request(data)
  }
}

module.exports = Payeer
