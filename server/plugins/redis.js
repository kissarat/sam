const {createClient} = require('redis')

module.exports = class Redis {
  constructor() {
    const driver = createClient()
    for(const key of ['get', 'set', 'lrange', 'rpush', 'lrem']) {
      this[key] = function (...args) {
        return new Promise(function (yes, no) {
          driver[key](...args, function (error, result) {
            if (error) {
              no(error)
            }
            else {
              yes(result)
            }
          })
        })
      }
    }
  }
}
