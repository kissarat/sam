/**
 * Last modified: 18.08.06 18:20:33
 * Hash: 4c3dc2d02461e337b5eb6a880fb9a6af6a5ba55c
 */

const {createClient} = require('redis')

module.exports = class Redis {
  constructor(config) {
    const driver = createClient(config.redis)
    for(const key of ['get', 'set', 'lrange', 'rpush', 'lrem']) {
      this[key] = function (...args) {
        return new Promise(function (yes, no) {
          driver[key](...args, function (error, result) {
            if (error) {
              no(error)
            }
            else {
              yes(result)
            }
          })
        })
      }
    }
  }
}
