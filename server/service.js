const child_process = require('child_process')
const fs = require('fs')
require('colors')

function stamp() {
  return new Date().toLocaleString()
}

process.argv.splice(0, 2, __dirname + '/index.js')

let child

function run() {
  child = child_process.spawn(
      'node',
      process.argv,
      {
        stdio: 'inherit',
        cwd: __dirname
      }
  )

  child.on('close', function (code) {
    child.kill('SIGINT')
    run()
  })
}

const sd = __dirname + '/../sites'
const dirs = []
for(let dir of fs.readdirSync(sd)) {
  dir = sd + `/${dir}/server`
  try {
    fs.accessSync(dir)
    dirs.push(dir)
  }
  catch (ex) {

  }
}
dirs.push(__dirname)

function recursive(base) {
  for(let dir of fs.readdirSync(base)) {
    dir = base + '/' + dir
    const stat = fs.lstatSync(dir)
    if (stat.isDirectory()) {
      recursive(dir)
    }
    fs.watch(dir, function (eventType, filename) {
      if (!/___jb_(tmp|old)___$/.test(filename)) {
        console.log(filename.green)
        child.kill('SIGINT')
      }
    })
  }
}

dirs.forEach(recursive)

run()
