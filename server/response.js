const {isObject, each, pick, clone} = require('lodash')
const {response} = require('express')

module.exports = {
  __proto__: response,

  _json(o) {
    if (this.req.headers.id) {
      this.setHeader('id', this.req.headers.id)
    }
    if (!(o instanceof Array)) {
      o.spend = Date.now() - this.req.start
    }
    if ('GET' === this.req.method) {
      if (+this.req.query.pretty) {
        this.setHeader('content-type', 'application/json')
        return this.end(JSON.stringify(o, null, '  '))
      }
      return this.jsonp(o)
    }
    if (o.notifications instanceof Array && this.req.site.telegram) {
      process.nextTick(async() => {
        const users = await this.req.table('user')
          .whereIn('id', o.notifications.map(n => n.user))
          .select('id', 'nick')
        for (const user of users) {
          const n = o.notifications.find(n => user.id === n.user)
          if (n && user.telegram) {
            n.chat_id = user.telegram
            n.nick = user.nick
            await this.req.site.telegram.sendMessage(n)
          }
        }
      })
    }
    return this.json(o)
  },

  async notify(ids, data) {
    if ('string' === typeof data) {
      data = {text: data}
    }
    const users = await this.req.table('user')
      .whereIn('id', ids)
      .select('telegram')
    for (const user of users) {
      data = clone(data)
      data.chat_id = user.telegram
      await this.site.telegram.sendMessage(data)
    }
  },

  _error(err) {
    const extractFromError = err instanceof Error
    let object = extractFromError ? pick(err, 'code', 'status', 'name') : err
    if (err instanceof Error) {
      object.error = pick(err, 'message', 'detail', 'table')
      if ('string' === typeof err.stack) {
        object.error.stack = err.stack
          .replace(/\s+at\s+/g, '{#}at ')
          .split('{#}')
      }
    }
    const isValidStatus = object.status >= 200 && object.status <= 600
    if (!object.status || !isValidStatus) {
      if (!isValidStatus) {
        object.originalStatus = object.status
      }
      object.status = 520
    }
    return object
  },

  error(err) {
    const object = this._error(err)
    this
      .status(object.status)
      ._json(object)
  },

  transmit(object) {
    if (isObject(object)) {
      if (!object.status && object.success) {
        object.status = 200
      }
      if ('number' === typeof object.status) {
        this.status(object.status)
      }
      each(object.headers, (value, key) => this.setHeader(key, value))
      if ('batch' === object.type && object.results instanceof Array) {
        for (const result of object.results) {
          if (result.errors instanceof Array) {
            result.errors = result.errors.map(err => this._error(err))
          }
        }
      }
      this._json(object)
    }
    else if (object) {
      this._json({
        status: 404,
        error: {
          status: 'not_found',
          message: 'Not an Object',
          value: object
        }
      })
    }
    else {
      this._json({
        status: 502,
        error: {
          status: 'no_response'
        }
      })
    }
  },

  async promise(p) {
    try {
      if (p && p.then instanceof Function) {
        if ('function' === typeof p.limit) {
          const o = {}
          if (this.req.query.count) {
            const rows = await p.clone().count()
            o.count = +rows[0].count
          }
          if (this.req.query.order) {
            for(let key of this.req.query.order.split(',')) {
              const desc = '-' === key[0]
              if (desc) {
                key = key.slice(1)
              }
              p.orderBy(key, desc ? 'desc' : 'asc')
            }
          }
          for (const key of ['limit', 'offset']) {
            const restrict = +this.req.query[key]
            if (restrict >= 0) {
              p[key](restrict)
            }
          }
          o.result = await p
          p = o
        }
        else {
          p = await p
        }
      }
      this.transmit(p)
    }
    catch (err) {
      this.error(err)
    }
  },

  check(name, options) {
    const result = this.req.check(name, options)
    if (result) {
      this.error(result)
    }
    return !result
  },

  notFound() {
    return this.error({status: 404})
  },

  badRequest(invalid) {
    return this.error({
      status: 400,
      error: {
        invalid
      }
    })
  },

  unauthorized() {
    return this.error({status: 401})
  },

  created(data = {}) {
    data.status = 201
    return this.error(data)
  },

  forbidden(error) {
    return this.error(error ? {status: 403, error} : {status: 403})
  },

  serverError(error) {
    return this.error(error ? {status: 502, error} : {status: 502})
  },
}
