const {each, defaults, isEmpty} = require('lodash')
const salo = require('salo')

function checkAccess(options, a, b) {
  const access = this.modules.access
  for (let key in options) {
    const decorator = access[key]
    if (decorator) {
      try {
        const v = decorator.call(this, options[key], a, b)
        if (v) {
          return v
        }
      }
      catch (ex) {
        const error = salo(ex)
        error.decorator = key
        return {
          status: 500,
          error
        }
      }
    }
    else {
      return {
        status: 500,
        error: {
          decorator: key,
          message: 'No such decorator'
        }
      }
    }
  }
}

function callAction(self, module, actionName, a, b) {
  const action = module[actionName]
  if (action instanceof Function) {
    return checkAccess.call(self, module.$call, a, b)
      || checkAccess.call(self, module['$' + actionName], a, b)
      || action.call(self, a, b)
  }
  else {
    throw new Error('No such action: ' + actionName)
  }
}

function checkActionAccess(self, module, actionName, a, b) {
  const action = module[actionName]
  if (action instanceof Function) {
    return checkAccess.call(self, module.$call, a, b)
      || checkAccess.call(self, module['$' + actionName], a, b)
      || true
  }
  else {
    throw new Error('No such action: ' + actionName)
  }
}

function decorate(module) {
  const any = module.$call
  each(Object.getPrototypeOf(module), function (fn, name) {
    if (fn instanceof Function) {
      const options = defaults(any, module['$' + name])
      if (!isEmpty(options)) {
        module[name] = function decorate(a, b) {
          return checkAccess.call(this, options, a, b) || fn.call(this, a, b)
        }
      }
    }
  })
  return module
}

module.exports = {checkAccess, callAction, decorate}
