module.exports = function notFound(req, res) {
  res
    .status(404)
    .json({
      url: req.url,
      error: {
        message: 'Not Found'
      },
      headers: req.headers
    })
}
