/**
 * Last modified: 18.06.03 16:58:08
 * Hash: b7a2ca5406fbf30f307f1b917c7bfdfe99d6ffcb
 */

const {isObject, isEmpty} = require('lodash')
const {callAction} = require('../decorate')

module.exports = function handle(req, res, next) {
  const routes = (req.app.reverseRoutes && req.app.reverseRoutes[req.pathRoutes[0]]) || req.pathRoutes
  const [moduleName, actionName] = routes
  const module = req.modules[moduleName]
  if (isObject(module) && actionName) {
    req.module = module
    const action = module[actionName]
    if (action instanceof Function) {
      try {
        req.action = action
        req.actionName = actionName
        req.moduleName = moduleName
        const a = isEmpty(req.query) ? req.body : req.query
        const b = req.body
        req.start = Date.now()
        const result = callAction(req, module, actionName, a || {}, b)
        res.promise(result)
      }
      catch (ex) {
        res.error(ex)
      }
    }
    else {
      console.error('No action: ' + actionName)
      next()
    }
  }
  else {
    next()
  }
}
