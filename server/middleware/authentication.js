const Token = require('../entities/token')
const User = require('../entities/user')

module.exports = async function authentication(req, res, next) {
  function assign(token, user) {
    req.token = new Token(token)
    req.user = new User(user)
    req.token.table = () => req.table('token')
    req.user.table = () => req.table('user')
  }

  req.pathRoutes = req.url
      .split('?')[0]
      .match(/[^/]+/g)
    || ['']
  const [first] = req.pathRoutes
  // let m
  // const hasAuthorizationHeader = 'string' === typeof req.headers.cookie && (m = /sam=(\w+)/.exec(req.headers.cookie))
  if ('~' === first[0]) {
    let id
    if ('~' === first[0]) {
      id = first.slice(1)
      req.url = req.url.replace('/' + first, '')
      req.pathRoutes = req.pathRoutes.slice(1)
    }
    // if (hasAuthorizationHeader) {
    //   id = m[1]
    // }

    try {
      const token = await req.table('token_user')
        .where('id', id)
        .first()
      if (token) {
        assign(token, token.user)
      }
      else {
        assign()
      }
      next()
    }
    catch (ex) {
      res.json(500, {
        status: 500,
        error: {
          message: 'Token finding error, authentication impossible. ' + ex.message
        }
      })
    }
  }
  else {
    assign()
    next()
  }
}
