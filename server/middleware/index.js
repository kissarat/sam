module.exports = {
  before(app) {
    app.use(require('./domain'))
    app.use(require('./authentication').bind(app))
    app.use(require('./logger'))
    // app.use(require('./lord'))
    app.post('/handshake/:time', require('./handshake').bind(app))
    app.use(require('./handle'))
  },

  after(app) {
    app.use(require('./not-found'))
  }
}
