const body = require('body')
const qs = require('querystring')
const {pick} = require('lodash')

const logRegex = /^.((user.(login|signup))|perfect|nix|payeer|blockio|transfer|home|ethereum)/
const notParseBodyRegex = /^.visit.trace|upload/

module.exports = function logger(req, res, next) {
  if (notParseBodyRegex.test(req.url)) {
    next()
  }
  else {
    body(req, req.app.config.data, async function (err, data) {
      if (err) {
        return res.error(err)
      }
      if (data) {
        try {
          if (req.is('application/json')) {
            req.body = JSON.parse(data)
          }
          else if (req.is('application/x-www-form-urlencoded')) {
            req.body = qs.parse(data)
          }
          else {
            res
              .status(415)
              .end()
          }
        }
        catch (ex) {
          return res.error(ex)
        }
      }
      if ('GET' !== req.method && logRegex.test(req.url)) {
        const record = pick(req, 'url', 'method', 'ip')
        if ('content-type' in req.headers) {
          const type = /\w+\/[\w\-]+/i.exec(req.headers['content-type'])
          if (type) {
            record.type = type[0]
          }
        }
        if (req.headers.id) {
          record.guid = req.headers.id
        }
        if (req.token.id) {
          record.token = req.token.id
        }
        if (req.user.id) {
          record.user = req.user.id
        }
        if (req.headers['user-agent']) {
          record.agent = req.headers['user-agent']
        }
        if (data) {
          record.data = data
        }
        try {
          await req.table('request')
            .insert(record)
        }
        catch (ex) {
          return res.error(ex)
        }
      }
      next()
    })
  }
}
