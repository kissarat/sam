module.exports = function (req, res, next) {
  const name = req.app.domains[req.hostname]
  if (name && (req.site = req.app.sites[name])) {
    if (req.site.config.enabled) {
      next()
    }
    else {
      res.status(502).json({
        status: 502,
        error: {
          message: 'Site is disabled'
        }
      })
    }
  }
  else {
    res.status(500).json({
      status: 500,
      error: {
        message: name + ' site is not found for ' + req.hostname
      }
    })
  }
}
