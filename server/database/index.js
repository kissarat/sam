const {extend} = require('lodash')
const {timeId} = require('./utils')
const Entity = require('./entity')
const knex = require('knex')
const loadSchema = require('./load-schema')

async function log(entity, action, data, user, ip) {
  const id = timeId()
  await this.table('log').insert({id, entity, action, data, user, ip})
  return id
}

const codes = {
  UNIQUE_VIOLATION: '23505'
}

const proto = Object.assign({
  Entity,
  loadSchema,
  log,
  timeId
}, codes)

function database(config) {
  let db = database[config.id]
  if (!db) {
    db = knex(config)
    db.config = config
    extend(db, proto)
    database[config.id] = db
  }
  return db
}

Object.assign(database, codes)
module.exports = database
