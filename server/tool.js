function resolve(modules, key, ...rest) {
  const fn = modules[key]
  if ('function' === typeof fn) {
    return fn
  }
  else if (fn && 'object' === typeof fn && rest.length > 0) {
    this.module = fn
    return resolve(fn, ...rest)
  }
}

module.exports = {resolve}
