const cluster = require('cluster')
const conspiracy = require('./conspiracy')
const decorate = require('./decorate')
const express = require('express')
const fs = require('fs')
const http = require('http')
const middleware = require('./middleware/index')
const mixin = require('merge-descriptors')
const modules = require('./modules')
const numCPUs = require('os').cpus().length
const request = require('./request')
const response = require('./response')
const Site = require('./site')
const WebSocket = require('ws')
const {each, merge, defaultsDeep, isEmpty} = require('lodash')
const tool = require('./tool')
const qs = require('querystring')
const Token = require('./entities/token')
const User = require('./entities/user')

class Application extends Site {
  constructor(config, sites) {
    super(config)
    this.sites = {}
    this.domains = {}
    each(sites, (siteConfig, id) => {
      if (siteConfig.id && siteConfig.id !== id) {
        id = siteConfig.id
      }
      try {
        const local = require(siteConfig.dir + '/local')
        siteConfig = defaultsDeep(local, siteConfig)
      }
      catch (ex) {
        console.warn('No local configuration for ' + id)
      }
      if (false === siteConfig.enabled) {
        console.warn(siteConfig.id + ' disabled')
        return
      }
      for (const domain of siteConfig.domains) {
        this.domains[domain] = id
      }
      defaultsDeep(siteConfig, config)
      if (!siteConfig.id) {
        siteConfig.id = id
      }
      this.sites[id] = new Site(siteConfig)
    })
  }

  async bootstrap(isWeb = true) {
    if (isWeb) {
      this.initExpress()
      // this.initSocket()
    }
    await this.initAllDatabases()
    for (const id in this.sites) {
      await this.sites[id].loadPlugins()
    }
    await this.initModules()
  }

  async initAllDatabases() {
    await this.initDatabase()
    for (const id in this.sites) {
      await this.sites[id].initDatabase()
    }
  }

  initExpress() {
    mixin(this, express.application, false)
    const app = this
    this.listen = function listen() {
      const server = http.createServer((req, res, next) => this.handle(req, res, next))
      // app.initSocket(server)
      return server.listen.apply(server, arguments);
    }

    this.request = {__proto__: request, app: this}
    this.response = {__proto__: response, app: this}
    this.init()
  }

  initModules() {
    middleware.before(this)

    // bridges.register(this)
    this.modules = modules
    this.loadModules()
    for (const id in this.sites) {
      this.sites[id].loadModules(this)
    }

    // conspiracy.routes(this)
    middleware.after(this)
  }

  getSite(host) {
    return (host = this.domains[host]) && this.sites[host]
  }

  initSocket(port) {
    const app = this
    const flow = new WebSocket.Server({port})
    flow.on('connection', async function (socket, req) {
      socket.json = o => socket.send(JSON.stringify(o))

      const site = req.headers.host && app.getSite(req.headers.host)
      if (!site) {
        return socket.json({status: 502})
      }
      if (!site.subscribers) {
        site.subscribers = {}
      }

      mixin(req, request)
      req.app = app
      req.site = site
      req.cookies = req.headers.cookie ? qs.parse(req.headers.cookie, '; ') : {}
      if (req.cookies.labiak) {
        const token = await req.table('token_user').where('id', req.cookies.labiak).first()
        if (token) {
          req.token = new Token(token)
          req.token.table = () => req.table('token')
          req.user = token.user ? new User(token.user) : null
        }
      }
      if (!req.token) {
        req.token = {}
      }
      if (!req.user) {
        req.user = {guest: true}
      }

      socket.respond = function (input, output = {}) {
        output.id = input.id
        socket.json(output)
      }

      socket.on('message', async function (message) {
        const o = JSON.parse(message)
        try {
          const r = Object.create(req)
          const action = tool.resolve.call(r, site.modules, ...o.path.split('/'))
          if (action) {
            let output = await action.call(r, isEmpty(o.query) ? o.data : o.query, o.data)
            if (output.length >= 0) {
              output = {result: output}
            }
            if (!output.status) {
              output.status = 200
            }
            socket.respond(o, output)
          }
          else {
            socket.respond(o, {status: 404})
          }
        }
        catch (ex) {
          console.error(ex)
          socket.respond(o, {
            status: 500,
            message: ex.message || ex.toString()
          })
        }
      })
    })
    this.flow = flow
  }

  run(port) {
    process.title = 'sam-socket'
    const w = this.config.cluster
    if (1 === w.number) {
      this.work(1)
    }
    // else if (this.isEnabled('socket')) {
    //   console.error('Socket with cluster does not work')
    //   process.exit()
    // }
    else {
      this.cluster()
    }
  }


  cluster() {
    if (cluster.isMaster) {
      const w = this.config.cluster
      const number = w.cpu ? numCPUs : w.number
      for (let i = 1; i <= number; i++) {
        setTimeout(this.work.bind(this, i), i * 300)
      }
    }
  }

  timeString() {
    return new Date().toLocaleString()
  }

  work(i) {
    let port = this.config.http.port + i
    // this.initSocket(port)
    this.listen(port, () => console.log(`+ ${this.timeString()}: Server started at port ${port}`))
  }
}

module.exports = Application
