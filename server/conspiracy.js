const {each, omit, sortBy, fromPairs} = require('lodash')
const fs = require('fs')

// JSON.stringify(_.shuffle(_.range(0, 256)).map(a => ('0' + a.toString(16)).slice(-2)).join('').match(/.{96}/g), null, '\t')
// JSON.stringify(btoa(_.shuffle(_.range(0, 256)).map(a => String.fromCharCode(a)).join('')).match(/.{96}/g), null, '\t')

module.exports = {
  routes(app) {
    if (app.isEnabled('conspiracy')) {
      let secret = app.config.conspiracy.secret
      if (secret instanceof Array) {
        secret = secret.join('')
      }
      secret = new Uint8Array(new Buffer(secret, 'base64'))
      const routesFile = app.config.configDir + '/routes.json'
      fs.readFile(routesFile, function (err, routesFileData) {
        const routes = {}
        const reverseRoutes = []
        let i = 0
        let max = 0
        each(omit(app.modules, 'error', 'access'), function (module, moduleName) {
          const moduleRoute = routes[moduleName] = {}
          let j = 0
          each(module, function (action, actionName) {
            const route = ('0' + (secret[j * 5 + i]).toString(36)).slice(-2)
            moduleRoute[actionName] = route
            reverseRoutes.push([route, [moduleName, actionName]])
            max = Math.max(max, j)
            j++
          })
          i++
        })
        const newRoutesFileData = new Buffer(JSON.stringify(routes, null, '\t'))
        app.routes = routes
        app.reverseRoutes = fromPairs(sortBy(reverseRoutes, 0))
        if (!routesFileData || !routesFileData.equals(newRoutesFileData)) {
          fs.writeFile(routesFile, newRoutesFileData, function () {
            console.log('Routes file written')
          })
        }
      })
    }
    else {
      app.routes = {}
      app.reverseRoutes = {}
    }
  }
}
