const passport = require('passport')
const {Strategy} = require('passport-facebook')
const {Router} = require('express')
const db = require('schema-db')

function callback(accessToken, refreshToken, profile, done) {
  console.log(accessToken, refreshToken, profile)
  db.table('social').insert({type: 'facebook', data: profile._json}).then(function () {
    done()
  })
    .catch(done)
}

const scope = ['public_profile',
  'user_friends', 'email', 'user_about_me', 'user_birthday',
  'user_education_history', 'user_hometown', 'user_location', 'user_posts', 'user_relationships',
  'user_relationship_details', 'user_religion_politics', 'user_tagged_places', 'user_website',
  'user_work_history', 'manage_pages', 'read_insights'
]

function handle(req, res) {
  res.json({
    params: req.query
  })
}

module.exports = {
  callback, handle,

  register(config) {
    passport.use(new Strategy(config, callback))
  },

  router() {
    const router = Router()
    router.get('/', passport.authenticate('facebook', {scope}), handle)
    router.get('/callback',
      passport.authenticate('facebook', {
          successRedirect: '/cabinet',
          failureRedirect: '/login'
        },
        callback)
    );
    return router
  }
}
