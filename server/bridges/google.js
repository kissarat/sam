const passport = require('passport')
const {OAuth2Strategy} = require('passport-google-oauth')
const {Router} = require('express')

function callback(accessToken, refreshToken, profile, done) {
  console.log(JSON.stringify([accessToken, refreshToken, profile, done], null, '\t'))
}

const scope = ['plus.login', 'plus.me', 'userinfo.email', 'userinfo.profile',
  'contacts.readonly', 'user.addresses.read', 'user.birthday.read', 'user.emails.read',
  'user.phonenumbers.read', 'userinfo.email', 'userinfo.profile', 'plus.circles.read',
  'plus.media.upload', 'plus.stream.write', 'userlocation.beacon.registry']
  .map(a => 'https://www.googleapis.com/auth/' + a)

function handle(req, res) {
  res.json({
    params: req.query
  })
}

module.exports = {
  callback, handle,

  register(config) {
    passport.use(new OAuth2Strategy(config, callback))
  },

  router() {
    const router = Router()
    router.get('/', passport.authenticate('google', {scope}), handle)
    router.get('/callback',
      passport.authenticate('google', {
          failureRedirect: '/login'
        },
        callback)
    );
    return router
  }
}
