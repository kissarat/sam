const {Router} = require('express')
const config = require('../../config')
const {each} = require('lodash')
const passport = require('passport')

function register(app) {
  const router = Router()

  router.get('/', function (req, res) {
    const bridges = []
    each(config.bridges, function ({enabled}, name) {
      if (enabled) {
        bridges.push(name)
      }
    })
    res.json({
      time: new Date().toISOString(),
      bridges
    })
  })

  each(config.bridges, function (thisConfig, name) {
    if (false === thisConfig.enabled) {
      console.error(`Bridge "${name}" disabled`)
    }
    else {
      const module = require('./' + name)
      const url = '/' + name
      thisConfig.callbackURL = config.origin + `/serve/bridge/${name}/callback`
      const strategy = module.register(thisConfig)
      strategy.site = app
      if (module.router instanceof Function) {
        router.use(url, module.router())
      }
      console.error(`Bridge "${name}" enabled`)
    }
  })

  app.use('/bridge', router)
}

module.exports = {register}
