const app = require('./boot')

app.bootstrap(true)
    .then(function () {
      app.run()
    })
    .catch(function (err) {
      console.error(err)
      process.exit(1)
    })
