module.exports = {
  isEnabled(name) {
    const moduleConfig = this.config[name]
    return moduleConfig && false !== moduleConfig.enabled
  },

  actionFilter(name) {
    return /^[a-z]/.test(name)
  },

  // getModuleActions(name, filter = false) {
  //   const keys = Object.keys(Object.getPrototypeOf(this.modules[name]))
  //   return filter ? keys.filter(this.actionFilter) : keys
  // },

  publish(module, source) {
    if ('string' === typeof module) {
      module = this.modules[module]
    }
    const definition = {}
    Object.getOwnPropertyNames(source).forEach(name => {
      definition[name] = {
        value: source[name],
        enumerable: this.actionFilter(name)
      }
    })
    // console.log(definition)
    Object.defineProperties(module, definition)
  }
}
