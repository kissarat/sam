/**
 * Last modified: 18.06.03 17:08:44
 * Hash: 25e04ca65030a41101d1be66f8f6e2a5636af8e2
 */

const database = require('./database')
const features = require('./features')
const plugins = require('./plugins')
const utils = require('../common/utils')
const {EventEmitter} = require('events')
const {isEmpty, isObject} = require('lodash')

class Site extends EventEmitter {
  constructor(config) {
    super()
    this.config = config
  }

  async loadPlugins() {
    for (const name in plugins) {
      const Plugin = plugins[name]
      const config = this.config[name]
      if (isObject(config) && false !== config.enabled) {
        const plugin = new Plugin(this, name)
        Object.defineProperties(plugin, {
          site: {
            get: () => this
          }
        })
        this[name] = plugin
        if ('function' === typeof plugin.load) {
          await plugin.load()
        }
      }
      else {
        // console.warn(`Plugin "${name}" disabled for ${this.config.id} ` + this.config.origin)
      }
    }
  }

  loadModules(app) {
    let source = this.config.dir + '/server/modules'
    if ('string' === typeof source) {
      try {
        source = require(source)
      }
      catch (ex) {
        console.warn('No modules directory ' + source)
      }
    }

    const modules = app ? Object.create(app.modules) : {}
    for (const name in source) {
      const config = this.config[name]
      // console.log(name, utils.isEnabled(config))
      if (utils.isEnabled(config)) {
        const Module = source[name]
        modules[name] = 'function' === typeof Module
          ? new Module(app || this, app ? this : false)
          : Object.create(Module)
      }
      else if (this.config.id) {
        return console.warn(`Module "${name}" disabled for ` + this.config.id + ' ' + this.config.origin)
      }
    }
    this.modules = modules
  }

  async initDatabase() {
    if (!isEmpty(this.config.database)) {
      this.database = database(this.config.database)
      if (this.config.database.schema) {
        return this.database.loadSchema(true)
      }
    }
    else if (this.config.id) {
      console.error('No database configuration for ' + this.config.id + ' ' + this.config.origin)
    }
    // process.exit(1)
  }

  async getSetting(key) {
    const setting = await this.database.table('setting').where('key', key).first()
    if (setting) {
      return setting.value
    }
  }

  async getSettings() {
    const settings = {}
    for (const {key, value} of await this.database.table('setting')) {
      settings[key] = value
    }
    return settings
  }

  setSetting(key, value) {
    return this.database.table('setting')
      .where('key', key)
      .update({value})
  }

  async setSettings(settings) {
    for (const key in settings) {
      await this.setSetting(key, settings[key])
    }
  }

  table(name) {
    return this.database.table(name)
  }
}

Object.assign(Site.prototype, features)
module.exports = Site
