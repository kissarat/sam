const {merge} = require('lodash')

class HttpError extends Error {
  constructor(options) {
    super()
    merge(this, options)
  }
}

module.exports = {HttpError}
