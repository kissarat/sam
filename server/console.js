const {isEmpty} = require('lodash')
const app = require('./boot')
const request = require('./request')

function die(...args) {
  console.error(...args)
  process.exit(1)
}

const site = app.sites[process.env.SITE]
if (!site) {
  die('Invalid site: ' + process.env.SITE)
}
const context = {
  __proto__: request,
  app,
  site,
  ip: null,
  user: {
    id: 1,
    nick: 'admin',
    admin: true
  }
}

function run(...args) {
  const params = {}
  const data = {}
  let action = null
  for(const arg of args) {
    let [str, key, oper, value] = /^(-?[\w_]+)([\/=])(.+)$/.exec(arg)
    if (str) {
      if ('/' === oper) {
        if ((action = site.modules[key]) && (action = action[value])) {
          // console.log(key, value, action)
        }
        else {
          die('Route not found: ' + arg)
        }
      }
      else if ('=' === oper) {
        if ('-' === key[0]) {
          params[key.slice(1)] = value
        }
        else {
          data[key] = value
        }
      }
      else {
        die('Unknown operator: ' + oper)
      }
    }
    else {
      die('Invalid argument: ' + arg)
    }
  }
  return action.call(context, isEmpty(params) ? data : params, data)
}

app.bootstrap(true)
    .then(function () {
      return run(...process.argv.slice(2))
    })
    .then(function (result) {
      console.log(context.config.locals.pretty
          ? JSON.stringify(result, null, '\t')
          : JSON.stringify(result))
      process.exit()
    })
    .catch(function (err) {
      console.error(err)
      process.exit(1)
    })
