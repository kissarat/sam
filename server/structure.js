module.exports = {
  async tree({id, level, view}) {
    const q = this.table(view)
      .where('root', +id)
    if (level) {
      q.where('level', '<=', +level)
    }
    q.orderBy('level')
    q.orderBy('id')
    const rows = await q
    if (0 === rows.length) {
      return {status: 404}
    }

    for(const row of rows) {
      row.id = +row.id
      row.level = +row.level
    }

    function build(root) {
      const children = rows
        .filter(row => row.parent === root.id)
        .map(row => build(row))
      if (children.length > 0) {
        root.children = children
      }
      return root
    }

    const root = rows.find(row => row.level <= 0)
    return root ? build(root) : {
        status: 500,
        error: {
          message: 'Root not found'
        }
      }
  }
}
