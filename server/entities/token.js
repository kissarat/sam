const {extend, pick} = require('lodash')

class Token {
  constructor(object) {
    extend(this, object)
  }

  toString() {
    return this.id
  }

  update(data) {
    data = pick(data, 'user', 'expires', 'handshake')
    if (!data.handshake) {
      data.time = new Date()
    }
    return this.table()
      .where('id', this.id)
      .update(data, ['id', 'user', 'time', 'expires'])
      .then(function (r) {
        if (r.length > 0) {
          r[0].success = true
          return r[0]
        }
        return {found: false}
      })
  }
}

module.exports = Token
