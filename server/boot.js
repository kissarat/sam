/**
 * Last modified: 18.06.06 05:11:36
 * Hash: dc5482291e3cca1f2fd92302003c0ba355128949
 */

const Application = require('./application')

const config = require('../config')
const sites = {
  // mlbot: require('../sites/mlbot/config'),
  goldenlife: require('../sites/olympus/config'),
  // inbisoft: require('../sites/inbisoft/config'),
  external: require('../sites/external/config'),
  // ladder: require('../sites/ladder/config'),
  // july: require('../sites/july/config'),
  // havka: require('../sites/havka/config'),
}

const app = new Application(config, sites)

module.exports = app

function onExit() {
  console.log(`- ${app.timeString()} exit`)
  process.exit()
}


process.on('SIGINT', onExit)
// process.on('SIGKILL', onExit)

