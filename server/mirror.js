const express = require('express')
const {Router} = require('express')

const router = Router()

router.get('/query', function (req, res) {
  res.json({
    query: req.query
  })
})

const app = express()

app.use('/mirror', router)

app.listen(8000)
