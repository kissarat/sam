const ArrayModel = require('../../common/models/model')
const {isEmpty, isObject, pick, omit, filter, keyBy, map} = require('lodash')

class DataModel extends ArrayModel {
  table() {
    return this.database.table(this.id)
  }

  find(query) {
    let q
    const select = query.select || this.query.select
    if (select instanceof Array) {
      q = this.database.select(select)
        .from(this.id)
      const match = this.omit(query)
      if (!isEmpty(match)) {
        q.where(match)
      }
    }
    else {
      q = this.where(query)
    }
    const sort = query.sort || this.query.sort
    if (isObject(sort) && !isEmpty(sort)) {
      for (const name in sort) {
        q.orderBy(name, sort[name] > 0 ? 'asc' : 'desc')
      }
    }
    const search = query.search || this.query.search
    if (search instanceof Array && search.length > 0) {
      const fields = filter((select instanceof Array ? pick(this.fields, select) : this.fields), f => 'string' === f.type)
      if (fields.length > 0) {
        q.where(function () {
          for (const {name} of fields) {
            for (const term of search) {
              q.orWhere(name, 'ilike', `%${term}%`)
            }
          }
        })
      }
    }
    const offset = query.offset || this.query.offset
    if (offset >= 0) {
      q.offset(offset)
    }
    const limit = query.limit || this.query.limit
    if (limit >= 0) {
      q.limit(limit)
    }
    return q
  }

  returningKeys() {
    return map(pick(this.fields, ['id', 'created', 'time']), f => f.name)
  }

  insert(objects) {
    return this.table()
      .insert(objects, this.returningKeys())
  }

  async put(objects) {
    const time = new Date()
    const insert = objects.filter(o => !(o.id > 0) && !o.created)
    for (const newObject of insert) {
      newObject.created = time
    }
    let created = insert.length > 0
      ? await this.insert(insert)
      : []
    const forUpdate = objects.filter(o => o.id > 0)
    const absent = []
    const updated = []
    const returning = this.returningKeys()
    if (forUpdate.length > 0) {
      let oldObjects = await this.table()
        .whereIn('id', forUpdate.map(o => o.id))
      oldObjects = keyBy(oldObjects, 'id')
      for (const o of forUpdate) {
        const old = oldObjects[o.id]
        if (old) {
          o.time = time
          const [changed] = await this.table()
            .where('id', o.id)
            .update(o, returning)
          updated.push(changed)
        }
        else {
          absent.push(o)
        }
      }
    }
    return {
      affected: created.length + updated.length,
      time,
      created,
      updated,
      absent
    }
  }

  async batch(_1, queue) {

  }

  where(query) {
    const q = this.table()
    const match = this.omit(query)
    if (!isEmpty(match)) {
      q.where(match)
    }
    return q
  }

  update(query, changes) {
    return this.where(query)
      .update(changes, this.returningKeys())
  }

  remove(query) {
    return this.where(query)
      .del()
  }
}

module.exports = DataModel
