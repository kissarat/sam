// const _ = require('lodash')

const isClient = !!('undefined' !== typeof window && window.document)

const exports = {}

// exports.merge = _.merge
// exports.moment = require('moment-timezone')
exports.Translation = require('./i18n/translation')
exports.global = isClient ? window : global
exports.isClient = isClient
exports.isServer = !isClient
exports.isChrome = isClient && !!self.chrome && !!window.chrome.webstore
exports.isBlink = isClient && exports.isChrome && !!window.CSS
exports.isFirefox = isClient && self.InstallTrigger
exports.hasStorage = isClient && window.localStorage && window.Storage && localStorage instanceof Storage

exports.DEBUG = true
Object.defineProperty(exports, 'DEBUG', {
  writable: false,
  value: !!('undefined' !== typeof localStorage && +localStorage.getItem('debug'))
})

exports.debug = exports.DEBUG ? console.log.bind(console) : Function()

exports.announce = function announce(objects) {
  Object.assign(exports.global, objects)
}

function boot(cb) {
  boot.listeners.push(cb)
}

boot.listeners = []
exports.boot = boot

Object.assign(exports.global, exports)

module.exports = exports
