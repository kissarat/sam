var _ = require('lodash')

function Translation(language, dictionary) {
  this.language = language
  this.dictionary = {}
  for (var name in dictionary) {
    this.dictionary[name.toLocaleLowerCase()] = dictionary[name]
  }
}

Translation.prototype = {
  translate: function (text, vars) {
    if (!text) {
      return text
    }
    text = text.toLocaleLowerCase()
    text = this.dictionary[text] || Translation.list.en.dictionary[text] || text
    return Translation.substitute(text, vars)
  },

  mass: function (texts, vars) {
    const self = this
    return texts.map(function (text) {
      return self.translate(text, vars)
    })
  },

  inverse: function (text) {
    for (var name in this.dictionary) {
      if (text === this.dictionary[name].toLowerCase()) {
        return name
      }
    }
  },

  massInverse: function (texts) {
    const self = this
    return texts.map(function (text) {
      return self.inverse(text)
    })
  },

  toFunction: function () {
    const self = this

    function t(text, vars) {
      return self.translate(text, vars)
    }

    t.language = function () {
      return self.language
    }
    return t
  }

}

Translation.substitute = function (text, vars) {
  if (_.isObject(vars)) {
    return text.replace(/{[\w_]+}/g, function (s) {
      return vars[s.slice(1, -1)] || s
    })
  }
  return text
}

Translation.multitranslate = function (text, vars) {
  var translations = {}
  for (var language in Translation.languages) {
    translations[language] = Translation.list[language].translate(text, vars)
  }
  return translations
}

/**
 * @param {string} text
 * @return Translation
 */
Translation.findTranslation = function (text) {
  for (var language in Translation.languages) {
    if (Translation.languages[language].test(text)) {
      return Translation.list[language]
    }
  }
}

Translation.languages = {
  en: /^(English|eng?)$/i,
  ru: /^(Russ?ian|Русс?кий|rus?)$/i
}

var ru = require('./locales/ru')
var en = {}
for (var name in ru) {
  en[name] = name
}

Translation.list = {
  en: new Translation('en', en),
  ru: new Translation('ru', ru),
  it: new Translation('it', require('./locales/it'))
}

module.exports = Translation
