function isEnabled(config) {
  return !config || !(config && false === config.enabled)
}

module.exports = {isEnabled}
