function isNull(value) {
  return undefined === value || null === value
}

module.exports = {
  toString() {
    return `${this.name}`
  },

  keys: ['required', 'type', 'maxLength'],

  positive({value}) {
    return value > 0
  },

  required({value, required, generated}) {
    return !required || generated || !isNull(value)
  },

  // trim({name, value}) {
  //   if ('string' === typeof value) {
  //     this[name] = value.trim()
  //   }
  // },

  type({value, name, type, items}) {
    if (isNull(value)) {
      return true
    }
    switch (type) {
      case 'short':
        return -32767 < value && value < 32767
      case 'integer':
        return -2147483647 < value && value < 2147483647
      case 'float':
        return Number.isFinite(value)
      case 'enum':
      case 'USER-DEFINED':
        return items.indexOf(value) >= 0
      case 'string':
        if ('string' === typeof value) {
          this[name] = value.trim()
          return
        }
        return false
      case 'time':
        value = new Date(value)
        if (Number.isFinite(value.getTime())) {
          this[name] = value
        }
        return
      default:
        return type === typeof value
    }
  },

  integer({value}) {
    return Number.isSafeInteger(value)
  },

  // items({value, items}) {
  //   return items.indexOf(value) >= 0
  // },

  short({value, short}) {
    return true
  },

  maxLength({value, maxLength}) {
    return isNull(value) || value.length <= maxLength
  },

  time({name, value}) {
    if (!(value instanceof Date)) {
      this[name] = value
    }
  }
}
