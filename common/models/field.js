const {defaults, merge, isEmpty, isObject, pick, omit, matches} = require('lodash')
const validation = require('./validation')

class Field {
  constructor(options) {
    merge(this, options)
  }

  toString() {
    return `${this.name}:${this.type}`
  }

  rules() {
    const _rules = []
    for (const key of validation.keys) {
      if (key in this) {
        _rules.push(validation[key])
      }
    }
    return _rules
  }

  invalid(changes) {
    const field = this
    return {
      __proto__: this,

      toString () {
        return field.toString() + ` ${this.rule}`
      },

      toJSON() {
        const json = pick(this, 'name', 'value', 'rule')
        json.message = this.toString()
        return json
      },

      get object() {
        return changes
      },

      get value() {
        return changes[this.name]
      }
    }
  }

  validate(changes) {
    for (const rule of this.rules()) {
      const error = this.invalid(changes)
      const val = rule.call(changes, error)
      if (false === val || isObject(val)) {
        error.rule = rule.name
        if (isObject(val)) {
          merge(error, val)
        }
        return error
      }
    }
  }
}

module.exports = Field
