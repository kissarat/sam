const Model = require('./model')

class Factory {
  load(schema) {
    if (!schema) {
      try {
        schema = require('./schema')
      }
      catch (ex) {
        schema = {}
      }
    }
    this[Symbol.for('schema')] = schema
    return this
  }

  getModel(name) {
    if (!this[name]) {
      this[name] = new Model(this[Symbol.for('schema')][name])
    }
    return this[name]
  }
}

module.exports = new Factory()
