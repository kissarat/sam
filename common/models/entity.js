const {merge, pick, isEmpty} = require('lodash')

export default class Entity {
  constructor(options) {
    if (options) {
      merge(this, options)
    }
  }

  apply(object) {
    Object.setPrototypeOf(object, this)
    return object
  }

  validate() {
    return this.model.validate(this)
  }

  save() {
    return this.model.put(this)
  }
}
