const {defaults, merge, isEmpty, isObject, pick, omit, matches, filter} = require('lodash')
const Field = require('./field')

/**
 * @property {string} store
 * @property {Field[]} fields
 */
class Model {
  constructor(options) {
    merge(this, options)
    if (isObject(this.fields)) {
      for(const id in this.fields) {
        this.fields[id] = new Field(this.fields[id])
      }
    }
    if (!isObject(this.query)) {
      this.query = {}
    }
  }

  action(method) {
    switch (method) {
      case 'GET':
        return this.find
      case 'POST':
        return this.batch
      case 'PATCH':
        return this.update
    }
  }

  getPlural() {
    return this.id + 's'
  }

  /**
   * @param {Object[]} array
   * @return {Object[]}
   */
  mapEntities(array) {
    if (this.Entity instanceof Object) {
      for (const entity of array) {
        entity.__proto__ = this.Entity.prototype
        Object.defineProperty(entity, 'model', {value: this})
      }
    }
    else {
      console.warn('Entity class of Model is not found')
      return array
    }
  }

  omit(query) {
    query = defaults(query, this.query)
    return query.select instanceof Array
      ? pick(query, query.select)
      : omit(query, 'select', 'sort', 'search', 'offset', 'limit')
  }

  matches(query) {
    return matches(this.omit(query))
  }

  match(query) {
    return this.storage.filter(this.matches(query))
  }

  find(query) {
    let result = this.match(query)
    if (result.length > 0) {
      if (query.select instanceof Array) {
        result = result.map(o => pick(o, query.select))
      }
      if (isObject(query.sort) && !isEmpty(query.sort)) {
        result.sort((a, b) => {
          for (const name in query.sort) {
            const left = a[name]
            const right = b[name]
            const order = query.sort[name]
            let compare = 0
            switch (typeof left) {
              case 'string':
                compare = left.localeCompare(right)
                break

              case 'number':
              case 'boolean':
                compare = left - right
                break

              case 'undefined':
              case 'object':
                if (!left) {
                  compare = right ? -order : 0
                }
                else {
                  throw new Error('Object compare', left)
                }
                break
            }
            if (0 !== compare) {
              return (compare > 0 ? 1 : -1) * order
            }
          }
          return 0
        })
      }
      if (query.search instanceof Array) {
        result = result.filter(o => {
          for (const search of query.search) {
            for (const name in o) {
              const value = o[name]
              if ('string' === typeof value) {
                if (value.match(search)) {
                  return true
                }
              }
            }
          }
          return false
        })
      }
    }
    return result.slice(this.offset || 0, this.limit || result.length)
  }

  first(query) {
    return this.find(query)[0]
  }

  create(options) {
    const entity = new this.Entity(options)
    Object.defineProperty(entity, 'model', {value: this})
    return entity
  }

  clone(toPick = ['query']) {
    return new this.constructor(pick(this, toPick))
  }

  specify(query) {
    const model = this.clone()
    defaults(query, model.query)
    model.query = query
    return model
  }

  createPage(limit = 12, offset = 0) {
    return this.specify({limit, offset})
  }

  put(objects) {
    for (const o of objects) {
      const exist = this.storage.find(x => x.id === o.id)
      if (isObject(exist)) {
        merge(exist, o)
      }
      else {
        this.storage.push(o)
      }
    }
  }

  update(query, changes) {
    const array = this.find(query)
    for (const o of array) {
      merge(o, changes)
    }
  }

  remove(query) {
    this.storage = this.storage.filter(a => !this.matches(query)(a))
  }

  validate(changes) {
    const errors = []
    if (isObject(this.fields)) {
      for (const name in this.fields) {
        const error = this.fields[name].validate(changes)
        if (error) {
          errors.push(error)
        }
      }
    }
    return errors
  }

  pick(object) {
    pick(object, Object.keys(this.fields))
  }
}

module.exports = Model
