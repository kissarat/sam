CREATE OR REPLACE VIEW token_user AS
  SELECT
    t.created,
    t.handshake,
    t.hard,
    t.id,
    t.ip,
    t.name,
    t.time,
    t.type,
    u.email,
    u.nick,
    u.admin,
    u.leader,
    u.avatar,
    u.parent,
    coalesce(u.mlbot > CURRENT_TIMESTAMP, FALSE) AS mlbot,
    row_to_json(u.*)                             AS "user"
  FROM token t
    LEFT JOIN "user" u ON t.user = u.id
  WHERE expires > CURRENT_TIMESTAMP;

CREATE OR REPLACE VIEW user_about AS
  SELECT
    id,
    type,
    nick,
    forename,
    surname,
    admin,
    leader,
    skype,
    avatar,
    null as facebook,
    null as vkontakte,
    null as twitter,
    null as google,
    null as odnoklassniki,
    null as about
  FROM "user";

CREATE OR REPLACE VIEW transfer_user AS
  SELECT
    tr.*,
    f.nick AS from_nick,
    t.nick AS to_nick
  FROM transfer tr
    LEFT JOIN "user" f ON f.id = tr."from"
    LEFT JOIN "user" t ON t.id = tr."to";

CREATE OR REPLACE VIEW balance AS
  WITH
      a AS (

      SELECT
        "to" AS id,
        amount
      FROM transfer it
      WHERE "to" IS NOT NULL AND it.status = 'success'
      UNION ALL
      SELECT
        "from" AS id,
        -amount
      FROM transfer ot
      WHERE "from" IS NOT NULL AND ot.status = 'success'
    ),
      b AS (
        SELECT
          id,
          sum(amount) :: INT AS balance
        FROM a
        GROUP BY id
    )
  SELECT
    u.id,
    coalesce(b.balance, 0) AS balance
  FROM b
    RIGHT JOIN "user" u ON b.id = u.id;

CREATE OR REPLACE VIEW referral AS
  WITH RECURSIVE r(id, "nick", parent, email, root, level, forename, surname, skype, mlbot, avatar) AS (
    SELECT
      id,
      nick,
      parent,
      email,
      id AS root,
      0  AS level,
      forename,
      surname,
      skype,
      mlbot,
      avatar
    FROM "user"
    UNION
    SELECT
      u.id,
      u.nick,
      u.parent,
      u.email,
      r.root,
      r.level + 1 AS level,
      u.forename,
      u.surname,
      u.skype,
      u.mlbot,
      u.avatar
    FROM "user" u
      JOIN r ON u.parent = r.id
    WHERE r.level < 50
  )
  SELECT
    id,
    nick,
    parent,
    email,
    root,
    level,
    forename,
    surname,
    skype,
    mlbot,
    avatar
  FROM r
  ORDER BY root, level, id;

CREATE OR REPLACE VIEW informer AS
  SELECT
    b.*,
    coalesce((SELECT sum(amount)
              FROM "transfer" a
              WHERE a.type = 'payment' AND a.status = 'success' AND a.to = b.id), 0)            AS payment,
    coalesce((SELECT sum(amount)
              FROM "transfer" a
              WHERE a.type = 'withdraw' AND a.status = 'success' AND a.to = b.id), 0)           AS withdraw,
    coalesce((SELECT sum(amount)
              FROM "transfer" a
              WHERE a.type IN ('accrue', 'bonus') AND a.status = 'success' AND a.to = b.id), 0) AS accrue,
    coalesce((SELECT sum(amount)
              FROM "transfer" a
              WHERE a.type IN ('accrue', 'bonus') AND a.status = 'success' AND a.to = b.id
                    AND created > CURRENT_TIMESTAMP -
                                  INTERVAL '1 day'), 0)                                         AS accrue_day,
    (SELECT count(*)
     FROM "visit" v
     WHERE v.url = '/ref/' || u.nick)                                                           AS visit,
    (SELECT count(*)
     FROM "user" r
     WHERE r.parent = u.id)                                                                     AS invited,
    (SELECT count(*)
     FROM referral r
     WHERE r.root = u.id AND r.level <= 7)                                                      AS referral,
    (SELECT count(*)
     FROM referral r
       JOIN transfer t ON r.id = t."from"
     WHERE t.type = 'buy' AND r.root = u.id AND r.level <= 7)                                   AS sales,
    (SELECT sum(t.amount)
     FROM referral r
       JOIN transfer t ON r.id = t."from"
     WHERE t.type = 'buy' AND r.root = u.id AND r.level <= 7)                                   AS turnover,
    u.nick,
    u.type,
    u.surname,
    u.forename,
    u.skype,
    u.avatar,
    u.leader,
    u.email,
    u.facebook,
    u.vkontakte,
    u.twitter,
    u.google,
    u.odnoklassniki,
    u.phone,
    u.about,
    to_json(p.*)                                                                                AS parent
  FROM balance b
    JOIN "user" u ON b.id = u.id
    LEFT JOIN user_about p ON u.parent = p.id;

CREATE OR REPLACE VIEW sponsor AS
  WITH RECURSIVE r(id, "nick", parent, root, level, leader) AS (
    SELECT
      id,
      nick,
      parent,
      id AS root,
      0  AS level,
      leader
    FROM "user"
    UNION
    SELECT
      u.id,
      u.nick,
      u.parent,
      r.root,
      r.level + 1 AS level,
      u.leader
    FROM "user" u
      JOIN r ON u.id = r.parent
    WHERE r.level < 50
  )
  SELECT
    id,
    nick,
    parent,
    root,
    level,
    leader
  FROM r
  ORDER BY root, level, id;

CREATE OR REPLACE VIEW message_user AS
  SELECT
    m.id,
    m.article,
    m.parent,
    m.created,
    m.text,
    row_to_json(u) AS "user"
  FROM message m
    JOIN user_about u ON m.user = u.id;

CREATE OR REPLACE VIEW article_author AS
  SELECT
    a.*,
    u.nick,
    u.surname,
    u.forename,
    u.email,
    u.skype,
    u.avatar,
    u.admin
  FROM article a LEFT JOIN "user" u ON a."user" = u.id;

CREATE OR REPLACE VIEW article_relation AS
  SELECT
    aa.*,
    c.path AS from_path
  FROM article_author aa
    JOIN relation r ON aa.id = r."to"
    JOIN article c ON c.id = r."from";

CREATE OR REPLACE VIEW statistics AS
  SELECT
    (SELECT sum(amount)
     FROM transfer
     WHERE type = 'payment' AND status = 'success')                        AS payment,
    (SELECT sum(amount)
     FROM transfer
     WHERE type = 'payment' AND status =
                                'pending')                                 AS pending,
    (SELECT sum(amount)
     FROM transfer
     WHERE type = 'withdraw' AND status =
                                 'success')                                AS withdraw,
    (SELECT sum(amount)
     FROM transfer
     WHERE type = 'accrue' AND status =
                               'success')                                  AS accrue,
    (SELECT sum(amount)
     FROM transfer
     WHERE type = 'buy' AND status =
                            'success')                                     AS buy,
    (SELECT count(*)
     FROM
         "user")                                                           AS users,
    (SELECT count(*)
     FROM "user" ud
     WHERE ud.created > CURRENT_TIMESTAMP -
                        INTERVAL '1 day')                                  AS users_day,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires >
                                CURRENT_TIMESTAMP)                         AS visitors,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires > CURRENT_TIMESTAMP
           AND handshake > CURRENT_TIMESTAMP -
                           INTERVAL '1 day')                               AS visitors_day,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires > CURRENT_TIMESTAMP
           AND handshake > CURRENT_TIMESTAMP -
                           INTERVAL '1 day')                               AS new_visitors_day,
    (SELECT count(*)
     FROM log
     WHERE entity = 'handshake' AND action = 'update'
           AND to_timestamp(id / (1000 * 1000 * 1000)) > CURRENT_TIMESTAMP -
                                                         INTERVAL '1 day') AS activities_day,
    (SELECT count(*)
     FROM visit
     WHERE url LIKE '/ref/%' AND visit.created > CURRENT_TIMESTAMP -
                                                 INTERVAL '1 day')         AS referral_day,
    (SELECT sum(balance)
     FROM balance)                                                         AS balance;

CREATE OR REPLACE VIEW user_log AS
  SELECT
    l.id,
    l."user",
    u.nick,
    l.entity,
    l.action,
    l.ip,
    l.data
  FROM log l LEFT JOIN "user" u ON l."user" = u.id;

CREATE OR REPLACE VIEW top_referrals AS
  SELECT
    replace(url, '/ref/', '') AS nick,
    count
  FROM (SELECT
          url,
          count(*)
        FROM visit
        WHERE url LIKE '/ref/%'
        GROUP BY url) t
  WHERE count > 1
  ORDER BY count DESC;

CREATE OR REPLACE VIEW withdrawable AS
  SELECT
    t.id,
    (CASE WHEN b.balance - t.amount >= 0
      THEN 'success'
     ELSE 'canceled' END) AS will,
    t.amount,
    t.wallet,
    t."from",
    u.nick as "from_nick",
    b.balance,
    t.txid,
    b.balance - t.amount  AS remain,
    t.ip,
    t.text,
    t.vars,
    t.system,
    t.created
  FROM transfer t
    JOIN balance b ON t."from" = b.id
    JOIN "user" u ON t."from" = u.id
  WHERE t.type = 'withdraw' AND t.status = 'created' AND t.wallet IS NOT NULL
        AND t.txid IS NULL AND t.node IS NULL AND t.amount > 0 AND t.ip IS NOT NULL;

CREATE OR REPLACE VIEW file_download AS
  SELECT
    d.file,
    count(*),
    cat
  FROM file f
    JOIN download d ON f.id = d.file
  WHERE cat IS NOT NULL
  GROUP BY cat, file;

CREATE OR REPLACE VIEW user_parent AS
  SELECT
    u.id,
    u.nick,
    p.nick AS parent_nick
  FROM "user" u
    JOIN "user" p
      ON u.parent = p.id;

CREATE OR REPLACE VIEW children AS
  SELECT
    id,
    nick,
    surname,
    forename,
    email,
    skype,
    parent,
    created,
    (SELECT count(*)
     FROM "user" c
     WHERE c.parent = p.id) as count
  FROM "user" p;

CREATE OR REPLACE VIEW table_size AS
  WITH
      a AS (
        SELECT
          c.oid,
          nspname                               AS table_schema,
          relname                               AS "table_name",
          c.reltuples                           AS row_estimate,
          pg_total_relation_size(c.oid)         AS total_bytes,
          pg_indexes_size(c.oid)                AS index_bytes,
          pg_total_relation_size(reltoastrelid) AS toast_bytes
        FROM pg_class c
          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
        WHERE relkind = 'r'
    ),
      b AS (
        SELECT
          *,
          total_bytes - index_bytes - COALESCE(toast_bytes, 0) AS table_bytes
        FROM a
    )
  SELECT
    "table_name",
    pg_size_pretty(total_bytes) AS total,
    pg_size_pretty(index_bytes) AS INDEX,
    pg_size_pretty(toast_bytes) AS toast,
    pg_size_pretty(table_bytes) AS "table",
    total_bytes
  FROM b
  WHERE table_schema = 'public';

CREATE OR REPLACE VIEW user_telegram AS
  SELECT
    coalesce(u.telegram, t.id) AS telegram,
    t.context,
    u.id,
    u.nick,
    u.email,
    u.admin,
    u.surname,
    u.forename
  FROM "user" u FULL JOIN telegram t ON u.telegram = t.id;
