#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git pull origin master

if [ Darwin = `uname` ]; then
    psql postgres -c 'DROP DATABASE sam'
    psql postgres -c 'CREATE DATABASE sam OWNER sam'
    psql sam -c 'CREATE EXTENSION pgcrypto'
else
echo eec0pheeTho2iaka | su postgres -S bash -c <<LINUX
    psql postgres -c 'DROP DATABASE sam'
    psql postgres -c 'CREATE DATABASE sam OWNER sam'
    psql sam -c 'CREATE EXTENSION pgcrypto'
LINUX
fi

cat $DIR/tables.sql $DIR/views.sql | PGPASSWORD=fjbgbsA4vDbBEZ5bRnneARD psql sam -U sam -h localhost
