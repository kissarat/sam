CREATE TYPE user_type AS ENUM ('new', 'native', 'leader', 'special');

CREATE TABLE "user" (
  id            SERIAL PRIMARY KEY,
  nick          VARCHAR(24) NOT NULL,
  email         VARCHAR(48) NOT NULL,
  "admin"       BOOLEAN     NOT NULL DEFAULT FALSE,
  leader        BOOLEAN     NOT NULL DEFAULT FALSE,
  skype         VARCHAR(54),
  avatar        VARCHAR(192),
  created       TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP,
  surname       VARCHAR(48),
  forename      VARCHAR(48),
  type          user_type   NOT NULL,
  blockio       VARCHAR(64),
  gavatar       INT,
  mlbot         TIMESTAMP,
  secret        CHAR(60),
  time          TIMESTAMP,
  parent        INT REFERENCES "user" (id) ON DELETE CASCADE ON UPDATE CASCADE,
  phone         VARCHAR(20),
  perfect       VARCHAR(10),
  facebook      VARCHAR(64),
  vkontakte     VARCHAR(64),
  twitter       VARCHAR(64),
  google        VARCHAR(64),
  telegram      INT,
  odnoklassniki VARCHAR(64),
  about         VARCHAR(4096),
  hard          JSON,
  soft          JSON
);
CREATE UNIQUE INDEX user_nick
  ON "user" (lower(nick));
CREATE UNIQUE INDEX user_email
  ON "user" (lower(email));

INSERT INTO "user" (type, nick, email, admin) VALUES ('native', 'admin', 'admin@inbisoft.com', TRUE);

CREATE TYPE token_type AS ENUM ('server', 'browser', 'app', 'code', 'password', 'vkontakte', 'facebook', 'google');

CREATE TABLE token (
  id        VARCHAR(240) PRIMARY KEY,
  "user"    INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "type"    token_type NOT NULL DEFAULT 'browser',
  created   TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time      TIMESTAMP,
  handshake TIMESTAMP,
  expires   TIMESTAMP  NOT NULL,
  ip        INET,
  hard      JSON,
  soft      JSON,
  name      VARCHAR(240)
);

CREATE TABLE program (
  id     SMALLINT PRIMARY KEY,
  leader INT NOT NULL DEFAULT 0
);

CREATE TABLE reward (
  program SMALLINT NOT NULL,
  level   SMALLINT NOT NULL,
  percent FLOAT    NOT NULL
);

CREATE TYPE article_type AS ENUM ('cat', 'page', 'product', 'ticket', 'note');

CREATE TABLE article (
  id       SERIAL PRIMARY KEY NOT NULL,
  path     VARCHAR(96) UNIQUE,
  "type"   article_type       NOT NULL DEFAULT 'page',
  program  SMALLINT REFERENCES program (id) ON UPDATE NO ACTION ON DELETE NO ACTION,
  price    INT,
  name     VARCHAR(96)        NOT NULL,
  short    VARCHAR(140),
  time     TIMESTAMP,
  created  TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP,
  text     VARCHAR(48000),
  image    VARCHAR(256),
  "user"   INT REFERENCES "user" (id),
  priority SMALLINT           NOT NULL DEFAULT 0,
  active   BOOLEAN            NOT NULL DEFAULT TRUE
);

CREATE TABLE relation (
  "from" INT NOT NULL REFERENCES article (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "to"   INT NOT NULL REFERENCES article (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  UNIQUE ("from", "to")
);

CREATE TYPE transfer_type AS ENUM ('internal', 'payment', 'withdraw', 'accrue', 'buy', 'earn', 'support', 'write-off', 'bonus');
CREATE TYPE transfer_status AS ENUM ('created', 'success', 'fail', 'canceled', 'pending', 'virtual');
CREATE TYPE transfer_system AS ENUM ('blockio', 'ethereum', 'payeer', 'perfect', 'nix', 'advcash');

CREATE TABLE transfer (
  id       SERIAL PRIMARY KEY,
  "from"   INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "to"     INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  amount   INT             NOT NULL,
  "type"   transfer_type   NOT NULL,
  "status" transfer_status NOT NULL,
  node     INT,
  text     VARCHAR(192),
  vars     JSON,
  wallet   VARCHAR(64),
  txid     VARCHAR(64),
  ip       INET,
  system   transfer_system,
  created  TIMESTAMP       NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time     TIMESTAMP
);

CREATE TABLE node (
  id      SERIAL PRIMARY KEY,
  "user"  INT       NOT NULL REFERENCES "user" (id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  article INT       NOT NULL REFERENCES article (id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  parent  INT       NOT NULL REFERENCES node (id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time    TIMESTAMP
);

CREATE TABLE "visit" (
  "id"      SERIAL PRIMARY KEY,
  "token"   VARCHAR(240),
  "url"     VARCHAR(240) NOT NULL,
  "spend"   INT,
  ip        INET,
  agent     VARCHAR(240),
  "created" TIMESTAMP DEFAULT current_timestamp
);

CREATE TABLE exchange (
  "from" CHAR(3)   NOT NULL,
  "to"   CHAR(3)   NOT NULL,
  rate   FLOAT8    NOT NULL,
  time   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE ip (
  id      INET PRIMARY KEY,
  text    VARCHAR(80),
  editor  INT       NOT NULL,
  ip      INET      NOT NULL,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time    TIMESTAMP
);

INSERT INTO ip (id, editor, ip) VALUES ('127.0.0.1', 1, '127.0.0.1');

CREATE TYPE blockchain_network AS ENUM ('BTC', 'BTCTEST', 'ETH');

CREATE TABLE blockchain_transaction (
  id            CHAR(64)           NOT NULL,
  amount        DECIMAL(15, 8)     NOT NULL,
  network       blockchain_network NOT NULL,
  fee           DECIMAL(15, 8)     NOT NULL,
  created       TIMESTAMP          NOT NULL,
  confirmations INT,
  time          TIMESTAMP
);

CREATE TABLE ethereum_transaction (
  id CHAR(66) NOT NULL,
  "from" CHAR(42) NOT NULL,
  "to" CHAR(42) NOT NULL,
  amount NUMERIC(24,18) NOT NULL DEFAULT 0,
  time TIMESTAMP NOT NULL
);

CREATE TABLE telegram (
  id      INT PRIMARY KEY,
  context JSON
);

CREATE TABLE log (
  id     BIGINT PRIMARY KEY,
  entity VARCHAR(16) NOT NULL,
  action VARCHAR(32) NOT NULL,
  ip     INET,
  "user" INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  data   JSON
);

CREATE TYPE request_method AS ENUM ('OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE', 'TRACE', 'CONNECT');

CREATE TABLE request (
  id     BIGSERIAL PRIMARY KEY,
  time   TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ip     INET,
  url    VARCHAR(4096)  NOT NULL,
  method request_method NOT NULL,
  type   VARCHAR(64),
  agent  VARCHAR(512),
  "data" TEXT,
  guid   CHAR(32),
  token  VARCHAR(240),
  "user" INT
);

CREATE TABLE message (
  id      SERIAL PRIMARY KEY NOT NULL,
  "user"  INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  article INT REFERENCES article (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  parent  INT REFERENCES message (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  created TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP,
  text    VARCHAR(4096)
);

CREATE TABLE file (
  id      VARCHAR(64) PRIMARY KEY,
  name    VARCHAR(192),
  mime    VARCHAR(192) NOT NULL,
  size    INT          NOT NULL,
  "user"  INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  ip      INET,
  cat     VARCHAR(96),
  url     VARCHAR(240),
  created TIMESTAMP    NOT NULL DEFAULT CURRENT_TIMESTAMP,
  time    TIMESTAMP
);

CREATE TABLE download (
  id      BIGSERIAL PRIMARY KEY,
  file    VARCHAR(64) NOT NULL REFERENCES file (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  ip      INET,
  agent   VARCHAR(240),
  created TIMESTAMP   NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE report (
  id      BIGSERIAL PRIMARY KEY,
  token   VARCHAR(240),
  url     VARCHAR(240),
  name    VARCHAR(240),
  message TEXT,
  stack   TEXT,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE daemon (
  id      VARCHAR(48) PRIMARY KEY,
  expires TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP + INTERVAL '100 years',
  type    token_type,
  script  TEXT      NOT NULL
);

CREATE TABLE social (
  id      BIGSERIAL PRIMARY KEY,
  type    token_type NOT NULL,
  created TIMESTAMP  NOT NULL DEFAULT CURRENT_TIMESTAMP,
  data    JSON       NOT NULL
);

CREATE TABLE setting (
  key VARCHAR(64),
  value TEXT
);

INSERT INTO setting VALUES
  ('eth.block.number', '0x1723ae');
