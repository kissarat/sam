#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

psql postgres -c 'DROP DATABASE "sam"'
psql postgres -c 'CREATE DATABASE "sam" OWNER "sam"'
psql sam -c 'CREATE EXTENSION pgcrypto'
psql sam -U sam < $DIR/../node_modules/schema-db/meta.sql
psql sam -U sam < $DIR/tables.sql
psql sam -U sam < $DIR/views.sql
