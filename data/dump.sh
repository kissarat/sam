#!/usr/bin/env bash

if [ "$#" -gt 1 ]; then
    echo "Illegal number of parameters"
else
    TABLE=$1
    BACKUP=_$TABLE
    FILENAME="/tmp/$BACKUP.sql"
    psql sam -c "CREATE TABLE \"$BACKUP\" AS SELECT * FROM \"$TABLE\""
    pg_dump -d sam -t ${BACKUP} > ${FILENAME}
    psql sam -c "DROP TABLE \"$BACKUP\""
fi
