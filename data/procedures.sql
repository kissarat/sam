CREATE OR REPLACE FUNCTION change_password(login TEXT, password TEXT)
  RETURNS TEXT AS $$
DECLARE
  "encrypted" TEXT;
BEGIN
  IF "password" IS NULL
  THEN
    "password" = '12345678';
  END IF;
  "encrypted" = crypt("password", gen_salt('bf', 10));
  IF login IS NULL
  THEN
    UPDATE "user"
    SET secret = "encrypted";
  ELSE
    UPDATE "user"
    SET secret = "encrypted"
    WHERE nick = login;
  END IF;
  RETURN "encrypted";
END
$$ LANGUAGE plpgsql;

--========================== update_timestamp ============================

CREATE OR REPLACE FUNCTION update_timestamp()
  RETURNS TRIGGER AS $$
BEGIN
  NEW.time = now();
  RETURN NEW;
END;
$$ LANGUAGE 'plpgsql';

CREATE TRIGGER update_timestamp
BEFORE UPDATE
  ON node
FOR EACH ROW EXECUTE PROCEDURE
  update_timestamp();

--========================== transfer_send ============================

CREATE OR REPLACE FUNCTION transfer_send(_from INT, _to INT, _amount INT, _type transfer_type, _ip INET)
  RETURNS INT AS $$
DECLARE
  _balance RECORD;
  _result  INT;
BEGIN
  SELECT
    id,
    balance - _amount AS balance
  FROM balance
  WHERE id = _from
  INTO _balance;
  IF _balance.balance >= 0
  THEN
    INSERT INTO transfer ("from", "to", "amount", "type", "status", "ip")
    VALUES (_from, _to, _amount, _type, 'success', _ip)
    RETURNING id
      INTO _result;
  ELSE
    RETURN -402;
  END IF;
  RETURN _result;
END
$$ LANGUAGE plpgsql;
