
--========================== view ============================

DROP VIEW IF EXISTS remuneration;
DROP VIEW IF EXISTS user_info;
DROP VIEW IF EXISTS statistics;
DROP VIEW IF EXISTS sponsor_node;
DROP VIEW IF EXISTS open_matrix;
DROP VIEW IF EXISTS matrix;
DROP VIEW IF EXISTS informer;
DROP VIEW IF EXISTS balance;
DROP VIEW IF EXISTS article_author;
DROP VIEW IF EXISTS sponsor_participant;
DROP VIEW IF EXISTS participant_children;
DROP VIEW IF EXISTS participant;
DROP VIEW IF EXISTS sponsor_node;
DROP VIEW IF EXISTS sponsor_matrix;
DROP VIEW IF EXISTS matrix;
DROP MATERIALIZED VIEW IF EXISTS virtual;
DROP MATERIALIZED VIEW IF EXISTS size;
DROP VIEW IF EXISTS sponsor;
DROP VIEW IF EXISTS referral;
DROP VIEW IF EXISTS balance;
DROP VIEW IF EXISTS transfer_user;
DROP VIEW IF EXISTS message_user;
DROP VIEW IF EXISTS user_about;
DROP VIEW IF EXISTS buy_cart;
DROP VIEW IF EXISTS cart_item;
DROP VIEW IF EXISTS token_user;

--========================== table ============================

DROP TABLE IF EXISTS "exchange";
DROP TABLE IF EXISTS "shop_level";
DROP TABLE IF EXISTS "visit";
DROP TABLE IF EXISTS "node";
DROP TABLE IF EXISTS "program";
DROP TABLE IF EXISTS "item";
DROP TABLE IF EXISTS "cart";
DROP TABLE IF EXISTS "transfer";
DROP TABLE IF EXISTS "report";
DROP TABLE IF EXISTS "download";
DROP TABLE IF EXISTS "file";
DROP TABLE IF EXISTS "daemon";
DROP TABLE IF EXISTS "skype";
DROP TABLE IF EXISTS "relation";
DROP TABLE IF EXISTS "message";
DROP TABLE IF EXISTS "article";
DROP TABLE IF EXISTS "request";
DROP TABLE IF EXISTS "log";
DROP TABLE IF EXISTS "social";
DROP TABLE IF EXISTS "token";
DROP TABLE IF EXISTS "user";

--========================== type ============================

DROP TYPE transfer_type;
DROP TYPE transfer_status;
DROP TYPE transfer_system;
DROP TYPE article_type;
DROP TYPE request_method;
DROP TYPE token_type;
DROP TYPE user_type;

--========================== sequence ============================

DROP SEQUENCE node_id_seq;
