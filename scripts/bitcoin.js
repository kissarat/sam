const config = require('../config')
const db = require('knex')(config.database)
const BlockIO = require('../server/plugins/blockio')

process.title = 'sam-bitcoin'

const timer = setTimeout(function () {
  console.error('Process timeout')
  process.exit(1)
}, 50000)

function table(name) {
  return db.table(name)
}

run.call({
  config,
  blockio: new BlockIO({config}),
  site: {database: {table}},
  table,
})
  .then(() => clearTimeout(timer))
