const fs = require('fs')
const path = require('path')

const dir = process.argv[process.argv.length - 1]

for (const name of fs.readdirSync(dir)) {
  if (/^\w+$/.test(name)) {
    try {
      const {domain, address, description} = require(path.resolve(dir, name, 'content'))
      console.log([address, /*(domain || ''),*/ description].join('\t'))
    }
    catch (ex) {

    }
  }
}
