const fs = require('fs')
const request = require('request-promise')
const path = require('path')

const name = process.argv[process.argv.length - 1]

const dir = __dirname + `/../sites/${name}/public`
let source = fs.readFileSync(dir + '/ui.html').toString('utf8')
const gist = require(dir + '/cdn-gist')

const files = {}
const filenames = {}

for (const relative of gist.files) {
  const file = dir + relative
  const filename = path.basename(file)
  filenames[filename] = relative
  files[filename] = {
    content: fs.readFileSync(file).toString('utf8')
  }
}

const body = JSON.stringify({
  description: name + ' CDN ' + (new Date()).toISOString(),
  files
})

const headers = {
  'Content-Type': 'application/json',
  'User-Agent': 'SoftServe CDN',
  'Authorization': 'token ' + (process.env.GIST_CDN_TOKEN || process.env.GITHUB)
}

request({
  url: 'https://api.github.com/gists/' + gist.id,
  method: 'PATCH',
  body,
  headers
})
  .then(function (data) {
    data = JSON.parse(data)
    for (const filename in data.files) {
      if (filenames[filename]) {
        source = source.replace(filenames[filename], data.files[filename].raw_url)
          .replace('gist.githubusercontent.com', 'cdn.rawgit.com')
      }
    }
    fs.writeFileSync(dir + '/index.html', source)
  })
  .catch(function (err) {
    console.error('ERROR:', err.error || err)
    process.exit(1)
  })
