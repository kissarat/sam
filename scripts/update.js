const fs = require('fs')

module.exports = async function (file) {
  const string = (await this.read(...arguments))
      .toString('utf8')
  let o = JSON.parse(fs.readFileSync(file))
  Object.assign(o, JSON.parse(string))
  o = Object.keys(o).sort().reduce((a, key) => (a[key] = o[key]) && o, o)
  fs.writeFileSync(file, this.generate(o))
}
