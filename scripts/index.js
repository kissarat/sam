class Context {
  read() {
    return new Promise(function (resolve, reject) {
      const stdin = process.openStdin()

      const chucks = []

      stdin.on('data', function (d) {
        chucks.push(d)
      })

      stdin.on('error', function (err) {
        reject(err)
      })

      stdin.on('end', function () {
        resolve(Buffer.concat(chucks))
      })
    })
  }

  generate(o) {
    return JSON.stringify(o, null, '  ')
  }
}

async function main() {
  const context = new Context()
  let result = await require('./' + process.argv[2]).apply(context, process.argv.slice(3))
  if (result) {
    if ('object' === typeof result) {
      if (result instanceof Buffer) {
        process.stdout.write(result)
      }
      else {
        result = context.generate(result)
      }
    }
    console.log(result)
  }
}

main()
