const fs = require('fs');
const check = require('syntax-error');

const file = process.argv[process.argv.length - 1]
const src = fs.readFileSync(file);

const err = check(src, file);
if (err) {
  console.error('ERROR DETECTED' + new Array(62).join('!'));
  console.error(err);
  console.error(new Array(76).join('-'));
}
