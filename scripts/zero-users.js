const fs = require('fs')
const path = require('path')

const dir = process.argv[process.argv.length - 1]

const users = []
for (const name of fs.readdirSync(dir)) {
  if (/^\w+$/.test(name)) {
    try {
      const {cert_auth_type, cert_user_id} = require(path.resolve(dir, name, 'content'))
      users.push([cert_auth_type, name, cert_user_id])
    }
    catch (ex) {

    }
  }
}
console.log(users.sort((a, b) => a[2].localeCompare(b[2])).map(a => a.join('\t')).join('\n'))
