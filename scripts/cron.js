const config = require('../config')
const db = require('knex')(config.database)
const BlockIO = require('block_io')
const {find, keyBy, uniq, omit, isObject, isEmpty} = require('lodash')

const blockIO = new BlockIO(config.blockio.key, config.blockio.secret, config.blockio.version)
process.title = 'sam-cron'

async function run() {
  let transfers = await db.table('transfer')
    .whereRaw(`type = 'payment' AND status in ('created', 'pending') AND wallet is not null`)
  if (0 === transfers.length) {
    process.exit()
  }
  const addresses = uniq(transfers.map(t => t.wallet)).join(',')
  transfers = keyBy(transfers, 'id')
  blockIO.get_transactions({type: 'received', addresses}, async function (err, r) {
    if ('success' === r.status && isObject(r.data) && r.data.txs instanceof Array) {
      const success = []
      for(let i = 0; i < r.data.txs.length; i++) {
        const tx = r.data.txs[i]
        const received = tx.amounts_received[0]
        const transfer = find(transfers, {wallet: received.recipient})
        if (transfer) {
          delete transfers[transfer.id]
          const status = tx.confirmations >= config.blockio.confirmations ? 'success' : 'pending'
          if (transfer.status !== status) {
            success.push({
              id: transfer.id,
              wallet: received.recipient,
              changes: {
                amount: Math.floor(received.amount * config.money.scale),
                txid: tx.txid,
                status,
                time: new Date()
              }
            })
          }
        }
        if (isEmpty(transfers)) {
          break
        }
      }
      if (0 === success.length) {
        process.exit(1)
      }
      else {
        try {
          for(const {id, wallet, changes} of success) {
            const old = await db.table('transfer')
              .whereRaw('id <> ? AND txid = ?', [id, changes.txid])
              .first('id')
            if (!old) {
              await db.table('transfer')
                .where('id', id)
                .update(changes)
              if ('success' === changes.status) {
                await new Promise(resolve => blockIO.archive_address({address: wallet}, resolve))
              }
            }
          }
          process.exit()
        }
        catch (ex) {
          console.error(ex)
          process.exit(1)
        }
      }
    }
    else {
      console.error(err || r)
      process.exit(1)
    }
  })
}

void run()
