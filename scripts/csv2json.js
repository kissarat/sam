module.exports = async function csv2json() {
  const buffer = await this.read()
  const table = buffer.toString('utf8').trim()
      .split(/\s*\n\s*/)
      .map(
          row => row.slice(1, -1).split('","')
              .filter(s => s.trim())
              .map(s => s.replace(/""/g, '"'))
      )
  const o = {}
  for (const row of table) {
    o[row[0]] = row[2]
  }
  return o
}
